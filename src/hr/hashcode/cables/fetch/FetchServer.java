package hr.hashcode.cables.fetch;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.hashcode.cables.fetch.FetchResult.ErrorCode;
import hr.hashcode.cables.fetch.neal.G1Fetcher;
import hr.hashcode.cables.fetch.netconf.G2Fetch;
import hr.hashcode.cables.mom.MomStore;
import hr.hashcode.cables.ran.Meta;
import hr.hashcode.cables.ran.Version;
import hr.hashcode.cables.ran.Version.Type;

public class FetchServer {

	private static final Logger logger = LoggerFactory.getLogger(Fetcher.class);

	private final boolean counters;
	private final boolean descriptions;
	private final String username;
	private final String password;
	private final List<Thread> threads;
	private final Queue<String> ipQueue;
	private final BlockingQueue<FetchResult> results;
	private final MomStore momStore;
	private final Map<Version.Type, FetchFilter> filters;

	public FetchServer(boolean counters, boolean descriptions, int threadCount, MomStore momStore, String username,
			String password, List<String> ipAddresses, Map<Type, FetchFilter> filters) {
		this.counters = counters;
		this.descriptions = descriptions;
		this.username = username;
		this.password = password;
		this.ipQueue = new ConcurrentLinkedQueue<>(ipAddresses);
		this.results = new ArrayBlockingQueue<>(ipQueue.size());
		this.filters = filters;
		this.momStore = momStore;

		List<Thread> threads = new ArrayList<>();
		for (int i = 0; i < threadCount; i++) {
			Thread thread = new Thread(this::task);
			threads.add(thread);
		}
		this.threads = threads;
	}

	public void start() {
		threads.forEach(Thread::start);
	}

	public void cancel() {
		threads.forEach(Thread::interrupt);
	}

	public List<FetchResult> drain() {
		List<FetchResult> result = new ArrayList<>();
		results.drainTo(result);
		return result;
	}

	public FetchResult next() throws InterruptedException {
		return results.take();
	}

	private void task() {
		while (true) {
			String ipAddress = ipQueue.poll();
			if (ipAddress == null)
				break;

			long startTime = System.nanoTime();
			FetchResult result = fetch(ipAddress);
			try {
				results.put(result);
			} catch (InterruptedException e) {
				break;
			}

			long endTime = System.nanoTime();
			logger.info(ipAddress + " finished fetching in " + (endTime - startTime) / 1_000_000);
		}
	}

	private FetchResult fetch(String ipAddress) {
		Version version = FetchVersion.version(ipAddress);
		if (version == null) {
			logger.error("Could not find version for: " + ipAddress);
			return FetchResult.error(ipAddress, ErrorCode.VERSION);
		}

		Meta meta = momStore.get(version, ipAddress);
		if (meta == null) {
			logger.error("Could not process meta for: " + ipAddress);
			return FetchResult.error(ipAddress, ErrorCode.META);
		}

		Fetcher fetcher;
		Version.Type type = meta.version().type();
		FetchFilter filter = filters.getOrDefault(type, new FetchFilter());
		switch (type) {
			case MSRBS:
				fetcher = new G2Fetch(ipAddress, meta, username, () -> password, filter);
				break;
			case RBS:
			case ERBS:
			case RNC:
				fetcher = new G1Fetcher(ipAddress, meta, filter);
				break;
			default:
				return FetchResult.error(ipAddress, ErrorCode.UNSUPPORTED_NODE_TYPE);
		}
		return fetcher.fetch();
	}

}
