package hr.hashcode.cables.fetch;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.hashcode.cables.mom.MomStore;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.Version;
import hr.hashcode.cables.util.Util;

public class Listener {

	private static final Logger logger = LoggerFactory.getLogger(Listener.class);

	private final int port;

	private final MomStore momStore;
	private volatile ServerSocket server;
	private volatile boolean stopped;

	public Listener(int port) {
		this.port = port;
		this.momStore = new MomStore(Arrays.asList(Paths.get("/home/rns1/jarxml")));
	}

	public void start() throws IOException {

		server = new ServerSocket(port);

		new Thread(this::waitForRequest).start();
	}

	void stop() throws IOException {
		stopped = true;
		server.close();
	}

	private void waitForRequest() {
		while (true) {
			try {
				Socket socket = server.accept();
				new Thread(() -> processRequest(socket)).start();
			} catch (IOException e) {
				if (!stopped)
					logger.error("Error accepting request", e);
				break;
			}
		}
	}

	private void processRequest(Socket socket) {

		try {
			InputStream input = socket.getInputStream();

			ObjectInputStream in = new ObjectInputStream(input);

			FetchRequest request = (FetchRequest) in.readObject();

			FetchServer server = new FetchServer(true, true, 20, momStore, "bts", "bts", request.ips(),
					request.filters());
			server.start();

			OutputStream output = socket.getOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(output);

			out.writeObject(new Acknowledge());
			out.flush();

			while (true) {
				Object next = in.readObject();
				if (next instanceof CloseRequest) {
					server.cancel();
					out.writeObject(new Acknowledge());
					out.flush();
					break;
				} else if (next instanceof StatusRequest) {
					List<FetchResult> results = server.drain();
					List<String> failed = new ArrayList<>();
					List<NodeState> states = new ArrayList<>();

					for (FetchResult result : results)
						if (result.success())
							states.add(result.state());
						else
							failed.add(result.ipAddress());

					StatusResult result = new StatusResult(failed, states);

					ByteArrayOutputStream bytes = new ByteArrayOutputStream();
					try (GZIPOutputStream gzip = new GZIPOutputStream(bytes);
							BufferedOutputStream buffer = new BufferedOutputStream(gzip, 8192);
							ObjectOutputStream obj = new ObjectOutputStream(buffer)) {
						obj.writeObject(result);
					}

					out.writeObject(bytes.toByteArray());
					out.flush();
				}
			}

		} catch (IOException | ClassNotFoundException e) {
			try {
				socket.close();
			} catch (IOException ignored) {}
		}
	}

	public static class FetchRequest implements Serializable {

		private static final long serialVersionUID = 1L;
		private final String[] ips;
		private final Map<Version.Type, FetchFilter> filters;

		public FetchRequest(Collection<String> ips, Map<Version.Type, FetchFilter> filters) {
			this.ips = ips.toArray(new String[0]);
			this.filters = filters;
		}

		public List<String> ips() {
			return Arrays.asList(ips);
		}

		public Map<Version.Type, FetchFilter> filters() {
			return filters;
		}
	}

	public static class Acknowledge implements Serializable {
		private static final long serialVersionUID = 1L;

	}

	public static class StatusRequest implements Serializable {
		private static final long serialVersionUID = 1L;

	}

	public static class CloseRequest implements Serializable {
		private static final long serialVersionUID = 1L;

	}

	public static class StatusResult implements Serializable {
		private static final long serialVersionUID = 1L;

		private final String[] failed;
		private final NodeState[] states;

		public StatusResult(Collection<String> failed, Collection<NodeState> states) {
			this.failed = Util.toArray(failed, String.class);
			this.states = Util.toArray(states, NodeState.class);
		}

		public List<String> failed() {
			return Arrays.asList(failed);
		}

		public List<NodeState> states() {
			return Arrays.asList(states);
		}

	}

}
