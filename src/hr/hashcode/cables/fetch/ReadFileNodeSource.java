package hr.hashcode.cables.fetch;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;

import hr.hashcode.cables.fetch.moshell.ParseMoshellDump;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.Serialize;
import hr.hashcode.cables.util.IoUtil;

public class ReadFileNodeSource {

	public interface Reporter {
		void success(NodeState state);

		void failed(File file);

		void finish();
	}

	private final int threadCount;
	private final Reporter reporter;
	private final ConcurrentLinkedQueue<File> queue;

	private final Thread mainThread;

	public ReadFileNodeSource(int threadCount, List<File> files, Reporter reporter) {
		this.threadCount = threadCount;
		this.queue = new ConcurrentLinkedQueue<>(files);
		this.mainThread = new Thread(this::read);
		this.reporter = reporter;
	}

	public void cancel() {
		mainThread.interrupt();
	}

	public void start() {
		mainThread.start();
	}

	private void read() {
		CountDownLatch latch = new CountDownLatch(threadCount);
		List<Thread> readers = new ArrayList<>();
		for (int i = 0; i < threadCount; i++) {
			Runnable runnable = () -> {
				try {
					while (true) {
						File file = queue.poll();
						if (file == null || Thread.interrupted())
							break;
						NodeState state = read(file);
						if (state == null)
							reporter.failed(file);
						else
							reporter.success(state);
					}
				} finally {
					latch.countDown();
				}
			};
			Thread reader = new Thread(runnable);
			readers.add(reader);
		}
		readers.forEach(Thread::start);
		try {
			latch.await();
			reporter.finish();
		} catch (InterruptedException e) {
			readers.forEach(Thread::interrupt);
		}
	}

	private NodeState read(File file) {
		byte[] bytes;
		try {
			bytes = IoUtil.contents(file);
		} catch (IOException e) {
			return null;
		}

		NodeState state = null;
		try {
			try (ObjectInputStream objInput = new ObjectInputStream(new ByteArrayInputStream(bytes))) {
				state = (NodeState) objInput.readObject();
			} catch (IOException | ClassNotFoundException e) {
				// do nothing
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		try {
			if (state == null)
				state = ParseMoshellDump.read(bytes);
		} catch (Throwable t) {
			t.printStackTrace();
		}

		try {
			if (state == null)
				state = Serialize.fromBytes(bytes);
		} catch (Throwable t) {
			t.printStackTrace();
		}

		return state;
	}

}
