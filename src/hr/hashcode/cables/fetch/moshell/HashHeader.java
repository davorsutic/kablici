package hr.hashcode.cables.fetch.moshell;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.hashcode.cables.fetch.moshell.MarkedContents.Entry;

class HashHeader {

	private static final Pattern separatorPattern = Pattern.compile("^#{20,}$", Pattern.MULTILINE);

	static MarkedContents split(String string) {
		Matcher matcher = separatorPattern.matcher(string);
		List<MarkedContents.Entry> result = new ArrayList<>();

		if (matcher.find()) {
			while (true) {
				int last = matcher.end();
				if (!matcher.find())
					return null;
				String header = string.substring(last + 1, matcher.start());
				last = matcher.end();
				int start;
				if (matcher.find())
					start = matcher.start();
				else
					start = string.length();

				String contents = string.substring(last + 1, start);
				result.add(new Entry(header, contents));

				if (start == string.length())
					break;
			}
		}
		return new MarkedContents(result);
	}

}
