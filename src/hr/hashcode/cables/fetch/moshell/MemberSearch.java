package hr.hashcode.cables.fetch.moshell;

import java.util.ArrayList;
import java.util.List;

public class MemberSearch {

	private int current = -1;
	private final String[] lines;

	MemberSearch(String string) {
		this.lines = string.split("\r?\n");
	}

	private boolean jel(char ch) {
		String line = lines[current];
		if (line.length() < 20)
			return false;
		for (int i = 0; i < line.length(); i++)
			if (line.charAt(i) != ch)
				return false;
		return true;
	}

	boolean search(char ch) {
		for (current++; current < lines.length; current++)
			if (jel(ch))
				return true;
		return false;
	}

	boolean search(char ch, char end) {
		for (current++; current < lines.length; current++) {
			if (jel(ch))
				return true;
			if (jel(end))
				return false;
		}
		return false;
	}

	List<String> exhaust(char ch) {
		List<String> list = new ArrayList<>();
		for (current++; current < lines.length; current++) {
			if (jel(ch))
				break;
			list.add(lines[current]);
		}

		return list;
	}

	List<String> exhaustToEmpty() {
		List<String> list = new ArrayList<>();
		for (current++; current < lines.length; current++) {
			if (lines[current].trim().isEmpty())
				break;
			list.add(lines[current]);
		}
		return list;
	}

	boolean atEnd() {
		return current >= lines.length;
	}

	String previous() {
		if (current == 0)
			return null;
		else
			return lines[current - 1];
	}

	String next() {
		if (current == lines.length - 1)
			return null;
		return lines[current + 1];
	}

	void peek(int k) {
		int n = Math.min(current + k, lines.length);
		for (int i = current + 1; i < n; i++)
			System.out.println(lines[i]);

	}

}
