package hr.hashcode.cables.fetch.moshell;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.hashcode.cables.fetch.moshell.MarkedContents.Entry;

public class ObjectParamSplitter {

	private static final Pattern separatorPattern = Pattern.compile("^={20,}$", Pattern.MULTILINE);
	private static final Pattern objNamePattern = Pattern.compile("^\\S+\\s+(\\S+)");

	static MarkedContents split(String contents) {
		List<Entry> entries = new ArrayList<>();
		Matcher sepMatcher = separatorPattern.matcher(contents);
		Matcher objMatcher = objNamePattern.matcher(contents);

		if (sepMatcher.find()) {
			int last = sepMatcher.end();
			while (sepMatcher.find()) {
				objMatcher.region(last + 1, sepMatcher.start());
				if (!objMatcher.find())
					return null;
				String objName = objMatcher.group(1);
				last = sepMatcher.end();

				if (!sepMatcher.find())
					return null;
				String params = contents.substring(last + 1, sepMatcher.start());
				last = sepMatcher.end();
				Entry entry = new Entry(objName, params);
				entries.add(entry);
			}
		}
		return new MarkedContents(entries);
	}
}
