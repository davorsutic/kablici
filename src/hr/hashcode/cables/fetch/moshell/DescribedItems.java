package hr.hashcode.cables.fetch.moshell;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.hashcode.cables.fetch.moshell.MarkedContents.Entry;

public class DescribedItems {

	private static final Pattern middlePattern = Pattern.compile("^-{20,}$", Pattern.MULTILINE);
	private static final Pattern endPattern = Pattern.compile("^\\*{20,}$", Pattern.MULTILINE);

	static MarkedContents split(String table) {
		List<Entry> entries = new ArrayList<>();
		Matcher middleMatcher = middlePattern.matcher(table);
		Matcher endMatcher = endPattern.matcher(table);
		int last = -1;
		int length = table.length();
		while (true) {
			middleMatcher.region(last + 1, length);
			if (!middleMatcher.find())
				break;

			String member = table.substring(last + 1, middleMatcher.start());

			endMatcher.region(middleMatcher.end() + 1, length);

			if (!endMatcher.find())
				return null;

			String description = table.substring(middleMatcher.end() + 1, endMatcher.start());

			Entry entry = new Entry(member, description);
			entries.add(entry);
			last = endMatcher.end();
		}
		return new MarkedContents(entries);
	}

}
