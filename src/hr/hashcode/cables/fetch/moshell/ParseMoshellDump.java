package hr.hashcode.cables.fetch.moshell;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import hr.hashcode.cables.fetch.moshell.MarkedContents.Entry;
import hr.hashcode.cables.ran.Meta;
import hr.hashcode.cables.ran.Meta.Mim;
import hr.hashcode.cables.ran.Meta.MoArray;
import hr.hashcode.cables.ran.Meta.MoAttr;
import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.Meta.MoEnum;
import hr.hashcode.cables.ran.Meta.MoStruct;
import hr.hashcode.cables.ran.Meta.MoType;
import hr.hashcode.cables.ran.Meta.Parent;
import hr.hashcode.cables.ran.Meta.SimpleType;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.ran.Version;
import hr.hashcode.cables.util.Util;

public class ParseMoshellDump {

	private ParseMoshellDump() {}

	private final Map<String, Mim> mims = new HashMap<>();
	private final Map<String, MoStruct> structs = new TreeMap<>();
	private final Map<String, MoClass> classes = new TreeMap<>();
	private final Map<String, MoEnum> enums = new TreeMap<>();
	private final Map<MoType, MoArray> arrays = new IdentityHashMap<>();
	private final Map<String, String> derivedTypes = new HashMap<>();
	private final Map<String, String> descriptions = new HashMap<>();

	private Mim mim(String name) {
		int index = name.indexOf('.');
		if (index == -1)
			return mims.get(null);
		String mimName = name.substring(0, index);
		return mims.computeIfAbsent(mimName, Mim::new);
	}

	private String afterMim(String string) {
		int index = string.indexOf('.');
		return string.substring(index + 1);
	}

	private static final Pattern headerPattern = Pattern.compile("(E?RBS_NODE_MODEL_[^_]*)_([^_]*)_([^_]*)");

	private static Version version(String part) {
		return Version.of(part);
	}

	private static Mim mimString(String part) {
		Matcher matcher = headerPattern.matcher(part);
		if (matcher.find())
			return new Mim(matcher.group(1), matcher.group(2), matcher.group(3), null);
		return null;
	}

	public static NodeState read(InputStream input) throws IOException {
		byte[] bytes = Util.drain(input);
		return read(bytes);
	}

	public static NodeState read(byte[] bytes) {
		try {
			return new ParseMoshellDump().readFrom(bytes);
		} catch (Exception e) { // TODO make this more elegant
			e.printStackTrace();
			return null;
		}
	}

	private NodeState readFrom(byte[] bytes) {

		ScriptContents scriptContents = ScriptContents.of(bytes);

		Mim mim = mimString(scriptContents.mom);
		if (mim != null)
			mims.put(null, mim);
		Version version = version(scriptContents.mom);

		descriptions(scriptContents.types);
		parseClassesAndTypes(scriptContents.typeDetails);
		List<Parent> parents = parents(scriptContents.momTree);
		Meta meta = new Meta(version, classes.values(), parents);

		NodeState state = parseList(meta, scriptContents.params, scriptContents.mib);

		return state;
	}

	private NodeState parseList(Meta meta, String part, String mibPrefix) {

		NodeState.Builder builder = NodeState.newBuilder(meta);

		Set<String> corrupt = new HashSet<>();
		MarkedContents table = ObjectParamSplitter.split(part);
		ManagedObject[] objectsList = new ManagedObject[table.entries.size()];
		Map<String, List<ManagedObject>> objects = new HashMap<>();
		Entry rootEntry = table.entries.get(0);
		String rootName = rootEntry.header();
		ManagedObject rootObj = builder.newObject(rootName);
		objectsList[0] = rootObj;
		for (int i = 1; i < table.entries.size(); i++) {
			Entry entry = table.entries.get(i);
			String name = rootName + "," + entry.header();
			try {
				ManagedObject obj = builder.newObject(name);
				objectsList[i] = obj;
				String start = obj.moClass + "=" + obj.name;
				objects.computeIfAbsent(start, x -> new ArrayList<>()).add(obj);
				for (ManagedObject temp = obj.parent; temp != null; temp = temp.parent) {
					start = temp.moClass + "=" + temp.name + "," + start;
					objects.computeIfAbsent(start, x -> new ArrayList<>()).add(obj);
				}
			} catch (IllegalArgumentException e) {
				corrupt.add(entry.header());
				// System.err.println(e.getMessage());
			}
		}

		ValueParser parser = new ValueParser(objects, builder);
		for (int i = 0; i < table.entries.size(); i++) {
			Entry entry = table.entries.get(i);
			ManagedObject object = objectsList[i];
			if (object == null)
				continue;
			Map<String, MoAttr> attrs =
					Arrays.stream(object.moClass.attrs()).collect(Collectors.toMap(x -> x.name, x -> x));
			List<Integer> marks = new ArrayList<>();
			List<String> params = Arrays.asList(entry.contents().split("\r?\n"));
			for (int j = 0; j < params.size(); j++) {
				String first = column(params.get(j), 0);
				if (attrs.containsKey(first))
					marks.add(j);
			}

			for (int j = 0; j < marks.size(); j++) {
				String line = params.get(marks.get(j));
				String key = column(line, 0);
				int start;
				for (start = key.length() + 1; start < line.length(); start++)
					if (line.charAt(start) != ' ')
						break;
				StringBuilder valBuilder = new StringBuilder(line.substring(start));
				int limit;
				if (j == marks.size() - 1)
					limit = params.size();
				else
					limit = marks.get(j + 1);
				for (int k = marks.get(j) + 1; k < limit; k++)
					valBuilder.append("\n").append(params.get(k));
				String value = valBuilder.toString();
				MoAttr attr = attrs.get(key);
				Object parsed = parser.parse(attr.type, value);
				builder.set(object, attr, parsed);
			}
		}
		return builder.build(mibPrefix);
	}

	private void parseClassesAndTypes(String part) {

		MarkedContents types = HashHeader.split(part);

		derivedTypes(types.get("Derived"));
		enums(types.get("Enum"));
		structs(types.get("Struct"));
		classes(types.get("MO Class\\s+Attribute"));
	}

	private static String column(String line, int index) {
		return line.split("\\s+")[index];
	}

	private static final Pattern threeItems = Pattern.compile("(\\S+)\\s+(\\S+)\\s+(\\S+)");

	private Map<String, List<MoAttr>> attributes(String contents) {

		Map<String, List<MoAttr>> attributes = new TreeMap<>();
		Matcher matcher = threeItems.matcher("");
		for (Entry entry : DescribedItems.split(contents).entries) {
			String line = entry.header();
			matcher.reset(line);
			matcher.find();
			String name = matcher.group(1);
			String attrName = matcher.group(2);
			String type = matcher.group(3);

			MoType moType = moType(type);
			MoAttr moAttr = new MoAttr(attrName, moType, entry.contents());
			attributes.computeIfAbsent(name, x -> new ArrayList<>()).add(moAttr);
		}
		return attributes;
	}

	private void classes(String contents) {
		attributes(contents).forEach((x, y) -> {
			// TODO find the key
			String description = descriptions.get(x);
			MoClass moClass = new MoClass(mim(x), afterMim(x), y, null, description);
			classes.put(x, moClass);
		});
	}

	private void structs(String contents) {
		attributes(contents).forEach((x, y) -> {
			MoStruct struct = new MoStruct(afterMim(x), y);
			structs.put(x, struct);
		});
	}

	private MoType moType(String type) {
		String[] parts = type.split("(:|-)", 2);
		switch (parts[0]) {
			case "int8":
			case "uint8":
			case "int16":
			case "uint16":
			case "int32":
			case "uint32":
			case "long":
			case "froId":
				return SimpleType.INTEGER;
			case "int64":
			case "uint64":
			case "longlong":
				return SimpleType.LONG;
			case "double":
			case "float":
				return SimpleType.FLOAT;
			case "string":
				return SimpleType.STRING;
			case "boolean":
				return SimpleType.BOOLEAN;
			case "moRef":
				return SimpleType.REFERENCE;
			case "derivedRef":
				String derived = derivedTypes.get(parts[1]);
				if (derived == null)
					throw new IllegalArgumentException(type);
				return moType(derived);
			case "enumRef":
				return fromMap(parts, enums);
			case "structRef":
				return fromMap(parts, structs);
			case "sequence":
				return arrayType(parts);
			default:
				throw new IllegalArgumentException(type);
		}
	}

	private MoArray arrayType(String[] elems) {
		if (elems.length != 2)
			throw new IllegalArgumentException(Arrays.toString(elems));
		MoType elemType = moType(elems[1]);
		MoArray array = arrays.get(elemType);
		if (array == null) {
			array = new MoArray(elemType);
			arrays.put(elemType, array);
		}
		return array;
	}

	private static MoType fromMap(String[] elems, Map<String, ? extends MoType> map) {
		if (elems.length != 2)
			throw new IllegalArgumentException(Arrays.toString(elems));
		String type = elems[1];
		MoType moType = map.get(type);
		if (moType == null)
			throw new IllegalArgumentException(type);
		return moType;
	}

	private void enums(String contents) {
		for (Entry entry : DescribedItems.split(contents).entries) {
			String line = entry.header().trim();
			String name = column(line, 0);
			String[] elems = line.split("\\s+", 2)[1].split(", ");
			List<MoEnum.Element> elements = new ArrayList<>();
			for (String elem : elems) {
				String[] parts = elem.split(":");
				String elemName = parts[1];
				int index = Integer.parseInt(parts[0]);
				elements.add(new MoEnum.Element(elemName, index, null));
			}
			MoEnum moEnum = new MoEnum(afterMim(name), elements);
			enums.put(name, moEnum);
		}
	}

	private void derivedTypes(String contents) {
		if (contents == null)
			return;
		for (Entry entry : DescribedItems.split(contents).entries) {
			String line = entry.header();
			if (line.trim().isEmpty())
				continue;
			String derived = column(line, 0);
			String type = column(line, 1);
			derivedTypes.put(derived, type);
		}
	}

	private static final Pattern dashPattern = Pattern.compile("^-{20,}$", Pattern.MULTILINE);
	private static final Pattern classNamePattern = Pattern.compile("^(\\S+)", Pattern.MULTILINE);

	private void descriptions(String contents) {

		MarkedContents types = HashHeader.split(contents);
		String classes = types.get("MO Class");

		if (classes == null)
			return;

		Matcher dashMatcher = dashPattern.matcher(classes);
		Matcher classNameMatcher = classNamePattern.matcher(classes);
		int last = -1;
		while (dashMatcher.find()) {
			classNameMatcher.region(last + 1, dashMatcher.start() - 1);
			if (classNameMatcher.find()) {
				String className = classNameMatcher.group(1);
				if (classNameMatcher.find()) {
					String description = classes.substring(classNameMatcher.start(), dashMatcher.start() - 1);
					descriptions.put(className, description);
				}
			}
			last = dashMatcher.end();
		}

	}

	private static final Pattern cardPattern = Pattern.compile("^(.*)\\[(?:([0-9]*)|([0-9]*)?-([0-9]*)?)\\]");

	private int whitespaceCount(String string) {
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) != ' ')
				return i;
		}
		return string.length();
	}
	private static final Pattern rootPattern = Pattern.compile("MO classes under ([^ ]+)");

	private static int parseInt(String string, int def) {
		try {
			return Integer.parseInt(string);
		} catch (NumberFormatException e) {
			return def;
		}
	}

	private List<Parent> parents(String part) {
		MemberSearch search = new MemberSearch(part);
		search.search('-');
		search.search('-');
		search.search('-');
		search.search('-');
		search.search('-');
		search.search('-');
		Matcher rootMatcher = rootPattern.matcher(search.previous());
		if (!rootMatcher.find())
			throw new IllegalArgumentException(search.previous());
		MoClass rootClass = classes.get(rootMatcher.group(1));
		if (rootClass == null)
			throw new IllegalArgumentException(search.previous());

		List<Parent> parenthoods = new ArrayList<>();
		Deque<Integer> levels = new ArrayDeque<>();
		Deque<MoClass> parents = new ArrayDeque<>();
		levels.addLast(-1);
		parents.addLast(rootClass);
		for (String line : search.exhaustToEmpty()) {
			int wsCount = whitespaceCount(line);
			line = line.trim();
			Matcher matcher = cardPattern.matcher(line);
			if (!matcher.find())
				throw new IllegalArgumentException(line);
			MoClass child = classes.get(matcher.group(1));
			if (child == null)
				throw new IllegalArgumentException(line);
			MoClass parent;

			while (wsCount <= levels.getLast()) {
				levels.removeLast();
				parents.removeLast();
			}

			parent = parents.getLast();
			parents.addLast(child);
			levels.addLast(wsCount);

			String group = matcher.group(2);

			String first;
			String second;
			if (group != null) {
				first = group;
				second = group;
			} else {
				first = matcher.group(3);
				second = matcher.group(4);
			}

			int lower = parseInt(first, 0);
			int upper = parseInt(second, Integer.MAX_VALUE);
			Parent parenthood = new Parent(parent, child, lower, upper);
			parenthoods.add(parenthood);
		}
		return parenthoods;
	}

}
