package hr.hashcode.cables.fetch.moshell;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.hashcode.cables.fetch.moshell.MarkedContents.Entry;

class CommandSplitter {

	private static final String separator = "ENTITY_SEPARATOR";

	private static final Pattern pattern = Pattern.compile("^" + Pattern.quote(separator + "> "), Pattern.MULTILINE);
	private static final Pattern nonEmpty = Pattern.compile("^(.*\\S.*)$", Pattern.MULTILINE);

	static MarkedContents split(String text) {
		String[] elems = pattern.split(text);
		List<Entry> commands = new ArrayList<>();

		for (String elem : elems) {
			Matcher matcher = nonEmpty.matcher(elem);
			if (matcher.find()) {
				String first = matcher.group(1);
				Entry entry = new Entry(first, elem);
				commands.add(entry);
			}
		}

		return new MarkedContents(commands);
	}

}
