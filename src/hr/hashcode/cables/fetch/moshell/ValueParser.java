package hr.hashcode.cables.fetch.moshell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.hashcode.cables.ran.Meta;
import hr.hashcode.cables.ran.Meta.MoArray;
import hr.hashcode.cables.ran.Meta.MoAttr;
import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.Meta.MoEnum;
import hr.hashcode.cables.ran.Meta.MoEnum.Element;
import hr.hashcode.cables.ran.Meta.MoStruct;
import hr.hashcode.cables.ran.Meta.MoType;
import hr.hashcode.cables.ran.Meta.Parent;
import hr.hashcode.cables.ran.Meta.SimpleType;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.NodeState.ManagedArray;
import hr.hashcode.cables.ran.NodeState.ManagedAttribute;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.ran.NodeState.ManagedStruct;
import hr.hashcode.cables.ran.NodeState.UnexpectedType;
import hr.hashcode.cables.util.Util;

public class ValueParser {

	private final Map<String, List<ManagedObject>> objects;
	private final NodeState.Builder builder;
	private final Meta meta;

	ValueParser(Map<String, List<ManagedObject>> objects, NodeState.Builder builder) {
		this.objects = new HashMap<>(objects);
		this.builder = builder;
		this.meta = builder.meta();
	}

	private static String column(String line, int index) {
		return line.split("\\s+")[index];
	}

	Object parse(MoType type, String value) {
		try {
			return parseHelper(type, value);
		} catch (ParseException e) {
			return e.report();
		}
	}

	private Object parseHelper(MoType type, String value) throws ParseException {
		if (type == SimpleType.INTEGER)
			return parseInt(value);
		else if (type == SimpleType.STRING)
			return parseString(value);
		else if (type == SimpleType.BOOLEAN)
			return parseBoolean(value);
		else if (type == SimpleType.LONG)
			return parseLong(value);
		else if (type == SimpleType.FLOAT)
			return parseFloat(value);
		else if (type == SimpleType.REFERENCE)
			return parseObject(value);
		else if (type instanceof MoEnum)
			return parseEnum((MoEnum) type, value);
		else if (type instanceof MoStruct)
			return parseStruct((MoStruct) type, value);
		else if (type instanceof MoArray)
			return parseArray((MoArray) type, value);
		else
			throw new AssertionError(String.valueOf(type));
	}

	private static final Pattern structMemberPattern = Pattern.compile("^ >>> [0-9]+\\.([^ ]*) = (.*)$");

	ManagedStruct parseStruct(MoStruct moStruct, String value) throws ParseException {
		String[] lines = value.split("\r?\n");
		Map<String, String> values = new HashMap<>();
		StringBuilder builder = null;
		String attrname = null;
		for (int i = 1; i < lines.length; i++) {
			Matcher matcher = structMemberPattern.matcher(lines[i]);
			if (matcher.find()) {
				if (attrname != null)
					values.put(attrname, builder.toString());
				attrname = matcher.group(1);
				builder = new StringBuilder(matcher.group(2));
			} else {
				if (builder == null)
					throw fail(value);
				builder.append("\n").append(lines[i]);
			}
		}
		if (attrname != null)
			values.put(attrname, builder.toString());
		List<ManagedAttribute> members = new ArrayList<>();
		for (MoAttr attr : moStruct.attrs) {
			String memberValue = values.get(attr.name);
			Object parsed;
			if (memberValue == null)
				parsed = NodeState.FETCH_FAILURE;
			else
				try {
					parsed = parseHelper(attr.type, memberValue);
				} catch (ParseException e) {
					parsed = e.report();
				}
			members.add(new ManagedAttribute(attr, parsed));
		}
		ManagedStruct struct = new ManagedStruct(moStruct, members);
		return struct;
	}

	MoEnum.Element parseEnum(MoEnum moEnum, String value) throws ParseException {
		Integer index = parseInt(column(value, 0));
		if (index == null)
			return null;
		Element elem = moEnum.byIndex(index);
		if (elem == null)
			throw fail(value);
		return elem;
	}

	private static String[] split(String value) {
		value = value.trim();
		if (value.isEmpty())
			return new String[0];
		return value.split("\\s+");
	}

	private static Object[] splitAndTransform(String value, Transform function) {

		String[] elems = split(value);
		Object[] result = new Object[elems.length];
		for (int i = 0; i < elems.length; i++) {
			try {
				result[i] = function.transform(elems[i]);
			} catch (ParseException e) {
				result[i] = new UnexpectedType(elems[i]);
			}
		}
		return result;
	}

	private interface Transform {
		Object transform(String value) throws ParseException;
	}
	private static final Pattern arrayPattern = Pattern.compile("^([^\\[]*)\\[([0-9]*)\\] = (.*)", Pattern.DOTALL);

	ManagedArray parseArray(MoArray arrayType, String value) throws ParseException {
		Matcher matcher = arrayPattern.matcher(value);
		int count;
		String contents;
		if (matcher.find()) {
			count = Integer.parseInt(matcher.group(2));
			contents = matcher.group(3);
		} else {
			count = -1;
			contents = value;
		}
		MoType elemType = arrayType.elemType;

		final Object[] array;
		if (elemType == SimpleType.INTEGER) {
			array = splitAndTransform(contents, this::parseInt);
		} else if (elemType == SimpleType.BOOLEAN) {
			array = splitAndTransform(contents, this::parseBoolean);
		} else if (elemType == SimpleType.LONG) {
			array = splitAndTransform(contents, this::parseLong);
		} else if (elemType == SimpleType.FLOAT) {
			array = splitAndTransform(contents, this::parseFloat);
		} else if (elemType == SimpleType.STRING) {
			if (count >= 0) {
				String[] elems = split(contents);
				if (elems.length == count)
					array = elems;
				else {
					array = new Object[count];
					array[0] = contents;
				}
			} else
				array = contents.split("\\s+");
		} else if (elemType == SimpleType.REFERENCE) {
			if (contents.trim().isEmpty())
				array = new ManagedArray[0];
			else {

				if (count >= 0) {
					String[] lines = contents.split("\r?\n");
					if (lines.length != count + 1)
						throw fail(contents);
					array = new Object[count];
					for (int i = 1; i <= count; i++) {
						String line = lines[i];
						int index = line.indexOf('=');
						try {
							array[i - 1] = parseObject(line.substring(index + 2));
						} catch (ParseException e) {
							array[i - 1] = e.report();
						}
					}
				} else {
					String[] refs = contents.trim().split("\\s+");
					array = new Object[refs.length];
					for (int i = 1; i <= refs.length; i++) {
						String ref = refs[i];
						int index = ref.indexOf('=');
						try {
							array[i - 1] = parseObject(ref.substring(index + 2));
						} catch (ParseException e) {
							array[i - 1] = e.report();
						}
					}
				}
			}
		} else if (elemType instanceof MoEnum) {
			int index = contents.indexOf('(');
			if (index != -1)
				contents = contents.substring(0, index);
			array = splitAndTransform(contents, x -> parseEnum((MoEnum) elemType, x));
		} else if (elemType instanceof MoStruct) {

			List<String> values = new ArrayList<>();
			String[] lines = value.split("\r?\n");
			StringBuilder builder = null;
			for (int i = 1; i < lines.length; i++) {
				String line = lines[i];
				if (line.matches(" >>> Struct\\[[0-9]*\\]  has [0-9]* members:")) {
					if (builder != null)
						values.add(builder.toString());
					builder = new StringBuilder(line);
				} else {
					if (builder == null)
						throw fail(value);
					builder.append("\n").append(line);
				}
			}
			if (builder != null)
				values.add(builder.toString());

			array = new Object[values.size()];
			for (int i = 0; i < values.size(); i++) {
				try {
					array[i] = parseStruct((MoStruct) elemType, values.get(i));
				} catch (ParseException e) {
					array[i] = e.report();
				}
			}
		} else
			throw new AssertionError();
		return new ManagedArray(elemType, array);
	}

	private List<MoClass> parents(String moClass) {
		List<MoClass> parents = new ArrayList<>();
		for (Parent parent : meta.parents()) {
			if (parent.child.name.equals(moClass))
				parents.add(parent.parent);
		}

		return parents;
	}

	private List<ManagedObject> ofClass(String moClass) {
		Set<ManagedObject> cands = Util.identitySet();
		for (List<ManagedObject> list : objects.values())
			for (ManagedObject obj : list)
				if (obj.moClass.name.equals(moClass))
					cands.add(obj);
		return new ArrayList<>(cands);
	}

	private String fill(String moClass) {
		List<ManagedObject> cands = ofClass(moClass);
		if (cands.size() > 1)
			throw new IllegalArgumentException();
		else if (cands.size() == 1)
			return cands.get(0).ldn();
		else {
			List<MoClass> parents = parents(moClass);

			if (parents.size() == 1) {
				return fill(parents.get(0).name) + "," + moClass + "=1";
			} else if (parents.size() == 0)
				return builder.root().ldn();
		}
		throw new IllegalArgumentException();
	}

	private String fillFirst(String moClass) {
		List<MoClass> parents = parents(moClass);

		if (parents.size() == 1)
			return fill(parents.get(0).name);
		else
			throw new IllegalArgumentException();

	}

	private ManagedObject parseObject(String ref) throws ParseException {
		if (ref.trim().isEmpty())
			return null;
		List<ManagedObject> cands = objects.get(ref);
		if (cands == null) {
			int index = ref.indexOf('=');
			String firstClass = ref.substring(0, index);

			String fullRef = fillFirst(firstClass) + "," + ref;

			ManagedObject object = builder.newObject(fullRef);
			cands = Collections.singletonList(object);
			objects.put(ref, cands);
		}
		if (cands == null || cands.size() != 1)
			throw fail(ref);
		return cands.get(0);
	}

	private Boolean parseBoolean(String value) throws ParseException {
		value = value.trim();
		if (value.isEmpty())
			return null;
		else if ("true".equalsIgnoreCase(value))
			return true;
		else if ("false".equalsIgnoreCase(value))
			return false;
		else
			throw fail(value);
	}

	private Integer parseInt(String value) throws ParseException {
		if (value.trim().isEmpty())
			return null;
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException e) {
			String first = column(value, 0);
			if (first.length() == value.length())
				throw fail(value);
			return parseInt(first);
		}
	}

	private Float parseFloat(String value) throws ParseException {
		if (value.trim().isEmpty())
			return null;
		try {
			return Float.parseFloat(value);
		} catch (NumberFormatException e) {
			throw fail(value);
		}
	}

	private Long parseLong(String value) throws ParseException {
		if (value.trim().isEmpty())
			return null;
		try {
			return Long.parseLong(value);
		} catch (NumberFormatException e) {
			throw fail(value);
		}
	}

	private String parseString(String value) {
		return value;
	}

	private ParseException fail(String value) {
		return new ParseException(value);
	}

	private static class ParseException extends Exception {
		private static final long serialVersionUID = 1L;

		private final String string;

		private ParseException(String string) {
			this.string = string;
		}

		UnexpectedType report() {
			return new UnexpectedType(string);
		}
	}

}
