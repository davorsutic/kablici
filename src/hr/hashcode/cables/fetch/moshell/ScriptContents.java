package hr.hashcode.cables.fetch.moshell;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import hr.hashcode.cables.fetch.moshell.MarkedContents.Entry;

class ScriptContents {

	private static final String separator = "ENTITY_SEPARATOR";
	private static final String momVersion = "momversion";
	private static final String mibPrefix = "mibprefix";
	private static final String momTreeCommand = "momt";
	private static final String momTypesCommand = "mom";
	private static final String momTypeDetailsCommand = "momc . . .";
	private static final String sget = "sget ";
	private static final String print = "print ";
	private static final String lpr = "lpr";

	final String mib;
	final String mom;
	final String momTree;
	final String typeDetails;
	final String types;
	final String params;

	private ScriptContents(String mib, String mom, String momTree, String typeDetails, String types, String params) {
		this.mib = mib;
		this.mom = mom;
		this.momTree = momTree;
		this.typeDetails = typeDetails;
		this.types = types;
		this.params = params;
	}

	private static String extract(MarkedContents contents, String var) {
		String start = "^" + print + var + "=";
		String command = contents.get(start);
		if (command == null)
			return null;
		Pattern pattern = Pattern.compile("^" + var + "=(.*)$", Pattern.MULTILINE);
		Matcher matcher = pattern.matcher(command);
		if (matcher.find())
			return matcher.group(1);
		else
			return null;
	}

	static ScriptContents of(byte[] bytes) {
		String contents = new String(bytes);

		MarkedContents commands = split(contents);

		String mom = extract(commands, momVersion);
		String mib = extract(commands, mibPrefix);

		String typeDetails = commands.get("^" + momTypeDetailsCommand + "$");
		String momTree = commands.get("^" + momTreeCommand + "$");
		String params = commands.get("^" + sget);
		String types = commands.get("^" + momTypesCommand + "$");

		if (mom == null || mib == null || typeDetails == null || momTree == null || params == null)
			return null;

		if (types == null)
			types = "";

		return new ScriptContents(mib, mom, momTree, typeDetails, types, params);
	}

	private static final Pattern separatorPattern =
			Pattern.compile("^" + Pattern.quote(separator + "> "), Pattern.MULTILINE);
	private static final Pattern nonEmpty = Pattern.compile("^(.*\\S.*)$", Pattern.MULTILINE);

	static MarkedContents split(String text) {
		String[] elems = separatorPattern.split(text);
		List<Entry> commands = new ArrayList<>();

		for (String elem : elems) {
			Matcher matcher = nonEmpty.matcher(elem);
			if (matcher.find()) {
				String first = matcher.group(1);
				Entry entry = new Entry(first, elem);
				commands.add(entry);
			}
		}

		return new MarkedContents(commands);
	}

	private final static String rbsClasses =
			"AntennaBranch,AntFeederCable,AuxPlugInUnit,Cabinet,Carrier,DigitalCable,EcBus,EcPort,EquipmentSupportFunction,ExternalNode,HwUnit,InterPiuLink,Iub,ManagedElement,PiuType,PlugInUnit,RbsLocalCell,RbsSlot,RbsSubrack,RbsSynchronization,Sector,SectorAntenna,Slot,Subrack,SubrackProdType";
	private final static String erbsClasses =
			"AntennaUnitGroup,AuxPlugInUnit,Cabinet,EcBus,EcPort,EquipmentSupportFunction,EUtranCellFDD,ExternalNode,ManagedElement,PiuType,PlugInUnit,RbsSlot,RbsSubrack,RfBranch,RfPort,RiLink,RiPort,SectorCarrier,SectorEquipmentFunction,Slot,Subrack,SubrackProdType";
	private final static String msrbsClasses =
			"AntennaUnitGroup,Cabinet,EcBus,EcPort,EquipmentSupportFunction,EUtranCellFDD,ExternalNode,FieldReplaceableUnit,HwItem,ManagedElement,RfBranch,RfPort,RiLink,RiPort,SectorCarrier,SectorEquipmentFunction";

	private static List<String> list(String classes) {
		return Arrays.asList(classes.split(","));
	}

	private final static List<String> rbsList = list(rbsClasses);
	private final static List<String> erbsList = list(erbsClasses);
	private final static List<String> msrbsList = list(msrbsClasses);

	private static class CommandBuilder {
		private final StringBuilder builder = new StringBuilder();

		CommandBuilder command(String command) {
			builder.append(command).append("\n");
			return this;
		}

		CommandBuilder print(String variable) {
			return command(print + variable + "=$" + variable);
		}

		CommandBuilder getObjects(String version, Collection<String> classes) {
			command("if $" + momVersion + " ~ ^" + version);
			String target;
			if (classes.isEmpty())
				target = ".";
			else
				target = classes.stream().collect(Collectors.joining("|", "^(", ")="));
			command("\t" + sget + target);
			return command("fi");
		}

		CommandBuilder empty() {
			return command("");
		}

		String build() {
			return builder.toString();
		}
	}

	static String script() {
		List<String> empty = Collections.emptyList();
		return script(empty, empty, empty);
	}

	static String restricted() {
		return script(rbsList, erbsList, msrbsList);
	}

	private static String script(Collection<String> rbs, Collection<String> erbs, Collection<String> msrbs) {
		return new CommandBuilder()
				.command("re")
				.command("lt all")
				.empty()
				.command("sget 0 userLabel > $userLabel")
				.command("$log = $ipaddress_$userLabel_$momversion.log")
				.command("p " + separator)
				.command("l+osmm $log")
				.empty()
				.print(momVersion)
				.command(momTypeDetailsCommand)
				.command(momTreeCommand)
				.command(momTypesCommand)
				.print(mibPrefix)
				.command(lpr)
				.getObjects("RBS", rbs)
				.getObjects("ERBS", erbs)
				.getObjects("MSRBS", msrbs)
				.command("l-")
				.command("!gzip -f $log")
				.build();
	}

	public static void main(String[] args) {
		System.out.println(restricted());
	}
}
