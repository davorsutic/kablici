package hr.hashcode.cables.fetch.moshell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MarkedContents {

	static class Entry {
		private final String header;
		private final String contents;

		Entry(String header, String contents) {
			this.header = header;
			this.contents = contents;
		}

		String header() {
			return header;
		}

		String contents() {
			return contents;
		}

		@Override
		public String toString() {
			return header;
		}
	}

	final List<Entry> entries;

	MarkedContents(List<Entry> entries) {
		this.entries = Collections.unmodifiableList(new ArrayList<>(entries));
	}

	String get(String prefix) {
		Pattern pattern = Pattern.compile(prefix);
		Matcher matcher = pattern.matcher("");
		for (Entry entry : entries) {
			matcher.reset(entry.header);
			if (matcher.find())
				return entry.contents;
		}
		return null;
	}

}
