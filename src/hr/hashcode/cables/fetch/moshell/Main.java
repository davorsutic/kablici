package hr.hashcode.cables.fetch.moshell;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.zip.GZIPInputStream;

import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.util.Util;

public class Main {

	public static void main(String[] args) throws IOException {

		for (File file : new File("moshell").listFiles((dir, name) -> name.endsWith(".log.gz"))) {
			byte[] bytes;
			try (InputStream input =
					Files.newInputStream(file.toPath());
					InputStream gzip = new GZIPInputStream(input)) {
				bytes = Util.drain(gzip);
			}
			NodeState state = ParseMoshellDump.read(bytes);
			System.err.println(file);
			System.err.println(state.objects().size());
			System.err.println("*********");

		}

	}

}
