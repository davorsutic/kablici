package hr.hashcode.cables.fetch;

import hr.hashcode.cables.fetch.neal.G1Fetcher;
import hr.hashcode.cables.mom.MomFetchG2;
import hr.hashcode.cables.ran.Version;
import hr.hashcode.cables.util.Ports;

public class FetchVersion {
	public static Version version(String ipAddress) {
		if (Ports.corba(ipAddress))
			return G1Fetcher.version(ipAddress);
		else if (Ports.ssl(ipAddress))
			return MomFetchG2.version(ipAddress);
		else
			return null;
	}
}
