package hr.hashcode.cables.fetch;

public interface Fetcher {
	FetchResult fetch();
}
