package hr.hashcode.cables.fetch;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import hr.hashcode.cables.mom.MomStore;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.Version;

public class FetchNodeSource {

	public interface Reporter {
		void success(NodeState state);

		void failed(String ip);

		void finish();
	}

	private final Thread thread;

	private final List<String> ips;
	private final Reporter reporter;
	private final FetchServer server;

	public FetchNodeSource(Map<Version.Type, FetchFilter> filters, List<String> ips, Reporter reporter) {
		this.reporter = reporter;
		this.ips = new ArrayList<>(ips);
		this.thread = new Thread(this::fetach);
		MomStore momStore = new MomStore(Arrays.asList(Paths.get("C:\\Users\\makkusu\\git\\kablici\\data")));
		this.server = new FetchServer(false, false, 20, momStore, "bts", "bts", ips, filters);
	}

	public void cancel() {
		thread.interrupt();
	}

	public void start() {
		thread.start();
	}

	private void fetach() {

		server.start();
		try {
			for (int finished = 0; finished < ips.size(); finished++) {
				FetchResult result;
				try {
					result = server.next();
				} catch (InterruptedException e) {
					break;
				}

				try {
					if (result.success())
						reporter.success(result.state());
					else
						reporter.failed(result.ipAddress());
				} catch (Throwable e) {}
			}
			reporter.finish();
		} finally {
			server.cancel();
		}
	}
}
