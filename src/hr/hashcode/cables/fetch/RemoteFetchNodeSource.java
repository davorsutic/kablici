package hr.hashcode.cables.fetch;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import hr.hashcode.cables.fetch.Listener.CloseRequest;
import hr.hashcode.cables.fetch.Listener.FetchRequest;
import hr.hashcode.cables.fetch.Listener.StatusRequest;
import hr.hashcode.cables.fetch.Listener.StatusResult;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.Version;

public class RemoteFetchNodeSource {

	public interface Reporter {
		void success(NodeState state);

		void failed(String ip);

		void finish();

		void connectionError();

		void protocolError();
	}

	private final Thread thread;

	private final List<String> ips;
	private final Reporter reporter;
	private final String address;
	private final int port;
	private final Map<Version.Type, FetchFilter> filters;

	public RemoteFetchNodeSource(String address, int port, Map<Version.Type, FetchFilter> filters, List<String> ips,
			Reporter reporter) {
		this.address = address;
		this.port = port;
		this.reporter = reporter;
		this.ips = new ArrayList<>(ips);
		this.thread = new Thread(this::fetach);
		this.filters = filters;
	}

	public void cancel() {
		thread.interrupt();
	}

	public void start() {
		thread.start();
	}

	private void fetach() {

		try (Socket socket = new Socket(address, port)) {

			OutputStream output = socket.getOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(output);

			out.writeObject(new FetchRequest(ips, filters));
			out.flush();

			InputStream input = socket.getInputStream();
			ObjectInputStream in = new ObjectInputStream(input);

			in.readObject();

			int finished = 0;
			while (finished < ips.size()) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					break;
				}

				out.writeObject(new StatusRequest());
				out.flush();

				byte[] bytes = (byte[]) in.readObject();
				StatusResult result;
				try (ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
						GZIPInputStream gzip = new GZIPInputStream(byteStream);
						ObjectInputStream object = new ObjectInputStream(gzip)) {
					result = (StatusResult) object.readObject();
				}

				for (NodeState state : result.states()) {
					reporter.success(state);
					finished++;
				}
				for (String ip : result.failed()) {
					reporter.failed(ip);
					finished++;
				}
			}

			out.writeObject(new CloseRequest());
			out.flush();

			in.readObject();
			reporter.finish();
		} catch (IOException e) {
			reporter.connectionError();
		} catch (ClassNotFoundException e) {
			reporter.protocolError();
		}
	}

}
