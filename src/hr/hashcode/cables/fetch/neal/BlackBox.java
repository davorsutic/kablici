package hr.hashcode.cables.fetch.neal;

import java.lang.reflect.Array;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.function.Function;

import hr.hashcode.cables.ran.Meta.MoArray;
import hr.hashcode.cables.ran.Meta.MoType;
import se.ericsson.cello.neal.cm.Mo;
import se.ericsson.cello.neal.cm.Struct;
import se.ericsson.cello.neal.cm.TypeEnumeration;
import se.ericsson.cello.neal.cm.Value;

class BlackBox {

	private static final Map<Integer, Token<?>> tokens = new HashMap<>();

	static final Token<Integer> INT =
			token(TypeEnumeration.INT, Integer.class, Value::getIntValue, Value::getIntArrayValue);
	static final Token<String> STRING =
			token(TypeEnumeration.STRING, String.class, Value::getStringValue, Value::getStringArrayValue);
	static final Token<Boolean> BOOLEAN =
			token(TypeEnumeration.BOOLEAN, Boolean.class, Value::getBooleanValue, Value::getBooleanArrayValue);
	static final Token<Long> LONG =
			token(TypeEnumeration.LONG, Long.class, Value::getLongValue, Value::getLongArrayValue);
	static final Token<Float> FLOAT =
			token(TypeEnumeration.FLOAT, Float.class, Value::getFloatValue, Value::getFloatArrayValue);
	static final Token<Mo> MO =
			token(TypeEnumeration.MO, Mo.class, Value::getMoValue, Value::getMoArrayValue);
	static final Token<String> FDN =
			token(TypeEnumeration.FDN, String.class, Value::getStringValue, Value::getStringArrayValue);
	static final Token<Struct> STRUCT =
			token(TypeEnumeration.STRUCT, Struct.class, Value::getStructValue, Value::getStructArrayValue);

	static final Token<Object[]> ARRAY = token(TypeEnumeration.ARRAY, Object[].class, null, null);

	private static <T> Token<T> token(int mark, Class<T> clazz, Function<Value, T> single, Function<Value, ?> array) {
		Token<T> token = new Token<>(mark, clazz, single, array);
		tokens.put(mark, token);
		return token;
	}

	private final Map<Integer, Map<Class<?>, Combiner<?, ?, ?>>> combiners = new HashMap<>();

	<T> void register(Token<T> extractor) {
		register(extractor, extractor.clazz, (type, object) -> object);
	}

	<T, V> void register(Token<T> extractor, Class<V> clazz, WeakConverter<T, V> converter) {
		register(extractor, clazz, (type, object) -> converter.convert(object));
	}

	<T, U extends MoType, V> void register(Token<T> extractor, Class<V> clazz, Converter<T, U, V> converter) {
		Combiner<T, U, V> combiner = new Combiner<>(extractor, converter);
		combiners.computeIfAbsent(extractor.mark, x -> new IdentityHashMap<>()).put(clazz, combiner);
	}

	private <U extends MoType> Combiner<?, U, ?> combiner(int mark, U type, Value value) throws ProcessException {
		Class<?> clazz = type.clazz();
		Combiner<?, ?, ?> combiner = combiners.getOrDefault(mark, Collections.emptyMap()).get(clazz);
		if (combiner == null)
			throw fail(value);
		@SuppressWarnings("unchecked")
		Combiner<?, U, ?> cast = (Combiner<?, U, ?>) combiner;
		return cast;
	}

	private static ProcessException fail(Value value) {
		return new ProcessException(value.getValue().toString());
	}

	<U extends MoType> Object parse(U type, Value value) throws ProcessException {
		if (value == null)
			return null;

		int mark = value.getType();
		if (mark == TypeEnumeration.VOID)
			return null;
		if (value.getValue() == null)
			return null;

		Combiner<?, U, ?> combiner = combiner(mark, type, value);

		if (mark == TypeEnumeration.ARRAY) {
			// TODO this is not good. it assumes that ManagedArray is specified with ARRAY token
			if (!(type instanceof MoArray))
				throw new ProcessException(value.getValue().toString());
			MoType elemType = ((MoArray) type).elemType;
			int innerMark = value.getArrayType();
			Combiner<?, MoType, ?> innerCombiner = combiner(innerMark, elemType, value);
			Object[] array = innerCombiner.combineArray(elemType, value);
			@SuppressWarnings("unchecked")
			Combiner<Object[], U, ?> cast = (Combiner<Object[], U, ?>) combiner;
			return cast.converter.convert(type, array);
		} else
			return combiner.combine(type, value);

	}

	static class Token<T> {
		private final int mark;
		private final Class<T> clazz;
		private final Function<Value, T> function;
		private final Function<Value, ?> arrayFunction;

		private Token(int mark, Class<T> clazz, Function<Value, T> function, Function<Value, ?> arrayFunction) {
			this.mark = mark;
			this.clazz = clazz;
			this.function = function;
			this.arrayFunction = arrayFunction;
		}

		T get(Value value) {
			return function.apply(value);
		}

		T[] getArray(Value value) {
			Object initial = arrayFunction.apply(value);
			int length = Array.getLength(initial);
			@SuppressWarnings("unchecked")
			T[] array = (T[]) Array.newInstance(clazz, length);
			for (int i = 0; i < length; i++)
				array[i] = clazz.cast(Array.get(initial, i));
			return array;
		}
	}

	interface Converter<T, U extends MoType, V> {
		V convert(U type, T object) throws ProcessException;
	}

	interface WeakConverter<T, V> {
		V convert(T object) throws ProcessException;
	}

	private static class Combiner<T, U extends MoType, V> {

		private final Token<T> extractor;
		private final Converter<T, U, V> converter;

		Combiner(Token<T> extractor, Converter<T, U, V> converter) {
			this.extractor = extractor;
			this.converter = converter;
		}

		V combine(U type, Value value) throws ProcessException {
			T extracted = extractor.get(value);
			if (extracted == null)
				return null;
			V converted = converter.convert(type, extracted);
			return converted;
		}

		Object[] combineArray(U type, Value value) throws ProcessException {
			T[] extracted = extractor.getArray(value);
			int length = extracted.length;
			Object[] array = new Object[length];
			for (int i = 0; i < length; i++) {
				T elem = extracted[i];
				if (elem == null)
					array[i] = null;
				else
					try {
						array[i] = converter.convert(type, elem);
					} catch (ProcessException e) {
						array[i] = e.report();
					}
			}
			return array;
		}
	}
}
