package hr.hashcode.cables.fetch.neal;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import hr.hashcode.cables.ran.Meta.MoArray;
import hr.hashcode.cables.ran.Meta.MoAttr;
import hr.hashcode.cables.ran.Meta.MoEnum;
import hr.hashcode.cables.ran.Meta.MoEnum.Element;
import hr.hashcode.cables.ran.Meta.MoStruct;
import hr.hashcode.cables.ran.Meta.MoType;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.NodeState.ManagedArray;
import hr.hashcode.cables.ran.NodeState.ManagedAttribute;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.ran.NodeState.ManagedStruct;
import se.ericsson.cello.neal.cm.Mo;
import se.ericsson.cello.neal.cm.Struct;
import se.ericsson.cello.neal.cm.Value;

public class NealObjects {

	private final Function<String, ManagedObject> refQuery;
	private final BlackBox box;

	NealObjects(Function<String, ManagedObject> refQuery) {
		this.refQuery = refQuery;
		this.box = init();
	}

	private BlackBox init() {
		BlackBox box = new BlackBox();
		box.register(BlackBox.INT);
		box.register(BlackBox.STRING);
		box.register(BlackBox.BOOLEAN);
		box.register(BlackBox.LONG);
		box.register(BlackBox.FLOAT);
		box.register(BlackBox.LONG, Integer.class, NealObjects::fromLong);
		box.register(BlackBox.INT, Long.class, NealObjects::fromInt);
		box.register(BlackBox.INT, MoEnum.Element.class, NealObjects::convertEnum);
		box.register(BlackBox.MO, ManagedObject.class, this::convertRef);
		box.register(BlackBox.FDN, ManagedObject.class, this::convertFdn);
		box.register(BlackBox.STRUCT, ManagedStruct.class, this::convertStruct);
		box.register(BlackBox.ARRAY, ManagedArray.class, this::convertArray);
		return box;
	}

	Object transform(MoType type, Value value) {
		try {
			return box.parse(type, value);
		} catch (ProcessException e) {
			return e.report();
		}
	}

	private ManagedStruct convertStruct(MoStruct type, Struct object) {
		List<ManagedAttribute> attributes = new ArrayList<>();
		for (MoAttr moAttr : type.attrs) {
			Value memberValue = object.getMember(moAttr.name);
			Object parsed;
			if (memberValue == null)
				parsed = NodeState.FETCH_FAILURE;
			else
				parsed = transform(moAttr.type, memberValue);

			attributes.add(new ManagedAttribute(moAttr, parsed));
		}
		return new ManagedStruct(type, attributes);
	}

	private static Element convertEnum(MoEnum type, Integer index) throws ProcessException {
		MoEnum.Element element = type.byIndex(index);
		if (element == null)
			throw fail(String.valueOf(index));
		return element;
	}

	private ManagedObject convertRef(Mo object) throws ProcessException {
		String ldn = object.getLdn();
		return searchRef(ldn);
	}

	private ManagedObject convertFdn(String fdn) throws ProcessException {
		if (fdn.isEmpty())
			return null;
		return searchRef(fdn);
	}

	private ManagedObject searchRef(String dn) throws ProcessException {
		ManagedObject ref = refQuery.apply(dn);
		if (ref == null)
			throw fail(dn);
		return ref;
	}

	private static Long fromInt(Integer value) {
		return (long) value.intValue();
	}

	private static Integer fromLong(Long value) throws ProcessException {
		long unbox = value.longValue();
		if (unbox == (int) unbox)
			return (int) unbox;
		throw fail(String.valueOf(value));
	}

	private ManagedArray convertArray(MoArray arrayType, Object[] array) {
		return new ManagedArray(arrayType.elemType, array);
	}

	private static ProcessException fail(String value) {
		return new ProcessException(value);
	}

}
