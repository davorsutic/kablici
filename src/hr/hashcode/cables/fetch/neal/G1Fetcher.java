package hr.hashcode.cables.fetch.neal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.hashcode.cables.fetch.FetchFilter;
import hr.hashcode.cables.fetch.FetchResult;
import hr.hashcode.cables.fetch.FetchResult.ErrorCode;
import hr.hashcode.cables.fetch.Fetcher;
import hr.hashcode.cables.ran.Meta;
import hr.hashcode.cables.ran.Meta.MoAttr;
import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.NodeState.Builder;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.ran.NodeState.UnexpectedType;
import hr.hashcode.cables.ran.Version;
import se.ericsson.cello.neal.Node;
import se.ericsson.cello.neal.NodeAccess;
import se.ericsson.cello.neal.cm.CmException;
import se.ericsson.cello.neal.cm.CmService;
import se.ericsson.cello.neal.cm.DnAttributes;
import se.ericsson.cello.neal.cm.DnNameValues;
import se.ericsson.cello.neal.cm.MibInfo;
import se.ericsson.cello.neal.cm.Mo;
import se.ericsson.cello.neal.cm.NameValue;
import se.ericsson.cello.neal.cm.Scope;

public class G1Fetcher implements Fetcher {

	public static Version version(String ipAddress) {
		Node node;
		try {
			node = NodeAccess.getNode(ipAddress);
		} catch (IOException e) {
			return null;
		}
		try {
			CmService cm = node.getCmService();
			MibInfo info = cm.getMibInfo();
			String mark = info.mibType + "_" + info.mimVersion.replace('.', '_');
			return Version.of(mark);
		} catch (CmException e) {
			return null;
		} finally {
			node.free();
		}
	}

	private static final Logger logger = LoggerFactory.getLogger(G1Fetcher.class);

	private static final int BATCH_LIMIT = 1000;

	private final String ipAddress;
	private final Meta meta;
	private final FetchFilter filter;

	public G1Fetcher(String ipAddress, Meta meta, FetchFilter filter) {
		this.ipAddress = ipAddress;
		this.meta = meta;
		this.filter = filter;
	}

	@Override
	public FetchResult fetch() {
		long start = System.nanoTime();

		Node node;
		try {
			node = NodeAccess.getNode(ipAddress);
		} catch (IOException e) {
			return FetchResult.error(ipAddress, ErrorCode.IO_ERROR);
		}

		try {
			CmService cmService = node.getCmService();
			String mibPrefix = cmService.getMibPrefix(null);
			FetchInstance instance = new FetchInstance(cmService, mibPrefix, ipAddress, meta, filter);
			FetchResult result = instance.go();
			long end = System.nanoTime();
			System.out.println(ipAddress + ": Fetching finished in " + (end - start) / 1_000_000);
			return result;
		} catch (CmException e) {
			logger.error("Error while fetching: " + ipAddress, e);
			return FetchResult.error(ipAddress, ErrorCode.UNEXPECTED_SERVER_REPLY);
		} catch (Throwable e) {
			logger.error("Unexpected error: " + ipAddress, e);
			return FetchResult.error(ipAddress, ErrorCode.UNEXPECTED_ERROR);
		} finally {
			node.free();
		}
	}

	private static class FetchInstance {

		private final Map<String, MoClass> classes;
		private final Map<String, ManagedObject> byLdn = new LinkedHashMap<>();
		private final String mibPrefix;

		private final String ipAddress;
		private final Builder builder;
		private final CmService cmService;
		private final NealObjects parser;
		private final FetchFilter filter;
		private final Meta meta;

		FetchInstance(CmService cmService, String mibPrefix, String ipAddress, Meta meta, FetchFilter filter) {
			this.meta = meta;
			this.builder = NodeState.newBuilder(meta);
			this.classes = meta.classes().stream()
					.collect(Collectors.toMap(x -> x.name, x -> x));
			this.ipAddress = ipAddress;
			this.cmService = cmService;
			this.mibPrefix = mibPrefix;
			this.parser = new NealObjects(this::getRef);
			this.filter = filter;
		}

		private ManagedObject getRef(String dn) {
			if (dn == null)
				return null;

			dn = dn.trim();

			String ldn;
			if (dn.startsWith(mibPrefix)) {
				int start = mibPrefix.length();
				if (start > 0)
					start++;
				ldn = dn.substring(start);
			} else
				ldn = dn;
			return object(ldn);
		}

		ManagedObject object(String ldn) {
			if (ldn == null || ldn.isEmpty())
				return null;
			ManagedObject object = byLdn.get(ldn);
			if (object != null)
				return object;
			int index = ldn.lastIndexOf(',');
			String parentLdn;
			if (index == -1)
				parentLdn = null;
			else
				parentLdn = ldn.substring(0, index);
			int sep = ldn.lastIndexOf('=');
			MoClass moClass = classes.get(ldn.substring(index + 1, sep));
			String name = ldn.substring(sep + 1);
			object = builder.newObject(object(parentLdn), moClass, name);
			byLdn.put(ldn, object);
			return object;
		}

		FetchResult go() {
			Mo root = cmService.getRootMo();
			Map<MoClass, ClassObjects> byClass = new IdentityHashMap<>();
			for (MoClass moClass : meta.classes()) {
				if (filter.test(moClass.name())) {
					Mo[] mos;
					if (moClass.name.equals(root.getType()))
						mos = new Mo[]{root};
					else
						mos = root.getChildren(moClass.name, "", Scope.ALL_LEVELS, null);
					ClassObjects classObjects = new ClassObjects(moClass);
					byClass.put(moClass, classObjects);
					// for unknown reason node sometimes returns two objects with same ldn, this set
					// unifies duplicates
					Set<Mo> uniques = new LinkedHashSet<>(Arrays.asList(mos));
					for (Mo mo : uniques) {
						ManagedObject object = object(mo.getLdn());
						classObjects.objects.add(object);
					}

				}
			}

			List<ClassObjects> classObjects = new ArrayList<>(byClass.values());
			Collections.sort(classObjects, Comparator.comparing((ClassObjects x) -> x.moClass.name));
			for (ClassObjects entry : classObjects) {
				MoClass moClass = entry.moClass;

				String className = moClass.name();

				MoAttr[] moAttrs = Arrays.stream(moClass.attrs())
						.filter(x -> filter.test(className, x.name))
						.filter(x -> !x.name.startsWith("pm"))
						.toArray(MoAttr[]::new);

				String[] attrs = Arrays.stream(moAttrs)
						.map(x -> x.name)
						.toArray(String[]::new);

				for (ManagedObject[] objs : entry.batches(BATCH_LIMIT)) {
					DnAttributes[] dnAttrs = Arrays.stream(objs)
							.map(x -> new DnAttributes(x.ldn(), attrs))
							.toArray(DnAttributes[]::new);

					try {
						DnNameValues[] dnNameValues = cmService.getMoAttributes(dnAttrs, null);
						int len = objs.length;
						for (int i = 0; i < len; i++) {
							ManagedObject object = objs[i];
							NameValue[] nameValues = dnNameValues[i].getNameValues();
							for (int j = 0; j < moAttrs.length; j++) {
								NameValue nameValue = nameValues[j];
								MoAttr attr = moAttrs[j];
								Object value = parser.transform(attr.type, nameValue);
								if (value instanceof UnexpectedType)
									warn(object + "," + attr + "," + nameValue);
								builder.set(object, attr, value);
							}
						}
					} catch (CmException e) {
						for (ManagedObject object : objs) {
							String ldn = object.ldn();
							for (MoAttr attr : moAttrs) {
								String[] singleAttr = {attr.name};
								DnAttributes[] singleDnAttr = {new DnAttributes(ldn, singleAttr)};
								try {
									NameValue nameValue =
											cmService.getMoAttributes(singleDnAttr, null)[0].getNameValues()[0];
									Object value = parser.transform(attr.type, nameValue);
									if (value instanceof UnexpectedType)
										warn(object + "," + attr + "," + nameValue);
									builder.set(object, attr, value);
								} catch (CmException ee) {
									String message = "Fetching attributes failed: " + object + "," + attr;
									warn(message);
									debug(message, e);
									builder.set(object, attr, NodeState.FETCH_FAILURE);
								}
							}
						}
					}

				}
			}
			NodeState state = builder.build(mibPrefix);
			return FetchResult.of(ipAddress, state);
		}

		String format(String message) {
			return "IP: " + ipAddress + ", " + message;
		}

		private void warn(String message) {
			logger.warn(format(message));
		}

		private void debug(String message) {
			logger.debug(format(message));
		}

		private void debug(String message, Throwable t) {
			logger.debug(format(message), t);
		}

	}

	private static class ClassObjects {
		private final MoClass moClass;
		private final List<ManagedObject> objects = new ArrayList<>();

		private ClassObjects(MoClass moClass) {
			this.moClass = moClass;
		}

		List<ManagedObject[]> batches(int limit) {
			int params = moClass.length();
			int batchSize = (limit - 1) / params + 1;
			List<ManagedObject[]> batches = new ArrayList<>();
			for (int i = 0; i < objects.size(); i += batchSize) {
				int len = Math.min(objects.size() - i, batchSize);
				ManagedObject[] array = new ManagedObject[len];
				for (int j = 0; j < len; j++)
					array[j] = objects.get(i + j);
				batches.add(array);
			}
			return batches;
		}
	}

}
