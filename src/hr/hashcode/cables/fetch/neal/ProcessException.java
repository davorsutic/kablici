package hr.hashcode.cables.fetch.neal;

import hr.hashcode.cables.ran.NodeState.UnexpectedType;

class ProcessException extends Exception {
	private static final long serialVersionUID = 1L;

	private final String value;

	ProcessException(String value) {
		this.value = value;
	}

	UnexpectedType report() {
		return new UnexpectedType(value);
	}

}
