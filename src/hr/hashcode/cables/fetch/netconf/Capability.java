package hr.hashcode.cables.fetch.netconf;

/**
 * Capabilities exchanged between parties in the netconf protocol.
 * 
 * @author Srdan Maksimovic
 */
public enum Capability {

	BASE_1_0("urn", "ietf", "params", "netconf", "base", "1.0"),
	BASE_1_1("urn", "ietf", "params", "netconf", "base", "1.1"),
	WRITABLE_RUNNING("urn", "ietf", "params", "netconf", "capability", "writable-running", "1.0"),
	CANDIDATE("urn", "ietf", "params", "netconf", "capability", "candidate", "1.0"),
	CONFIRMED_COMMIT("urn", "ietf", "params", "netconf", "capability", "confirmed-commit", "1.0"),
	ROLLBACK_ON_ERROR("urn", "ietf", "params", "netconf", "capability", "rollback-on-error", "1.0"),
	VALIDATE("urn", "ietf", "params", "netconf", "capability", "validate", "1.0"),
	STARTUP("urn", "ietf", "params", "netconf", "capability", "startup", "1.0"),
	URL("urn", "ietf", "params", "netconf", "capability", "url", "1.0"),
	XPATH("urn", "ietf", "params", "netconf", "capability", "xpath", "1.0"),
	PARTIAL_LOCK("urn", "ietf", "params", "netconf", "capability", "partial-lock", "1.0"),
	NOTIFICATION("urn", "ietf", "params", "netconf", "capability", "notification", "1.0"),
	INTERLEAVE("urn", "ietf", "params", "netconf", "capability", "interleave", "1.0"),
	WITH_DEFAULTS("urn", "ietf", "params", "netconf", "capability", "with-defaults", "1.0"),
	ERICSSON_NOTIFICATION("urn", "ericsson", "com", "netconf", "notification", "1.0"),
	ACTION("urn", "ericsson", "com", "netconf", "action", "1.0"),
	EBASE_0_1_0("urn", "com", "ericsson", "ebase", "0.1.0"),
	EBASE_1_1_0("urn", "com", "ericsson", "ebase", "1.1.0"),
	EBASE_1_2_0("urn", "com", "ericsson", "ebase", "1.2.0"),
	HEARTBEAT("urn", "ericsson", "com", "netconf", "heartbeat", "1.0"),
	OPERATION("com", "ericsson", "netconf", "operation", "1.0");

	private final Namespace namespace;

	private Capability(String first, String... others) {
		this.namespace = Namespace.of(first, others);
	}

	Namespace namespace() {
		return namespace;
	}

}

//
