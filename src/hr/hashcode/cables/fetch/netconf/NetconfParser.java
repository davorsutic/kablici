package hr.hashcode.cables.fetch.netconf;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import hr.hashcode.cables.ran.Meta.MoArray;
import hr.hashcode.cables.ran.Meta.MoAttr;
import hr.hashcode.cables.ran.Meta.MoEnum;
import hr.hashcode.cables.ran.Meta.MoStruct;
import hr.hashcode.cables.ran.Meta.MoType;
import hr.hashcode.cables.ran.Meta.SimpleType;
import hr.hashcode.cables.ran.NodeState.ManagedArray;
import hr.hashcode.cables.ran.NodeState.ManagedAttribute;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.ran.NodeState.ManagedStruct;
import hr.hashcode.cables.ran.NodeState.UnexpectedType;

/**
 * Parser of XML messages exchanged by netconf protocol
 * 
 * @author Srdan Maksimovic
 */
final class NetconfParser {

	private final Function<String, ManagedObject> refStore;

	NetconfParser(Function<String, ManagedObject> refStore) {
		this.refStore = refStore;
	}

	private static class ParseException extends Exception {
		private static final long serialVersionUID = 1L;

		private final String string;

		private ParseException(String string) {
			this.string = string;
		}

	}

	private String form(List<XmlNode> nodes) {
		return nodes.stream()
				.map(this::form)
				.collect(Collectors.joining(", ", "[", "]"));
	}

	private String form(XmlNode node) {
		List<XmlNode> children = node.children();
		if (children.isEmpty())
			return node.contents();
		else {
			Map<String, List<XmlNode>> byName = children.stream()
					.collect(Collectors.groupingBy(XmlNode::name));
			byName = new TreeMap<>(byName);

			List<String> elements = new ArrayList<>();
			for (Map.Entry<String, List<XmlNode>> entry : byName.entrySet()) {
				List<XmlNode> list = entry.getValue();
				String strValue;
				if (list.size() == 1)
					strValue = form(list.get(0));
				else
					strValue = form(list);
				elements.add(entry.getKey() + "=" + strValue);
			}

			return elements.stream().collect(Collectors.joining(", ", "{", "}"));
		}
	}

	private ParseException fail(XmlNode node) {
		return fail(form(node));
	}

	private ParseException fail(List<XmlNode> nodes) {
		return fail(form(nodes));
	}

	private ParseException fail(String value) {
		return new ParseException(value);
	}

	private static Map<String, List<XmlNode>> childrenMap(XmlNode node) {
		return node.children().stream().collect(Collectors.groupingBy(XmlNode::name));
	}

	private Integer parseInteger(String value) throws ParseException {
		try {
			return Integer.valueOf(value);
		} catch (NumberFormatException e) {
			throw fail(value);
		}
	}

	private Long parseLong(String value) throws ParseException {
		try {
			return Long.parseLong(value);
		} catch (NumberFormatException e) {
			throw fail(value);
		}
	}

	private Double parseDouble(String value) throws ParseException {
		try {
			return Double.parseDouble(value);
		} catch (NumberFormatException e) {
			throw fail(value);
		}
	}

	private Boolean parseBoolean(String value) throws ParseException {
		if ("true".equalsIgnoreCase(value))
			return Boolean.TRUE;
		else if ("false".equalsIgnoreCase(value))
			return Boolean.FALSE;
		else
			throw fail(value);
	}

	private MoEnum.Element parseEnum(MoEnum moEnum, String value) throws ParseException {
		MoEnum.Element member = moEnum.byName(value);
		if (member == null)
			throw fail(value);
		return member;
	}

	private ManagedStruct parseStruct(MoStruct moStruct, XmlNode node) throws ParseException {
		Map<String, List<XmlNode>> children = childrenMap(node);

		List<ManagedAttribute> attributes = new ArrayList<>();
		for (MoAttr attr : moStruct.attrs) {
			String name = attr.name;
			List<XmlNode> values = children.get(name);// TODO array has some kind of notification if empty
			if (values == null)
				continue;
			Object value = parseValue(attr.type, values);
			attributes.add(new ManagedAttribute(attr, value));
		}

		return new ManagedStruct(moStruct, attributes);
	}

	private ManagedArray parseArray(MoType elemType, List<XmlNode> nodes) throws ParseException {
		List<Object> elems = new ArrayList<>();
		for (XmlNode node : nodes) {
			Object element = parseValue(elemType, node);
			elems.add(element);
		}
		return new ManagedArray(elemType, elems.toArray());
	}

	Object parse(MoType type, List<XmlNode> nodes) {
		try {
			return parseValue(type, nodes);
		} catch (ParseException e) {
			return new UnexpectedType(e.string);
		}
	}

	private Object parseValue(MoType type, List<XmlNode> nodes) throws ParseException {
		final Object parsed;
		if (nodes.size() == 1) {
			XmlNode first = nodes.get(0);
			String unset = first.attribute("unset");
			if ("true".equals(unset))
				return null;
		}
		if (type instanceof MoArray) { // TODO check if array is empty, or unset
			MoArray arrayType = (MoArray) type;
			parsed = parseArray(arrayType.elemType, nodes);
		} else {
			if (nodes.size() == 0)
				return null;
			if (nodes.size() > 1)
				throw fail(nodes);
			XmlNode node = nodes.get(0);
			parsed = parseValue(type, node);
		}
		return parsed;
	}

	private Object parseValue(MoType type, XmlNode node) throws ParseException {

		if (type instanceof SimpleType || type instanceof MoEnum) {
			if (node.children().size() > 0)
				throw fail(node);
			if (node.contents() == null) {
				if (type == SimpleType.STRING)
					return "";
				else
					return null;
			}
		}

		Object parsed;
		if (type == SimpleType.INTEGER)
			parsed = parseInteger(node.contents());
		else if (type == SimpleType.FLOAT)
			parsed = parseDouble(node.contents());
		else if (type == SimpleType.LONG)
			parsed = parseLong(node.contents());
		else if (type == SimpleType.STRING)
			parsed = node.contents();
		else if (type == SimpleType.BOOLEAN)
			parsed = parseBoolean(node.contents());
		else if (type instanceof MoEnum) {
			MoEnum moEnum = (MoEnum) type;
			parsed = parseEnum(moEnum, node.contents());
		} else if (type == SimpleType.REFERENCE) {
			String ldn = node.contents();
			if (ldn == null || ldn.trim().isEmpty())
				parsed = null;
			else {
				ManagedObject object = refStore.apply(ldn.trim());
				if (object == null)
					throw fail(ldn);
				parsed = object;
			}
		} else if (type instanceof MoStruct) {
			MoStruct structType = (MoStruct) type;
			parsed = parseStruct(structType, node);
		} else
			throw fail(node);
		return parsed;
	}

}
