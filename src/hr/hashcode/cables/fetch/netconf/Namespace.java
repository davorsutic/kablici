package hr.hashcode.cables.fetch.netconf;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;

import hr.hashcode.cables.util.Util;

/**
 * Representation of tree-like namespace.
 * 
 * @author Srdan Maksimovic
 *
 */
final class Namespace {

	private final Namespace parent;
	private final String name;

	static Namespace of(String first, String... others) {
		Namespace start = new Namespace(first);
		return of(start, others);
	}

	static Namespace of(Namespace first, String... others) {
		Namespace result = first;
		for (String other : others)
			result = new Namespace(result, other);
		return result;
	}

	private Namespace(String name) {
		this.name = Objects.requireNonNull(name);
		this.parent = null;
	}

	private Namespace(Namespace parent, String name) {
		this.parent = Objects.requireNonNull(parent);
		this.name = Objects.requireNonNull(name);
	}

	/**
	 * Returns the components of the namespace.
	 * 
	 * @return namespace components
	 */
	String[] components() {
		Deque<String> deque = new ArrayDeque<>();
		for (Namespace temp = this; temp != null; temp = temp.parent)
			deque.addFirst(temp.name);
		return Util.toArray(deque, String.class);
	}

}
