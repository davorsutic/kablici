package hr.hashcode.cables.fetch.netconf;

public interface PasswordProvider {
	String password();
}
