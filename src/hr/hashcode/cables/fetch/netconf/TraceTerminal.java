package hr.hashcode.cables.fetch.netconf;

import java.io.Closeable;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper around {@code Terminal} that monitors the communication and logs it to provided {@code OutputStream}s. It also implements
 * {@code Terminal} interface so it can transparently replace the monitored component.
 * <p>
 * All the IO errors related to logging actions are silently suppressed and do not make impact to the observed communication.
 * </p>
 * 
 * @author Srdan Maksimovic
 *
 */
public class TraceTerminal implements Terminal {

	private final static Logger logger = LoggerFactory.getLogger(TraceTerminal.class);

	public static TraceTerminal ignoreSent(Terminal terminal, OutputStream sent, OutputStream received) {
		return new TraceTerminal(sent, received, terminal);
	}

	public static TraceTerminal regular(Terminal terminal, OutputStream sent, OutputStream received) {
		return new TraceTerminal(terminal, sent, received);
	}

	private final Terminal terminal;
	private final CommentOutput sent;
	private final TraceInput received;

	private TraceTerminal(OutputStream sent, OutputStream received, Terminal terminal) {
		this.terminal = terminal;
		this.sent = new CommentOutput(terminal, sent);
		this.received = new TraceInput(terminal, received);
	}

	/**
	 * Creates a new wrapper for terminal that logs the communication with it.
	 * 
	 * @param terminal underlying terminal
	 * @param sent stream for storing information sent to terminal, if {@code null}, no logging will be done
	 * @param received stream for storing information received from terminal, if {@code null} no logging will be done
	 */
	private TraceTerminal(Terminal terminal, OutputStream sent, OutputStream received) {
		this.terminal = terminal;
		this.sent = new TraceOutput(terminal, sent);
		this.received = new TraceInput(terminal, received);
	}

	private static void close(Closeable stream) {
		try {
			stream.close();
		} catch (IOException e) {
			logger.warn("Couldn't close stream", e);
		}
	}

	@Override
	public void close() throws IOException {
		close(sent);
		close(received);
		terminal.close();
	}

	@Override
	public InputStream input() {
		return received;
	}

	@Override
	public OutputStream output() {
		return sent;
	}

	/**
	 * Appends a comment to the log of received information. It does not impact original communication.
	 * 
	 * @param message message to be logged
	 */
	public void commentReceived(String message) {
		received.comment(message);
	}

	/**
	 * Appends a comment to the log of sent information. It does not impact the original communication.
	 * 
	 * @param message message to be logged
	 */
	public void commentSent(String message) {
		sent.comment(message);
	}

	/** {@code OutputStream} that ignores all the provided data. */
	private static final PrintStream nullStream;

	static {
		OutputStream nullOutput = new OutputStream() {
			@Override
			public void write(int b) {}

			@Override
			public void write(byte[] b, int off, int len) {}

			@Override
			public void write(byte[] b) {}
		};
		nullStream = new PrintStream(nullOutput);
	};

	/**
	 * Monitors data received over input stream and writes it to the provided target stream.
	 */
	private static class TraceInput extends FilterInputStream {

		private final PrintStream log;

		private TraceInput(Terminal terminal, OutputStream received) {
			super(terminal.input());
			this.log = (received == null) ? nullStream : new PrintStream(received);
		}

		@Override
		public int read() throws IOException {
			int result = super.read();
			if (result != -1)
				log.write(result);
			return result;
		}

		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			int result = super.read(b, off, len);
			if (result != -1)
				log.write(b, off, result);
			return result;
		}

		@Override
		public int read(byte[] b) throws IOException {
			int result = super.read(b);
			if (result != -1)
				log.write(b);
			return result;
		}

		public void comment(String message) {
			log.println(message);
		}

		@Override
		public void close() throws IOException {
			log.close();
			super.close();
		}

	}

	private static class CommentOutput extends FilterOutputStream {

		private final PrintStream log;

		CommentOutput(Terminal terminal, OutputStream sent) {
			super(terminal.output());
			this.log = (sent == null) ? nullStream : new PrintStream(sent);
		}

		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			super.out.write(b, off, len);
			// not invoked as super.write(byte[], int, int) because it repeatedly calls super.out.write(int)
		}

		public void comment(String message) {
			log.println(message);
		}

		@Override
		public void close() throws IOException {
			log.close();
			super.close();
		}

	}

	/**
	 * Monitors data sent to output stream and writes it to the provided target stream.
	 */
	private static class TraceOutput extends CommentOutput {

		private final PrintStream log;

		private TraceOutput(Terminal terminal, OutputStream sent) {
			super(terminal, sent);
			this.log = (sent == null) ? nullStream : new PrintStream(sent);
		}

		@Override
		public void write(int b) throws IOException {
			log.write(b);
			super.write(b);
		}

		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			log.write(b, off, len);
			super.write(b, off, len);
		}

		@Override
		public void write(byte[] b) throws IOException {
			log.write(b);
			super.write(b);
		}

		@Override
		public void flush() throws IOException {
			log.flush();
			super.flush();
		}
	}

}
