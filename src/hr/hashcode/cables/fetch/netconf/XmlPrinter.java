package hr.hashcode.cables.fetch.netconf;

import java.util.Arrays;
import java.util.List;

import hr.hashcode.cables.fetch.netconf.XmlNode.Attribute;

public final class XmlPrinter {

	private XmlPrinter() {}

	public enum PrintOption {
		NO_TAB, NO_NEWLINE, NO_HEADER;
	}

	private static class Printer {

		private static final String HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

		private final StringBuilder builder = new StringBuilder();
		private final boolean useTabs;
		private final boolean useNewLines;

		private int indentLevel;

		private Printer(boolean printHeader, boolean useTabs, boolean useNewLines) {
			this.useTabs = useTabs;
			this.useNewLines = useNewLines;

			if (printHeader) {
				append(HEADER);
				newLine();
			}
		}

		private void newLine() {
			if (useNewLines)
				append("\n");
		}

		private void append(String... strings) {
			for (String string : strings)
				builder.append(string);
		}

		private void indent() {
			if (useTabs)
				for (int i = 0; i < indentLevel; i++)
					builder.append("    ");
		}

		private String quote(String string) {
			return string
					.replaceAll("&", "&amp;")
					.replaceAll("<", "&lt;")
					.replaceAll(">", "&gt;")
					.replaceAll("'", "&apos;")
					.replaceAll("\"", "&quot;");
		}

		private void printNode(XmlNode node) {
			String name = node.name();
			String contents = node.contents();

			indent();
			append("<", name);
			for (Attribute attribute : node.attributes())
				append(" ", attribute.name(), "=\"", quote(attribute.value()), "\"");

			if (contents != null) {
				append(">", quote(contents), "</", name, ">");
			} else {
				if (node.children().size() > 0) {
					append(">");
					newLine();

					indentLevel++;
					for (XmlNode child : node.children())
						printNode(child);
					indentLevel--;

					indent();
					append("</", name, ">");
				} else
					append("/>");
			}

			newLine();
		}

		public String build() {
			return builder.toString();
		}
	}

	/**
	 * Provides a string representation of an XML node. Default format uses newlines and tabs for better visual
	 * presentation. XML header is used as well. That behaviour can be changed by using one of {@code PrintOption}s.
	 * 
	 * @param node
	 *        XML node to be formatted
	 * @param options
	 *        printing options
	 * @return string representation of an XML node
	 */
	public static String print(XmlNode node, PrintOption... options) {

		List<PrintOption> list = Arrays.asList(options);

		boolean useNewLines = !list.contains(PrintOption.NO_NEWLINE);
		boolean useTabs = !list.contains(PrintOption.NO_NEWLINE) && !list.contains(PrintOption.NO_TAB);
		boolean useHeader = !list.contains(PrintOption.NO_HEADER);

		Printer printer = new Printer(useHeader, useTabs, useNewLines);
		printer.printNode(node);
		return printer.build();
	}

}
