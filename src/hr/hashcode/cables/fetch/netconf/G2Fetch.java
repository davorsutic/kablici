package hr.hashcode.cables.fetch.netconf;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.hashcode.cables.fetch.FetchFilter;
import hr.hashcode.cables.fetch.FetchResult;
import hr.hashcode.cables.fetch.FetchResult.ErrorCode;
import hr.hashcode.cables.fetch.Fetcher;
import hr.hashcode.cables.fetch.netconf.NetconfProtocol.ProtocolException;
import hr.hashcode.cables.ran.Meta;
import hr.hashcode.cables.ran.Meta.MoAttr;
import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.Meta.Parent;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.util.Util;

public final class G2Fetch implements Fetcher {

	private final Logger logger = LoggerFactory.getLogger(G2Fetch.class);

	private final String ipAddress;
	private final String username;
	private final PasswordProvider password;
	private final Meta meta;

	private final Map<MoClass, List<MoClass>> children;
	private final Map<MoClass, List<MoClass>> parents;
	private final List<MoClass> roots;
	private final FetchFilter filter;

	public G2Fetch(String ipAddress, Meta meta, String username, PasswordProvider password, FetchFilter filter) {
		this.ipAddress = ipAddress;
		this.meta = meta;
		this.username = username;
		this.password = password;
		this.filter = filter;

		Set<MoClass> roots = Util.identitySet(meta.classes());

		this.parents = new IdentityHashMap<>();
		this.children = new IdentityHashMap<>();
		for (Parent parent : meta.parents()) {
			children.computeIfAbsent(parent.parent, x -> new ArrayList<>())
					.add(parent.child);
			parents.computeIfAbsent(parent.child, x -> new ArrayList<>())
					.add(parent.parent);
			roots.remove(parent.child);
		}
		this.roots = new ArrayList<>(roots);
	}

	/* There are two threads that perform this task. One is submitting requests, the other one is receiving them. If
	 * only one thread is doing both tasks there is a risk that one of the buffers will get full and a deadlock might
	 * occur. */

	/**
	 * Request for fetching a single class.
	 */
	private static class Request {

		/** Marks that a final request has been submitted. */
		static final Request PILL = new Request(null);

		/** Indicates an error in sending requests. */
		static final Request ERROR = new Request(null);

		private final List<MoClass> chain;

		private Request(List<MoClass> chain) {
			this.chain = chain;
		}

	}

	/** Queue with unprocessed requests for fetching classes. */
	private final BlockingQueue<Request> queue = new LinkedBlockingQueue<>();

	private static class Container {
		private final ManagedObject object;
		private final XmlNode node;

		private Container(ManagedObject object, XmlNode node) {
			this.object = object;
			this.node = node;
		}
	}

	private FetchResult receive(NetconfProtocol protocol) {

		try {
			protocol.receiveHello();

			XmlNode dnPrefix = protocol.receive();
			String mibPrefix = dnPrefix.child("data")
					.child("ManagedElement")
					.child("dnPrefix")
					.contents();

			Processor processor = new Processor(meta, mibPrefix);

			while (true) {
				Request request;
				try {
					request = queue.take();
				} catch (InterruptedException e) {
					return FetchResult.error(ipAddress, ErrorCode.INTERRUPTED);
				}
				if (request == Request.ERROR)
					return FetchResult.error(ipAddress, ErrorCode.IO_ERROR);

				if (request == Request.PILL)
					break;

				XmlNode result = protocol.receive();
				XmlNode data = result.child("data");
				if (data.children().isEmpty())
					continue;

				processor.process(request.chain, data);

			}

			protocol.receive();

			NodeState state = processor.build();
			return FetchResult.of(ipAddress, state);
		} catch (IOException e) {
			logger.error("IO error", e);
			return FetchResult.error(ipAddress, ErrorCode.IO_ERROR);
		} catch (ProtocolException e) {
			logger.error("Protocol error", e);
			return FetchResult.error(ipAddress, ErrorCode.UNEXPECTED_SERVER_REPLY);
		}
	}

	private static class Processor {

		private final NodeState.Builder builder;

		private final Map<ManagedObject, Map<MoClass, Map<String, ManagedObject>>> children = new IdentityHashMap<>();
		private final List<Container> containers = new ArrayList<>();
		private final Map<String, ManagedObject> byLdn = new HashMap<>();
		private final String mibPrefix;
		private final Map<String, List<MoClass>> classes;

		Processor(Meta meta, String mibPrefix) {
			this.builder = NodeState.newBuilder(meta);
			this.classes = Util.group(meta.classes(), MoClass::name);
			this.mibPrefix = mibPrefix;
		}

		void process(List<MoClass> chain, XmlNode data) {
			helper(chain.size() - 1, chain, data, null);
		}

		private void helper(int level, List<MoClass> chain, XmlNode current, ManagedObject parent) {
			MoClass moClass = chain.get(level);

			for (XmlNode child : current.children(moClass.name)) {
				XmlNode nameId = child.child(moClass.key.name);
				String name = nameId.contents();

				Map<String, ManagedObject> map = children
						.computeIfAbsent(parent, x -> new IdentityHashMap<>())
						.computeIfAbsent(moClass, x -> new HashMap<>());

				ManagedObject object = map.get(name);
				if (object == null) {
					object = builder.newObject(parent, moClass, name);
					map.put(name, object);
					byLdn.put(object.ldn(), object);
				}
				if (level == 0)
					containers.add(new Container(object, child));
				else
					helper(level - 1, chain, child, object);
			}
		}

		private ManagedObject ref(String dn) {
			String ldn;
			if (mibPrefix != null && dn.startsWith(mibPrefix)) {
				int start = mibPrefix.length();
				if (start < dn.length())
					start++;
				ldn = dn.substring(start);
			} else
				ldn = dn;
			return processRef(ldn);
		}

		private ManagedObject processRef(String ldn) {
			if (ldn == null || ldn.isEmpty())
				return null;
			ManagedObject object = byLdn.get(ldn);
			if (object != null)
				return object;
			int index = ldn.lastIndexOf(',');
			String parentLdn;
			if (index == -1)
				parentLdn = null;
			else
				parentLdn = ldn.substring(0, index);
			int sep = ldn.lastIndexOf('=');
			List<MoClass> moClasses = classes.getOrDefault(ldn.substring(index + 1, sep), Collections.emptyList());
			if (moClasses.size() != 1)
				throw new IllegalArgumentException(ldn);
			MoClass moClass = moClasses.get(0);
			String name = ldn.substring(sep + 1);
			object = builder.newObject(processRef(parentLdn), moClass, name);
			byLdn.put(ldn, object);
			return object;
		}

		NodeState build() {

			NetconfParser parser = new NetconfParser(this::ref);

			for (Container container : containers) {
				ManagedObject object = container.object;
				MoClass moClass = object.moClass;
				XmlNode node = container.node;
				for (MoAttr attr : moClass.attrs()) {
					List<XmlNode> value = node.children(attr.name);
					Object parsed = parser.parse(attr.type, value);
					builder.set(object, attr, parsed);
				}
			}

			return builder.build(mibPrefix);
		}

	}

	private void chainsHelper(MoClass current, Deque<MoClass> deque, List<List<MoClass>> chains) {

		if (deque.contains(current))
			return; // TODO allow to have repeated class at the time

		deque.addLast(current);

		List<MoClass> parents = this.parents.get(current);
		if (parents == null)
			chains.add(new ArrayList<>(deque));
		else
			for (MoClass parent : parents)
				chainsHelper(parent, deque, chains);

		deque.removeLast();
	}

	private List<List<MoClass>> chains(MoClass moClass) {
		List<List<MoClass>> result = new ArrayList<>();
		chainsHelper(moClass, new ArrayDeque<>(), result);
		return result;
	}

	private void send(NetconfProtocol protocol) {
		try {
			try {
				protocol.sendHello();

				XmlNode dnPrefix = CommandFormatter.list("ManagedElement", "dnPrefix");
				protocol.send(dnPrefix);

				for (MoClass moClass : meta.classes())
					if (filter.test(moClass.name())) {
						List<List<MoClass>> chains = chains(moClass);
						for (List<MoClass> chain : chains) {
							XmlNode node = CommandFormatter.list(chain, filter);
							Request request = new Request(chain);
							queue.put(request);
							protocol.send(node);
						}
					}

				protocol.sendClose();

				queue.put(Request.PILL);

			} catch (IOException e) {
				logger.error("IO error", e);
				queue.put(Request.ERROR);
			}
		} catch (InterruptedException e) {
			logger.error("Process interrupted", e);
		}
	}

	@Override
	public FetchResult fetch() {
		long start = System.nanoTime();

		Terminal terminal;
		try {
			terminal = JschTerminal.newNetconf(username, password, ipAddress);
		} catch (IOException e) {
			logger.error("Can't connect", e);
			return FetchResult.error(ipAddress, ErrorCode.IO_ERROR);
		}

		NetconfProtocol protocol = new NetconfProtocol(terminal);

		Thread sender = new Thread(() -> send(protocol));
		sender.start();

		FetchResult result = receive(protocol);

		try {
			protocol.close();
		} catch (IOException e) {
			logger.error("Could not close connection", e);
		}

		long end = System.nanoTime();
		System.out.println(ipAddress + ": Fetching finished in " + (end - start) / 1_000_000);
		return result;
	}

}
