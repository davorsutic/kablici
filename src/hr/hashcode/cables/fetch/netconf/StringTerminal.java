package hr.hashcode.cables.fetch.netconf;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * Wrapper for terminal that communicates via {@code String} messages. It provides method for reading received lines, one by one. All the
 * messages are assumed to be encoded in UTF-8 charset.
 * 
 * Implementation note: Buffered structures are used internally, so if the underlying terminal is being read from other component too, part
 * of information might appear to be lost.
 * 
 * @author Srdan Maksimovic
 *
 */
public class StringTerminal implements Closeable {

	private final Terminal terminal;

	private final Writer writer;
	private final BufferedReader reader;

	public StringTerminal(Terminal terminal) {
		this.terminal = terminal;
		this.writer = new BufferedWriter(new OutputStreamWriter(terminal.output(), UTF_8));
		this.reader = new BufferedReader(new InputStreamReader(terminal.input(), UTF_8));
	}

	/**
	 * Sends a list of commands to the underlying component. They are sent in provided order without appending newline characters. The
	 * output is flushed immediately.
	 * 
	 * @param commands a list of commands
	 * @throws IOException if an IO error occurs
	 */
	public void send(String... commands) throws IOException {
		for (String command : commands)
			writer.append(command);
		writer.flush();
	}

	/**
	 * Returns the next line from the input.
	 * 
	 * @return next line received, or {@code null} if the end of input has been reached.
	 * @throws IOException if an IO error occurs
	 */
	public String readLine() throws IOException {
		return reader.readLine();
	}

	/**
	 * <p>
	 * Closes the underlying terminal.
	 * </p>
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public void close() throws IOException {
		terminal.close();
	}

}
