package hr.hashcode.cables.fetch.netconf;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.ChannelSubsystem;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import hr.hashcode.cables.util.Ports;

/**
 * Ssh connection using Jsch library.
 * 
 * @author Srdan Maksimovic
 */
public class JschTerminal implements Terminal {

	private static final int DEFAULT_TIMEOUT = 30_000;

	public static Terminal newSsh(String user, PasswordProvider pass, String host) throws IOException {
		return newSsh(user, pass, host, Ports.ssh);
	}

	public static Terminal newSsh(String user, PasswordProvider pass, String host, int port) throws IOException {
		return newInstance(user, pass, host, port, null);
	}

	public static Terminal newNetconf(String user, PasswordProvider pass, String host) throws IOException {
		return newNetconf(user, pass, host, Ports.netconf);
	}

	public static Terminal newNetconf(String user, PasswordProvider pass, String host, int port) throws IOException {
		return newInstance(user, pass, host, port, "netconf");
	}

	private static Terminal newInstance(String user, PasswordProvider pass, String host, int port, String subsystem)
			throws IOException {
		Session session;
		try {
			JSch jsch = new JSch();
			// TODO add identity
			session = jsch.getSession(user, host, port);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pass.password());
			session.connect(DEFAULT_TIMEOUT);
		} catch (JSchException e) {
			throw new IOException(e);
		}

		InputStream input;
		OutputStream output;
		try {
			Channel channel;
			if (subsystem == null) {
				channel = session.openChannel("shell");
				ChannelShell channelShell = (ChannelShell) channel;
				channelShell.setPty(true);
			} else {
				channel = session.openChannel("subsystem");
				ChannelSubsystem channelSubsystem = (ChannelSubsystem) channel;
				channelSubsystem.setSubsystem(subsystem);
				channelSubsystem.setPty(true);
			}

			input = channel.getInputStream();
			output = channel.getOutputStream();
			channel.connect(DEFAULT_TIMEOUT);
		} catch (JSchException e) {
			session.disconnect();
			throw new IOException(e);
		} catch (IOException e) {
			session.disconnect();
			throw e;
		}

		return new JschTerminal(session, input, output);
	}

	private final Session session;
	private final InputStream input;
	private final OutputStream output;

	private JschTerminal(Session session, InputStream input, OutputStream output) {
		this.session = session;
		this.input = input;
		this.output = output;
	}

	@Override
	public void close() {
		session.disconnect();
	}

	@Override
	public InputStream input() {
		return input;
	}

	@Override
	public OutputStream output() {
		return output;
	}

	public static void main(String[] args) throws IOException {
		Terminal terminal = JschTerminal.newSsh("ncell", () -> "ekopasuta314", "150.236.158.35");
		terminal.close();
	}

}
