package hr.hashcode.cables.fetch.netconf;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import hr.hashcode.cables.fetch.FetchFilter;
import hr.hashcode.cables.ran.Meta.MoAttr;
import hr.hashcode.cables.ran.Meta.MoClass;

public final class CommandFormatter {

	private CommandFormatter() {}

	private static final String NAMESPACE =
			asString(Namespace.of("urn", "ietf", "params", "xml", "ns", "netconf", "base", "1.0"));

	private static final String MESSAGE_ID = "message-id";
	private static final String RPC = "rpc";
	private static final String RPC_REPLY = "rpc-reply";
	private static final String CLOSE_SESSION = "close-session";
	private static final String GET = "get";
	private static final String XMLNS = "xmlns";
	private static final String FILTER = "filter";
	private static final String HELLO = "hello";
	private static final String CAPABILITIES = "capabilities";
	private static final String CAPABILITY = "capability";

	private static String asString(Capability capability) {
		return asString(capability.namespace());
	}

	private static String asString(Namespace namespace) {
		return String.join(":", namespace.components());
	}

	static XmlNode hello(Capability... caps) {
		XmlNode.Builder builder = XmlNode.newBuilder();

		Set<Capability> set = new LinkedHashSet<>(Arrays.asList(caps));

		builder.startElement(HELLO).addAttribute(XMLNS, NAMESPACE);
		builder.startElement(CAPABILITIES);

		for (Capability capability : set) {
			String identifier = asString(capability);
			builder.simpleElement(CAPABILITY, identifier);
		}
		builder.endElement();
		builder.endElement();

		return builder.build();
	}

	static XmlNode rpcHeader(XmlNode node, String messageId) {
		return XmlNode.newBuilder()
				.startElement(RPC).addAttribute(MESSAGE_ID, messageId).addAttribute(XMLNS, NAMESPACE)
				.wrap(node)
				.endElement()
				.build();
	}

	static XmlNode list(List<MoClass> chain, FetchFilter filter) {
		if (chain.isEmpty())
			return null;
		MoClass moClass = chain.get(0);
		XmlNode.Builder builder = XmlNode.newBuilder()
				.startElement(moClass.name())
				.simpleElement(moClass.key.name);

		for (MoAttr member : moClass.attrs()) {
			if (moClass.key != member && filter.test(moClass.name, member.name))
				builder.simpleElement(member.name);
		}

		builder.endElement();

		XmlNode current = builder.build();

		for (int i = 1; i < chain.size(); i++) {
			MoClass parent = chain.get(i);
			current = XmlNode.newBuilder()
					.startElement(parent.name)
					.simpleElement(parent.key.name)
					.wrap(current)
					.endElement()
					.build();
		}

		XmlNode header = XmlNode.newBuilder()
				.startElement(GET)
				.addAttribute(XMLNS, NAMESPACE)
				.startElement(FILTER)
				.wrap(current)
				.endElement()
				.endElement()
				.build();

		return header;
	}

	static XmlNode list(String className, String attribute) {
		return XmlNode.newBuilder()
				.startElement(GET)
				.addAttribute(XMLNS, NAMESPACE)
				.startElement(FILTER)
				.startElement(className)
				.simpleElement(attribute)
				.endElement()
				.endElement()
				.endElement()
				.build();
	}

	static XmlNode close() {
		return XmlNode.newBuilder()
				.simpleElement(CLOSE_SESSION)
				.build();
	}

	static String extractMessageId(XmlNode node) {
		if (!RPC_REPLY.equals(node.name()))
			return null;
		return node.attribute(MESSAGE_ID);
	}

}
