package hr.hashcode.cables.fetch.netconf;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayDeque;
import java.util.Deque;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This class has method for parsing XML message written as string and creating a tree-like node representation.
 * 
 * @author Srdan Maksimovic
 *
 */
public final class XmlReader {

	/**
	 * Parser factory. It is reused to avoid repeated instantiation. Because there is no guarantee on thread safety
	 * access is synchronized.
	 */
	private static final SAXParserFactory parserFactory = SAXParserFactory.newInstance();

	private final SAXParser parser;

	public XmlReader() {
		synchronized (parserFactory) {
			try {
				this.parser = parserFactory.newSAXParser();
			} catch (ParserConfigurationException | SAXException e) {
				throw new AssertionError(e);
			}
		}
	}

	/**
	 * Parses string format of XML message and create a node representation.
	 * 
	 * @param string
	 *        XML message in string format
	 * @return node representation of a message or {@code null} if message is incorrectly formatted
	 */
	public XmlNode of(String string) {
		InputStream stream = new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8));
		try {
			return of(stream);
		} catch (IOException ignored) {
			throw new AssertionError(ignored);
		}
	}

	public XmlNode of(InputStream stream) throws IOException {
		Handler handler = new Handler();
		try {
			parser.parse(stream, handler);
		} catch (SAXException e) {
			return null;
		}

		return handler.build();
	}

	private static class Handler extends DefaultHandler {

		private final XmlNode.Builder builder = XmlNode.newBuilder();
		private final Deque<String> stack = new ArrayDeque<>();
		private final StringBuilder contents = new StringBuilder();
		private boolean started = false;

		private void clearContents() {
			contents.delete(0, contents.length());
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes)
				throws SAXException {

			if (stack.size() == 0) {
				if (started)
					throw new SAXException(qName);
				started = true;
			}

			clearContents();
			builder.startElement(qName);
			stack.push(qName);

			int n = attributes.getLength();
			for (int i = 0; i < n; i++) {
				String name = attributes.getQName(i);
				String value = attributes.getValue(i);
				builder.addAttribute(name, value);
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if (stack.size() == 0)
				throw new SAXException();

			String expected = stack.pop();

			if (!qName.equals(expected))
				throw new SAXException(qName + "," + expected);

			String completed = contents.toString();
			clearContents();

			if (!completed.trim().isEmpty())
				builder.setContents(completed);

			builder.endElement();
		}

		@Override
		public void endDocument() throws SAXException {
			if (stack.size() > 0)
				throw new SAXException(stack.toString());
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			contents.append(ch, start, length);
		}

		public XmlNode build() {
			return builder.build();
		}

	}

}
