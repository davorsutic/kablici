package hr.hashcode.cables.fetch.netconf;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

import hr.hashcode.cables.fetch.netconf.XmlPrinter.PrintOption;

/**
 * Immutable representation of XML node. It consists of attributes set and list of child nodes or textual contents.
 * Methods for listing attributes and children are available, as well as for acquiring them by their name.
 * 
 * @author Srdan Maksimovic
 */
public final class XmlNode {

	public static final class Attribute {
		private final String name;
		private final String value;

		private Attribute(String name, String value) {
			this.name = name;
			this.value = value;
		}

		public String name() {
			return name;
		}

		public String value() {
			return value;
		}
	}

	/**
	 * Temporary node representation used for building process
	 */
	private static class TempNode {
		private final String name;
		private String contents;
		private final List<Attribute> attributes = new ArrayList<>();
		private final List<XmlNode> children = new ArrayList<>();

		private TempNode(String name) {
			this.name = name;
		}

		private void addAttribute(Attribute attribute) {
			attributes.add(attribute);
		}

		private void addChild(XmlNode node) {
			if (contents != null)
				throw new IllegalStateException();
			children.add(node);
		}

		private void setContents(String contents) {
			if (this.contents != null || !children.isEmpty())
				throw new IllegalStateException(name + "," + contents);
			this.contents = contents;
		}

		private boolean hasContents() {
			return contents != null;
		}

		private XmlNode node() {
			return new XmlNode(name, contents, attributes, children);
		}

	}

	static Builder newBuilder() {
		return new Builder();
	}

	/**
	 * Class used for incremental building of XML node.
	 */
	static class Builder {

		private final Deque<TempNode> stack = new ArrayDeque<>();
		private XmlNode root;

		private Builder() {}

		private TempNode current() {
			if (stack.size() == 0)
				throw new IllegalStateException();
			return stack.peek();
		}

		public Builder addAttribute(String name, String value) {
			current().addAttribute(new Attribute(name, value));
			return this;
		}

		public Builder startElement(String name) {
			if (root != null)
				throw new IllegalStateException();
			if (stack.size() > 0 && current().hasContents())
				throw new IllegalStateException();

			stack.push(new TempNode(name));
			return this;
		}

		public Builder simpleElement(String name) {
			return startElement(name).endElement();
		}

		public Builder simpleElement(String name, String contents) {
			return startElement(name)
					.setContents(contents)
					.endElement();
		}

		public Builder setContents(String contents) {
			current().setContents(contents);
			return this;
		}

		public Builder wrap(XmlNode node) {
			current().addChild(node);
			return this;
		}

		public Builder endElement() {
			XmlNode node = current().node();
			stack.pop();
			if (stack.size() == 0)
				root = node;
			else
				current().addChild(node);
			return this;
		}

		public XmlNode build() {
			if (root == null)
				throw new IllegalStateException();
			return root;
		}

	}

	private final String name;
	private final String contents;
	private final List<Attribute> attributes;
	private final List<XmlNode> children;

	private XmlNode(String name, String value, List<Attribute> attributes) {
		this(name, value, attributes, null);
	}

	private XmlNode(String name, String contents, List<Attribute> attributes, List<XmlNode> children) {
		this.name = name;
		this.contents = contents;
		this.attributes = protect(attributes);
		this.children = protect(children);
	}

	private static <T> List<T> protect(List<T> list) {
		if (list == null || list.size() == 0)
			return Collections.emptyList();
		return Collections.unmodifiableList(new ArrayList<>(list));
	}

	public String name() {
		return name;
	}

	/**
	 * Returns the textual contents.
	 * 
	 * @return textual contents, or {@code null} if it is not defined
	 */
	public String contents() {
		return contents;
	}

	public List<Attribute> attributes() {
		return attributes;
	}

	public List<XmlNode> children() {
		return children;
	}

	/**
	 * Returns a child with given name,
	 * 
	 * @param name
	 *        child name
	 * @return a child of the node that has given name, or {@code null} if such node does not exist uniquelly
	 */
	public XmlNode child(String name) {
		XmlNode result = null;
		for (XmlNode child : children)
			if (child.name().equals(name)) {
				if (result != null)
					return null;
				result = child;
			}
		return result;
	}

	/**
	 * Lists all children with given name
	 * 
	 * @param name
	 *        child name
	 * @return list of children with given name
	 */
	public List<XmlNode> children(String name) {
		List<XmlNode> result = new ArrayList<>();
		for (XmlNode child : children)
			if (child.name().equals(name))
				result.add(child);
		return result;
	}

	/**
	 * Returns the value of the attribute.
	 * 
	 * @param name
	 *        attribute name
	 * @return attribute value, or {@code null} if attribute is not defined
	 */
	public String attribute(String name) {
		for (Attribute attr : attributes) {
			if (attr.name().equals(name))
				return attr.value();
		}
		return null;
	}

	@Override
	public String toString() {
		return XmlPrinter.print(this, PrintOption.NO_HEADER);
	}

}
