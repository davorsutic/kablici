package hr.hashcode.cables.fetch.netconf;

import java.io.Closeable;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Abstraction of the other party in low-level communication.
 * 
 * @author Srdan Maksimovic
 *
 */
public interface Terminal extends Closeable {

	/**
	 * Returns the stream for receiving information (from the user's point of view).
	 * 
	 * @return the input stream
	 */
	InputStream input();

	/**
	 * Returns the stream for delivering information (from the user's point of view).
	 * 
	 * @return the output stream
	 */
	OutputStream output();

}
