package hr.hashcode.cables.fetch.netconf;

import java.io.IOException;

/**
 * Executor of netconf protocol. It is in charge for enumerating netconf requests and unpacking received replies. It is
 * strongly suggested that different threads invoke methods for sending and receiving. Otherwise, there is a high risk
 * of deadlock occurrence.
 * 
 * @author Srdan Maksimovic
 *
 */
final class NetconfProtocol {

	private static final int STARTING_ID = 1;

	/** Hello message with standard capabilities. */
	private static final XmlNode hello = CommandFormatter.hello(
			Capability.BASE_1_0,
			Capability.EBASE_0_1_0,
			Capability.EBASE_1_1_0,
			Capability.EBASE_1_2_0);

	/** Message for closing session. */
	private static final XmlNode close = CommandFormatter.close();

	private final Messenger messenger;
	/** Converter from string to {@link XmlNode}s */
	private final XmlReader reader;

	/** The expected id of the next message to be received. */
	private int receivedId;

	/** The id of the next message to be sent. */
	private int sentId;

	/**
	 * Creates a new instance.
	 * 
	 * @param terminal
	 *        terminal used for exchanging messages
	 */
	NetconfProtocol(Terminal terminal) {
		this.messenger = new Messenger(terminal);
		this.reader = new XmlReader();
		this.receivedId = STARTING_ID;
		this.sentId = STARTING_ID;
	}

	/**
	 * Sends the opening message.
	 * 
	 * @throws IOException
	 *         if an IO error occurred
	 */
	void sendHello() throws IOException {
		sendNode(hello);
	}

	/**
	 * Receives the message with capabilities of the other side.
	 * 
	 * @return the hello message
	 * @throws IOException
	 *         if an IO error occurred
	 * @throws ProtocolException
	 *         if a protocol violation occurred
	 */
	XmlNode receiveHello() throws IOException, ProtocolException {
		return receiveNode();
	}

	/**
	 * Sends an XML message.
	 * 
	 * @param node
	 *        the message
	 * @throws IOException
	 *         if an IO error occurred
	 */
	void send(XmlNode node) throws IOException {
		int id = sentId;
		XmlNode rpcWrap = CommandFormatter.rpcHeader(node, String.valueOf(id));
		sendNode(rpcWrap);
		sentId++;
	}

	/**
	 * Waits for the next XML message to arrive.
	 * 
	 * @return the XML message
	 * @throws IOException
	 *         if an IO error occurred
	 * @throws ProtocolException
	 *         the a protocol violation occurred
	 */
	XmlNode receive() throws IOException, ProtocolException {
		XmlNode node = receiveNode();
		String messageId = CommandFormatter.extractMessageId(node);
		int id = Integer.parseInt(messageId);
		if (id == receivedId) {
			receivedId++;
			return node;
		} else
			throw new ProtocolException(node.toString());
	}

	private void sendNode(XmlNode node) throws IOException {
		String message = XmlPrinter.print(node);
		messenger.send(message);
	}

	private XmlNode receiveNode() throws IOException, ProtocolException {
		String message = messenger.receive();
		XmlNode node = reader.of(message);
		if (node == null)
			throw new ProtocolException(message);
		else
			return node;
	}

	/**
	 * Sends a message that will request the session termination. Upon the reception of this message other side is
	 * expected to close the connection.
	 * 
	 * @throws IOException
	 *         if an IO error occurs
	 */
	void sendClose() throws IOException {
		send(close);
	}

	/**
	 * Forceful termination of the connection.
	 * 
	 * @throws IOException
	 *         if an IO error occurs
	 */
	void close() throws IOException {
		messenger.close();
	}

	/**
	 * Marks a netconf protocol violation. It might be because the received {@code String} message can't be represented
	 * as an XML node, or because the message id of the reply does not correspond to the request.
	 */
	static class ProtocolException extends Exception {

		private static final long serialVersionUID = 1L;

		private final String message;

		ProtocolException(String message) {
			this.message = message;
		}

		String message() {
			return message;
		}
	}

	/**
	 * Performs conversion between XML and {@code String} messages. Delimiter is used to mark (determine) the end of
	 * sent (received) messages.
	 */
	private static class Messenger {
		/** Delimiter of messages in netconf protocol. */
		private static final String MESSAGE_END = "]]>]]>";

		private final StringTerminal stringTerminal;

		Messenger(Terminal terminal) {
			this.stringTerminal = new StringTerminal(terminal);
		}

		void send(String message) throws IOException {
			stringTerminal.send(message, MESSAGE_END, "\n");
		}

		String receive() throws IOException, ProtocolException {
			StringBuilder builder = new StringBuilder();
			while (true) {
				String line = stringTerminal.readLine();

				if (line == null)
					throw new ProtocolException(builder.toString());

				if (line.equals(MESSAGE_END))
					return builder.toString();

				builder.append(line).append("\n");
			}
		}

		void close() throws IOException {
			stringTerminal.close();
		}
	}

}
