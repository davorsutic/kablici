package hr.hashcode.cables.fetch;

import hr.hashcode.cables.ran.NodeState;

public class FetchResult {

	public static FetchResult of(String ipAddress, NodeState state) {
		return new FetchResult(ipAddress, state);
	}

	public static FetchResult error(String ipAddress, ErrorCode code) {
		return new FetchResult(ipAddress, code);
	}

	public enum ErrorCode {
		/** Meta could not be parsed. */
		META,

		/** Node version could not be determined. */
		VERSION,

		/** Unexpected error occurred. */
		UNEXPECTED_ERROR,

		/** Could not connect to the node. */
		IO_ERROR,

		/** Protocol violation. */
		UNEXPECTED_SERVER_REPLY,

		/** Fetching was cancelled. */
		INTERRUPTED,

		/** Node type is not supported. */
		UNSUPPORTED_NODE_TYPE

	}

	private final String ipAddress;
	private final ErrorCode error;
	private final NodeState state;

	private FetchResult(String ipAddress, NodeState state) {
		this.ipAddress = ipAddress;
		this.state = state;
		this.error = null;
	}

	private FetchResult(String ipAddress, ErrorCode error) {
		this.ipAddress = ipAddress;
		this.state = null;
		this.error = error;
	}

	public String ipAddress() {
		return ipAddress;
	}

	public boolean success() {
		return error == null;
	}

	private void check(boolean expected) {
		if (success() != expected)
			throw new IllegalStateException();
	}

	public ErrorCode error() {
		check(false);
		return error;
	}

	public NodeState state() {
		check(true);
		return state;
	}

}
