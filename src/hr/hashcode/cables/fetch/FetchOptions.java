package hr.hashcode.cables.fetch;

public class FetchOptions {

	public final boolean counters;
	public final boolean descriptions;

	FetchOptions(boolean counters, boolean descriptions) {
		this.counters = counters;
		this.descriptions = descriptions;
	}

}
