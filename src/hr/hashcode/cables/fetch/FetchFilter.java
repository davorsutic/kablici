package hr.hashcode.cables.fetch;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import hr.hashcode.cables.ran.Version;
import hr.hashcode.cables.util.Xml;

public class FetchFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private final Tester tester;

	public FetchFilter(Map<String, List<String>> attributes) {
		this.tester = new FullControl(attributes);
	}

	public FetchFilter(Collection<String> classes) {
		this.tester = new AllAttributes(classes);
	}

	public FetchFilter() {
		this.tester = new All();
	}

	public boolean test(String moClass) {
		return tester.test(moClass);
	}

	public boolean test(String moClass, String attribute) {
		return tester.test(moClass, attribute);
	}

	private interface Tester extends Serializable {
		boolean test(String clazz);

		boolean test(String clazz, String attribute);
	}

	private static class All implements Tester {
		private static final long serialVersionUID = 1L;

		@Override
		public boolean test(String clazz) {
			return true;
		}

		@Override
		public boolean test(String clazz, String attribute) {
			return true;
		}
	}

	private static class AllAttributes implements Tester {
		private static final long serialVersionUID = 1L;

		private final Set<String> classes;

		AllAttributes(Collection<String> classes) {
			this.classes = new HashSet<>(classes);
		}

		@Override
		public boolean test(String clazz) {
			return classes.contains(clazz);
		}

		@Override
		public boolean test(String clazz, String attribute) {
			return test(clazz);
		}

	}

	private static class FullControl implements Tester {
		private static final long serialVersionUID = 1L;

		private final Map<String, Set<String>> attributes;

		FullControl(Map<String, ? extends Collection<String>> attributes) {
			Map<String, Set<String>> map = new HashMap<>();
			attributes.forEach((x, y) -> map.put(x, new HashSet<>(y)));
			this.attributes = map;
		}

		@Override
		public boolean test(String clazz) {
			return attributes.containsKey(clazz);
		}

		@Override
		public boolean test(String clazz, String attribute) {
			return attributes.getOrDefault(clazz, Collections.emptySet())
					.contains(attribute);
		}
	}

	public static Map<Version.Type, FetchFilter> of(InputStream input) {
		Xml xml = Xml.read(input);

		Map<Version.Type, FetchFilter> result = new EnumMap<>(Version.Type.class);
		Xml filters = xml.child("filters");
		for (Xml filterXml : filters.children("filter")) {
			Version.Type type;
			try {
				type = Version.Type.valueOf(filterXml.attribute("node"));
			} catch (IllegalArgumentException | NullPointerException e) {
				continue;
			}
			Map<String, List<String>> map = new TreeMap<>();
			for (Xml clazz : filterXml.children("class")) {
				String className = clazz.attribute("name");
				List<String> attrs = new ArrayList<>();
				for (Xml attr : clazz.children("attribute")) {
					String attributeName = attr.attribute("name");
					attrs.add(attributeName);
				}
				map.put(className, attrs);
			}

			FetchFilter filter = new FetchFilter(map);
			result.put(type, filter);
		}
		return result;
	}

}
