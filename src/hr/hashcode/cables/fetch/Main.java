package hr.hashcode.cables.fetch;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import hr.hashcode.cables.mom.MomStore;
import hr.hashcode.cables.ran.NodeInfo;
import hr.hashcode.cables.ran.NodeState;

public class Main {

	public static void main(String[] args) {

		Map<String, String> values = new HashMap<>();
		List<String> ipAddresses = new ArrayList<>();

		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (arg.startsWith("-")) {
				String key;
				String value;
				if (arg.startsWith("--")) {
					if (arg.startsWith("---")) {
						System.err.println("Illegal arg: " + arg);
						return;
					}
					int index = arg.indexOf('=');
					key = arg.substring(2, index);
					value = arg.substring(index + 1);
				} else {
					key = arg;
					i++;
					if (i == args.length) {
						System.err.println("No value for arg: " + key);
						return;
					}
					value = args[i];
				}
				if (values.containsKey(key)) {
					System.err.println("Arg " + key + " repeated");
					return;
				}
				values.put(key, value);
			} else
				ipAddresses.add(arg);
		}

		if (ipAddresses.isEmpty()) {
			System.err.println("IP expected");
			return;
		}

		boolean counters = booleanProp(values, "counters", false);
		boolean descriptions = booleanProp(values, "descriptions", false);
		int threads = intProp(values, "threads", 20);
		String momFile = stringProp(values, "mom", null);

		MomStore momStore = new MomStore(Arrays.asList(Paths.get(momFile)));

		FetchServer server = new FetchServer(counters, descriptions, threads, momStore, "bts", "bts", ipAddresses,
				Collections.emptyMap());
		server.start();

		for (int i = 0; i < ipAddresses.size(); i++) {
			FetchResult result;
			try {
				result = server.next();
			} catch (InterruptedException e) {
				server.cancel();
				System.err.println("Cancelled");
				return;
			}
			if (result.success()) {
				writeToFile(result.ipAddress(), result.state());
			}
		}

	}

	private static void writeToFile(String ipAddress, NodeState state) {
		NodeInfo info = NodeInfo.info(state);
		String id =
				info.userLabel;
		if (id == null || id.trim().isEmpty())
			id = info.logicalName;
		if (id == null || id.trim().isEmpty())
			id = info.site;
		if (id == null)
			id = "";

		String filename = id + "_" + info.productName + "_" + info.mimName + "_" + info.mimVersion.replace('.', '_')
				+ "_" + ipAddress;

		System.err.println(filename);

		// long start = System.nanoTime();
		try (
				FileOutputStream file = new FileOutputStream(filename + ".dat.gz");
				GZIPOutputStream gzip = new GZIPOutputStream(file);
				OutputStream buffer = new BufferedOutputStream(gzip);
				ObjectOutputStream output = new ObjectOutputStream(buffer)) {
			output.writeObject(state);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static boolean booleanProp(Map<String, String> map, String name, boolean def) {
		String value = map.get(name);
		if ("true".equalsIgnoreCase(value))
			return true;
		else if ("false".equalsIgnoreCase(value))
			return false;
		else
			return def;
	}

	private static int intProp(Map<String, String> map, String name, int def) {
		String value = map.get(name);
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException e) {
			return def;
		}
	}

	private static String stringProp(Map<String, String> map, String name, String def) {
		String value = map.get(name);
		if (value == null)
			return def;
		return value;
	}

}
