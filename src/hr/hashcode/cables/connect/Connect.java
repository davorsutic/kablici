package hr.hashcode.cables.connect;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.List;
import java.util.zip.GZIPInputStream;

import hr.hashcode.cables.ran.NodeInfo;
import hr.hashcode.cables.ran.NodeState;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Connect extends Application {

	private static NodeState objects(InputStream input) throws ClassNotFoundException, IOException {
		try (ObjectInputStream objInput = new ObjectInputStream(input)) {
			return (NodeState) objInput.readObject();
		}
	}

	private static NodeState objects(File file) {
		try {
			return objects(new FileInputStream(file));
		} catch (ClassNotFoundException | IOException e) {}
		try {
			return objects(new GZIPInputStream(new FileInputStream(file)));
		} catch (ClassNotFoundException | IOException e) {
			return null;
		}
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		VBox box = new VBox();
		box.setMinHeight(300);
		box.setMinWidth(300);
		Scene scene = new Scene(box);
		scene.setOnDragOver(e -> {
			e.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			Dragboard dragboard = e.getDragboard();
			if (!dragboard.hasFiles())
				e.consume();
		});

		scene.setOnDragDropped(e -> {
			Dragboard dragboard = e.getDragboard();
			List<File> files = dragboard.getFiles();
			for (File file : files) {
				Runnable runnable = () -> {
					NodeState state = objects(file);
					if (state != null) {

						NodeInfo info = NodeInfo.info(state);
						// System.out.println(info.fdn);
						// System.out.println(info.logicalName);
						// System.out.println(info.mimName);
						// System.out.println(info.mimVersion);
						// System.out.println(info.productName);
						// System.out.println(info.site);
						// System.out.println(info.userLabel);

						String id = info.userLabel;
						if (id == null || id.isEmpty())
							id = info.logicalName;
						if (id == null || id.isEmpty())
							id = info.site;

						String title = id + " (" + info.productName + ", " + info.mimName + "_"
								+ info.mimVersion + ")";

						Platform.runLater(() -> {
							Draw draw = new Draw(state);
							draw.render();
							Scene newScene = new Scene(new ScrollPane(draw));
							Stage stage = new Stage();
							stage.setTitle(title);
							stage.setScene(newScene);
							stage.setMaximized(true);
							stage.show();
							draw.drawLines();
						});
					}
				};
				new Thread(runnable).start();
			}
			primaryStage.setIconified(true);
		});
		primaryStage.setTitle("Connect");
		primaryStage.setAlwaysOnTop(true);
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

}
