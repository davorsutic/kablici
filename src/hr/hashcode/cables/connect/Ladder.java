package hr.hashcode.cables.connect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import hr.hashcode.cables.connect.Description.Hashed;
import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.NodeState.ManagedObject;

public class Ladder extends Hashed {

	private final String[] classes;

	Ladder(String... classes) {
		super(classes);
		this.classes = classes.clone();
	}

	private boolean fits(List<MoClass> moClasses) {
		int current = classes.length - 1;
		if (current == -1)
			return true;
		for (MoClass moClass : moClasses) {
			if (moClass.name.equals(classes[current])) {
				current--;
				if (current < 0)
					return true;
			}
		}
		return false;
	}

	private List<MoClass> classes(ManagedObject object) {
		List<MoClass> result = new ArrayList<>();
		for (ManagedObject temp = object; temp != null; temp = temp.parent) {
			result.add(temp.moClass);
		}
		return result;
	}

	boolean fits(ManagedObject object) {
		return fits(classes(object));
	}

	boolean fits(ManagedObject object, MoClass child) {
		List<MoClass> classes = new ArrayList<>();
		classes.add(child);
		classes.addAll(classes(object));
		return fits(classes);
	}

	public String toString() {
		return Arrays.stream(classes).collect(Collectors.joining(","));
	}

	public static class EndLadder extends Ladder {

		EndLadder(String... classes) {
			super(classes);
		}

		private boolean fitsEnd(MoClass moClass) {
			int len = super.classes.length;
			return len > 0 && super.classes[len - 1].equals(moClass.name);
		}

		@Override
		boolean fits(ManagedObject object) {
			return fitsEnd(object.moClass) && super.fits(object);
		}

		@Override
		boolean fits(ManagedObject object, MoClass child) {
			return fitsEnd(child) && super.fits(object, child);
		}

	}

}
