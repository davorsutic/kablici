package hr.hashcode.cables.connect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import hr.hashcode.cables.connect.Ladder.EndLadder;
import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.NodeState.ManagedAttribute;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.ran.NodeState.ManagedStruct;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Description {

	interface Dormant {

		BooleanProperty isActive();
	}

	static class Hashed implements Dormant {

		private final Object[] objs;
		private BooleanProperty active = new SimpleBooleanProperty(true);

		Hashed(Object... objs) {
			this.objs = objs.clone();
		}

		@Override
		public int hashCode() {
			return 31 * getClass().hashCode() + Arrays.hashCode(objs);
		}

		public boolean equals(Object obj) {
			if (obj == this)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Hashed other = (Hashed) obj;
			return Arrays.equals(objs, other.objs);
		}

		public BooleanProperty isActive() {
			return active;
		}

	}

	enum Direction {
		HORIZONTAL, VERTICAL;
	}

	enum GroupOption {
		COMPACT, SEPARATE;
	}

	static class Options<E extends Enum<?>> {
		private static class Option<E extends Enum<?>> {
			private final Ladder ladder;
			private final E value;
			private Option(Ladder ladder, E value) {
				this.ladder = ladder;
				this.value = value;
			}
		}

		private final List<Option<E>> options = new ArrayList<>();
		private final E def;

		Options(E def) {
			this.def = def;
		}

		void register(E value, String... path) {
			options.add(new Option<>(new EndLadder(path), value));
		}

		E get(ManagedObject object) {
			for (Option<E> option : options) {
				if (option.ladder.fits(object))
					return option.value;
			}
			return def;
		}

		E get(ManagedObject parent, MoClass child) {
			for (Option<E> option : options) {
				if (option.ladder.fits(parent, child))
					return option.value;
			}
			return def;
		}
	}

	static class Orders {

		private static final int def = Integer.MAX_VALUE;

		private static class Order {
			private final Ladder ladder;
			private final Map<String, Integer> priority = new HashMap<>();

			Order(Ladder ladder, String... order) {
				this.ladder = ladder;
				for (int i = 0; i < order.length; i++)
					priority.put(order[i], i);
			}

			int priority(MoClass moClass) {
				return priority.getOrDefault(moClass.name, def);
			}
		}

		List<Order> orders = new ArrayList<>();

		void register(String className, String... order) {
			orders.add(new Order(new Ladder(className), order));
		}

		int priority(ManagedObject object, MoClass moClass) {
			for (Order order : orders) {
				if (order.ladder.fits(object))
					return order.priority(moClass);
			}
			return def;
		}
	}

	static class Attribute extends Hashed {
		final String moClass;
		final String name;

		Attribute(String moClass, String name) {
			super(moClass, name);
			this.moClass = moClass;
			this.name = name;
		}

		@Override
		public String toString() {
			return moClass + "," + name;
		}
	}

	static class DoubleLink extends Hashed {
		final String moClass;
		final String first;
		final String second;

		DoubleLink(String moClass, String first, String second) {
			super(moClass, first, second);
			this.moClass = moClass;
			this.first = first;
			this.second = second;
		}

		@Override
		public String toString() {
			return moClass + "," + first + "," + second;
		}

	}

	void add(String className, String... path) {
		String[] merged = Arrays.copyOf(path, path.length + 1);
		merged[path.length] = className;
		Ladder ladder = new EndLadder(merged);
		listenTo(ladder);
		classes.add(ladder);
	}

	void registerRef(String moClass, String param) {
		registerRef(moClass, param, false);
	}

	void registerRef(String moClass, String param, boolean active) {
		Attribute attr = new Attribute(moClass, param);
		attr.isActive().set(active);
		listenTo(attr);
		links.add(attr);
	}

	void registerRef(String moClass, String first, String second) {
		registerRef(moClass, first, second, false);
	}

	void registerRef(String moClass, String first, String second, boolean active) {
		DoubleLink link = new DoubleLink(moClass, first, second);
		link.isActive().set(active);
		listenTo(link);
		doubleLinks.add(link);
	}

	void addParam(String className, String... params) {
		for (String param : params) {
			Attribute attribute = new Attribute(className, param);
			listenTo(attribute);
			attributes.add(attribute);
		}
	}

	void addParam(String className, String param, boolean active) {
		Attribute attribute = new Attribute(className, param);
		attribute.isActive().set(active);
		listenTo(attribute);
		attributes.add(attribute);
	}

	final Options<Direction> nameDirection = new Options<>(Direction.HORIZONTAL);
	final Options<Direction> childrenDirection = new Options<>(Direction.VERTICAL);
	final Options<Direction> groupDirection = new Options<>(Direction.HORIZONTAL);
	final Options<GroupOption> grouping = new Options<>(GroupOption.SEPARATE);

	final Orders orders = new Orders();
	final ObservableList<Attribute> attributes = triggerList();
	final ObservableList<Attribute> links = triggerList();
	final ObservableList<DoubleLink> doubleLinks = triggerList();
	final ObservableList<Ladder> classes = triggerList();

	private final Map<String, Function<ManagedObject, ?>> split = new HashMap<>();

	private static <T> T get(ManagedObject object, String name, Class<T> clazz) {
		for (ManagedAttribute attr : object.attrs()) {
			if (attr.attribute.name.equals(name))
				return clazz.cast(attr.value);
		}
		return null;
	}

	void registerSplitter(String className, String param) {
		Function<ManagedObject, ?> function = object -> {
			int index = param.indexOf('.');
			if (index != -1) {
				Object maybeStruct = get(object, param.substring(0, index), Object.class);
				if (!(maybeStruct instanceof ManagedStruct))
					return null;

				ManagedStruct struct = (ManagedStruct) maybeStruct;
				String last = param.substring(index + 1);

				for (ManagedAttribute attr : struct.attrs())
					if (last.equalsIgnoreCase(attr.attribute.name))
						return attr.value;
				return null;
			} else
				return get(object, param, Object.class);
		};
		split.put(className, function);
	}

	Function<ManagedObject, ?> getSplitter(String className) {
		Function<ManagedObject, ?> result = split.get(className);
		if (result == null)
			return x -> null;
		else
			return result;
	}

	private void listenTo(Dormant dormant) {
		dormant.isActive().addListener((a, b, c) -> trigger());
	}

	private <T> ObservableList<T> triggerList() {
		ObservableList<T> list = FXCollections.observableArrayList();
		SimpleListProperty<T> prop = new SimpleListProperty<>(list);
		prop.addListener((a, b, c) -> trigger());
		return list;
	}

	private final List<Runnable> actions = new ArrayList<>();

	private void trigger() {
		actions.forEach(Runnable::run);
	}

	void register(Runnable runnable) {
		actions.add(runnable);
	}

}
