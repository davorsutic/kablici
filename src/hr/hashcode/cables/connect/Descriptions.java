package hr.hashcode.cables.connect;

import hr.hashcode.cables.connect.Description.Direction;
import hr.hashcode.cables.connect.Description.GroupOption;

public class Descriptions {

	private static final String managedElement = "ManagedElement";
	private static final String eNodeBFunction = "ENodeBFunction";
	private static final String nodeBFunction = "NodeBFunction";
	private static final String equipment = "Equipment";
	private static final String cabinet = "Cabinet";
	private static final String climate = "Climate";
	private static final String eUtranCellFDD = "EUtranCellFDD";
	private static final String sectorCarrier = "SectorCarrier";
	private static final String sectorEquipmentFunction = "SectorEquipmentFunction";
	private static final String antennaUnitGroup = "AntennaUnitGroup";
	private static final String rfBranch = "RfBranch";
	private static final String auPort = "AuPort";
	private static final String rbsSubrack = "RbsSubrack";
	private static final String rbsSlot = "RbsSlot";
	private static final String auxPlugInUnit = "AuxPlugInUnit";
	private static final String plugInUnit = "PlugInUnit";

	private static final String deviceGroup = "DeviceGroup";
	private static final String rfPort = "RfPort";
	private static final String riPort = "RiPort";
	private static final String riLink = "RiLink";

	private static final String subrack = "Subrack";
	private static final String slot = "Slot";

	private static final String ecBus = "EcBus";
	private static final String ecPort = "EcPort";
	private static final String equipmentSupportFunction = "EquipmentSupportFunction";
	private static final String externalNode = "ExternalNode";
	private static final String hwUnit = "HwUnit";
	private static final String antennaBranch = "AntennaBranch";
	private static final String antFeederCable = "AntFeederCable";
	private static final String digitalCable = "DigitalCable";
	private static final String aiDevice = "AiDevice";
	private static final String dpclDevice = "DpclDevice";
	private static final String tpaDevice = "TpaDevice";
	private static final String trDevice = "TrDevice";
	private static final String carrier = "Carrier";
	private static final String dbccDevice = "DbccDevice";
	private static final String powerDistribution = "PowerDistribution";
	private static final String powerSupply = "PowerSupply";
	private static final String sectorAntenna = "SectorAntenna";
	private static final String sector = "Sector";
	private static final String rbsLocalCell = "RbsLocalCell";
	private static final String rruDeviceGroup = "RruDeviceGroup";
	private static final String externalAntenna = "ExternalAntenna";
	private static final String retDevice = "RetDevice";

	private static final String fieldReplaceableUnit = "FieldReplaceableUnit";
	private static final String sfpModule = "SfpModule";
	private static final String nodeSupport = "NodeSupport";
	private static final String antennaUnit = "AntennaUnit";

	private static final String interPiuLink = "InterPiuLink";

	static Description rbs() {

		Description description = new Description();
		description.add(equipment);
		description.add(equipmentSupportFunction);
		description.add(climate);
		description.add(powerDistribution);
		description.add(powerSupply);
		description.add(antennaBranch);
		description.add(ecBus);
		description.add(ecPort);
		description.add(hwUnit);
		description.add(sectorAntenna);
		description.add(sector);
		description.add(carrier);
		description.add(aiDevice);
		description.add(dpclDevice);
		description.add(tpaDevice);
		description.add(trDevice);
		description.add(dbccDevice);
		description.add(slot);
		description.add(rbsLocalCell);
		description.add(rruDeviceGroup);
		description.add(externalAntenna);
		description.add(retDevice);
		description.add(rbsSlot);

		configureExternalNode(description);
		configureCabinet(description);
		configureAuxPlugInUnit(description);
		configurePlugInUnit(description);

		description.orders.register(sectorAntenna, antennaBranch, auxPlugInUnit);
		description.orders.register(equipment, externalAntenna, sectorAntenna, subrack, rbsSubrack, cabinet,
				externalNode);
		description.orders.register(nodeBFunction, rbsLocalCell, sector);
		description.orders.register(managedElement, nodeBFunction, equipment, equipmentSupportFunction);

		description.registerRef(auxPlugInUnit, "plugInUnitRef1");
		description.registerRef(auxPlugInUnit, "plugInUnitRef2");
		description.registerRef(auxPlugInUnit, "positionRef");
		description.registerRef(carrier, "aiDeviceRef");
		description.registerRef(carrier, "dbccDeviceRef");
		description.registerRef(carrier, "dpclDeviceRef");
		description.registerRef(carrier, "tpaDeviceRef");
		description.registerRef(carrier, "trDeviceRef");
		description.registerRef(climate, "controlDomainRef");
		description.registerRef(ecBus, "ecBusConnectorRef");
		description.registerRef(ecPort, "ecBusRef");
		description.registerRef(externalAntenna, "sectorRef");
		description.registerRef(externalAntenna, "sectorAntennaRef");
		description.registerRef(hwUnit, "positionRef");
		description.registerRef(plugInUnit, "positionRef");
		description.registerRef(powerDistribution, "controlDomainRef");
		description.registerRef(powerSupply, "controlDomainRef");
		description.registerRef(rbsLocalCell, "carriersRef");
		description.registerRef(sector, "sectorAntennasRef");
		description.registerRef(sector, "retDevicesRef");

		description.registerRef(antFeederCable, "antennaBranchRef", "connectedToObjectARef");
		description.registerRef(digitalCable, "connectedToObjectARef", "connectedToObjectBRef");
		description.registerRef(interPiuLink, "primaryPiuSlot", "secondaryPiuSlot");

		description.addParam(antennaBranch, "fqBandLowEdge");
		description.addParam(antennaBranch, "fqBandHighEdge");
		description.addParam(carrier, "numberOfRxBranches");
		description.addParam(carrier, "numberOfTxBranches");
		description.addParam(ecPort, "hubPosition");
		description.addParam(equipmentSupportFunction, "supportSystemControl");
		description.addParam(sector, "band");
		description.addParam(sector, "numberOfTxBranches");
		description.addParam(sector, "numberOfSectorAntennas");
		description.addParam(slot, "slotState");
		description.addParam(subrack, "subrackType");

		configureProduct(description, slot);
		configureProduct(description, hwUnit);

		description.grouping.register(GroupOption.COMPACT, rbsSlot);
		description.grouping.register(GroupOption.COMPACT, aiDevice);
		description.grouping.register(GroupOption.COMPACT, dpclDevice);
		description.grouping.register(GroupOption.COMPACT, tpaDevice);
		description.grouping.register(GroupOption.COMPACT, trDevice);
		description.grouping.register(GroupOption.COMPACT, dbccDevice);

		return description;
	}
	static Description erbs() {
		Description description = new Description();

		description.add(climate);
		description.add(ecBus);
		description.add(ecPort);
		description.add(equipment);
		description.add(eUtranCellFDD);
		description.add(rbsSlot);
		description.add(rbsSubrack);
		description.add(rfBranch);
		description.add(rfPort);
		description.add(riPort);
		description.add(slot);
		description.add(sectorCarrier);
		description.add(sectorEquipmentFunction);
		description.add(subrack);

		configureExternalNode(description);
		configureCabinet(description);
		configureRiLink(description);
		configurePlugInUnit(description);
		configureAuxPlugInUnit(description);

		description.orders.register(auxPlugInUnit, deviceGroup, riPort);
		description.orders.register(equipment, antennaUnitGroup, rbsSubrack, subrack, cabinet, ecBus,
				equipmentSupportFunction);
		description.orders.register(eNodeBFunction, eUtranCellFDD, sectorCarrier);
		description.orders.register(managedElement, eNodeBFunction, sectorEquipmentFunction, equipment);

		description.registerRef(cabinet, "positionRef");
		description.registerRef(climate, "controlDomainRef");
		description.registerRef(eUtranCellFDD, "hostingDigitalUnit");
		description.registerRef(eUtranCellFDD, "sectorCarrierRef");
		description.registerRef(rfBranch, "auPortRef");
		description.registerRef(rfBranch, "rfPortRef");
		description.registerRef(sectorEquipmentFunction, "rfBranchRef");
		description.registerRef(sectorCarrier, "rfBranchRxRef");
		description.registerRef(sectorCarrier, "rfBranchTxRef");
		description.registerRef(sectorCarrier, "sectorFunctionRef");
		description.registerRef(ecBus, "ecBusConnectorRef");
		description.registerRef(ecBus, "equipmentSupportFunctionRef");
		description.registerRef(externalNode, "equipmentSupportFunctionRef");
		description.registerRef(ecPort, "ecBusRef");

		description.addParam(ecPort, "hubPosition");
		description.addParam(ecPort, "cascadingOrder");

		description.groupDirection.register(Direction.VERTICAL, riPort);

		description.grouping.register(GroupOption.COMPACT, rfPort);
		description.grouping.register(GroupOption.COMPACT, auxPlugInUnit);
		description.grouping.register(GroupOption.COMPACT, rbsSlot);
		description.grouping.register(GroupOption.COMPACT, rfBranch);
		description.grouping.register(GroupOption.COMPACT, antennaUnitGroup);

		return description;
	}
	static Description msrbs() {

		Description description = new Description();

		description.add(antennaUnitGroup);
		description.add(auPort);
		description.add(cabinet);
		description.add(climate);
		description.add(ecBus);
		description.add(ecPort);
		description.add(equipment);
		description.add(equipmentSupportFunction);
		description.add(eUtranCellFDD);
		description.add(fieldReplaceableUnit);
		description.add(nodeSupport);
		description.add(rfBranch);
		description.add(rfPort);
		description.add(riPort);
		description.add(sfpModule);
		description.add(powerDistribution);
		description.add(powerSupply);
		description.add(sectorCarrier);
		description.add(sectorEquipmentFunction);

		configureExternalNode(description);
		configureCabinet(description);
		configureRiLink(description);
		configurePlugInUnit(description);
		configureAuxPlugInUnit(description);

		description.orders.register(antennaUnitGroup, rfBranch, antennaUnit);
		description.orders.register(equipment, antennaUnitGroup, fieldReplaceableUnit, cabinet, ecBus, externalNode);
		description.orders.register(eNodeBFunction, eUtranCellFDD, sectorCarrier);
		description.orders.register(managedElement, eNodeBFunction, nodeSupport, equipment);

		description.registerRef(climate, "controlDomainRef");
		description.registerRef(ecBus, "ecBusConnectorRef");
		description.registerRef(ecPort, "ecBusRef");
		description.registerRef(eUtranCellFDD, "sectorCarrierRef");
		description.registerRef(eUtranCellFDD, "hostingDigitalUnit");
		description.registerRef(fieldReplaceableUnit, "positionRef");
		description.registerRef(powerDistribution, "controlDomainRef");
		description.registerRef(powerSupply, "controlDomainRef");
		description.registerRef(rfBranch, "auPortRef");
		description.registerRef(rfBranch, "rfPortRef");
		description.registerRef(riPort, "sfpModuleRef");
		description.registerRef(sectorCarrier, "sectorFunctionRef");
		description.registerRef(sectorEquipmentFunction, "rfBranchRef");

		configureProduct(description, sfpModule);
		configureProduct(description, fieldReplaceableUnit);

		description.addParam(equipmentSupportFunction, "supportSystemControl");
		description.addParam(eUtranCellFDD, "earfcndl");
		description.addParam(eUtranCellFDD, "freqBand");
		description.addParam(fieldReplaceableUnit, "isSharedWithExternalMe");
		description.addParam(ecPort, "hubPosition");
		description.addParam(ecPort, "cascadingOrder");
		description.addParam(rfPort, "dlFrequencyRanges", false);
		description.addParam(rfPort, "ulFrequencyRanges", false);

		description.registerSplitter(fieldReplaceableUnit, "productData.productName");

		description.groupDirection.register(Direction.VERTICAL, rfPort);
		description.groupDirection.register(Direction.VERTICAL, sfpModule);

		description.grouping.register(GroupOption.COMPACT, antennaUnitGroup);
		description.grouping.register(GroupOption.COMPACT, fieldReplaceableUnit);
		description.grouping.register(GroupOption.COMPACT, riPort);
		description.grouping.register(GroupOption.COMPACT, eUtranCellFDD);
		description.grouping.register(GroupOption.COMPACT, sectorCarrier);
		description.grouping.register(GroupOption.COMPACT, sectorEquipmentFunction);

		return description;
	}

	private static void configureRiLink(Description description) {
		description.registerRef(riLink, "riPortRef1", "riPortRef2", true);
	}

	private static void configureExternalNode(Description description) {
		description.add(externalNode);

		description.addParam(externalNode, "fullDistinguishedName");
		description.addParam(externalNode, "informationOnly");
		description.addParam(externalNode, "logicalName");
		description.addParam(externalNode, "radioAccessTechnology");
		description.addParam(externalNode, "supportSystemControl");
	}

	private static void configureCabinet(Description description) {
		description.add(cabinet);

		configureProduct(description, cabinet);

		description.addParam(cabinet, "cabinetIdentifier");
		description.addParam(cabinet, "sharedCabinetIdentifier");
	}

	private static void configurePlugInUnit(Description description) {
		description.add(plugInUnit);

		configureProduct(description, plugInUnit);

		description.addParam(plugInUnit, "greendLed");
		description.addParam(plugInUnit, "redLed");
		description.addParam(plugInUnit, "unitType");
		description.addParam(plugInUnit, "yellowLed");

	}
	private static void configureAuxPlugInUnit(Description description) {
		description.add(auxPlugInUnit);

		configureProduct(description, auxPlugInUnit);

		description.addParam(auxPlugInUnit, "greenLed");
		description.addParam(auxPlugInUnit, "hubPosition");
		description.addParam(auxPlugInUnit, "redLed");
		description.addParam(auxPlugInUnit, "yellowLed");
	}

	private static void configureProduct(Description description, String className) {
		description.addParam(className, "productData.productName");
		description.addParam(className, "productData.productNumber");
		description.addParam(className, "productData.serialNumber");
		description.addParam(className, "productName");
		description.addParam(className, "productNumber");
		description.addParam(className, "serialNumber");
	}

}
