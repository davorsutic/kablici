package hr.hashcode.cables.connect;

import java.util.function.Function;

import hr.hashcode.cables.connect.Description.Dormant;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;

public class DormantTable<T extends Dormant> extends TableView<T> {

	DormantTable(ObservableList<T> items) {
		setItems(items);
		TableColumn<T, Boolean> isActive = new TableColumn<>("");
		isActive.setCellFactory(x -> new CheckBoxTableCell<>());
		isActive.setCellValueFactory(x -> x.getValue().isActive());
		isActive.setEditable(true);

		getColumns().add(isActive);
		setEditable(true);
	}

	<U> void column(String name, Function<T, U> extract) {
		TableColumn<T, U> column = new TableColumn<>(name);
		column.setCellValueFactory(x -> new SimpleObjectProperty<>(extract.apply(x.getValue())));
		column.setEditable(false);
		getColumns().add(column);
	}

}
