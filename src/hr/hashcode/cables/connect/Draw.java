package hr.hashcode.cables.connect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import hr.hashcode.cables.connect.Description.Attribute;
import hr.hashcode.cables.connect.Description.Direction;
import hr.hashcode.cables.connect.Description.DoubleLink;
import hr.hashcode.cables.connect.Description.GroupOption;
import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.NodeInfo;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.NodeState.ManagedArray;
import hr.hashcode.cables.ran.NodeState.ManagedAttribute;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.ran.NodeState.ManagedStruct;
import hr.hashcode.cables.util.MoUtil;
import javafx.application.Platform;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class Draw extends TabPane {

	private final Group area = new Group();

	private final List<ManagedObject> nodeObjects;

	private final Description description;
	private final ManagedObject root;
	public Draw(NodeState node) {
		this.nodeObjects = MoUtil.suffixSort(node.objects());

		NodeInfo info = NodeInfo.info(node);
		switch (info.type) {
			case RBS:
				this.description = Descriptions.rbs();
				break;
			case ERBS:
				this.description = Descriptions.erbs();
				break;
			case MSRBS:
				this.description = Descriptions.msrbs();
				break;
			default:
				this.description = new Description();
		}

		ManagedObject root = null;
		for (ManagedObject object : node.objects())
			if (object.parent == null) {
				root = object;
				break;
			}
		this.root = root;

		Tab tab = new Tab("Image", new ScrollPane(area));

		LabeledField newClass = new LabeledField("New class");
		newClass.register(text -> {
			description.add(text);
		});

		LabeledField newParam = new LabeledField("New parameter");
		newParam.register(2, array -> {
			description.addParam(array[0], array[1]);
		});

		LabeledField newLink = new LabeledField("New link");
		newLink.register(2, array -> {
			description.registerRef(array[0], array[1]);
		});

		LabeledField newDoubleLink = new LabeledField("New double link");
		newDoubleLink.register(3, array -> {
			description.registerRef(array[0], array[1], array[2]);
		});

		DormantTable<Ladder> classesTable = new DormantTable<>(description.classes);
		classesTable.column("Class", x -> x);
		DormantTable<Attribute> attributesTable = new DormantTable<>(description.attributes);
		attributesTable.column("Class", x -> x.moClass);
		attributesTable.column("Attribute", x -> x.name);
		DormantTable<Attribute> linksTable = new DormantTable<>(description.links);
		linksTable.column("Class", x -> x.moClass);
		linksTable.column("Reference", x -> x.name);
		DormantTable<DoubleLink> doubleLinksTable = new DormantTable<>(description.doubleLinks);
		doubleLinksTable.column("Class", x -> x.moClass);
		doubleLinksTable.column("Ref1", x -> x.first);
		doubleLinksTable.column("Ref2", x -> x.second);

		description.register(this::render);

		Button drawLines = new Button("Draw lines");
		drawLines.setOnAction(e -> drawLines());
		Tab conf = new Tab("Configuration",
				new HBox(pair(newClass, classesTable),
						pair(newParam, attributesTable),
						pair(newLink, linksTable),
						pair(newDoubleLink, doubleLinksTable), drawLines));
		getTabs().addAll(tab, conf);
	}

	VBox pair(LabeledField field, DormantTable<?> table) {
		return new VBox(field, table);
	}

	private class LabeledField extends HBox {

		TextField field;

		LabeledField(String text) {
			getChildren().add(new Label(text + ": "));
			this.field = new TextField();
			getChildren().addAll(field);
		}

		void register(int length, Consumer<String[]> action) {
			field.setOnKeyPressed(e -> {
				if (e.getCode() == KeyCode.ENTER) {
					String text = field.getText();
					String[] array = text.split("\\W+");
					if (array.length == length) {
						action.accept(array);
						field.setText("");
						render();
					}
				}

			});
		}

		void register(Consumer<String> action) {
			field.setOnKeyPressed(e -> {
				if (e.getCode() == KeyCode.ENTER) {
					action.accept(field.getText());
					field.setText("");
					render();
				}

			});
		}

	}

	private Entity getEntity(ManagedObject object) {
		Entity entity = entities.get(object);
		if (entity == null) {
			entity = new Entity(object);
			entities.put(object, entity);
			if (object.parent != null) {
				Entity parent = getEntity(object.parent);
				parent.addChild(entity);
			}
		}
		return entity;
	}

	public void render() {

		entities.clear();
		area.getChildren().clear();

		for (ManagedObject object : nodeObjects) {
			for (Ladder ladder : description.classes) {
				if (ladder.isActive().get() && ladder.fits(object)) {
					getEntity(object);
					break;
				}
			}
		}

		for (Attribute attribute : description.attributes) {
			if (attribute.isActive().get())
				for (ManagedObject object : objects(attribute.moClass)) {
					entities.get(object).addParam(attribute.name);
				}
		}

		entities.values().forEach(Entity::render);

		area.getChildren().add(entities.get(root).vbox);
		Platform.runLater(() -> drawLines());
	}

	public void drawLines() {

		for (Attribute link : description.links)
			if (link.isActive().get())
				drawRefs(link.moClass, link.name);

		for (DoubleLink link : description.doubleLinks)
			if (link.isActive().get())
				drawRefs(link.moClass, link.first, link.second);

	}

	private void drawRefs(String className, String field) {
		for (ManagedObject object : objects(className)) {
			Object ref = get(object, field, Object.class);
			if (ref == null)
				continue;
			if (ref instanceof ManagedObject)
				line(object, (ManagedObject) ref, false);
			else if (ref instanceof ManagedArray) {
				ManagedArray array = (ManagedArray) ref;
				for (int i = 0; i < array.length(); i++) {
					Object obj = array.value(i);
					if (obj == null)
						continue;
					line(object, (ManagedObject) obj, false);
				}
			} else
				throw new IllegalArgumentException(className + "," + field + "," + ref);
		}
	}

	private void drawRefs(String className, String field1, String field2) {
		for (ManagedObject object : objects(className))
			line(get(object, field1, ManagedObject.class), get(object, field2, ManagedObject.class), true);
	}

	private Bounds getRelativeBounds(Node node, Node relativeTo) {
		Bounds nodeBoundsInScene = node.localToScene(node.getBoundsInLocal());
		return relativeTo.sceneToLocal(nodeBoundsInScene);
	}

	private Point2D getCenter(Bounds b) {
		return new Point2D(b.getMinX() + b.getWidth() / 2, b.getMinY() + b.getHeight() / 2);
	}

	private final Color[] colors =
			{Color.LIGHTBLUE, Color.DARKBLUE, Color.CHOCOLATE, Color.DARKGREEN, Color.BLUEVIOLET, Color.BROWN,
					Color.FUCHSIA, Color.MEDIUMORCHID};
	private int index;

	private void line(ManagedObject obj1, ManagedObject obj2, boolean twoSided) {
		Entity sourceEnt = entities.get(obj1);
		Entity targetEnt = entities.get(obj2);
		if (sourceEnt == null)
			throw new IllegalArgumentException(obj1.toString());
		if (targetEnt == null)
			throw new IllegalArgumentException(obj2.toString());
		Node source = sourceEnt.nameNode;
		Node target = targetEnt.nameNode;

		Bounds srcBound = getRelativeBounds(source, area);
		Bounds targetBound = getRelativeBounds(target, area);
		Point2D n1Center = getCenter(srcBound);
		Point2D n2Center = getCenter(targetBound);

		double x1 = n1Center.getX();
		double y1 = n1Center.getY();

		double x2 = n2Center.getX();
		double y2 = n2Center.getY();

		Line line = new Line(x1, y1, x2, y2);
		Color color = colors[index % colors.length];
		index++;
		line.setStroke(color);
		line.setFill(color);
		line.setStrokeWidth(1);

		if (twoSided)
			line.getStrokeDashArray().addAll(3.);
		else {
			double x = x1 - x2;
			double y = y1 - y2;
			double z = Math.hypot(x, y);
			double len = 10;
			double dx = x / z * len;
			double dy = y / z * len;

			double angle = Math.toRadians(10);
			double xl = x2 + dx + (dx * Math.cos(angle) - dy * Math.sin(angle));
			double yl = y2 + dy + (dx * Math.sin(angle) + dy * Math.cos(angle));
			Line leftArrow = new Line(x2, y2, xl, yl);
			double xr = x2 + dx + (dx * Math.cos(-angle) - dy * Math.sin(-angle));
			double yr = y2 + dy + (dx * Math.sin(-angle) + dy * Math.cos(-angle));
			Line rightArrow = new Line(x2, y2, xr, yr);

			area.getChildren().addAll(leftArrow, rightArrow);
		}

		area.getChildren().add(line);
	}

	Circle circle(Point2D point) {
		return new Circle(point.getX(), point.getY(), 3);
	}

	private String cssLayout = "-fx-border-color: red;\n" +
			"-fx-border-insets: 1;\n" +
			"-fx-border-width: 1;\n" +
			"-fx-border-style: dashed;\n";

	private VBox vbox() {
		VBox box = new VBox();
		box.setStyle(cssLayout);
		box.setSpacing(1);
		return box;
	}

	private List<ManagedObject> objects(String className) {
		return nodeObjects.stream().filter(x -> x.moClass.name.equals(className)).collect(Collectors.toList());
	}

	private static <T> T get(ManagedObject object, String name, Class<T> clazz) {
		for (ManagedAttribute attr : object.attrs()) {
			if (attr.attribute.name.equals(name))
				return clazz.cast(attr.value);
		}
		return null;
	}

	private final Map<ManagedObject, Entity> entities = new IdentityHashMap<>();

	private class Entity {
		private final ManagedObject object;

		private final Pane vbox = vbox();
		private Pane childBox;
		private final Pane nameNode = new HBox();

		private final Map<MoClass, List<Entity>> children = new IdentityHashMap<>();
		private final Set<String> params = new TreeSet<>();

		Entity(ManagedObject object) {
			this.object = object;
		}

		void addParam(String... param) {
			params.addAll(Arrays.asList(param));
		}

		void addChild(Entity entity) {
			if (entity.object.parent != object)
				throw new IllegalArgumentException();
			children.computeIfAbsent(entity.object.moClass, x -> new ArrayList<>()).add(entity);
		}

		void render() {
			GroupOption groupOption = description.grouping.get(object);
			String text;
			if (groupOption == GroupOption.COMPACT)
				text = object.name;
			else
				text = object.moClass + "=" + object.name;
			Label label = new Label(text);

			Direction textDir = description.nameDirection.get(object);
			if (textDir == Direction.VERTICAL)
				label.setRotate(-90);

			nameNode.getChildren().addAll(new Group(label));

			Direction childDir = description.childrenDirection.get(object);
			if (childDir == Direction.HORIZONTAL)
				childBox = new HBox();
			else
				childBox = new VBox();

			VBox params = new VBox();
			for (String param : this.params) {
				int index = param.indexOf('.');
				if (index != -1) {
					Object maybeStruct = get(object, param.substring(0, index), Object.class);
					if (!(maybeStruct instanceof ManagedStruct))
						continue;

					ManagedStruct struct = (ManagedStruct) maybeStruct;
					String last = param.substring(index + 1);

					for (ManagedAttribute attr : struct.attrs())
						if (last.equalsIgnoreCase(attr.attribute.name)) {
							Object value = attr.value;
							Label paramLabel = new Label(last + ": " + value);
							params.getChildren().add(paramLabel);
							break;
						}
				}
				Object value = get(object, param, Object.class);
				if (value != null) {
					Label paramLabel = new Label(param + ": " + value);
					params.getChildren().add(paramLabel);
				}
			}

			vbox.getChildren().addAll(nameNode, params, childBox);
			List<ClassContainer> containers = new ArrayList<>();
			for (Map.Entry<MoClass, List<Entity>> entry : children.entrySet()) {
				MoClass moClass = entry.getKey();
				Pane pane;
				Direction dir = description.groupDirection.get(object, moClass);
				if (dir == Direction.VERTICAL)
					pane = new HBox();
				else
					pane = new VBox();

				Pane parent;
				GroupOption childGroupOption = description.grouping.get(object, moClass);
				if (childGroupOption == GroupOption.COMPACT) {
					parent = vbox();
					parent.getChildren().addAll(new Label(moClass.name), pane);
				} else
					parent = pane;

				List<Entity> list = entry.getValue();
				Map<Object, List<Entity>> splitGroups = new LinkedHashMap<>();
				Function<ManagedObject, ?> splitter = description.getSplitter(moClass.name);

				for (Entity entity : list)
					splitGroups.computeIfAbsent(splitter.apply(entity.object), x -> new ArrayList<>()).add(entity);

				for (List<Entity> sublist : splitGroups.values()) {
					Pane subbox;
					if (dir == Direction.VERTICAL)
						subbox = new VBox();
					else
						subbox = new HBox();
					for (Entity entity : sublist) {
						subbox.getChildren().add(entity.vbox);
					}
					pane.getChildren().add(subbox);
				}

				ClassContainer container = new ClassContainer(parent, description.orders.priority(object, moClass));
				containers.add(container);
			}

			Collections.sort(containers, Comparator.comparing(ClassContainer::priority));

			for (ClassContainer container : containers)
				childBox.getChildren().add(container.node);
		}

		@Override
		public String toString() {
			return object.toString();
		}
	}

	private class ClassContainer {
		private final Node node;
		private final int priority;

		private ClassContainer(Node node, int priority) {
			this.node = node;
			this.priority = priority;
		}

		int priority() {
			return priority;
		}

	}

}
