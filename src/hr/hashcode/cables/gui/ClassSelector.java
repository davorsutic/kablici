package hr.hashcode.cables.gui;

import java.util.Collections;
import java.util.Comparator;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import hr.hashcode.cables.ran.Meta.MoClass;
import javafx.beans.binding.Binding;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.collections.transformation.FilteredList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class ClassSelector extends VBox {

	private static class ClassSelect {
		private final MoClass moClass;
		private final BooleanProperty shown = new SimpleBooleanProperty(true);

		private ClassSelect(MoClass moClass) {
			this.moClass = moClass;
		}
	}

	private final TableView<ClassSelect> classSelector;
	private final ObservableSet<MoClass> visibleClasses;

	ClassSelector(List<MoClass> moClasses) {

		List<ClassSelect> sortedClasses = moClasses.stream()
				.sorted(Comparator.comparing(x -> x.name))
				.map(ClassSelect::new)
				.collect(Collectors.toList());

		TextBox classFilter = new TextBox("Class filter");
		Binding<Predicate<ClassSelect>> binding = Bindings.createObjectBinding(() -> {
			String text = classFilter.text().toLowerCase();
			return x -> x.moClass.name.toLowerCase().contains(text);
		}, classFilter.textProperty());

		FilteredList<ClassSelect> filtered = new FilteredList<>(FXCollections.observableArrayList(sortedClasses));
		filtered.predicateProperty().bind(binding);

		ObservableSet<MoClass> visible =
				FXCollections.observableSet(Collections.newSetFromMap(new IdentityHashMap<>()));

		visible.addAll(moClasses);
		for (ClassSelect select : sortedClasses)
			select.shown.addListener((obs, from, to) -> {
				if (to)
					visible.add(select.moClass);
				else
					visible.remove(select.moClass);
			});
		this.visibleClasses = FXCollections.unmodifiableObservableSet(visible);

		this.classSelector = new TableView<>(filtered);
		classSelector.setFocusTraversable(false);
		classSelector.setEditable(true);

		TableColumn<ClassSelect, Boolean> checkColumn = new TableColumn<>();
		checkColumn.setCellFactory(x -> new CheckBoxTableCell<>());
		checkColumn.setCellValueFactory(x -> x.getValue().shown);
		checkColumn.setEditable(true);
		classSelector.getColumns().add(checkColumn);

		TableColumn<ClassSelect, MoClass> classColumn = new TableColumn<>("Class");
		classColumn.setCellValueFactory(x -> new SimpleObjectProperty<>(x.getValue().moClass));
		classColumn.setEditable(false);
		classSelector.getColumns().add(classColumn);

		Button all = new Button("Select _all");
		all.setFocusTraversable(false);
		all.setOnAction(e -> classSelector.getItems().forEach(x -> x.shown.set(true)));

		Button none = new Button("Select _none");
		none.setFocusTraversable(false);
		none.setOnAction(e -> classSelector.getItems().forEach(x -> x.shown.set(false)));

		HBox buttons = new HBox(all, none);
		buttons.setSpacing(5);
		buttons.setAlignment(Pos.CENTER);

		VBox.setVgrow(classSelector, Priority.ALWAYS);

		getChildren().addAll(classFilter, classSelector, buttons);
	}

	ObservableSet<MoClass> visible() {
		return visibleClasses;
	}

}
