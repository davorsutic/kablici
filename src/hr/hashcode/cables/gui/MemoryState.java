package hr.hashcode.cables.gui;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ReadOnlyLongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.util.Duration;

final class MemoryState {

	private static final long DIV = 1_024 * 1_024;
	private static final long REFRESH_RATE = 100;

	private final LongProperty totalMemory = new SimpleLongProperty();
	private final LongProperty usedMemory = new SimpleLongProperty();
	private final LongProperty gcMemory = new SimpleLongProperty();

	private final Runtime runtime = Runtime.getRuntime();
	private final Timeline timeline;

	MemoryState() {
		update();
		gcMemory.set(usedMemory.get());
		KeyFrame keyFrame = new KeyFrame(Duration.millis(REFRESH_RATE), x -> update());
		this.timeline = new Timeline(keyFrame);
		timeline.setCycleCount(Animation.INDEFINITE);
	}

	void start() {
		timeline.play();
	}

	void stop() {
		timeline.stop();
	}

	private void update() {
		long free = runtime.freeMemory() / DIV;
		long total = runtime.totalMemory() / DIV;
		long used = total - free;

		if (used < usedMemory.get())
			gcMemory.set(used);
		totalMemory.set(total);
		usedMemory.set(used);
	}

	ReadOnlyLongProperty total() {
		return ReadOnlyLongProperty.readOnlyLongProperty(totalMemory);
	}

	ReadOnlyLongProperty used() {
		return ReadOnlyLongProperty.readOnlyLongProperty(usedMemory);
	}

	ReadOnlyLongProperty gc() {
		return ReadOnlyLongProperty.readOnlyLongProperty(gcMemory);
	}

}
