package hr.hashcode.cables.gui;

import java.io.IOException;
import java.io.InputStream;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Restore extends Application {

	private static Image getImage(String filename) {
		try (InputStream input = Restore.class.getResourceAsStream(filename)) {
			if (input == null)
				return null;
			return new Image(input);
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public void start(Stage stage) {

		stage.setTitle("Configi");
		for (int i : new int[]{16, 24, 32, 48, 64}) {
			Image image = getImage("logo" + i + ".png");
			if (image != null)
				stage.getIcons().add(image);
		}

		MainWindow mainWindow = new MainWindow();
		mainWindow.alwaysOnTop().addListener((v, o, n) -> stage.setAlwaysOnTop(n));

		Scene scene = new Scene(mainWindow);
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch();
	}
}
