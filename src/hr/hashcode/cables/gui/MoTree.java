package hr.hashcode.cables.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import hr.hashcode.cables.ran.NodeState.ManagedObject;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

class MoTree extends VBox {

	private final Map<ManagedObject, List<ManagedObject>> children = new IdentityHashMap<>();
	private final Map<ManagedObject, RealNode> items = new IdentityHashMap<>();
	private final TreeView<ManagedObject> tree;
	private final ObservableSet<ManagedObject> visibles;
	private final ObservableList<ManagedObject> visibleList;

	private class RealNode extends TreeItem<ManagedObject> {

		private final boolean isLeaf;
		private boolean processed;

		RealNode(ManagedObject object) {
			super(object);
			this.isLeaf = children.get(object).isEmpty();
		}

		@Override
		public ObservableList<TreeItem<ManagedObject>> getChildren() {
			ObservableList<TreeItem<ManagedObject>> items = super.getChildren();
			if (!processed) {
				processed = true;
				ManagedObject object = getValue();
				ObservableList<RealNode> childItems = FXCollections.observableArrayList();

				for (ManagedObject child : children.get(object)) {
					RealNode childWrap = new RealNode(child);
					MoTree.this.items.put(child, childWrap);
					childItems.add(childWrap);
				}

				FilteredList<RealNode> filtered = new FilteredList<>(childItems); // TODO with the current setup it
																					// might not be needed to use
																					// filtered list;
				filtered.setPredicate(x -> true);

				visibleList.addListener((ListChangeListener<ManagedObject>) x -> {
					filtered.setPredicate(xx -> visibles.contains(xx.getValue()));
					items.setAll(filtered);
				});
				items.setAll(filtered);
			}

			return items;
		}

		@Override
		public boolean isLeaf() {
			return isLeaf;
		}

	}

	MoTree(CentralUnit central, List<ManagedObject> objects, ObservableSet<ManagedObject> visibles,
			ObservableList<ManagedObject> visibleList) {
		this.visibleList = visibleList;
		this.visibles = visibles;

		for (ManagedObject object : objects)
			children.computeIfAbsent(object.parent, x -> new ArrayList<>()).add(object);

		for (ManagedObject object : objects)
			children.putIfAbsent(object, Collections.emptyList());

		RealNode rootNode = new RealNode(null);
		this.tree = new TreeView<>(rootNode);
		this.tree.setShowRoot(false);
		rootNode.setExpanded(true);
		for (TreeItem<ManagedObject> item : rootNode.getChildren())
			item.setExpanded(true);

		tree.getSelectionModel().selectedItemProperty().addListener((a, from, to) -> {
			ManagedObject object = to == null ? null : to.getValue();
			central.select(object);
		});
		tree.setCellFactory(x -> {
			TreeCell<ManagedObject> cell = new TreeCell<ManagedObject>() {
				final Tooltip tooltip = new Tooltip();

				@Override
				protected void updateItem(ManagedObject item, boolean empty) {
					super.updateItem(item, empty);
					if (item != null && !empty) {
						setText(item.moClass + "=" + item.name);
						String description = item.moClass.description;
						if (description != null) {
							tooltip.setText(StringWrapping.wrap(description, 100));
							setTooltip(tooltip);
						} else
							setTooltip(null);
					} else {
						setText(null);
						setTooltip(null);
					}
				}
			};

			MenuItem copy = new MenuItem("Copy description");
			ContextMenu menu = new ContextMenu(copy);
			menu.setOnAction(xx -> Clipboard.getSystemClipboard().setContent(
					Collections.singletonMap(DataFormat.PLAIN_TEXT,
							cell.getTreeItem().getValue().moClass.description)));

			cell.setOnMouseClicked(e -> {
				if (e.getButton() == MouseButton.SECONDARY)
					if (!cell.isEmpty() && cell.getTreeItem() != null)
						cell.setContextMenu(menu);
			});
			return cell;
		});
		VBox.setVgrow(tree, Priority.ALWAYS);

		getChildren().add(tree);
	}

	private RealNode createPath(ManagedObject object) {
		if (object == null)
			return null;
		RealNode node = items.get(object);
		if (node != null)
			return node;
		else {
			RealNode parent = createPath(object.parent);
			if (parent != null && !parent.isExpanded())
				parent.setExpanded(true);
			return items.get(object);
		}
	}

	void select(ManagedObject object) {
		if (!visibles.contains(object))
			return;
		MultipleSelectionModel<TreeItem<ManagedObject>> model = tree.getSelectionModel();
		TreeItem<ManagedObject> item = model.getSelectedItem();
		if (item == null || item.getValue() != object) {
			RealNode target = createPath(object);
			model.select(target);
			int index = model.getSelectedIndex();
			tree.scrollTo(index);
		}
	}
}
