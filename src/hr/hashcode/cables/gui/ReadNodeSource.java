package hr.hashcode.cables.gui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import hr.hashcode.cables.fetch.moshell.ParseMoshellDump;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.util.Util;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ReadNodeSource extends HBox {

	private final ProgressBar progressBar;
	private final Label text;

	private Agent agent; // restricted to FX thread

	private final BooleanProperty working = new SimpleBooleanProperty();
	private final Label finishedText;
	private final Label failedText;

	ReadNodeSource() {
		this.progressBar = new ProgressBar(0);
		this.text = new Label();
		this.finishedText = new Label();
		finishedText.setPrefWidth(40);
		this.failedText = new Label();
		failedText.setPrefWidth(30);

		Button cancel = new Button("Cancel");
		cancel.setOnAction(e -> {
			if (agent != null)
				agent.cancel();
		});

		disableProperty().bind(working.not());

		HBox finishedBox = new HBox(new Label("Finished:"), finishedText);
		HBox failedBox = new HBox(new Label("Failed:"), failedText);
		HBox textBox = new HBox(finishedBox, failedBox);

		VBox info = new VBox(progressBar, textBox, text);
		this.progressBar.prefWidthProperty().bind(info.widthProperty());
		getChildren().addAll(info, cancel);
	}

	private void setFinished(int finished, int total) {
		finishedText.setText(finished + "/" + total);
	}

	private void setFailed(int failed) {
		failedText.setText(String.valueOf(failed));
	}

	private void setText(String text) {
		Platform.runLater(() -> this.text.setText(text));
	}

	ReadOnlyBooleanProperty working() {
		return ReadOnlyBooleanProperty.readOnlyBooleanProperty(working);
	}

	void read(List<File> files, Consumer<NodeState> consumer) {
		List<File> copy = new ArrayList<>(files);
		if (copy.isEmpty())
			return;
		agent = new Agent(consumer, Runtime.getRuntime().availableProcessors() - 1, copy);
		agent.start();
	}

	private class Agent {

		private final Consumer<NodeState> consumer;
		private final int threadCount;
		private final Thread thread;
		private final List<File> files;

		Agent(Consumer<NodeState> consumer, int threadCount, List<File> files) {
			this.consumer = consumer;
			this.threadCount = threadCount;
			this.files = files;
			this.thread = new Thread(() -> {
				try {
					read();
				} finally {
					Platform.runLater(() -> working.set(false));
				}
			});
		}

		void cancel() {
			thread.interrupt();
		}

		void start() {
			working.set(true);
			setFinished(0, files.size());
			setFailed(0);
			setText("Reading in progress");
			thread.start();
		}

		private void read() {
			AtomicInteger counter = new AtomicInteger();
			CountDownLatch latch = new CountDownLatch(threadCount);
			AtomicInteger read = new AtomicInteger();
			AtomicInteger failed = new AtomicInteger();
			AtomicBoolean shouldStop = new AtomicBoolean();
			for (int i = 0; i < threadCount; i++) {
				Runnable runnable = () -> {
					try {
						while (true) {
							if (shouldStop.get())
								break;
							int index = counter.getAndIncrement();
							if (index >= files.size())
								break;
							File file = files.get(index);
							NodeState state = read(file);
							if (state == null) {
								System.err.println(file);
								failed.incrementAndGet();
							} else
								read.incrementAndGet();
							Platform.runLater(() -> {
								if (shouldStop.get())
									return;
								int failedCount = failed.get();
								int readCount = read.get();
								int finished = failedCount + readCount;
								setFinished(finished, files.size());
								setFailed(failedCount);
								if (state != null)
									consumer.accept(state);
								progressBar.setProgress((double) finished / files.size());
							});
						}
					} finally {
						latch.countDown();
					}
				};
				new Thread(runnable).start();
			}
			try {
				latch.await();
				setText("Finished");
			} catch (InterruptedException e) {
				shouldStop.set(true);
				setText("Interrupted");
			}
		}

		private NodeState read(File file) {
			byte[] bytes;
			try {
				bytes = Util.drain(FileFormats.tryGzip(file));
			} catch (IOException e) {
				return null;
			}
			NodeState state = null;
			try {
				try (ObjectInputStream objInput = new ObjectInputStream(new ByteArrayInputStream(bytes))) {
					state = (NodeState) objInput.readObject();
				} catch (IOException | ClassNotFoundException e) {
					// do nothing
				}
			} catch (Throwable t) {
				t.printStackTrace();
			}
			try {
				if (state == null)
					state = ParseMoshellDump.read(bytes);
			} catch (Throwable t) {
				t.printStackTrace();
			}

			// try {
			// if (state == null)
			// state = Serialize.fromBytes(bytes);
			// } catch (Throwable t) {
			// t.printStackTrace();
			// }

			return state;
		}

	}
}
