package hr.hashcode.cables.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringWrapping {

	private static final Pattern pattern = Pattern.compile("\r?\n");

	static String wrap(String string, int length) {
		String[] elems = pattern.split(string, -1);
		List<String> processed = new ArrayList<>();
		for (String elem : elems) {
			while (true) {
				if (elem.length() <= length) {
					processed.add(elem);
					break;
				} else {
					int index = elem.lastIndexOf(' ', length);
					if (index == -1)
						index = elem.indexOf(' ', length);
					if (index == -1) {
						processed.add(elem);
						break;
					} else {
						index++;
						processed.add(elem.substring(0, index));
						elem = elem.substring(index);
					}
				}
			}
		}

		return processed.stream().collect(Collectors.joining("\n"));
	}

}
