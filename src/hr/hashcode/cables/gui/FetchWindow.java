package hr.hashcode.cables.gui;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

class FetchWindow extends Stage {

	private FileChooser chooser;

	private FileChooser chooser() {
		if (chooser == null)
			chooser = new FileChooser();
		return chooser;
	}

	private final CheckBox useProxy = new CheckBox("_Use proxy");
	private final TextField proxyField = new TextField();
	private final TextField portField = new TextField();
	private final ObservableList<String> ips = FXCollections.observableArrayList();
	private final ListView<String> ipsView = new ListView<>(ips);

	private boolean lastChoice;

	FetchWindow() {

		TabPane tabs = new TabPane();
		tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		HBox proxyBox = new HBox(new LabeledField("_Address", proxyField), new LabeledField("_Port", portField));
		proxyBox.setSpacing(10);
		proxyBox.disableProperty().bind(useProxy.selectedProperty().not());

		VBox connection = new VBox(useProxy, proxyBox);
		connection.setSpacing(10);
		connection.setPadding(new Insets(10));
		Tab connectionTab = new Tab("Connection", connection);

		BooleanBinding okToRemove =
				Bindings.createBooleanBinding(this::okToRemove, ipsView.getSelectionModel().selectedItemProperty());

		Button browse = FxUtil.button("_Browse", this::readIpsFromFile);
		Button remove = FxUtil.button("_Remove", this::removeSelected);
		remove.disableProperty().bind(okToRemove.not());
		Button removeAll = FxUtil.button("R_emove all", ips::clear);

		TilePane targetButtons = FxUtil.verticalButtons(browse, remove, removeAll);

		TextField manual = new TextField();
		manual.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				String text = manual.getText();
				if (text != null && !text.trim().isEmpty()) {
					ips.add(text.trim());
					manual.clear();
				}
			}
		});

		VBox ipsBox = new VBox(new LabeledField("_Manual input", manual), new LabeledField("_Target nodes", ipsView));
		ipsBox.setSpacing(10);

		HBox targetBox = new HBox(ipsBox, targetButtons);
		targetBox.setPadding(new Insets(10));
		targetBox.setAlignment(Pos.CENTER);
		targetBox.setSpacing(5);
		Tab targetTab = new Tab("Targets", targetBox);

		tabs.getTabs().addAll(targetTab, connectionTab);

		BooleanBinding okToProceed = Bindings.createBooleanBinding(this::okToProceed,
				useProxy.selectedProperty(), proxyField.textProperty(), portField.textProperty(), ips);

		Button ok = FxUtil.button("OK", this::accept);
		ok.disableProperty().bind(okToProceed.not());
		Button cancel = FxUtil.button("Cancel", this::decline);
		TilePane allButtons = FxUtil.horizontalButtons(ok, cancel);

		VBox total = new VBox(tabs, allButtons);

		Scene scene = new Scene(total);
		scene.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ESCAPE)
				decline();
			else if (e.getCode() == KeyCode.ENTER && e.isControlDown() && okToProceed())
				accept();
		});
		setTitle("Fetch node states");
		setScene(scene);
		setResizable(false);
	}

	private boolean okToProceed() {
		if (ips.isEmpty())
			return false;
		if (!useProxy.isSelected())
			return true;
		String proxy = proxyField.getText();
		if (proxy == null || proxy.trim().isEmpty())
			return false;

		try {
			int port = Integer.parseInt(portField.getText().trim());
			return port >= 0 && port <= 65535;

		} catch (NumberFormatException e) {
			return false;
		}
	}

	private boolean okToRemove() {
		return ipsView.getSelectionModel().getSelectedItem() != null;
	}

	private void removeSelected() {
		int index = ipsView.getSelectionModel().getSelectedIndex();
		ips.remove(index);
	}

	private void readIpsFromFile() {
		FileChooser chooser = chooser();
		chooser.getExtensionFilters().setAll(new ExtensionFilter("Text file", "*.txt"),
				new ExtensionFilter("All files", "*.*"));
		File file = chooser.showOpenDialog(this);
		if (file != null) {
			chooser.setInitialDirectory(file.getParentFile());
			new Thread(() -> {
				try {
					List<String> lines = Files.readAllLines(file.toPath());
					Platform.runLater(() -> ips.addAll(lines));
				} catch (IOException ex) {
					// TODO indicate the error somewhere
				}
			}).start();
		}
	}

	private void accept() {
		lastChoice = true;
		this.hide();
	}

	private void decline() {
		lastChoice = false;
		this.hide();
	}

	private static class LabeledField extends HBox {

		LabeledField(String text, Node node) {
			Label label = new Label(text + ":");
			label.setMnemonicParsing(true);
			label.setLabelFor(node);
			getChildren().addAll(label, node);
			setAlignment(Pos.CENTER_LEFT);
			HBox.setHgrow(node, Priority.ALWAYS);
		}
	}

	private void check() {
		if (!lastChoice)
			throw new IllegalStateException();
	}

	private void checkProxy() {
		check();
		if (!useProxy.isSelected())
			throw new IllegalStateException();
	}

	boolean useProxy() {
		check();
		return useProxy.isSelected();
	}

	String proxy() {
		checkProxy();
		return proxyField.getText().trim();
	}

	int port() {
		checkProxy();
		return Integer.parseInt(portField.getText().trim());
	}

	List<String> ips() {
		check();
		return new ArrayList<>(ips);
	}

	boolean fetch() {
		showAndWait();
		return lastChoice;
	}
}
