package hr.hashcode.cables.gui;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ReadOnlyLongProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

class MemoryPane extends GridPane {

	private static class MemoryBar {
		private final ProgressBar progressBar = new ProgressBar(0);
		private final Text text = new Text();
		private final Label label;

		MemoryBar(String text) {
			this.label = new Label(text + ":");
		}

		void bind(ReadOnlyLongProperty used, ReadOnlyLongProperty total) {
			StringBinding textBinding =
					Bindings.createStringBinding(() -> used.get() + "M/" + total.get() + "M", used, total);
			text.textProperty().bind(textBinding);
			DoubleBinding progressBinding = Bindings.createDoubleBinding(() -> {
				if (total.get() == 0)
					return 0.;
				return (double) used.get() / total.get();
			}, used, total);
			progressBar.progressProperty().bind(progressBinding);
		}
	}

	private final MemoryBar first = new MemoryBar("Used memory");
	private final MemoryBar second = new MemoryBar("After GC");
	private final Button gc = new Button("Perform GC");

	MemoryPane(MemoryState memory) {
		first.bind(memory.used(), memory.total());
		second.bind(memory.gc(), memory.total());
		addRow(0, first.label, first.progressBar, first.text);
		addRow(1, second.label, second.progressBar, second.text);
		setHgap(5);
		add(gc, 3, 0, 1, 2);
		gc.setOnAction(e -> new Thread(System::gc).start());
	}
}
