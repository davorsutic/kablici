package hr.hashcode.cables.gui;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;

class History<T> {

	private static class Entry<T> {
		final T object;
		Entry<T> next;

		Entry(T object) {
			this.object = object;
			this.next = null;
		}

		@Override
		public String toString() {
			return String.valueOf(object);
		}
	}

	private Entry<T> back;
	private Entry<T> forward;
	private Entry<T> current = new Entry<>(null);

	private ReadOnlyObjectWrapper<T> forwardProp = new ReadOnlyObjectWrapper<>();
	private ReadOnlyObjectWrapper<T> backProp = new ReadOnlyObjectWrapper<>();

	History() {
		update();
	}

	ReadOnlyObjectProperty<T> backProperty() {
		return backProp.getReadOnlyProperty();
	}

	ReadOnlyObjectProperty<T> forwardProperty() {
		return forwardProp.getReadOnlyProperty();
	}

	private void update() {
		forwardProp.set(forward == null ? null : forward.object);
		backProp.set(back == null ? null : back.object);
	}

	void select(T object) {
		if (object == current.object) // if selected the same one
			return;

		if (current.object != null) { // if something is selected then put it in the 'back' part
			current.next = back;
			back = current;
		}

		if (forward != null && object == forward.object) { // if first element of 'forward' part is selected
			current = forward;
			forward = forward.next;
		} else {
			if (object != null) // if something is selected forward is invalidated
				forward = null;
			current = new Entry<>(object);
		}
		update();
	}

	T current() {
		return current.object;
	}

	T back() {
		if (back == null)
			throw new IllegalStateException();
		if (current.object != null) {
			current.next = forward;
			forward = current;
		}
		current = back;
		back = back.next;
		update();
		return current.object;
	}

	T forward() {
		if (forward == null)
			throw new IllegalStateException();

		if (current.object != null) {
			current.next = back;
			back = current;
		}
		current = forward;
		forward = forward.next;
		update();
		return current.object;
	}

}
