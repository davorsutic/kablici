package hr.hashcode.cables.gui;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import hr.hashcode.cables.connect.Draw;
import hr.hashcode.cables.equip.bind.Bind;
import hr.hashcode.cables.equip.bind.BindTrace;
import hr.hashcode.cables.equip.bind.CabinetSystem;
import hr.hashcode.cables.equip.bind.CabinetVisual;
import hr.hashcode.cables.equip.bind.DumpNormalize;
import hr.hashcode.cables.equip.bind.DumpToSite;
import hr.hashcode.cables.equip.bind.DumpToSite.HwPlacement;
import hr.hashcode.cables.equip.bind.ErbsBind;
import hr.hashcode.cables.equip.bind.MsrbsBind;
import hr.hashcode.cables.equip.bind.RbsBind;
import hr.hashcode.cables.equip.comp.MiniRbsFactory;
import hr.hashcode.cables.equip.comp.RbsFactory;
import hr.hashcode.cables.equip.comp.Site;
import hr.hashcode.cables.equip.rules.PlacementRules;
import hr.hashcode.cables.ran.NodeInfo;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.Version.Type;
import hr.hashcode.cables.util.MoUtil;
import hr.hashcode.cables.util.Util;
import hr.hashcode.cables.visual.conn.MainController;
import hr.hashcode.cables.visual.huge.SiteView;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.Window;

class NodeTableView {

	private static final boolean additionalFeatures = Boolean.parseBoolean(System.getProperty("additionalFeatures"));

	private final PlacementRules placementRules =
			PlacementRules.of(PlacementRules.class.getResourceAsStream("placement_rules.xml"));
	private final RbsFactory rbsFactory = RbsFactory.of(RbsFactory.class.getResourceAsStream("rbses.xml"));
	private final MiniRbsFactory miniRbsFactory =
			MiniRbsFactory.of(MiniRbsFactory.class.getResourceAsStream("rbses.xml"));

	private final TableView<Wrapper> view;

	private final Map<String, Wrapper> byFdn = new HashMap<>();
	private final Map<String, Set<Wrapper>> extFdns = new HashMap<>();
	private final Map<String, Set<Wrapper>> radios = new HashMap<>();
	private final Map<Wrapper, Wrapper> boundTo = new IdentityHashMap<>();
	private final Map<Wrapper, Set<Wrapper>> boundBy = new IdentityHashMap<>();

	private final ReadOnlyIntegerProperty sizeProperty;

	NodeTableView() {
		this.view = new TableView<>();

		this.sizeProperty = new SimpleListProperty<>(view.getItems()).sizeProperty();

		column("FDN", x -> x.info.fdn, Pos.CENTER_LEFT).setPrefWidth(600);
		column("Node Type", x -> x.info.type, Pos.CENTER);
		column("Version", x -> x.state.meta().version(), Pos.CENTER_LEFT).setPrefWidth(250);
		column("Object count", x -> x.count, Pos.CENTER);
		observableColumn("Group", x -> x.group).setPrefWidth(250);

		VBox.setVgrow(view, Priority.ALWAYS);

		view.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		view.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.DELETE) {
				List<Wrapper> wrappers = view.getSelectionModel().getSelectedItems();
				removeNodes(wrappers);
			}
		});

		view.setRowFactory(x -> {
			TableRow<Wrapper> row = new TableRow<>();

			row.setOnMouseClicked(e -> {
				if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2) {
					Wrapper wrapper = row.getItem();
					if (wrapper != null)
						open(wrapper);
				} else if (e.getButton() == MouseButton.SECONDARY) {
					List<Wrapper> wrappers = view.getSelectionModel().getSelectedItems();
					if (!wrappers.isEmpty()) {
						MenuItem viewer = new MenuItem("Open in MO view");
						MenuItem connect = new MenuItem("Open in connect view");
						MenuItem joint = new MenuItem("Open in joint view");
						MenuItem delete = new MenuItem("Remove");

						viewer.setOnAction(ee -> {
							for (Wrapper wrapper : wrappers)
								open(wrapper);
						});
						delete.setOnAction(ee -> removeNodes(wrappers));
						connect.setOnAction(ee -> {
							for (Wrapper wrapper : wrappers)
								openConnect(wrapper);
						});

						joint.setOnAction(ee -> showCabinet(wrappers));

						ContextMenu contextMenu = new ContextMenu(viewer, joint);
						if (additionalFeatures)
							contextMenu.getItems().add(connect);
						contextMenu.getItems().add(delete);

						view.setContextMenu(contextMenu);
					}
				}
			});
			return row;
		});
	}

	Node node() {
		return view;
	}

	ReadOnlyIntegerProperty sizeProperty() {
		return sizeProperty;
	}

	private void showCabinet(List<Wrapper> wrappers) {
		Set<Wrapper> neighbourhood = Util.identitySet(wrappers);
		Deque<Wrapper> queue = new ArrayDeque<>(wrappers);

		while (!queue.isEmpty()) {
			Wrapper next = queue.pop();

			for (Wrapper other : boundBy.getOrDefault(next, Collections.emptySet()))
				if (neighbourhood.add(other))
					queue.addLast(other);

			Wrapper other = boundTo.get(next);
			if (other != null && neighbourhood.add(other))
				queue.addLast(other);
		}

		List<NodeState> states = Util.map(neighbourhood, w -> w.state);
		Runnable runnable = () -> {
			List<CabinetSystem> systems = Bind.systems(states);
			List<Site> sites = new ArrayList<>();
			for (CabinetSystem system : systems) {
				DumpNormalize.normalize(system);

				HwPlacement placement = DumpToSite.hwPlacement(system, rbsFactory, miniRbsFactory, placementRules);
				sites.add(placement.site);
			}

			Platform.runLater(() -> {
				Stage stage = GuiUtil.maximizedIconified(view);
				stage.setTitle("Joint view");
				Scene scene = new Scene(pane(sites, systems));
				stage.setScene(scene);
				stage.show();
			});
		};
		Thread thread = new Thread(runnable);
		thread.setUncaughtExceptionHandler((t, x) -> {
			x.printStackTrace();
			Platform.runLater(() -> {
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText(null);
				alert.setContentText("Unexpected error has occurred!\nPlease contact the development team.");
				Window mainWindow = GuiUtil.window(view);
				Window alertWindow = GuiUtil.window(alert.getDialogPane());
				GuiUtil.acceptIcons(alertWindow, mainWindow);
				alert.showAndWait();
			});
		});
		thread.start();
	}

	private TabPane pane(List<Site> sites, List<CabinetSystem> systems) {
		TabPane main = new TabPane();
		main.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		int count = 0;
		CabinetVisual cabinet = additionalFeatures ? new CabinetVisual(sites, systems) : null;
		for (Site site : sites) {
			CabinetSystem system = systems.get(count);
			TabPane tabs = new TabPane();
			tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
			Tab hugeTab = new Tab("Overview", new SiteView(site));
			Tab connTab = new Tab("Connection flow", new MainController(site).getView());
			tabs.getTabs().addAll(hugeTab, connTab);

			// if (system.joint.longitude != 0 && system.joint.latitude != 0) {
			// WebView view = new WebView();
			// WebEngine engine = view.getEngine();
			// String coords = system.joint.latitude + "," + system.joint.longitude;
			// engine.load("https://www.google.com/maps/place/" + coords + "/@" + coords + ",13z");
			// Tab location = new Tab("Location", view);
			// tabs.getTabs().add(location);
			// }

			if (additionalFeatures) {
				tabs.getTabs().addAll(cabinet.singleInfoTabs.get(system));
				tabs.getTabs().add(cabinet.jointTabs.get(system));
				tabs.getTabs().add(cabinet.siteTabs.get(system));
			}

			Tab tab = new Tab(system.joint.name, tabs);
			count++;
			main.getTabs().add(tab);

		}
		return main;
	}

	void addNode(NodeState state) {
		String fdn = state.fdn;
		if (!byFdn.containsKey(fdn)) {
			Wrapper node = new Wrapper(state);
			byFdn.put(fdn, node);
			view.getItems().add(node);

			for (String extFdn : node.trace.externalFdns())
				extFdns.computeIfAbsent(extFdn, x -> Util.identitySet()).add(node);

			for (String radio : node.trace.radioSerials())
				radios.computeIfAbsent(radio, x -> Util.identitySet()).add(node);

			if (state.meta().version().type() == Type.RBS) {
				Set<Wrapper> wrappers = extFdns.getOrDefault(fdn, Collections.emptySet());
				List<Wrapper> lte = wrappers.stream().filter(x -> x.info.type != Type.RBS).collect(Collectors.toList());
				Wrapper master = null;
				if (lte.isEmpty()) {
					Set<Wrapper> radioOthers = Util.identitySet();
					for (String radio : node.trace.radioSerials())
						radioOthers.addAll(radios.getOrDefault(radio, Collections.emptySet()));
					radioOthers.remove(node);
					List<Wrapper> radioLte =
							radioOthers.stream().filter(x -> x.info.type != Type.RBS).collect(Collectors.toList());
					if (radioLte.size() == 1)
						master = radioLte.get(0);
				} else if (lte.size() == 1)
					master = lte.get(0);

				if (master != null) {
					boundTo.put(node, master);
					boundBy.computeIfAbsent(master, x -> Util.identitySet()).add(node);
					node.group.bind(master.group);
				}
			} else {
				Set<Wrapper> others = Util.identitySet();
				for (String extFdn : node.trace.externalFdns()) {
					Wrapper other = byFdn.get(extFdn);
					if (other != null)
						others.add(other);
				}

				for (String serial : node.trace.radioSerials()) {
					Set<Wrapper> radioOthers = radios.get(serial);
					if (radioOthers != null)
						others.addAll(radioOthers);
				}
				others.remove(node);

				for (Wrapper other : others) {
					if (other.info.type == Type.RBS && !boundTo.containsKey(other)) {
						boundTo.put(other, node);
						boundBy.computeIfAbsent(node, x -> Util.identitySet()).add(other);
						other.group.bind(node.group);
					}
				}

			}
		}

	}

	private void removeNodes(Collection<Wrapper> nodes) {
		nodes = new ArrayList<>(nodes);
		for (Wrapper node : nodes) {
			byFdn.remove(node.state.fdn);
			for (String extFdn : node.trace.externalFdns()) {
				Set<Wrapper> set = extFdns.get(extFdn);
				set.remove(node);
				if (set.isEmpty())
					extFdns.remove(extFdn);
			}

			for (String radio : node.trace.radioSerials()) {
				Set<Wrapper> set = radios.get(radio);
				set.remove(node);
				if (set.isEmpty())
					radios.remove(radio);
			}

			if (node.state.meta().version().type() == Type.RBS) {
				Wrapper other = boundTo.remove(node);
				if (other != null) {
					boundBy.get(other).remove(node);
					node.group.unbind();
					node.group.set("");
				}
			} else {
				Set<Wrapper> others = boundBy.remove(node);
				if (others != null)
					for (Wrapper other : others) {
						boundTo.remove(other);
						other.group.unbind();
						other.group.set("");
					}
			}

			view.getItems().remove(node);
		}
	}

	private void open(Wrapper wrapper) {
		Stage stage = GuiUtil.maximizedIconified(view);

		NodeState state = wrapper.state;
		CentralUnit central = new CentralUnit(state);

		Scene scene = new Scene(central);
		stage.setScene(scene);

		String title = MoUtil.rdn(wrapper.state);
		stage.setTitle(title);
		stage.show();
	}

	private void openConnect(Wrapper wrapper) {
		Draw draw = new Draw(wrapper.state);
		draw.render();

		Stage stage = GuiUtil.maximizedIconified(view);

		Scene scene = new Scene(draw);
		stage.setScene(scene);
		String title = MoUtil.rdn(wrapper.state);
		stage.setTitle(title);
		stage.show();
		draw.drawLines();
	}

	private TableColumn<Wrapper, String> observableColumn(String name,
			Function<Wrapper, ObservableValue<String>> function) {
		TableColumn<Wrapper, String> column = new TableColumn<>(name);
		column.setCellValueFactory(x -> function.apply(x.getValue()));
		view.getColumns().add(column);
		return column;
	}

	private <T> TableColumn<Wrapper, T> column(String name, Function<Wrapper, T> extract, Pos position) {
		TableColumn<Wrapper, T> column = new TableColumn<>(name);
		column.setEditable(false);
		column.setCellValueFactory(x -> new SimpleObjectProperty<>(extract.apply(x.getValue())));
		column.setCellFactory(x -> {
			TableCell<Wrapper, T> cell = new TableCell<Wrapper, T>() {
				@Override
				protected void updateItem(T item, boolean empty) {
					super.updateItem(item, empty);
					this.setAlignment(position);
					if (item == null || empty)
						setText(null);
					else
						setText(item.toString());
				}
			};
			return cell;
		});
		view.getColumns().add(column);
		return column;
	}

	private static class Wrapper {
		private final NodeState state;
		private final NodeInfo info;
		private final int count;
		private final BindTrace trace;
		private final StringProperty group;

		private Wrapper(NodeState state) {
			this.state = state;
			this.info = NodeInfo.info(state);
			this.count = state.objects().size();
			String initialGroup;
			switch (state.meta().version().type()) {
				case RBS:
					this.trace = RbsBind.trace(state);
					initialGroup = "";
					break;
				case ERBS:
					this.trace = ErbsBind.trace(state);
					initialGroup = MoUtil.rdn(state);
					break;
				case MSRBS:
					this.trace = MsrbsBind.trace(state);
					initialGroup = MoUtil.rdn(state);
					break;
				default:
					this.trace = null;
					initialGroup = "";
			}
			this.group = new SimpleStringProperty(initialGroup);
		}

	}

}
