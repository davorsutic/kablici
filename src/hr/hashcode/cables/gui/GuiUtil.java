package hr.hashcode.cables.gui;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.Window;

public class GuiUtil {
	public static Window window(Node node) {
		for (Node temp = node; temp != null; temp = temp.getParent()) {
			Scene scene = temp.getScene();
			if (scene != null) {
				Window window = scene.getWindow();
				if (window != null)
					return window;
			}
		}
		return null;
	}

	public static Stage maximizedIconified(Node node) {
		Stage stage = new Stage();
		Window window = window(node);
		acceptIcons(stage, window);
		acceptLocation(stage, window);
		stage.setMaximized(true);
		return stage;
	}

	public static void acceptIcons(Window to, Window from) {
		if (to instanceof Stage && from instanceof Stage)
			acceptIcons((Stage) to, (Stage) from);
	}

	private static void acceptIcons(Stage to, Stage from) {
		to.getIcons().setAll(from.getIcons());
	}

	public static void acceptLocation(Window to, Window from) {
		to.setX(from.getX());
		to.setY(from.getY());
	}

}
