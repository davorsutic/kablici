package hr.hashcode.cables.gui;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import hr.hashcode.cables.fetch.FetchFilter;
import hr.hashcode.cables.fetch.FetchNodeSource;
import hr.hashcode.cables.fetch.ReadFileNodeSource;
import hr.hashcode.cables.fetch.RemoteFetchNodeSource;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.Version;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

class NodeSourceManager {

	private static final String PROGRESS_SUFFIX = "...";
	private static final String FETCHING_IN_PROGRESS = "Fetching in progress";
	private static final String READING_FILES = "Reading files";
	private static final String COMPLETED = "Completed";
	private static final String CONNECTION_ERROR = "Connection error";
	private static final String COMMUNICATION_ERROR = "Communication error";

	private final NodeSourceMonitor monitor;
	private final BooleanProperty working;
	private final Consumer<NodeState> states;

	NodeSourceManager(Consumer<NodeState> states) {
		this.monitor = new NodeSourceMonitor();
		this.working = new SimpleBooleanProperty();
		this.states = states;

		monitor.disableProperty().bind(working.not());
	}

	NodeSourceMonitor monitor() {
		return monitor;
	}

	ReadOnlyBooleanProperty working() {
		return ReadOnlyBooleanProperty.readOnlyBooleanProperty(working);
	}

	void read(int threadCount, List<File> files) {
		NodeReporter reporter = new NodeReporter();
		ReadFileNodeSource nodeSource = new ReadFileNodeSource(threadCount, files, reporter);
		working.set(true);
		monitor.setOnCancel(() -> {
			reporter.cancelled = true;
			nodeSource.cancel();
			working.set(false);
		});
		init(files, READING_FILES);

		nodeSource.start();
	}

	void fetch(List<String> ips) {
		NodeReporter reporter = new NodeReporter();
		Map<Version.Type, FetchFilter> map = FetchFilter.of(MainWindow.class.getResourceAsStream("filter.xml"));
		FetchNodeSource nodeSource = new FetchNodeSource(map, ips, reporter);
		working.set(true);
		monitor.setOnCancel(() -> {
			reporter.cancelled = true;
			nodeSource.cancel();
			working.set(false);
		});
		init(ips, FETCHING_IN_PROGRESS);
		nodeSource.start();

	}
	void fetch(String proxy, int port, List<String> ips) {
		NodeReporter reporter = new NodeReporter();
		Map<Version.Type, FetchFilter> map = FetchFilter.of(MainWindow.class.getResourceAsStream("filter.xml"));
		RemoteFetchNodeSource nodeSource = new RemoteFetchNodeSource(proxy, port, map, ips, reporter);
		working.set(true);
		monitor.setOnCancel(() -> {
			reporter.cancelled = true;
			nodeSource.cancel();
			working.set(false);
		});
		init(ips, FETCHING_IN_PROGRESS);
		nodeSource.start();

	}

	private void init(Collection<?> items, String text) {
		monitor.setText(text + PROGRESS_SUFFIX);
		monitor.setTotal(items.size());
		monitor.setFinished(0);
		monitor.setFailed(0);
	}

	private class NodeReporter
			implements
				ReadFileNodeSource.Reporter,
				FetchNodeSource.Reporter,
				RemoteFetchNodeSource.Reporter {

		private int finished;
		private int failed;
		private boolean cancelled;

		@Override
		public void success(NodeState state) {
			Platform.runLater(() -> {
				if (cancelled)
					return;
				states.accept(state);
				finished++;
				monitor.setFinished(finished);
			});

		}

		@Override
		public void failed(File file) {
			Platform.runLater(() -> {
				if (cancelled)
					return;

				failed++;
				monitor.setFailed(failed);

				finished++;
				monitor.setFinished(finished);
			});
		}

		@Override
		public void finish() {
			Platform.runLater(() -> {
				monitor.setText(COMPLETED);
				working.set(false);
			});
		}

		@Override
		public void connectionError() {
			Platform.runLater(() -> {
				monitor.setText(CONNECTION_ERROR);
				working.set(false);
			});
		}

		@Override
		public void protocolError() {
			Platform.runLater(() -> {
				monitor.setText(COMMUNICATION_ERROR);
				working.set(false);
			});
		}

		@Override
		public void failed(String ip) {
			Platform.runLater(() -> {
				if (cancelled)
					return;

				failed++;
				monitor.setFailed(failed);

				finished++;
				monitor.setFinished(finished);
			});
		}
	}

}
