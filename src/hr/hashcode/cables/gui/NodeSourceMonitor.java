package hr.hashcode.cables.gui;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

class NodeSourceMonitor extends HBox {

	private final ProgressBar progressBar;
	private final Label text;
	private final Label totalText;
	private final Label finishedText;
	private final Label failedText;

	private final Button cancel;

	private int total;

	NodeSourceMonitor() {
		this.progressBar = new ProgressBar(0);
		this.text = new Label();
		this.finishedText = new Label();
		this.failedText = new Label();
		this.totalText = new Label();

		this.cancel = new Button("Cancel");

		HBox finishedBox = new HBox(new Label("Finished:"), finishedText, new Label("/"), totalText);
		setValue(finishedText, 0);
		setValue(totalText, 0);

		HBox failedBox = new HBox(new Label("Failed:"), failedText);
		setValue(failedText, 0);

		HBox empty = new HBox();
		HBox.setHgrow(empty, Priority.ALWAYS);
		HBox countBox = new HBox(finishedBox, empty, failedBox);

		HBox textBox = new HBox(text);
		textBox.setAlignment(Pos.CENTER);

		VBox info = new VBox(progressBar, countBox, textBox);
		info.setPrefWidth(150);

		this.progressBar.prefWidthProperty().bind(info.widthProperty());
		getChildren().addAll(info, cancel);
	}

	private static void setValue(Label label, int value) {
		label.setText(String.valueOf(value));
	}

	void setOnCancel(Runnable action) {
		cancel.setOnAction(e -> action.run());
	}

	void setFinished(int finished) {
		if (total != 0)
			progressBar.setProgress((double) finished / total);
		else
			progressBar.setProgress(0);
		setValue(finishedText, finished);
	}

	void setFailed(int failed) {
		setValue(failedText, failed);
	}

	void setTotal(int total) {
		this.total = total;
		setValue(totalText, total);
	}

	void setText(String text) {
		this.text.setText(text);
	}

}
