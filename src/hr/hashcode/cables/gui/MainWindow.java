package hr.hashcode.cables.gui;

import java.io.File;
import java.util.List;

import hr.hashcode.cables.equip.musenki.ConfChoice;
import hr.hashcode.cables.equip.musenki.Musenki;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

class MainWindow extends VBox {

	private final NodeTableView view;
	private final NodeSourceManager fetchManager;
	private final NodeSourceManager readFileManager;
	private final ReadOnlyBooleanProperty alwaysOnTop;

	private FetchWindow fetchWindow;
	private FileChooser fileChooser;

	MainWindow() {

		this.view = new NodeTableView();
		this.fetchManager = new NodeSourceManager(view::addNode);
		this.readFileManager = new NodeSourceManager(view::addNode);

		Menu node = new Menu("_Node");

		MenuItem fetch = item(node, "_Fetch state", this::fetchWindow);
		fetch.disableProperty().bind(fetchManager.working());

		MenuItem read = item(node, "_Read printout", this::chooseAndRun);
		read.disableProperty().bind(readFileManager.working());

		item(node, "_Build", this::openBuilder);

		Menu options = new Menu("_Options");
		CheckMenuItem alwaysOnTop = checkItem(options, "Always on _top");
		this.alwaysOnTop = ReadOnlyBooleanProperty.readOnlyBooleanProperty(alwaysOnTop.selectedProperty());

		Menu help = new Menu("_Help");
		item(help, "_About", () -> About.show(this));

		MenuBar menuBar = new MenuBar(node, options, help);

		MemoryState state = new MemoryState();
		MemoryPane memoryPane = new MemoryPane(state);
		HBox bottomStripe = new HBox(memoryPane, readFileManager.monitor(), fetchManager.monitor());
		bottomStripe.setSpacing(20);
		state.start();
		Label count = new Label();
		count.textProperty().bind(
				Bindings.createStringBinding(() -> "Loaded nodes: " + view.sizeProperty().get(), view.sizeProperty()));
		HBox countBox = new HBox(count);
		countBox.setPadding(new Insets(0, 20, 0, 0));
		countBox.setAlignment(Pos.CENTER_RIGHT);
		getChildren().addAll(menuBar, countBox, view.node(), bottomStripe);

		setOnDragOver(e -> {
			e.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			Dragboard board = e.getDragboard();
			if (readFileManager.working().get() || !board.hasFiles())
				e.consume();
		});

		setOnDragDropped(e -> {
			Dragboard board = e.getDragboard();
			List<File> files = board.getFiles();
			readFileManager.read(Runtime.getRuntime().availableProcessors() - 1, files);
		});

	}

	ReadOnlyBooleanProperty alwaysOnTop() {
		return alwaysOnTop;
	}

	private static MenuItem item(Menu menu, String name, Runnable action) {
		MenuItem item = new MenuItem(name);
		item.setMnemonicParsing(true);
		item.setOnAction(e -> action.run());
		menu.getItems().add(item);
		return item;
	}

	private static CheckMenuItem checkItem(Menu menu, String name) {
		CheckMenuItem item = new CheckMenuItem(name);
		item.setMnemonicParsing(true);
		item.setSelected(false);
		menu.getItems().add(item);
		return item;
	}

	private void chooseAndRun() {
		if (fileChooser == null) {
			fileChooser = new FileChooser();
			ExtensionFilter gzFilter = new ExtensionFilter("GZ files", "*.gz");
			ExtensionFilter allFilter = new ExtensionFilter("All files", "*.*");
			fileChooser.getExtensionFilters().setAll(gzFilter, allFilter);
		}

		List<File> files = fileChooser.showOpenMultipleDialog(GuiUtil.window(MainWindow.this));
		if (files != null) {
			fileChooser.setInitialDirectory(files.get(0).getParentFile());
			int maxThreads = Runtime.getRuntime().availableProcessors();
			readFileManager.read(maxThreads - 1, files);
		}
	}

	private void fetchWindow() {

		if (fetchWindow == null) {
			fetchWindow = new FetchWindow();
			Window window = GuiUtil.window(this);
			if (window != null) {
				fetchWindow.initModality(Modality.WINDOW_MODAL);
				fetchWindow.initOwner(window);
			}
			GuiUtil.acceptIcons(fetchWindow, window);
		}

		if (fetchWindow.fetch()) {
			List<String> ips = fetchWindow.ips();
			if (fetchWindow.useProxy()) {
				String proxy = fetchWindow.proxy();
				int port = fetchWindow.port();
				fetchManager.fetch(proxy, port, ips);
			} else
				fetchManager.fetch(ips);
		}
	}

	private void openBuilder() {
		ConfChoice confChoice = new ConfChoice(Musenki.merges());

		Stage stage = GuiUtil.maximizedIconified(this);
		Scene scene = new Scene(confChoice);
		stage.setScene(scene);

		stage.setTitle("Node builder");
		stage.show();
	}

}
