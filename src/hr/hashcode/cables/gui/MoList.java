package hr.hashcode.cables.gui;

import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.util.MoUtil;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class MoList extends VBox {

	private final TreeView<Wrapper> tree;
	private final Map<ManagedObject, TreeItem<Wrapper>> items = new IdentityHashMap<>();
	private final Map<MoClass, TreeItem<Wrapper>> classes = new IdentityHashMap<>();
	private final ObservableList<ManagedObject> visible;
	private final ObservableSet<ManagedObject> visibleSet;
	private final Map<MoClass, List<ManagedObject>> groups;

	MoList(CentralUnit central, Collection<ManagedObject> all, ObservableList<ManagedObject> visible,
			ObservableSet<ManagedObject> visibleSet) {

		this.groups = MoUtil.groupByClass(all);

		this.tree = new TreeView<>();
		tree.getSelectionModel().selectedItemProperty().addListener((a, b, item) -> {
			ManagedObject object = item == null ? null : item.getValue().object;
			if (object != null)
				central.select(object);
		});

		tree.setShowRoot(false);

		this.visible = visible;
		this.visibleSet = visibleSet;

		visible.addListener((ListChangeListener<ManagedObject>) x -> draw());
		draw();

		VBox.setVgrow(tree, Priority.ALWAYS);

		getChildren().add(tree);
	}

	void select(ManagedObject object) {
		if (!visibleSet.contains(object))
			return;

		MultipleSelectionModel<TreeItem<Wrapper>> model = tree.getSelectionModel();
		TreeItem<Wrapper> selected = model.getSelectedItem();
		if (selected == null || selected.getValue().object != object) {
			TreeItem<Wrapper> classItem = classes.get(object.moClass);
			if (!classItem.isExpanded())
				classItem.setExpanded(true);
			TreeItem<Wrapper> item = items.get(object);
			model.select(item);
			int index = model.getSelectedIndex();
			if (index > 0)
				index--;
			tree.scrollTo(index);
		}
	}

	private void draw() {
		TreeItem<Wrapper> root = new TreeItem<>();
		root.setExpanded(true);

		List<MoClass> byName = MoUtil.nameSort(groups.keySet());
		for (MoClass moClass : byName) {
			BranchItem classItem = new BranchItem(moClass);
			classes.put(moClass, classItem);
			root.getChildren().add(classItem);
		}

		tree.setRoot(root);
	}

	private final class Wrapper {

		private final ManagedObject object;
		private final MoClass moClass;

		Wrapper(ManagedObject object) {
			this.object = object;
			this.moClass = null;
		}

		Wrapper(MoClass moClass) {
			this.object = null;
			this.moClass = moClass;
		}

		public String toString() {
			if (object != null)
				return object.ldn();
			else
				return String.valueOf(moClass);
		}
	}

	private class BranchItem extends TreeItem<Wrapper> {

		private boolean expanded;

		BranchItem(MoClass moClass) {
			super(new Wrapper(moClass));
		}

		public ObservableList<TreeItem<Wrapper>> getChildren() {
			ObservableList<TreeItem<Wrapper>> list = super.getChildren();
			if (!expanded) {
				expanded = true;
				MoClass moClass = getValue().moClass;
				for (ManagedObject object : groups.get(moClass)) {
					TreeItem<Wrapper> item = new TreeItem<>(new Wrapper(object));
					items.put(object, item);
					list.add(item);
				}
			}
			return list;
		}

		public boolean isLeaf() {
			return false;
		}
	}
}
