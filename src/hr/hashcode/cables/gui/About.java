package hr.hashcode.cables.gui;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

class About {

	private static final Image logo = getImage("logo128.png");
	private static final String version = readVersion("version.txt");

	private static String readVersion(String filename) {
		try (InputStream input = About.class.getResourceAsStream(filename)) {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] bytes = new byte[8192];
			while (true) {
				int read = input.read(bytes);
				if (read == -1)
					break;
				out.write(bytes, 0, read);
			}
			return new String(out.toByteArray());
		} catch (IOException e) {
			return "unknown";
		}

	}

	private static Image getImage(String filename) {
		try (InputStream input = About.class.getResourceAsStream(filename)) {
			if (input == null)
				return null;
			return new Image(input);
		} catch (IOException e) {
			return null;
		}
	}

	static void show(Node node) {
		Font font = Font.font(50);
		Text wrangler = new Text("Configi");
		wrangler.setFont(font);
		wrangler.setFill(Color.BLUE);

		ImageView imageView = new ImageView(logo);

		Text versionText = new Text("Version: " + version);
		versionText.setFont(Font.font(13));
		versionText.setFill(Color.BLUE);

		Text hashCode = new Text("© HashCode 2018");
		hashCode.setFont(Font.font(14));

		VBox box = new VBox(wrangler, imageView, versionText, hashCode);
		box.setAlignment(Pos.CENTER);
		box.setSpacing(10);

		Window owner = GuiUtil.window(node);

		Stage stage = new Stage();
		stage.initOwner(owner);
		stage.initModality(Modality.WINDOW_MODAL);
		if (owner instanceof Stage) {
			Stage main = (Stage) owner;
			stage.getIcons().setAll(main.getIcons());
			DoubleBinding x = Bindings.createDoubleBinding(() -> {
				return main.getX() + (main.getWidth() - stage.getWidth()) / 2;
			}, stage.widthProperty());
			DoubleBinding y = Bindings.createDoubleBinding(() -> {
				return main.getY() + (main.getHeight() - stage.getHeight()) / 2;
			}, stage.heightProperty());
			x.addListener((v, o, n) -> stage.setX(n.doubleValue()));
			y.addListener((v, o, n) -> stage.setY(n.doubleValue()));
		}
		int dimension = 300;
		stage.setWidth(dimension);
		stage.setHeight(dimension);
		stage.setTitle("About Configi");

		Scene scene = new Scene(box);
		stage.setScene(scene);
		scene.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ESCAPE)
				stage.close();
		});

		stage.setResizable(false);
		stage.setIconified(false);

		stage.show();
	}

}
