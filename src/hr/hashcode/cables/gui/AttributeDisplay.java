package hr.hashcode.cables.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import hr.hashcode.cables.ran.Meta.MoArray;
import hr.hashcode.cables.ran.Meta.MoEnum;
import hr.hashcode.cables.ran.Meta.MoEnum.Element;
import hr.hashcode.cables.ran.Meta.MoStruct;
import hr.hashcode.cables.ran.Meta.MoType;
import hr.hashcode.cables.ran.Meta.SimpleType;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.NodeState.ManagedArray;
import hr.hashcode.cables.ran.NodeState.ManagedAttribute;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.ran.NodeState.ManagedStruct;
import hr.hashcode.cables.ran.NodeState.UnexpectedType;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.DataFormat;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class AttributeDisplay extends VBox {

	private final TreeView<Wrapper> display = new TreeView<>();

	private final CentralUnit central;

	AttributeDisplay(CentralUnit central) {

		this.central = central;

		display.setOnKeyPressed(e -> {
			KeyCode code = e.getCode();

			if (e.isControlDown() && code == KeyCode.C) {
				TreeItem<Wrapper> item = display.getSelectionModel().getSelectedItem();
				Wrapper wrapper = item.getValue();
				if (wrapper != null) {
					String text = wrapper.valueToString();
					Map<DataFormat, Object> content = Collections.singletonMap(DataFormat.PLAIN_TEXT, text);
					Clipboard.getSystemClipboard().setContent(content);
				}
			}
		});

		display.setShowRoot(false);
		display.setCellFactory(x -> {
			TreeCell<Wrapper> cell = new TreeCell<Wrapper>() {
				Tooltip tooltip = new Tooltip();

				protected void updateItem(Wrapper item, boolean empty) {
					super.updateItem(item, empty);
					if (item != null && !empty) {
						setText(item.getText());
						String description = item.tooltip();
						if (description != null) {
							tooltip.setText(StringWrapping.wrap(description, 100));
							setTooltip(tooltip);
						} else
							setTooltip(null);
					} else {
						setText(null);
						setTooltip(null);
					}
				}
			};

			return cell;
		});

		display.setOnMouseClicked(e -> {
			if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2) {
				MultipleSelectionModel<TreeItem<Wrapper>> model = display.getSelectionModel();
				TreeItem<Wrapper> item = model.getSelectedItem();
				if (item != null) {
					Wrapper wrapper = item.getValue();
					ManagedObject target = wrapper.jump();
					if (target != null)
						this.central.select(target);
				}
			}
		});
		VBox.setVgrow(display, Priority.ALWAYS);
		getChildren().add(display);
	}

	void display(ManagedObject object) {
		List<TreeItem<Wrapper>> items;
		if (object == null)
			items = Collections.emptyList();
		else {
			List<ManagedAttribute> attrs = object.attrs();
			Collections.sort(attrs, Comparator.comparing((ManagedAttribute x) -> x.attribute.name));
			items = namedItems(attrs);
		}
		TreeItem<Wrapper> root = new TreeItem<>();
		root.setExpanded(true);
		root.getChildren().setAll(items);

		display.setRoot(root);
	}

	private List<TreeItem<Wrapper>> namedItems(List<ManagedAttribute> attrs) {
		List<TreeItem<Wrapper>> result = new ArrayList<>();
		for (ManagedAttribute attr : attrs) {
			Wrapper wrapper = new NamedWrapper(attr);
			MoType moType = attr.attribute.type;
			Object value = attr.value;
			TreeItem<Wrapper> item = gather(wrapper, moType, value);
			result.add(item);
		}
		return result;
	}

	private TreeItem<Wrapper> gather(Wrapper wrapper, MoType moType, Object value) {
		List<TreeItem<Wrapper>> children;
		TreeItem<Wrapper> item = new TreeItem<>(wrapper);
		if (value == NodeState.FETCH_FAILURE) {
			children = Collections.emptyList();
		} else if (value instanceof UnexpectedType) {
			children = Collections.emptyList();
		} else if (value == null || moType instanceof SimpleType || moType instanceof MoEnum) {
			children = Collections.emptyList();
		} else if (moType instanceof MoArray) {
			ManagedArray array = (ManagedArray) value;
			children = arrayItems(((MoArray) moType).elemType, array);
			MoType elemType = ((MoArray) moType).elemType;
			if (elemType instanceof MoStruct || elemType == SimpleType.REFERENCE)
				item.setExpanded(true);
		} else if (moType instanceof MoStruct) {
			ManagedStruct struct = (ManagedStruct) value;
			children = namedItems(struct.attrs());
			item.setExpanded(true);
		} else
			throw new IllegalArgumentException();
		item.getChildren().setAll(children);
		return item;
	}

	private List<TreeItem<Wrapper>> arrayItems(MoType elemType, ManagedArray array) {
		if (array == null)
			return Collections.emptyList();
		List<TreeItem<Wrapper>> items = new ArrayList<>();
		for (Object value : array.values()) {
			int index = items.size();
			Wrapper wrapper = new NumberedWrapper(index, value);
			TreeItem<Wrapper> item = gather(wrapper, elemType, value);
			items.add(item);
		}
		return items;
	}

	private interface Wrapper {

		String getText();

		ManagedObject jump();

		String tooltip();

		String valueToString();
	}

	private static class NamedWrapper implements Wrapper {

		private final ManagedAttribute attribute;

		NamedWrapper(ManagedAttribute attribute) {
			this.attribute = attribute;
		}

		public String getText() {
			return attribute.attribute.name + ": " + attribute.value;
		}

		private static String trim(String string) {
			if (string == null)
				return null;
			String result = string.trim();
			if (result.isEmpty())
				return null;
			return result;
		}

		public String valueToString() {
			return String.valueOf(attribute.value);
		}

		public String tooltip() {
			String desc1 = trim(attribute.attribute.description);
			String desc2 = null;
			Object value = attribute.value;
			if (value instanceof MoEnum.Element) {
				MoEnum.Element element = (Element) value;
				desc2 = trim(element.description);
				if (desc2 != null)
					desc2 = attribute.value + " - " + desc2;
			}
			if (desc1 == null)
				return desc2;
			else if (desc2 == null)
				return desc1;
			else
				return desc1 + "\n\n" + desc2;
		}

		@Override
		public ManagedObject jump() {
			Object value = attribute.value;
			if (value instanceof ManagedObject)
				return (ManagedObject) value;
			return null;
		}
	}

	private static class NumberedWrapper implements Wrapper {

		private final int index;
		private final Object value;

		NumberedWrapper(int index, Object value) {
			this.index = index;
			this.value = value;
		}

		public String getText() {
			return "[" + index + "]: " + value;
		}

		public String tooltip() {
			if (value instanceof MoEnum.Element) {
				MoEnum.Element element = (Element) value;
				String desc = element.description;
				if (desc != null) {
					desc = desc.trim();
					if (!desc.isEmpty())
						return desc;
				}
			}
			return null;
		}

		@Override
		public ManagedObject jump() {
			if (value instanceof ManagedObject)
				return (ManagedObject) value;
			return null;
		}

		@Override
		public String valueToString() {
			return String.valueOf(value);
		}

	}

}
