package hr.hashcode.cables.gui;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.zip.GZIPInputStream;

import hr.hashcode.cables.fetch.FetchFilter;
import hr.hashcode.cables.fetch.FetchResult;
import hr.hashcode.cables.fetch.FetchServer;
import hr.hashcode.cables.fetch.Listener.CloseRequest;
import hr.hashcode.cables.fetch.Listener.FetchRequest;
import hr.hashcode.cables.fetch.Listener.StatusRequest;
import hr.hashcode.cables.fetch.Listener.StatusResult;
import hr.hashcode.cables.mom.MomStore;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.Version;
import hr.hashcode.cables.ran.Version.Type;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class FetchSource extends HBox {

	private final ProgressBar progressBar;
	private final Label text;

	private Agent agent; // restricted to FX thread

	private final BooleanProperty working = new SimpleBooleanProperty();
	private final Label finishedText;
	private final Label failedText;

	FetchSource() {
		this.progressBar = new ProgressBar(0);
		this.text = new Label();
		this.finishedText = new Label();
		finishedText.setPrefWidth(40);
		this.failedText = new Label();
		failedText.setPrefWidth(30);

		Button cancel = new Button("Cancel");
		cancel.setOnAction(e -> {
			if (agent != null)
				agent.cancel();
		});

		disableProperty().bind(working.not());

		HBox finishedBox = new HBox(new Label("Finished:"), finishedText);
		HBox failedBox = new HBox(new Label("Failed:"), failedText);
		HBox textBox = new HBox(finishedBox, failedBox);

		// setFinished(0, 0);
		// setFailed(0);

		VBox info = new VBox(progressBar, textBox, text);
		this.progressBar.prefWidthProperty().bind(info.widthProperty());

		getChildren().addAll(info, cancel);
	}

	private void setFinished(int finished, int total) {
		finishedText.setText(finished + "/" + total);
	}

	private void setFailed(int failed) {
		failedText.setText(String.valueOf(failed));
	}

	private void setText(String text) {
		Platform.runLater(() -> this.text.setText(text));
	}

	ReadOnlyBooleanProperty working() {
		return ReadOnlyBooleanProperty.readOnlyBooleanProperty(working);
	}

	void fetch(List<String> ips, Map<Type, FetchFilter> filters, Consumer<NodeState> consumer) {
		List<String> copy = new ArrayList<>(ips);
		if (copy.isEmpty())
			return;
		agent = new Agent(consumer);
		working.set(true);
		setText("Fetching in progress");
		setFinished(0, copy.size());
		setFailed(0);
		progressBar.setProgress(0);
		new Thread(() -> agent.fetach(copy, filters)).start();
	}

	void fetch(String address, int port, List<String> ips, Map<Type, FetchFilter> filters,
			Consumer<NodeState> consumer) {

		List<String> copy = new ArrayList<>(ips);
		if (copy.isEmpty())
			return;
		agent = new Agent(consumer);
		working.set(true);
		setText("Fetching in progress");
		setFinished(0, copy.size());
		setFailed(0);
		progressBar.setProgress(0);
		new Thread(() -> agent.fetach(address, port, copy, filters)).start();
	}

	private class Agent {

		private final Consumer<NodeState> consumer;
		private volatile Thread thread;

		Agent(Consumer<NodeState> consumer) {
			this.consumer = consumer;
		}

		volatile boolean cancel;

		private void cancel() {
			this.cancel = true;
			if (thread != null)
				thread.interrupt();
		}

		public void fetach(List<String> ips, Map<Version.Type, FetchFilter> filters) {

			this.thread = Thread.currentThread();
			try {
				MomStore momStore = new MomStore(Arrays.asList(Paths.get("C:\\Users\\makkusu\\git\\kablici\\data")));

				FetchServer server = new FetchServer(false, false, 20, momStore, "bts", "bts", ips, filters);
				server.start();
				int finished = 0;
				int failed = 0;
				while (finished < ips.size()) {
					FetchResult result;
					try {
						result = server.next();
					} catch (InterruptedException e) {
						setText("Interrupted");
						return;
					}
					finished++;
					if (!result.success())
						failed++;
					int ffailed = failed;
					int ffinished = finished;
					Platform.runLater(() -> {
						setFailed(ffailed);
						setFinished(ffinished, ips.size());
						if (result.success())
							consumer.accept(result.state());
						progressBar.setProgress((double) ffinished / ips.size());
					});
				}
				setText("Finished");
			} finally {
				this.thread = null;
				Platform.runLater(() -> working.set(false));
			}
		}

		public void fetach(String address, int port, List<String> ips, Map<Type, FetchFilter> filters) {

			try (Socket socket = new Socket(address, port)) {

				OutputStream output = socket.getOutputStream();
				ObjectOutputStream out = new ObjectOutputStream(output);

				out.writeObject(new FetchRequest(ips, filters));
				out.flush();

				InputStream input = socket.getInputStream();
				ObjectInputStream in = new ObjectInputStream(input);

				in.readObject(); // ack

				int fetched = 0;
				int failed = 0;

				while (fetched + failed < ips.size()) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						break;
					}

					if (cancel)
						break;

					out.writeObject(new StatusRequest());
					out.flush();

					byte[] bytes = (byte[]) in.readObject();
					StatusResult result;
					try (ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
							GZIPInputStream gzip = new GZIPInputStream(byteStream);
							BufferedInputStream buffer = new BufferedInputStream(gzip);
							ObjectInputStream object = new ObjectInputStream(buffer)) {
						result = (StatusResult) object.readObject();
					}

					fetched += result.states().size();
					failed += result.failed().size();
					int finished = fetched + failed;
					int ffailed = failed;
					Platform.runLater(() -> {
						for (NodeState state : result.states()) {
							if (cancel)
								return;
							consumer.accept(state);
						}
						progressBar.setProgress((double) finished / ips.size());
						setFinished(finished, ips.size());
						setFailed(ffailed);
					});
				}

				out.writeObject(new CloseRequest());
				out.flush();

				in.readObject(); // ack
				setText("Finished fetching");
			} catch (IOException e) {
				setText("Connection error");
			} catch (ClassNotFoundException e) {
				setText("Protocol violation");
			} finally {
				Platform.runLater(() -> {
					working.set(false);
					agent = null;
				});
			}
		}

	}
}
