package hr.hashcode.cables.gui;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class FileFormats {

	private FileFormats() {}

	public static InputStream tryGzip(File file) throws FileNotFoundException {
		InputStream stream = gzip(file);
		if (stream == null)
			stream = regular(file);
		return new BufferedInputStream(stream);
	}

	private static InputStream regular(File file) throws FileNotFoundException {
		return new FileInputStream(file);
	}

	private static InputStream gzip(File file) throws FileNotFoundException {
		InputStream input = regular(file);
		try {
			return new GZIPInputStream(input);
		} catch (IOException e) {
			try {
				input.close();
			} catch (IOException ee) {}
			return null;
		}
	}

}
