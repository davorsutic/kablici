package hr.hashcode.cables.gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;

class FxUtil {

	static final double SPACING = 10;
	static final double PADDING = 10;

	private static final Insets insets = new Insets(PADDING);

	static Button button(String label, Runnable runnable) {
		Button button = new Button(label);
		button.setOnAction(e -> runnable.run());
		return button;
	}

	private static TilePane buttonPane(Button... buttons) {
		for (Button button : buttons)
			button.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		TilePane pane = new TilePane(buttons);
		pane.setAlignment(Pos.CENTER);
		pane.setPadding(insets);
		return pane;
	}

	static TilePane verticalButtons(Button... buttons) {
		TilePane pane = buttonPane(buttons);
		pane.setPrefRows(buttons.length);
		pane.setPrefColumns(1);
		pane.setVgap(SPACING);
		return pane;
	}

	static TilePane horizontalButtons(Button... buttons) {
		TilePane pane = buttonPane(buttons);
		pane.setPrefRows(1);
		pane.setPrefColumns(buttons.length);
		pane.setHgap(SPACING);
		return pane;
	}

}
