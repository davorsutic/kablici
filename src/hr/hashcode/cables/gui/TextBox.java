package hr.hashcode.cables.gui;

import javafx.beans.property.StringProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;

class TextBox extends HBox {

	private final TextField field = new TextField();

	TextBox(String name) {
		setAlignment(Pos.CENTER_LEFT);
		getChildren().addAll(new Label(name + ": "), field);
		field.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ESCAPE)
				field.setText("");
		});
	}

	String text() {
		return field.getText();
	}

	StringProperty textProperty() {
		return field.textProperty();
	}
}
