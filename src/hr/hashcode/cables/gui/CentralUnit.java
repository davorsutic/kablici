package hr.hashcode.cables.gui;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Set;

import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.util.MoUtil;
import javafx.beans.binding.Binding;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.collections.SetChangeListener;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

public class CentralUnit extends SplitPane {

	static {
		try {
			hackTooltip();
		} catch (Exception e) {}
	}

	static void hackTooltip() throws Exception {
		Class<?>[] innerClasses = Tooltip.class.getDeclaredClasses();
		Class<?> behaviorClass = null;
		for (Class<?> temp : innerClasses)
			if (temp.getSimpleName().equals("TooltipBehavior"))
				behaviorClass = temp;
		if (behaviorClass == null)
			return;

		Constructor<?> constructor =
				behaviorClass.getDeclaredConstructor(Duration.class, Duration.class, Duration.class, boolean.class);
		constructor.setAccessible(true);
		Object tooltipBehavior =
				constructor.newInstance(new Duration(250), new Duration(Integer.MAX_VALUE), new Duration(200), false);
		Field fieldBehavior = Tooltip.class.getDeclaredField("BEHAVIOR");
		fieldBehavior.setAccessible(true);
		fieldBehavior.set(null, tooltipBehavior);

	}

	private final History<ManagedObject> history = new History<>();
	private final ClassSelector classSelector;
	private final AttributeDisplay attributeDisplay;
	private final MoTree moTree;
	private final MoList moList;
	private final MoPath moPath;

	private final ObservableList<ManagedObject> visibles;
	private final ObservableSet<ManagedObject> visibleSet;
	private final NodeState state;

	CentralUnit(NodeState state) {
		this.state = state;
		this.attributeDisplay = new AttributeDisplay(this);
		this.moPath = new MoPath(this);
		this.classSelector = new ClassSelector(state.classes());
		classSelector.visible().addListener((SetChangeListener<MoClass>) x -> setVisible());

		this.visibles = FXCollections.observableArrayList();
		this.visibleSet = FXCollections.observableSet(identitySet());
		setVisible();

		List<ManagedObject> objects = MoUtil.suffixSort(state.objects());

		this.moTree = new MoTree(this, objects, visibleSet, visibles);
		this.moList = new MoList(this, objects, visibles, visibleSet);

		TextBox objectSearch = new TextBox("Search");
		objectSearch.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER)
				search(objectSearch.text(), false, !e.isShiftDown());
		});

		objectSearch.textProperty().addListener((a, b, c) -> {
			if (c != null && !c.isEmpty())
				search(objectSearch.text(), true, true);
		});

		int total = state.objects().size();
		Text count = new Text();
		Binding<String> textBinding = Bindings
				.createStringBinding(() -> "Objects: " + visibles.size() + "/" + total, visibles);
		count.textProperty().bind(textBinding);

		count.setTextAlignment(TextAlignment.CENTER);

		Button backButton = new Button("_Back");
		backButton.disableProperty().bind(history.backProperty().isNull());
		backButton.setFocusTraversable(false);
		backButton.setOnAction(e -> {
			ManagedObject target = history.back();
			select(target);
		});

		Button forwardButton = new Button("_Forward");
		forwardButton.disableProperty().bind(history.forwardProperty().isNull());
		forwardButton.setFocusTraversable(false);
		forwardButton.setOnAction(e -> {
			ManagedObject target = history.forward();
			select(target);
		});

		HBox searchBar = new HBox(objectSearch, count);
		searchBar.setSpacing(10);
		searchBar.setAlignment(Pos.CENTER_LEFT);

		HBox navigateBar = new HBox(backButton, forwardButton);
		navigateBar.setSpacing(10);
		navigateBar.setAlignment(Pos.CENTER_LEFT);

		SplitPane moViews = new SplitPane(moTree, moList);
		moViews.setDividerPositions(0.6);
		moViews.setOrientation(Orientation.VERTICAL);
		VBox.setVgrow(moViews, Priority.ALWAYS);

		VBox moView = new VBox(searchBar, navigateBar, moViews);
		moView.setAlignment(Pos.CENTER);

		SplitPane details = new SplitPane(moPath, attributeDisplay);
		details.setOrientation(Orientation.VERTICAL);
		details.setDividerPositions(0.1);

		getItems().addAll(classSelector, moView, details);
		setDividerPositions(0.2, 0.5);

		for (ManagedObject root : state.objects())
			if (root.parent == null) {
				select(root);
				return;
			}
	}

	private void search(String text, boolean fix, boolean forward) {
		String[] elems = text.split("\\s+");

		for (int i = 0; i < elems.length; i++)
			elems[i] = elems[i].toLowerCase();

		ManagedObject object = history.current();

		int index = -1; // TODO if index is -1 this will not work
		for (int i = 0; i < visibles.size(); i++)
			if (object == visibles.get(i)) {
				index = i;
				break;
			}

		int offset = fix ? 0 : 1;

		int len = visibles.size();
		int sign = forward ? 1 : -1;

		for (int i = 0; i < len; i++) {
			int ii = (index + (i + offset) * sign) % len;
			if (ii < 0)
				ii += len;
			ManagedObject obj = visibles.get(ii);
			if (fits(obj, elems)) {
				select(obj);
				return;
			}
		}
	}

	void select(ManagedObject object) {
		attributeDisplay.display(object);
		history.select(object);
		moTree.select(object); // TODO -> circular
		moList.select(object);
		moPath.set(object);
	}

	private boolean fits(ManagedObject obj, String[] elems) {
		String text = (obj.moClass + "=" + obj.name).toLowerCase();

		for (String elem : elems)
			if (!text.contains(elem))
				return false;

		return true;
	}

	private static <T> Set<T> identitySet() {
		return Collections.newSetFromMap(new IdentityHashMap<>());
	}

	private void setVisible() { // TODO clear this up
		Set<MoClass> classes = classSelector.visible();

		Set<ManagedObject> visible = identitySet();
		outer: for (ManagedObject object : state.objects()) {
			for (ManagedObject temp = object; temp != null; temp = temp.parent)
				if (!classes.contains(temp.moClass))
					continue outer;

			for (ManagedObject temp = object; temp != null; temp = temp.parent)
				if (!visible.add(temp))
					break;
		}

		this.visibleSet.clear();
		this.visibleSet.addAll(visible);
		this.visibles.setAll(MoUtil.suffixSort(visible)); // TODO the order of operations on list and set is relevant.
															// fix this in
		// MoTree

	}

}
