package hr.hashcode.cables.gui;

import hr.hashcode.cables.ran.NodeState.ManagedObject;
import javafx.application.Platform;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class MoPath extends VBox {

	private final TreeView<ManagedObject> tree = new TreeView<>();

	MoPath(CentralUnit center) {
		tree.setCellFactory(t -> new TreeCell<ManagedObject>() {
			protected void updateItem(ManagedObject object, boolean empty) {
				super.updateItem(object, empty);
				if (object != null && !empty) {
					setText(object.moClass + "=" + object.name);
				} else
					setText(null);
				setDisclosureNode(null);
			}
		});

		tree.getSelectionModel().selectedItemProperty().addListener((obs, from, to) -> {
			if (to != null && to.getValue() != null)
				Platform.runLater(() -> center.select(to.getValue()));
		});

		VBox.setVgrow(tree, Priority.ALWAYS);
		getChildren().add(tree);
	}

	void set(ManagedObject object) {
		LeafItem item = null;
		for (ManagedObject temp = object; temp != null; temp = temp.parent) {
			LeafItem parent = new LeafItem(temp);
			parent.setGraphic(null);
			if (item != null)
				parent.getChildren().add(item);
			item = parent;
			item.setExpanded(true);
			item.addEventHandler(TreeItem.branchCollapsedEvent(), e -> {
				e.consume();
				e.getTreeItem().setExpanded(true);
			});
		}
		tree.setRoot(item);
	}

	private static class LeafItem extends TreeItem<ManagedObject> {

		LeafItem(ManagedObject object) {
			super(object);
		}
	}

}
