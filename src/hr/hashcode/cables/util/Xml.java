package hr.hashcode.cables.util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class Xml {

	private final Node node;

	private Xml(Node node) {
		this.node = node;
	}

	public String name() {
		return node.getNodeName();
	}

	private static boolean isValid(Node node) {
		return node.getNodeType() == Node.ELEMENT_NODE;
	}

	public Xml child(String name) {
		Node result = null;
		for (Node temp = node.getFirstChild(); temp != null; temp = temp.getNextSibling())
			if (isValid(temp) && name.equals(temp.getNodeName())) {
				if (result != null)
					return null;
				result = temp;
			}
		if (result != null)
			return new Xml(result);
		return null;
	}

	public List<Xml> children(String... names) {
		List<Xml> result = new ArrayList<>();
		List<String> list = Arrays.asList(names);
		for (Node temp = node.getFirstChild(); temp != null; temp = temp.getNextSibling())
			if (list.isEmpty() || isValid(temp) && list.contains(temp.getNodeName()))
				result.add(new Xml(temp));
		return result;
	}

	public String attribute(String name) {
		NamedNodeMap map = node.getAttributes();
		if (map == null)
			return null;
		Node item = map.getNamedItem(name);
		if (item == null)
			return null;
		return item.getTextContent();
	}

	public static Xml read(InputStream input) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(input);

			document.normalize();
			return new Xml(document);
		} catch (Exception e) {
			return null;
		}
	}

	public String contents() {
		if (node.getNodeType() == Node.ELEMENT_NODE)
			return node.getTextContent();
		throw new IllegalStateException(String.valueOf(node.getNodeType()));
	}

	@Override
	public String toString() {
		return name();
	}
}
