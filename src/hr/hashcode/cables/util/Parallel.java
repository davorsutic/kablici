package hr.hashcode.cables.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

public class Parallel {

	public static <T, V> List<V> map(Function<T, V> function, List<T> objects, int concurrency)
			throws InterruptedException {
		List<T> copy = new ArrayList<>(objects);
		@SuppressWarnings("unchecked")
		V[] results = (V[]) new Object[copy.size()];
		List<Runnable> tasks = new ArrayList<>();

		for (int i = 0; i < copy.size(); i++) {
			int ii = i;
			Runnable task = () -> {
				results[ii] = function.apply(objects.get(ii));
			};
			tasks.add(task);
		}

		run(tasks, concurrency);

		return new ArrayList<>(Arrays.asList(results));
	}

	public static void run(List<Runnable> tasks, int concurrency) throws InterruptedException {
		if (concurrency <= 0)
			throw new IllegalArgumentException(String.valueOf(concurrency));
		int threadCount = Math.min(concurrency, tasks.size());

		ConcurrentLinkedQueue<Runnable> queue = new ConcurrentLinkedQueue<>(tasks);
		CountDownLatch latch = new CountDownLatch(threadCount);
		AtomicReference<Throwable> throwable = new AtomicReference<>();
		List<Thread> threads = new ArrayList<>();
		for (int i = 0; i < threadCount; i++) {
			Runnable worker = () -> {
				try {
					while (true) {
						Runnable task = queue.poll();
						if (task == null)
							break;

						if (Thread.interrupted())
							return;

						try {
							task.run();
						} catch (Throwable t) {
							throwable.compareAndSet(null, t);
							while (latch.getCount() > 0)
								latch.countDown();
							return;
						}

					}
				} finally {
					latch.countDown();
				}
			};
			Thread thread = new Thread(worker);
			threads.add(thread);
			thread.start();
		}

		try {
			latch.await();
		} catch (InterruptedException e) {
			threads.forEach(Thread::interrupt);
			throw e;
		}

		Throwable t = throwable.get();
		if (t instanceof Error)
			throw (Error) t;
		else if (t instanceof RuntimeException)
			throw (RuntimeException) t;
		else if (t instanceof Exception)
			throw new IllegalStateException(t);
	}
}
