package hr.hashcode.cables.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.GZIPInputStream;

public class FileContents {

	private FileContents() {}

	public static byte[] contents(Path path) throws IOException {
		byte[] original = Files.readAllBytes(path);
		try (InputStream input = new ByteArrayInputStream(original);
				InputStream gzip = new GZIPInputStream(input)) {
			return Util.drain(gzip);
		} catch (IOException e) {
			return original;
		}
	}
}
