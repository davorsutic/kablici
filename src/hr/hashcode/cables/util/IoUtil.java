package hr.hashcode.cables.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class IoUtil {

	private IoUtil() {}

	private static final int BUFFER_SIZE = 1 << 13;

	public static byte[] drain(InputStream input) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		byte[] buffer = new byte[BUFFER_SIZE];
		while (true) {
			int read = input.read(buffer);
			if (read == -1)
				return output.toByteArray();
			output.write(buffer, 0, read);
		}
	}

	public static byte[] contents(File file) throws IOException {
		byte[] bytes;
		try (InputStream input = new FileInputStream(file)) {
			bytes = drain(input);
		}

		try (InputStream arrayStream = new ByteArrayInputStream(bytes);
				GZIPInputStream gzip = new GZIPInputStream(arrayStream)) {
			return drain(gzip);
		} catch (IOException e) {
			return bytes;
		}
	}

}
