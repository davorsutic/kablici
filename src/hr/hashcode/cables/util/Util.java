package hr.hashcode.cables.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Util {

	public static <T> Set<T> identitySet(Collection<T> collection) {
		Set<T> set = identitySet();
		set.addAll(collection);
		return set;
	}

	public static <T> Set<T> identitySet() {
		return Collections.newSetFromMap(new IdentityHashMap<>());
	}

	public static <T> T[] toArray(Collection<T> collection, Class<T> clazz) {
		if (clazz.isPrimitive())
			throw new IllegalArgumentException(clazz.toString());
		@SuppressWarnings("unchecked")
		T[] array = (T[]) Array.newInstance(clazz, collection.size());
		return collection.toArray(array);
	}

	public static <T> List<T> toList(T[] array) {
		return new ArrayList<>(Arrays.asList(array));
	}

	public static byte[] drain(InputStream input) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		byte[] buffer = new byte[1 << 13];
		while (true) {
			int read = input.read(buffer);
			if (read == -1)
				return output.toByteArray();
			output.write(buffer, 0, read);
		}
	}

	public static <T, V> List<V> map(Collection<T> collection, Function<T, V> function) {
		return collection.stream()
				.map(function)
				.collect(Collectors.toList());
	}

	public static <T> List<T> filter(Collection<T> collection, Predicate<T> filter) {
		return collection.stream()
				.filter(filter)
				.collect(Collectors.toList());
	}

	public static <K, V> Map<K, V> toMap(Collection<V> collection, Function<V, K> key) {
		return collection.stream().collect(Collectors.toMap(key, x -> x));
	}

	public static <K, V> Map<K, List<V>> group(Collection<V> collection, Function<V, K> key) {
		return collection.stream().collect(Collectors.groupingBy(key));
	}

	public static <T> Set<T> intersection(Collection<T> set1, Collection<T> set2){
		Set<T> set = new HashSet<>(set1);
		set.retainAll(set2);
		return set;
	}

	private static final Pattern pattern = Pattern.compile("\r?\n");

	public static List<String> lines(String string) {
		return Arrays.asList(pattern.split(string, -1));
	}

}
