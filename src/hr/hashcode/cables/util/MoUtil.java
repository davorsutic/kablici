package hr.hashcode.cables.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.NodeState.ManagedObject;

public class MoUtil {

	private MoUtil() {}

	public static Map<MoClass, List<ManagedObject>> groupByClass(Collection<ManagedObject> objects) {
		Map<MoClass, List<ManagedObject>> result = new IdentityHashMap<>();
		for (ManagedObject obj : objects)
			result.computeIfAbsent(obj.moClass, x -> new ArrayList<>()).add(obj);
		return result;
	}

	public static List<MoClass> nameSort(Collection<MoClass> classes) {
		return classes.stream()
				.sorted(Comparator.comparing(MoClass::name)
						.thenComparing(x -> x.mim.name))
				.collect(Collectors.toList());
	}

	public static List<ManagedObject> ldnSort(Collection<ManagedObject> objects) {
		return objects.stream()
				.sorted(Comparator.comparing(ManagedObject::ldn))
				.collect(Collectors.toList());
	}

	public static String rdn(NodeState state) {
		String fdn = state.fdn;
		String rdn;
		if (fdn != null) {
			int index = fdn.lastIndexOf('=');
			rdn = fdn.substring(index + 1);
		} else
			rdn = null;
		return rdn;
	}

	public static List<ManagedObject> suffixSort(Collection<ManagedObject> objects) {
		return objects.stream()
				.map(SuffixedString::new)
				.sorted().map(x -> x.object)
				.collect(Collectors.toList());
	}

	private static class SuffixedString implements Comparable<SuffixedString> {

		private final String prefix;
		private final int suffix;
		private final ManagedObject object;

		SuffixedString(ManagedObject object) {
			this.object = object;
			String ldn = object.ldn();
			int index = ldn.length();
			while (index > 0 && Character.isDigit(ldn.charAt(index - 1))) {
				index--;
			}
			if (index == ldn.length() || ldn.length() - index > 3) {
				this.prefix = ldn;
				this.suffix = 0;
			} else {
				this.prefix = ldn.substring(0, index);
				this.suffix = Integer.parseInt(ldn.substring(index));
			}
		}

		@Override
		public int compareTo(SuffixedString o) {
			int cmp = prefix.compareTo(o.prefix);
			if (cmp == 0)
				cmp = Integer.compare(suffix, o.suffix);
			return cmp;
		}

	}

}
