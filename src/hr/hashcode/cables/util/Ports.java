package hr.hashcode.cables.util;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Ports {

	private Ports() {}

	public static final int ssh = 22;
	public static final int http = 80;
	public static final int ssl = 443;
	public static final int netconf = 830;
	public static final int ncli = 2023;
	public static final int coli = 4192;
	public static final int corba = 56834;

	private static final int delay = 3000;

	private static boolean checkPort(String ip, int port) {
		try (Socket socket = new Socket()) {
			new Thread(() -> {
				try {
					Thread.sleep(delay);
					socket.close();
				} catch (IOException | InterruptedException e) {}
			}).start();
			socket.connect(new InetSocketAddress(ip, port));
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public static boolean ssh(String ip) {
		return checkPort(ip, ssh);
	}

	public static boolean http(String ip) {
		return checkPort(ip, http);
	}

	public static boolean ssl(String ip) {
		return checkPort(ip, ssl);
	}

	public static boolean netconf(String ip) {
		return checkPort(ip, netconf);
	}

	public static boolean ncli(String ip) {
		return checkPort(ip, ncli);
	}

	public static boolean coli(String ip) {
		return checkPort(ip, coli);
	}

	public static boolean corba(String ip) {
		return checkPort(ip, corba);
	}

}
