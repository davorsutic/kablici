package hr.hashcode.cables.visual.conn.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.RemoteRadioUnit;
import hr.hashcode.cables.equip.comp.SiteContext;
import hr.hashcode.cables.visual.conn.DragClickListener;
import hr.hashcode.cables.visual.huge.Controller;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import hr.hashcode.cables.visual.huge.units.DigitalUnitController;
import hr.hashcode.cables.visual.huge.units.ExternalRadioUnitController;
import hr.hashcode.cables.visual.huge.units.R503Controller;
import hr.hashcode.cables.visual.huge.units.RadioUnitController;
import hr.hashcode.cables.visual.huge.units.UnitVisualController;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

/**
 * @author Fran Stanic
 */
public class Model {

	public enum VisibilityState {
		SHOW,
		FADE,
		NONE
	}

	private Map<Class<? extends UnitVisualController>, List<UnitVisualController>> units = new HashMap<>();
	private SiteContext siteContext;
	private Pane unitPane;

	public Model(SiteContext siteContext, Pane unitPane) {
		this.siteContext = siteContext;
		this.unitPane = unitPane;

		//create units
		Map<Band, List<RemoteRadioUnit>> groupedRadios = siteContext.getGroupedExternalRadios();
		groupedRadios.values().forEach(radioList -> addUnit(new ExternalRadioUnitController(radioList)));
		siteContext.getInternalComponents().forEach(component -> addUnit(createController(component)));
	}

	private UnitVisualController createController(Component unit) {
		UnitVisualController unitVisualController = null;
		if (unit instanceof DigitalUnit) {
			unitVisualController = new DigitalUnitController((DigitalUnit) unit);
		} else if (unit instanceof R503) {
			unitVisualController = new R503Controller((R503) unit);
		} else if (unit instanceof RadioUnitBase){
			unitVisualController = new RadioUnitController((RadioUnitBase) unit);
			unitVisualController.getView().setRotate(-90);
		}
		return unitVisualController;
	}

	public void setVisibilityState(UnitVisualController controller, VisibilityState visibilityState) {
		double opacity;
		if (visibilityState == VisibilityState.SHOW) {
			opacity = 1;
		} else if (visibilityState == VisibilityState.FADE) {
			opacity = 0.15;
		} else {
			opacity = 0;
		}
		controller.getView().setOpacity(opacity);
	}

	private void addUnit(UnitVisualController unit) {
		Controller.setBorder((Region) unit.getView(), RbsVisual.BORDER_WIDTH);
		new DragClickListener<>(unit, null, u -> u.getView().setRotate(u.getView().getRotate() + 90));
		unitPane.getChildren().add(unit.view);
		units.computeIfAbsent(unit.getClass(), c -> new ArrayList<>()).add(unit);
	}

	public <T extends UnitVisualController> List<T> getUnits(Class<T> cls) {
		return (List<T>) units.computeIfAbsent(cls, c -> new ArrayList<>());
	}

	public List<Band> getCoveredBands(UnitVisualController controller) {
		return siteContext.getCoveredBandsFor(controller.getUnit());
	}

	public Map<Band, List<RadioUnitController>> getRadiosGroupedByBand() {
		List<RadioUnitController> controllers = getUnits(RadioUnitController.class);
		Map<Band, List<RadioUnitController>> map = new HashMap<>();
		for (RadioUnitController controller : controllers) {
			Band band = getCoveredBands(controller).get(0);
			map.computeIfAbsent(band, b -> new ArrayList<>()).add(controller);
		}
		return map;
	}

	public List<UnitVisualController> getUnits() {
		return units.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
	}

}
