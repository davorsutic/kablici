package hr.hashcode.cables.visual.conn.model;

import hr.hashcode.cables.visual.utility.ViewUtility;
import hr.hashcode.cables.visual.huge.CableVisual;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.layout.Region;
import javafx.scene.shape.Line;

/**
 * @author Fran Stanic
 */
public class StretchableCable extends Line {

	private CableVisual cable;

	public StretchableCable(CableVisual cable) {
		this.cable = cable;
		cable.port1.getView().localToSceneTransformProperty().addListener((observable, oldValue, newValue) -> {
			if (getParent() == null) {
				return;
			}
			setStart(ViewUtility.convertCenter((Region) cable.port1.getView(), getParent()));
		});

		cable.port2.getView().localToSceneTransformProperty().addListener((observable, oldValue, newValue) -> {
			if (getParent() == null) {
				return;
			}
			setEnd(ViewUtility.convertCenter((Region) cable.port2.getView(), getParent()));
		});

		parentProperty().addListener(new ChangeListener<Parent>() {
			@Override
			public void changed(ObservableValue<? extends Parent> observable, Parent oldValue, Parent newValue) {
				setStart(ViewUtility.convertCenter((Region) cable.port1.getView(), getParent()));
				setEnd(ViewUtility.convertCenter((Region) cable.port2.getView(), getParent()));
				parentProperty().removeListener(this);
			}
		});
	}

	private void setStart(Point2D start) {
		if (start == null) {
			return;
		}
		setStartX(start.getX());
		setStartY(start.getY());
	}

	private void setEnd(Point2D end) {
		if (end == null) {
			return;
		}
		setEndX(end.getX());
		setEndY(end.getY());
	}

	public CableVisual getCable() {
		return cable;
	}
}
