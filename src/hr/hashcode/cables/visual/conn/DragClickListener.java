package hr.hashcode.cables.visual.conn;

import java.util.function.Consumer;

import hr.hashcode.cables.visual.huge.Controller;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 * @author Fran Stanic
 */
public class DragClickListener<T extends Controller> {

	private T controller;
	private Point2D downLocation;
	private Point2D originalTranslate;
	private boolean clickPossible;

	private Point2D getPoint(MouseEvent mouseEvent) {
		Point2D p = new Point2D(mouseEvent.getX(), mouseEvent.getY());
		return controller.getView().localToParent(p);
	}

	public DragClickListener(T controller, Consumer<T> leftClickConsumer, Consumer<T> rightClickConsumer) {
		this.controller = controller;
		Node node = controller.getView();
		node.setOnMousePressed(mouseEvent -> {
			downLocation = getPoint(mouseEvent);
			originalTranslate = new Point2D(node.getTranslateX(), node.getTranslateY());
			clickPossible = true;
		});
		node.setOnMouseDragged(mouseEvent -> {
			mouseEvent.consume();
			Point2D screen = getPoint(mouseEvent);
			Point2D newTranslate = originalTranslate.add(screen.subtract(downLocation));
			double x = Math.max(newTranslate.getX(), -node.getLayoutX());
			double y = Math.max(newTranslate.getY(), -node.getLayoutY());
			node.setTranslateX(x);
			node.setTranslateY(y);
			if (screen.distance(downLocation) > 10) {
				clickPossible = false;
			}
		});

		node.setOnMouseReleased(event -> {
			if (clickPossible) {
				if (event.getButton() == MouseButton.PRIMARY) {
					if (leftClickConsumer != null) {
						leftClickConsumer.accept(controller);
					}
				} else {
					if (rightClickConsumer != null) {
						rightClickConsumer.accept(controller);
					}
				}
			}
		});
	}
}
