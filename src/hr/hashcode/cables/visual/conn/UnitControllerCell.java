package hr.hashcode.cables.visual.conn;

import java.util.List;
import java.util.Objects;

import hr.hashcode.cables.visual.conn.model.Model;
import hr.hashcode.cables.visual.huge.units.UnitVisualController;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListCell;

/**
 * @author Fran Stanic
 */
public final class UnitControllerCell extends ListCell<UnitControllerCell.Element> {

	public static class Element {
		List<? extends UnitVisualController> units;
		String name;
		UnitControllerCell unitControllerCell;
		UnitVisualController.Type type;

		public UnitVisualController.Type getType() {
			return type;
		}

		public void setVisibilityState(Model.VisibilityState visibilityState) {
			if (visibilityState == Model.VisibilityState.FADE) {
				unitControllerCell.checkBox.setIndeterminate(true);
			} else {
				unitControllerCell.checkBox.setIndeterminate(false);
				if (visibilityState == Model.VisibilityState.SHOW) {
					unitControllerCell.checkBox.setSelected(true);
				} else {
					unitControllerCell.checkBox.setSelected(false);
				}
			}
			units.forEach(u -> unitControllerCell.model.setVisibilityState(u, visibilityState));
		}

		public Element(List<? extends UnitVisualController> units, String name, UnitVisualController.Type type) {
			this.units = units;
			this.name = name;
			this.type = type;
		}
	}

	private Model model;
	private Element element;
	private CheckBox checkBox = new CheckBox();

	public UnitControllerCell(Model model) {
		this.model = model;
		checkBox.setSelected(true);
		checkBox.setAllowIndeterminate(true);
		checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (Objects.equals(newValue, true)) {
				element.setVisibilityState(Model.VisibilityState.SHOW);
			} else if (Objects.equals(newValue, false)) {
				element.setVisibilityState(Model.VisibilityState.NONE);
			}
		});

		checkBox.indeterminateProperty().addListener((observable, oldValue, newValue) -> {
			if (Objects.equals(newValue, true)) {
				element.setVisibilityState(Model.VisibilityState.FADE);
			}
		});
	}

	@Override
	protected void updateItem(UnitControllerCell.Element item, boolean empty) {
		super.updateItem(item, empty);
		this.element = item;
		if (empty) {
			setGraphic(null);
		} else {
			element.unitControllerCell = this;
			setText(item.name);
			setGraphic(checkBox);
		}
	}
}
