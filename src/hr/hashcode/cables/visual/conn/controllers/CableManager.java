package hr.hashcode.cables.visual.conn.controllers;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.Port;
import hr.hashcode.cables.visual.conn.model.Model;
import hr.hashcode.cables.visual.conn.model.StretchableCable;
import hr.hashcode.cables.visual.huge.CableVisual;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import hr.hashcode.cables.visual.huge.units.PortController;
import hr.hashcode.cables.visual.huge.units.UnitVisualController;
import javafx.beans.binding.Bindings;
import javafx.scene.Group;

/**
 * @author Fran Stanic
 */
public class CableManager {

	private final Group cablePane;
	private Model model;
	private List<StretchableCable> cables = new ArrayList<>();

	public CableManager(Group cablePane, Model model) {
		this.model = model;
		this.cablePane = cablePane;
	}

	public <T extends Port> void cable(List<Connector<T>> connectors) {
		Map<Port, PortController> map = new IdentityHashMap<>();
		model.getUnits().forEach(x -> map.putAll(x.getPortNodes()));

		for (Connector<T> connector : connectors) {
			PortController first = map.get(connector.first());
			PortController second = map.get(connector.second());
			if (first == null || second == null) {
				throw new IllegalStateException();
			}
			UnitVisualController u1 = first.getUnit();
			UnitVisualController u2 = second.getUnit();
			StretchableCable stretchableCable = new StretchableCable(new CableVisual(first, second));
			stretchableCable.setStrokeWidth(RbsVisual.BORDER_WIDTH);

			stretchableCable.opacityProperty().bind(Bindings.createDoubleBinding(new Callable<Double>() {
				@Override
				public Double call() throws Exception {
					return Math.min(u1.getView().getOpacity(), u2.getView().getOpacity());
				}
			}, u1.getView().opacityProperty(), u2.getView().opacityProperty()));

			cablePane.getChildren().add(stretchableCable);
			cables.add(stretchableCable);
		}
	}

}
