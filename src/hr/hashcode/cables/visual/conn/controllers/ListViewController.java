package hr.hashcode.cables.visual.conn.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.visual.conn.UnitControllerCell;
import hr.hashcode.cables.visual.conn.model.Model;
import hr.hashcode.cables.visual.huge.units.RadioUnitController;
import hr.hashcode.cables.visual.huge.units.UnitVisualController;
import javafx.collections.FXCollections;
import javafx.scene.control.ListView;

/**
 * @author Fran Stanic
 */
public class ListViewController {

	private final Model model;
	private final ListView<UnitControllerCell.Element> listView;

	public ListViewController(Model model, ListView<UnitControllerCell.Element> listView) {
		this.listView = listView;
		this.model = model;

		listView.setCellFactory(l -> new UnitControllerCell(model));
	}

	public void setStateOfAllElements(Model.VisibilityState visibilityState) {
		for (UnitControllerCell.Element element : listView.getItems()) {
			element.setVisibilityState(visibilityState);
		}
	}

	private UnitControllerCell.Element createElement(List<? extends UnitVisualController> controllers) {
		UnitVisualController controller = controllers.get(0);
		List<Band> coveredBands = model.getCoveredBands(controller);
		String name = controller.getName() + " " + coveredBands;
		return new UnitControllerCell.Element(controllers, name, controller.getType());
	}

	public void configure() {
		List<UnitControllerCell.Element> elements = new ArrayList<>();
		Map<Band, List<RadioUnitController>> radios = model.getRadiosGroupedByBand();
		for (UnitVisualController u : model.getUnits()) {
			if (!(u instanceof RadioUnitController)) {
				elements.add(createElement(Collections.singletonList(u)));
			}
		}

		radios.forEach((band, controllers) -> {
			elements.add(createElement(controllers));
		});

		elements.sort(Comparator.comparingInt(e -> e.getType().ordinal()));
		listView.setItems(FXCollections.observableArrayList(elements));
	}

}
