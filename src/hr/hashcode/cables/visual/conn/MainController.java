package hr.hashcode.cables.visual.conn;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.comp.Site;
import hr.hashcode.cables.equip.comp.SiteContext;
import hr.hashcode.cables.visual.conn.controllers.CableManager;
import hr.hashcode.cables.visual.conn.controllers.ListViewController;
import hr.hashcode.cables.visual.conn.model.Model;
import hr.hashcode.cables.visual.huge.Controller;
import hr.hashcode.cables.visual.huge.units.DigitalUnitController;
import hr.hashcode.cables.visual.huge.units.ExternalRadioUnitController;
import hr.hashcode.cables.visual.huge.units.R503Controller;
import hr.hashcode.cables.visual.huge.units.RadioUnitController;
import hr.hashcode.cables.visual.huge.units.UnitVisualController;
import hr.hashcode.cables.visual.utility.ViewUtility;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

/**
 * @author Fran Stanic
 */
public class MainController extends Controller {

	private HBox view;

	@FXML
	private Button selectAllButton;

	@FXML
	private Button deselectAllButton;

	@FXML
	private ListView<UnitControllerCell.Element> componentListView;

	@FXML
	private ScrollPane scrollPane;

	@FXML
	private StackPane scrollPaneContent;

	@FXML
	private Group group;

	@FXML
	private Group scrollPaneGroup;

	@FXML
	private Group cablePane;

	@FXML
	private Pane unitPane;

	@FXML
	private Button zoomToFitButton;

	private final CableManager cableManager;

	private final ListViewController listViewController;

	private Model model;

	@Override
	public Pane getView() {
		return view;
	}

	private void loadFromFXML() {
		URL resource = MainController.class.getResource("main_controller.fxml");
		FXMLLoader loader = new FXMLLoader(resource);
		loader.setController(this);
		loader.setRoot(view);
		try {
			loader.load();
		} catch (IOException ex) {
			// throw as cause of RuntimeException
			throw new RuntimeException(ex);
		}
		ViewUtility.makePannableZoomableScrollPane(scrollPane, scrollPaneContent, group, scrollPaneGroup);
		zoomToFitButton.setOnAction(m -> ViewUtility.zoomToFit(scrollPane, scrollPaneGroup));

		selectAllButton.setOnAction(c -> listViewController.setStateOfAllElements(Model.VisibilityState.SHOW));
		deselectAllButton.setOnAction(c -> listViewController.setStateOfAllElements(Model.VisibilityState.NONE));
	}

	public MainController(Site site) {
		view = new HBox();
		loadFromFXML();

		SiteContext siteContext = SiteContext.from(site);
		model = new Model(siteContext, unitPane);
		cableManager = new CableManager(cablePane, model);
		listViewController = new ListViewController(model, componentListView);

		cableManager.cable(site.riConnectors);
		listViewController.configure();
		Platform.runLater(() -> {
			layoutCabinet();
			Platform.runLater(() -> ViewUtility.zoomToFit(scrollPane, scrollPaneGroup));
		});
	}

	private void layoutCabinet() {
		double x = 500;
		double y = 100;

		List<UnitVisualController> external = model.getUnits().stream()
													  .filter(u -> u instanceof ExternalRadioUnitController)
													  .collect(Collectors.toList());

		for (UnitVisualController r : external) {
			Bounds bounds = r.getView().getBoundsInParent();
			r.getView().setTranslateX(x);
			r.getView().setTranslateY(y);
			x += bounds.getWidth();
		}

		List<UnitVisualController> radios = model.getUnits().stream().filter(u -> u instanceof RadioUnitController)
													.collect(Collectors.toList());

		x = 100;
		y += 700;

		for (UnitVisualController r : radios) {
			Bounds bounds = r.getView().getBoundsInParent();
			r.getView().setTranslateX(x);
			r.getView().setTranslateY(y);
			x += bounds.getWidth();
		}

		x = 500;
		y += 400;

		List<UnitVisualController> digital = model.getUnits().stream()
													 .filter(u -> u instanceof DigitalUnitController || u instanceof R503Controller)
													 .collect(Collectors.toList());

		for (UnitVisualController r : digital) {
			r.getView().setTranslateX(x);
			r.getView().setTranslateY(y);
			y += 150;
		}

	}

}
