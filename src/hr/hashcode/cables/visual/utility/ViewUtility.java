package hr.hashcode.cables.visual.utility;

import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * @author Fran Stanic
 */
public class ViewUtility {

	public static Point2D getScreenCenter(Region region) {
		double cX = region.getLayoutBounds().getMinX() + region.getLayoutBounds().getWidth() / 2;
		double cY = region.getLayoutBounds().getMinY() + region.getLayoutBounds().getHeight() / 2;
		Point2D center = new Point2D(cX, cY);
		return region.localToScreen(center);
	}

	public static Point2D convertCenter(Region from, Node to) {
		Point2D center = getScreenCenter(from);
		if (center == null) {
			return null;
		}
		return to.screenToLocal(center);
	}

	private static class Separator extends Pane {

	}

	public static void createSeparators(Pane parent, double separatorSize) {
		parent.getChildren().removeIf(n -> n instanceof Separator);
		boolean isVbox = parent instanceof VBox;
		int size = parent.getChildren().size();
		for (int i = 0; i < size - 1; i++) {
			boolean managed = parent.getChildren().get(2 * i + 1).isManaged();
			if (managed) {
				Pane p = new Separator();
				if (isVbox) {
					p.setPrefSize(-1, separatorSize);
				} else {
					p.setPrefSize(separatorSize, -1);
				}
				p.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
				parent.getChildren().add(2 * i + 1, p);
			}
		}
	}

	private static double getZoomToFit(ScrollPane scrollPane, Node zoomTarget) {
		Bounds b = scrollPane.getViewportBounds();
		Bounds cb = zoomTarget.getBoundsInLocal();
		double zX = b.getWidth() / cb.getWidth();
		double zY = b.getHeight() / cb.getHeight();
		return Math.min(zX, zY);
	}

	public static void zoomToFit(ScrollPane scrollPane, Node zoomTarget) {
		double zoom = getZoomToFit(scrollPane, zoomTarget);
		zoomTarget.setScaleX(zoom);
		zoomTarget.setScaleY(zoom);
	}

	public static void makePannableZoomableScrollPane(ScrollPane scrollPane, Pane scrollPaneContent,
															 Group groupWrappingActualContent, Node zoomTarget) {

		groupWrappingActualContent.layoutBoundsProperty().addListener((observable, oldBounds, newBounds) -> {
			// keep it at least as large as the content
			scrollPaneContent.setMinWidth(newBounds.getWidth());
			scrollPaneContent.setMinHeight(newBounds.getHeight());
		});

		scrollPane.addEventFilter(ScrollEvent.ANY, new EventHandler<ScrollEvent>() {
			@Override
			public void handle(ScrollEvent evt) {
				if (evt.isControlDown()) {
					evt.consume();

					final double zoomFactor = evt.getDeltaY() > 0 ? 1.05 : 1 / 1.05;
					double minZoom = getZoomToFit(scrollPane, zoomTarget);
					double scaleX =
							zoomFactor * zoomTarget.getScaleX();// Math.min(Math.max(zoomFactor * zoomTarget.getScaleX(), 0.4), 2.5);
					double scaleY =
							zoomFactor * zoomTarget.getScaleY();// Math.min(Math.max(zoomFactor * zoomTarget.getScaleY(), 0.4), 2.5);
					if (scaleX < minZoom && zoomFactor < 1) {
						// return;
					}
					Bounds groupBounds = groupWrappingActualContent.getLayoutBounds();
					final Bounds viewportBounds = scrollPane.getViewportBounds();

					// calculate pixel offsets from [0, 1] range
					double valX = scrollPane.getHvalue() * (groupBounds.getWidth() - viewportBounds.getWidth());
					double valY = scrollPane.getVvalue() * (groupBounds.getHeight() - viewportBounds.getHeight());

					Point2D scene = scrollPane.localToScene(new Point2D(evt.getX(), evt.getY()));
					Point2D local = zoomTarget.sceneToLocal(scene);

					// calculate adjustment of scroll position (pixels)
					Point2D adjustment = zoomTarget.getLocalToParentTransform().deltaTransform(local.multiply(zoomFactor - 1));

					// do the resizing
					zoomTarget.setScaleX(scaleX);
					zoomTarget.setScaleY(scaleY);

					// refresh ScrollPane scroll positions & content bounds
					scrollPane.layout();

					// convert back to [0, 1] range
					// (too large/small values are automatically corrected by ScrollPane)
					groupBounds = groupWrappingActualContent.getLayoutBounds();
					scrollPane.setHvalue((valX + adjustment.getX()) / (groupBounds.getWidth() - viewportBounds.getWidth()));
					scrollPane.setVvalue((valY + adjustment.getY()) / (groupBounds.getHeight() - viewportBounds.getHeight()));
				}
			}
		});

	}
}
