package hr.hashcode.cables.visual.site;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.comp.Antenna;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.EcPort;
import hr.hashcode.cables.equip.comp.ExtensionRack;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.MiniRbs;
import hr.hashcode.cables.equip.comp.Port;
import hr.hashcode.cables.equip.comp.PortSet;
import hr.hashcode.cables.equip.comp.PowerDistributionUnit;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnit;
import hr.hashcode.cables.equip.comp.RemoteRadioUnit;
import hr.hashcode.cables.equip.comp.Site;
import hr.hashcode.cables.equip.comp.Site.UnitQuantity;
import hr.hashcode.cables.equip.comp.Subrack;
import hr.hashcode.cables.equip.comp.SupportControlUnit;
import hr.hashcode.cables.equip.comp.SupportHubUnit;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;

public class SiteVisual extends VBox {

	private static CheckBox unchecked(String label) {
		CheckBox box = new CheckBox(label);
		box.setSelected(false);
		return box;
	}

	public SiteVisual(Site site) {

		CheckBox idl = unchecked("IDL cables");
		CheckBox ec = unchecked("EC cables");
		CheckBox ri = unchecked("RI cables");
		CheckBox rf = unchecked("RF cables");
		CheckBox dc = unchecked("DC cables");
		VBox choices = new VBox(dc, ec, ri, rf, idl);

		ObservableList<UnitQuantity> quantList = FXCollections.observableArrayList(site.inventory.quantities());
		TableView<UnitQuantity> quantities = new TableView<>(quantList);
		TableColumn<UnitQuantity, String> name = new TableColumn<>("Name");
		name.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().name()));
		TableColumn<UnitQuantity, Integer> count = new TableColumn<>("Count");
		count.setCellValueFactory(x -> new SimpleObjectProperty<>(x.getValue().count()));
		quantities.getColumns().add(name);
		quantities.getColumns().add(count);

		VBox controls = new VBox(choices, quantities);

		HBox antennas = new HBox();
		for (Antenna antenna : site.antennas)
			antennas.getChildren().add(new AntennaVisual(antenna));

		HBox remotes = new HBox();
		for (RemoteRadioUnit radio : site.remotes) {
			remotes.getChildren().add(rotate(new RemoteRadioUnitVisual(radio)));
		}

		HBox rbses = new HBox();
		rbses.setSpacing(50);
		for (MiniRbs rbs : site.miniRbses)
			rbses.getChildren().add(process(rbs));
		for (MacroRbs rbs : site.rbses)
			rbses.getChildren().add(process(rbs));
		VBox units = new VBox(antennas, remotes, rbses);

		DoubleProperty zoom = new SimpleDoubleProperty(1);
		Button zoomIn = new Button("Zoom in");
		zoomIn.setOnAction(e -> zoom.set(zoom.get() + 0.1));
		Button zoomOut = new Button("Zoom out");
		zoomOut.setOnAction(e -> zoom.set(zoom.get() - 0.1));
		HBox buttons = new HBox(zoomIn, zoomOut);

		Group group = new Group();
		group.scaleXProperty().bind(zoom);
		group.scaleYProperty().bind(zoom);
		group.scaleZProperty().bind(zoom);
		group.getChildren().add(units);

		Pane pane = new Pane(group);

		ScrollPane scrollPane = new ScrollPane(pane);
		VBox contents = new VBox(buttons, scrollPane);

		Node ref = units;

		List<Shape> shapes = new ArrayList<>();
		connect(site.dcConnectors, ref, shapes, dc.selectedProperty());
		connect(site.ecConnectors, ref, shapes, ec.selectedProperty());
		connect(site.idlConnectors, ref, shapes, idl.selectedProperty());
		connect(site.riConnectors, ref, shapes, ri.selectedProperty());
		connect(site.rfConnectors, ref, shapes, rf.selectedProperty());
		group.getChildren().addAll(shapes);

		SplitPane split = new SplitPane(controls, scrollPane);
		split.setDividerPositions(0.05);

		getChildren().addAll(split);
	}

	private void connect(List<? extends Connector<?>> list, Node ref, List<Shape> shapes, BooleanProperty show) {
		for (Connector<?> conn : list) {
			Shape shape = connect(ref, conn);
			shape.strokeWidthProperty().bind(Bindings.when(show).then(1).otherwise(0));
			shape.setStroke(Color.RED);
			shapes.add(shape);
		}
	}

	private final Map<Port, Label> portNodes = new IdentityHashMap<>();

	private Line connect(Node ref, Connector<?> connector) {
		Node node1 = portNodes.get(connector.first());
		Node node2 = portNodes.get(connector.second());
		if (node1 == null || node2 == null)
			throw new IllegalArgumentException();

		ObjectBinding<Bounds> bounds1 = bounds(node1, ref);
		ObjectBinding<Bounds> bounds2 = bounds(node2, ref);

		Line line = new Line();
		line.startXProperty().bind(x(bounds1));
		line.startYProperty().bind(y(bounds1));
		line.endXProperty().bind(x(bounds2));
		line.endYProperty().bind(y(bounds2));
		return line;
	}

	private static ObjectBinding<Bounds> bounds(Node node, Node ref) {
		ReadOnlyObjectProperty<Bounds> bounds = node.boundsInLocalProperty();
		return Bindings.createObjectBinding(() -> {
			return ref.sceneToLocal(node.localToScene(bounds.get()));
		}, bounds);
	}

	private static DoubleBinding x(ObservableValue<Bounds> bounds) {
		return Bindings.createDoubleBinding(() -> (bounds.getValue().getMaxX() + bounds.getValue().getMinX()) / 2,
				bounds);
	}

	private static DoubleBinding y(ObservableValue<Bounds> bounds) {
		return Bindings.createDoubleBinding(() -> (bounds.getValue().getMaxY() + bounds.getValue().getMinY()) / 2,
				bounds);
	}

	private Region process(MacroRbs rbs) {
		switch (rbs.type()) {
			case "RBS6201":
				return new Rbs6201Visual(rbs);
			case "RBS6202":
				return new Rbs6202Visual(rbs);
			case "RBS6102":
				return new Rbs6102Visual(rbs);
			case "RBS6131":
				return new Rbs6131Visual(rbs);
			default:
				return new HBox();
		}
	}

	private Region process(MiniRbs rbs) {
		return new Rbs6601Visual(rbs);
	}

	private class Rbs6201Visual extends VBox {
		Rbs6201Visual(MacroRbs rbs) {
			List<PowerDistributionUnit> pdus = rbs.pdus();
			List<Subrack> subracks = rbs.subracks();
			List<ExtensionRack> extensions = rbs.extensions();
			getChildren().add(new ScuVisual(rbs.scu()));
			getChildren().add(new ShuVisual(rbs.shu()));
			getChildren().add(new PduVisual(pdus.get(0)));
			getChildren().add(new SubrackVisual(subracks.get(0)));
			getChildren().add(new PduVisual(pdus.get(1)));
			getChildren().add(new SubrackVisual(subracks.get(1)));
			getChildren().add(new ExtensionVisual(extensions.get(0)));
			setSpacing(10);
			setPadding(new Insets(10));
			setBorderStyle(this);
		}
	}

	private class Rbs6202Visual extends VBox {
		Rbs6202Visual(MacroRbs rbs) {
			List<PowerDistributionUnit> pdus = rbs.pdus();
			List<Subrack> subracks = rbs.subracks();
			List<ExtensionRack> extensions = rbs.extensions();
			getChildren().add(new ScuVisual(rbs.scu()));
			getChildren().add(new PduVisual(pdus.get(0)));
			getChildren().add(new SubrackVisual(subracks.get(0)));
			getChildren().add(new ExtensionVisual(extensions.get(0)));
			setSpacing(30);
			setPadding(new Insets(30));
			setBorderStyle(this);
		}
	}

	private class Rbs6102Visual extends VBox {

		Rbs6102Visual(MacroRbs rbs) {
			List<PowerDistributionUnit> pdus = rbs.pdus();
			List<Subrack> subracks = rbs.subracks();
			List<ExtensionRack> extensions = rbs.extensions();
			getChildren().add(new ShuVisual(rbs.shu()));

			VBox middle = new VBox();
			middle.getChildren().add(new ScuVisual(rbs.scu()));
			middle.getChildren().add(new PduVisual(pdus.get(2)));
			middle.getChildren().add(new PduVisual(pdus.get(1)));
			middle.getChildren().add(new PduVisual(pdus.get(0)));

			VBox left = new VBox();
			left.getChildren().add(new SubrackVisual(subracks.get(1)));
			left.getChildren().add(new ExtensionVisual(extensions.get(3)));
			left.getChildren().add(new ExtensionVisual(extensions.get(4)));
			left.getChildren().add(new ExtensionVisual(extensions.get(5)));

			VBox right = new VBox();
			right.getChildren().add(new SubrackVisual(subracks.get(0)));
			right.getChildren().add(new ExtensionVisual(extensions.get(0)));
			right.getChildren().add(new ExtensionVisual(extensions.get(1)));
			right.getChildren().add(new ExtensionVisual(extensions.get(2)));

			HBox subVisual = new HBox(left, middle, right);

			getChildren().add(subVisual);

			setSpacing(30);
			setPadding(new Insets(30));
			setBorderStyle(this);
		}

	}

	private class Rbs6131Visual extends GridPane {

		Rbs6131Visual(MacroRbs rbs) {
			List<PowerDistributionUnit> pdus = rbs.pdus();
			List<Subrack> subracks = rbs.subracks();
			List<ExtensionRack> extensions = rbs.extensions();
			add(new ShuVisual(rbs.shu()), 0, 0, 2, 1);

			VBox upper = new VBox();
			upper.getChildren().add(new ScuVisual(rbs.scu()));
			upper.getChildren().add(new PduVisual(pdus.get(1)));
			upper.getChildren().add(new PduVisual(pdus.get(0)));

			add(upper, 1, 1);

			add(rotate(new ExtensionVisual(extensions.get(0))), 0, 2);
			add(new SubrackVisual(subracks.get(0)), 1, 2);
			add(new SubrackVisual(subracks.get(1)), 1, 3);

			setHgap(30);
			setVgap(30);
			setPadding(new Insets(30));
			setBorderStyle(this);
		}
	}

	private class Rbs6601Visual extends HBox {

		Rbs6601Visual(MiniRbs rbs) {

			VBox left = new VBox();
			DigitalUnitWcdma duw = rbs.duw();
			if (duw != null) {
				DigitalUnitWcdmaVisual duwVisual = new DigitalUnitWcdmaVisual(duw);
				setBorderStyle(duwVisual);
				left.getChildren().add(duwVisual);
			} else {
				VBox units = new VBox();
				for (int i = 0; i < 2; i++) {
					Region region;
					DigitalUnit du = rbs.du(i);
					if (du != null)
						region = new DigitalUnitVisual(du);
					else {
						R503 r503 = rbs.r503(i);
						if (r503 != null)
							region = new R503Visual(r503);
						else
							region = new Dummy();
					}
					units.getChildren().add(region);
					setBorderStyle(region);
				}
				left.getChildren().add(units);
			}

			GridPane ecPane = new GridPane();
			List<EcPort> ecPorts = rbs.ecPorts().ports();

			ecPane.add(port(ecPorts.get(2)), 0, 0);
			ecPane.add(port(ecPorts.get(0)), 0, 1);
			ecPane.add(port(ecPorts.get(1)), 1, 1);

			HBox dc = ports(rbs.dcPorts());
			VBox right = new VBox(ecPane, dc);

			getChildren().addAll(left, right);

			setBorderStyle(this);
		}

	}

	private class PduVisual extends HBox {

		PduVisual(PowerDistributionUnit pdu) {
			Label name = new Label(pdu.name());
			HBox dcPorts = ports(pdu.dcPorts());
			Label ecPort = port(pdu.ecPort());
			getChildren().addAll(name, dcPorts, ecPort);
			setSpacing(30);

			setBorderStyle(this);
		}
	}

	private static Node rotate(Node node) {
		node.setRotate(-90);
		return new HBox(new Group(node));
	}

	private class SubrackVisual extends HBox {

		SubrackVisual(Subrack subrack) {

			List<Region> nodes = new ArrayList<>();

			DigitalUnit unit = subrack.digital();
			Region unitVisual;
			if (unit instanceof DigitalUnitWcdma)
				unitVisual = new DigitalUnitWcdmaVisual((DigitalUnitWcdma) unit);
			else
				unitVisual = new Dummy();
			nodes.add(unitVisual);

			for (RadioUnit radio : subrack.radios()) {
				if (radio == null)
					nodes.add(new Dummy());
				else
					nodes.add(new RadioUnitVisual(radio));
			}

			for (Region node : nodes) {
				setBorderStyle(node);
				Node rotated = rotate(node);
				HBox.setHgrow(rotated, Priority.ALWAYS);
				getChildren().add(rotated);
			}
			setSpacing(5);

			setBorderStyle(this);
		}
	}

	private class DigitalUnitWcdmaVisual extends HBox {
		public DigitalUnitWcdmaVisual(DigitalUnitWcdma duw) {
			Label name = new Label(duw.name());
			HBox riPorts = ports(duw.riPorts());
			Label idlPort = port(duw.idlPort());
			Label ecPort = port(duw.ecPort());
			Label dcPort = port(duw.dcPort());
			getChildren().addAll(name, riPorts, idlPort, ecPort, dcPort);
			setSpacing(30);
		}
	}

	private class DigitalUnitVisual extends HBox {

		public DigitalUnitVisual(DigitalUnit du) {
			Label name = new Label(du.name());
			HBox riPorts = ports(du.riPorts());
			Label ecPort = port(du.ecPort());
			Label dcPort = port(du.dcPort());
			getChildren().addAll(name, riPorts, ecPort, dcPort);
			setSpacing(30);
		}
	}

	private class RadioUnitVisual extends HBox {
		public RadioUnitVisual(RadioUnit radio) {
			Label name = new Label(radio.name());
			HBox riPorts = ports(radio.riPorts());
			HBox rfPorts = ports(radio.rfPorts());
			Label dcPort = port(radio.dcPort());
			getChildren().addAll(name, riPorts, rfPorts, dcPort);
			setSpacing(30);
			setBorderStyle(this);
		}
	}

	private class RemoteRadioUnitVisual extends HBox {
		public RemoteRadioUnitVisual(RemoteRadioUnit radio) {
			Label name = new Label(radio.name());
			HBox riPorts = ports(radio.riPorts());
			HBox rfPorts = ports(radio.rfPorts());
			getChildren().addAll(name, riPorts, rfPorts);
			setSpacing(30);
			setBorderStyle(this);
		}
	}

	private class R503Visual extends HBox {

		R503Visual(R503 r503) {
			Label name = new Label(r503.name());

			HBox riPorts = new HBox();
			riPorts.getChildren().add(group(r503, 16, 4));
			riPorts.getChildren().add(group(r503, 12, 4));
			riPorts.getChildren().add(group(r503, 7, 4));
			riPorts.getChildren().add(group(r503, 3, 3));
			riPorts.setSpacing(10);

			Label ecPort = port(r503.ecPort());
			Label dcPort = port(r503.dcPort());
			getChildren().addAll(name, riPorts, dcPort, ecPort);

			setSpacing(30);
			setBorderStyle(this);
		}

		private HBox group(R503 r503, int start, int count) {
			HBox group = new HBox();
			for (int i = 0; i < count; i++)
				group.getChildren().add(port(r503.riPort(String.valueOf(start - i))));
			group.setSpacing(2);
			return group;
		}

	}

	private class ExtensionVisual extends VBox {

		ExtensionVisual(ExtensionRack extension) {

			for (int i = 0; i < extension.length(); i++) {
				Region region;
				DigitalUnit unit = extension.unit(String.valueOf(i + 1));
				if (unit != null)
					region = new DigitalUnitVisual(unit);
				else {
					R503 r503 = extension.r503(i);
					if (r503 != null)
						region = new R503Visual(r503);
					else
						region = new Dummy();
				}
				setBorderStyle(region);
				getChildren().add(region);
			}

			setSpacing(5);

			setBorderStyle(this);
		}
	}

	private static class Dummy extends HBox {

		Dummy() {
			super(new Label("Dummy"));
			HBox.setHgrow(this, Priority.ALWAYS);
		}

	}

	private class ShuVisual extends HBox {
		ShuVisual(SupportHubUnit shu) {
			Label label = new Label(shu.name());

			List<EcPort> aRow =
					shu.ecPorts().ports().stream().filter(x -> x.name().startsWith("A")).collect(Collectors.toList());
			List<EcPort> bRow =
					shu.ecPorts().ports().stream().filter(x -> x.name().startsWith("B")).collect(Collectors.toList());
			List<EcPort> others = new ArrayList<>(shu.ecPorts().ports());
			others.removeAll(aRow);
			others.removeAll(bRow);

			HBox aPorts = ports(aRow);
			HBox bPorts = ports(bRow);
			VBox rows = new VBox();
			rows.getChildren().addAll(aPorts, bPorts);
			if (!others.isEmpty())
				rows.getChildren().add(ports(others));

			getChildren().add(label);
			getChildren().add(rows);
			setBorderStyle(this);
			setSpacing(30);
		}
	}

	private class ScuVisual extends HBox {
		ScuVisual(SupportControlUnit scu) {
			Label label = new Label(scu.name());
			HBox ecPorts = ports(scu.ecPorts());
			HBox dcPorts = ports(scu.dcPorts());
			getChildren().addAll(label, ecPorts, dcPorts);
			setBorderStyle(this);
			setSpacing(30);
		}
	}

	private class AntennaVisual extends VBox {
		AntennaVisual(Antenna antenna) {
			Label label = new Label(antenna.name());

			HBox rfPorts = ports(antenna.rfPorts());
			getChildren().addAll(label, rfPorts);
			setBorderStyle(this);
		}
	}

	private HBox ports(PortSet<? extends Port> set) {
		return ports(set.ports());
	}

	private HBox ports(List<? extends Port> set) {
		HBox box = new HBox();
		for (Port port : set) {
			Label label = port(port);
			box.getChildren().add(label);
		}
		box.setSpacing(10);
		return box;
	}

	private Label port(Port port) {
		Label label = new Label(port.name());
		label.setAlignment(Pos.CENTER);
		// label.setMinWidth(20);
		// label.setMinHeight(20);
		setBorderStyle(label);
		portNodes.put(port, label);
		return label;
	}

	static void setBorderStyle(Region region) {
		region.setStyle("-fx-border-color: blue;" +
				"-fx-border-width: 1;");
	}

}
