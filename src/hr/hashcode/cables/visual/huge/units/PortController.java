package hr.hashcode.cables.visual.huge.units;

import hr.hashcode.cables.equip.comp.Port;
import hr.hashcode.cables.visual.huge.Controller;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;

/**
 * @author Fran Stanic
 */
public class PortController extends Controller {

	private UnitVisualController unit;
	private Port port;
	private Label label;

	public PortController(UnitVisualController unit, Port port, Point2D portSize) {
		this.unit = unit;
		this.port = port;
		label = new Label(port.name());
		label.setMinWidth(portSize.getX());
		label.setMinHeight(portSize.getY());
		label.setAlignment(Pos.CENTER);
		Controller.setBorder(label, RbsVisual.PORT_BORDER_WIDTH);
	}

	@Override
	public Node getView() {
		return label;
	}

	public Port getPort() {
		return port;
	}

	public UnitVisualController getUnit() {
		return unit;
	}
}
