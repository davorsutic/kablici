package hr.hashcode.cables.visual.huge.units;

import hr.hashcode.cables.equip.bind.JointNodesInfo;
import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * @author Fran Stanic
 */
public class HubSimpleController extends UnitVisualController {

	private JointNodesInfo.ExtendedHub hub;

	private Label nameLabel;

	public HubSimpleController(JointNodesInfo.ExtendedHub hub) {
		this.hub = hub;
		view = new VBox();

		nameLabel = new Label(hub.name);
		nameLabel.setFont(FONT);
		((VBox) view).setAlignment(Pos.CENTER);
		view.getChildren().add(nameLabel);

		setBackgroundColor(RbsVisual.SHU_COLOR);
	}

	@Override
	public Type getType() {
		return Type.HUB;
	}

	@Override
	public Component getUnit() {
		return hub;
	}

	@Override
	public String getName() {
		return hub.name;
	}

	@Override
	protected Pane getEcPortContainer() {
		return view;
	}
}
