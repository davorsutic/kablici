package hr.hashcode.cables.visual.huge.units;

import hr.hashcode.cables.equip.bind.JointNodesInfo;
import hr.hashcode.cables.equip.comp.Component;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * @author Fran Stanic
 */
public class EcOtherController extends UnitVisualController {

	private JointNodesInfo.ExtendedEcOther other;

	private Label nameLabel;

	public EcOtherController(JointNodesInfo.ExtendedEcOther other) {
		this.other = other;
		view = new VBox();

		nameLabel = new Label(other.name);
		nameLabel.setFont(FONT);
		view.getChildren().add(nameLabel);
		addPort(other.ecPort.port());
	}

	@Override
	public Type getType() {
		return Type.EC_OTHER;
	}

	@Override
	public String getName() {
		return other.name;
	}

	@Override
	public Component getUnit() {
		return other;
	}

	@Override
	protected Pane getEcPortContainer() {
		return view;
	}
}
