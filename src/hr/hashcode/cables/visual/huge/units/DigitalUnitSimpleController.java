package hr.hashcode.cables.visual.huge.units;

import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;

/**
 * @author Fran Stanic
 */
public class DigitalUnitSimpleController extends UnitVisualController {

	private DigitalUnit digital;

	private Label nameLabel;

	public DigitalUnitSimpleController(DigitalUnit digital) {
		this.digital = digital;
		view = new HBox(30);
		((HBox) view).setAlignment(Pos.CENTER);
		nameLabel = new Label(digital.name());
		nameLabel.setFont(FONT);

		if (getType() == Type.DUW) {
			setSize(RbsVisual.UNIT_LENGTH, RbsVisual.UNIT_HEIGHT * 2);
			setBackgroundColor(RbsVisual.DUW_COLOR);
		} else {
			setSize(RbsVisual.UNIT_LENGTH, RbsVisual.UNIT_HEIGHT);
			setBackgroundColor(RbsVisual.DUS_COLOR);
		}

		nameLabel.setPrefHeight(view.getMaxHeight());
		nameLabel.setAlignment(Pos.CENTER);
		nameLabel.setTextAlignment(TextAlignment.CENTER);

		view.getChildren().addAll(nameLabel);

		addPorts(digital.riPorts());
		addPorts(digital.ecPorts());
		addPorts(digital.dcPorts());
		addPorts(digital.idlPorts());

	}

	@Override
	public Type getType() {
		if(digital instanceof DigitalUnitWcdma){
			return Type.DUW;
		}
		return Type.DUS;
	}

	@Override
	public String getName() {
		return digital.type();
	}

	@Override
	public Component getUnit() {
		return digital;
	}

	public DigitalUnit getDigital() {
		return digital;
	}
}
