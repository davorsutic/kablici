package hr.hashcode.cables.visual.huge.units;

import java.util.List;

import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.RemoteRadioUnit;
import hr.hashcode.cables.visual.huge.Controller;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import hr.hashcode.cables.visual.utility.ViewUtility;
import javafx.scene.Group;
import javafx.scene.layout.HBox;

/**
 * @author Fran Stanic
 */
public class ExternalRadioUnitController extends UnitVisualController {

	private List<RemoteRadioUnit> units;

	public ExternalRadioUnitController(List<RemoteRadioUnit> units) {
		this.units = units;
		view = new HBox();
		for (RemoteRadioUnit u : units) {
			RadioUnitController controller = new RadioUnitController(u);
			controller.getView().visibleProperty().bind(this.getView().visibleProperty());
			controller.getView().opacityProperty().bind(this.getView().opacityProperty());
			controller.getView().setRotate(-90);
			getPortNodes().putAll(controller.getPortNodes());
			view.getChildren().add(new Group(controller.view));
		}

		ViewUtility.createSeparators(view, RbsVisual.BORDER_WIDTH);

		Controller.setBorder(view, RbsVisual.BORDER_WIDTH);
		setBackgroundColor(RbsVisual.RU_COLOR);
	}

	@Override
	public Type getType() {
		return Type.RADIO_EXTERNAL;
	}

	@Override
	public Component getUnit() {
		return units.get(0);
	}

	public List<RemoteRadioUnit> getRemoteRadioUnits() {
		return units;
	}

	@Override
	public String getName() {
		return units.get(0).type();
	}
}
