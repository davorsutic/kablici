package hr.hashcode.cables.visual.huge.units;

import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextAlignment;

/**
 * @author Fran Stanic
 */
public class DigitalUnitController extends UnitVisualController {

	private DigitalUnit digital;

	private Label nameLabel;
	private HBox riPorts;
	private HBox allPorts;

	public DigitalUnitController(DigitalUnit digital) {
		this.digital = digital;
		view = new HBox(30);
		nameLabel = new Label(digital.name());
		nameLabel.setFont(FONT);

		riPorts = new HBox();
		allPorts = new HBox();
		riPorts.setAlignment(Pos.CENTER);
		allPorts.setAlignment(Pos.CENTER);

		allPorts.getChildren().add(riPorts);

		riPorts.setSpacing(10);
		allPorts.setSpacing(20);

		addPorts(digital.riPorts());
		addPorts(digital.ecPorts());
		addPorts(digital.dcPorts());

		// allPorts.setVisible(false);

		if (getType() == Type.DUW) {
			DigitalUnitWcdma duw = (DigitalUnitWcdma) digital;
			addPorts(duw.idlPorts());
			setSize(RbsVisual.UNIT_LENGTH * RbsVisual.NON_SIMPLE_SCALE, RbsVisual.UNIT_HEIGHT * 2);
			setBackgroundColor(RbsVisual.DUW_COLOR);
		} else {
			setSize(RbsVisual.UNIT_LENGTH * RbsVisual.NON_SIMPLE_SCALE, RbsVisual.UNIT_HEIGHT * 1.2);
			setBackgroundColor(RbsVisual.DUS_COLOR);
		}

		nameLabel.setPrefHeight(view.getMaxHeight());
		nameLabel.setAlignment(Pos.CENTER);
		nameLabel.setTextAlignment(TextAlignment.CENTER);

		view.getChildren().addAll(nameLabel, allPorts);
	}

	@Override
	public Type getType() {
		if(digital instanceof DigitalUnitWcdma){
			return Type.DUW;
		}
		return Type.DUS;
	}


	@Override
	public String getName() {
		return digital.name();
	}

	@Override
	public Component getUnit() {
		return digital;
	}

	@Override
	protected Pane getRiPortContainer() {
		return riPorts;
	}

	@Override
	protected Pane getEcPortContainer() {
		return allPorts;
	}

	@Override
	protected Pane getIdlPortContainer() {
		return allPorts;
	}
}
