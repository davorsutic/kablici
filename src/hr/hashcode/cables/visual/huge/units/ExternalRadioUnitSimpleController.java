package hr.hashcode.cables.visual.huge.units;

import java.util.ArrayList;
import java.util.List;

import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.RemoteRadioUnit;
import hr.hashcode.cables.visual.huge.Controller;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import hr.hashcode.cables.visual.utility.ViewUtility;
import javafx.scene.Group;
import javafx.scene.layout.HBox;

/**
 * @author Fran Stanic
 */
public class ExternalRadioUnitSimpleController extends UnitVisualController {

	private List<RemoteRadioUnit> units;
	private List<RadioUnitSimpleController> unitControllers = new ArrayList<>();

	public ExternalRadioUnitSimpleController(List<RemoteRadioUnit> units) {
		this.units = units;
		view = new HBox();
		for (RemoteRadioUnit u : units) {
			RadioUnitSimpleController controller = new RadioUnitSimpleController(u);
			unitControllers.add(controller);
			controller.getView().setRotate(-90);
			getPortNodes().putAll(controller.getPortNodes());
			view.getChildren().add(new Group(controller.view));
		}

		ViewUtility.createSeparators(view, RbsVisual.BORDER_WIDTH);

		Controller.setBorder(view, RbsVisual.BORDER_WIDTH);
		setBackgroundColor(RbsVisual.RU_COLOR);
	}

	@Override
	public Type getType() {
		return Type.RADIO_EXTERNAL;
	}

	@Override
	public Component getUnit() {
		return units.get(0);
	}

	public List<RemoteRadioUnit> getRemoteRadioUnits() {
		return units;
	}

	public List<RadioUnitSimpleController> getUnitControllers() {
		return unitControllers;
	}

	@Override
	public String getName() {
		return units.get(0).type();
	}
}
