package hr.hashcode.cables.visual.huge.units;

import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.Port;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * @author Fran Stanic
 */
public class R503Controller extends UnitVisualController {

	private R503 r503;

	private Label nameLabel;
	private HBox riPorts;

	private HBox riGroup(R503 r503, int start, int count) {
		HBox box = new HBox();
		box.setAlignment(Pos.CENTER);
		box.setSpacing(5);
		for (int i = start; i > start - count; i--) {
			Port port = r503.riPorts().port(String.valueOf(i));
			PortController portController = registerAndCreateController(port);
			box.getChildren().add(portController.getView());
		}
		return box;
	}

	public R503Controller(R503 r503) {
		this.r503 = r503;

		view = new HBox();

		riPorts = new HBox();
		riPorts.setSpacing(10);
		riPorts.setAlignment(Pos.CENTER);
		((HBox) view).setAlignment(Pos.CENTER);

		nameLabel = new Label("R503");
		nameLabel.setFont(FONT);
		riPorts.getChildren().add(riGroup(r503, 16, 4));
		riPorts.getChildren().add(riGroup(r503, 12, 4));
		riPorts.getChildren().add(riGroup(r503, 7, 4));
		riPorts.getChildren().add(riGroup(r503, 3, 3));

		view.getChildren().addAll(nameLabel, riPorts);
		addPort(r503.ecPort());
		((HBox) view).setSpacing(30);
		view.setPadding(new Insets(10));
		//view.setPadding(new Insets(20));
		setSize(-1, RbsVisual.UNIT_HEIGHT + 20);
		setBackgroundColor(RbsVisual.R503_COLOR);
	}

	@Override
	public Type getType() {
		return Type.R503;
	}

	@Override
	public Component getUnit() {
		return r503;
	}


	public R503 getR503() {
		return r503;
	}

	@Override
	public String getName() {
		return r503.name();
	}

	@Override
	protected Pane getEcPortContainer() {
		return view;
	}
}
