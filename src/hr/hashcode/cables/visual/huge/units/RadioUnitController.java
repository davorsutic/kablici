package hr.hashcode.cables.visual.huge.units;

import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.visual.huge.Controller;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * @author Fran Stanic
 */
public class RadioUnitController extends UnitVisualController {

	/** The percentage of unit width that will be used for port. */
	private static final double WIDTH_FACTOR = 0.6;

	/** The width of the port. */
	private static final double PORT_WIDTH = WIDTH_FACTOR * 2 * RbsVisual.UNIT_HEIGHT;

	/** The width of the padding (on all sides). */
	private static final double PADDING = (1 - WIDTH_FACTOR) * RbsVisual.UNIT_HEIGHT;

	private RadioUnitBase radio;

	private Label nameLabel;
	private VBox allPorts;

	public RadioUnitController(RadioUnitBase ext) {
		this.radio = ext;

		view = new StackPane();

		nameLabel = new Label(radio.type());
		nameLabel.setFont(FONT);

		allPorts = new VBox();
		allPorts.setRotate(90);
		allPorts.setSpacing(PADDING);
		allPorts.setPadding(new Insets(0, 0, PADDING, 0));

		//addPorts(radio.rfPorts());
		addPorts(radio.riPorts());

		double width = RbsVisual.PORT_BORDER_WIDTH;
		for (PortController port : getPortNodes().values()) {
			Controller.setBorder((Region) port.getView(), width, width, width, width);
		}

		Group g = new Group(allPorts);
		Group g2 = new Group(nameLabel);
		view.getChildren().addAll(g2, g);

		StackPane.setAlignment(g2, Pos.CENTER_RIGHT);
		StackPane.setMargin(g2, new Insets(0, PADDING, 0, 0));
		StackPane.setAlignment(g, Pos.CENTER_LEFT);
		setSize(RbsVisual.UNIT_LENGTH * RbsVisual.NON_SIMPLE_SCALE, RbsVisual.UNIT_HEIGHT * 2);
		setBackgroundColor(RbsVisual.RU_COLOR);
	}

	@Override
	public Type getType() {
		return Type.RADIO;
	}

	@Override
	public Component getUnit() {
		return radio;
	}

	@Override
	public String getName() {
		return radio.type();
	}

	@Override
	protected Pane getRiPortContainer() {
		return allPorts;
	}

	@Override
	protected Pane getRfPortContainer() {
		return allPorts;
	}

	@Override
	protected Point2D getDefaultPortSize() {
		return new Point2D(PORT_WIDTH, PORT_WIDTH);
	}

	public RadioUnitBase getRadio() {
		return radio;
	}
}
