package hr.hashcode.cables.visual.huge.units;

import java.util.IdentityHashMap;
import java.util.Map;

import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.DcPort;
import hr.hashcode.cables.equip.comp.EcPort;
import hr.hashcode.cables.equip.comp.IdlPort;
import hr.hashcode.cables.equip.comp.Port;
import hr.hashcode.cables.equip.comp.PortSet;
import hr.hashcode.cables.equip.comp.RfPort;
import hr.hashcode.cables.equip.comp.RiPort;
import hr.hashcode.cables.visual.huge.Controller;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * @author Fran Stanic
 */
public abstract class UnitVisualController extends Controller {

	public enum Type {
		DUS,
		DUW,
		R503,
		RADIO,
		RADIO_EXTERNAL,
		ANTENNA,
		EC_OTHER,
		HUB
	}

	public static final Font FONT = Font.font(Font.getDefault().getFamily(), 30);

	private final Map<Port, PortController> portNodes = new IdentityHashMap<>();
	public Pane view;

	public abstract Component getUnit();

	public abstract String getName();

	public abstract Type getType();

	public void setBackgroundColor(Color backgroundColor) {
		view.setBackground(new Background(new BackgroundFill(backgroundColor, null, null)));
	}

	@Override
	public Node getView() {
		return view;
	}

	private void registerPort(Port port, PortController portController) {
		portNodes.put(port, portController);
	}

	protected PortController registerAndCreateController(Port port, Point2D portSize) {
		PortController portController = new PortController(this, port, portSize);
		registerPort(port, portController);
		return portController;
	}

	protected PortController registerAndCreateController(Port port) {
		return registerAndCreateController(port, getDefaultPortSize());
	}

	protected void addPort(Port port, Point2D portSize) {
		PortController portController = registerAndCreateController(port, portSize);
		Pane holder = null;
		if (port instanceof DcPort) {
			holder = getDcPortContainer();
		} else if (port instanceof EcPort) {
			holder = getEcPortContainer();
		} else if (port instanceof IdlPort) {
			holder = getIdlPortContainer();
		} else if (port instanceof RfPort) {
			holder = getRfPortContainer();
		} else if (port instanceof RiPort) {
			holder = getRiPortContainer();
		}

		if (holder != null) {
			holder.getChildren().add(portController.getView());
		}
	}

	protected void addPort(Port port) {
		addPort(port, getDefaultPortSize());
	}

	protected void addPorts(PortSet<?> ports, Point2D portSize) {
		for (Port port : ports.ports()) {
			addPort(port, portSize);
		}
	}

	protected void addPorts(PortSet<?> ports) {
		addPorts(ports, getDefaultPortSize());
	}

	protected Point2D getDefaultPortSize() {
		return new Point2D(RbsVisual.PORT_WIDTH, RbsVisual.PORT_HEIGHT);
	}

	public PortController getPortController(String portName) {
		return portNodes.values().stream().filter(p -> p.getPort().name().equals(portName)).findFirst().get();
	}

	public Map<Port, PortController> getPortNodes() {
		return portNodes;
	}

	protected Pane getDcPortContainer() {
		return null;
	}

	protected Pane getEcPortContainer() {
		return null;
	}

	protected Pane getIdlPortContainer() {
		return null;
	}

	protected Pane getRfPortContainer() {
		return null;
	}

	protected Pane getRiPortContainer() {
		return null;
	}

}
