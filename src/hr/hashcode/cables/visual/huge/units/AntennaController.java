package hr.hashcode.cables.visual.huge.units;

import hr.hashcode.cables.equip.comp.Antenna;
import hr.hashcode.cables.equip.comp.Component;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * @author Fran Stanic
 */
public class AntennaController extends UnitVisualController {

	private Antenna antenna;

	private Label nameLabel;
	private HBox ports;

	public AntennaController(Antenna antenna) {
		this.antenna = antenna;
		view = new VBox();

		nameLabel = new Label("Ant");
		nameLabel.setFont(FONT);
		view.getChildren().add(nameLabel);
		ports = new HBox();
		view.getChildren().add(ports);

		addPorts(antenna.rfPorts());
	}

	@Override
	public Type getType() {
		return Type.ANTENNA;
	}

	@Override
	public Component getUnit() {
		return antenna;
	}

	@Override
	public String getName() {
		return antenna.name();
	}

	@Override
	protected Pane getRfPortContainer() {
		return ports;
	}
}
