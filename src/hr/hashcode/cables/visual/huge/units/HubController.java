package hr.hashcode.cables.visual.huge.units;

import java.util.Arrays;

import hr.hashcode.cables.equip.bind.JointNodesInfo;
import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * @author Fran Stanic
 */
public class HubController extends UnitVisualController {

	private JointNodesInfo.ExtendedHub hub;

	private Label nameLabel;

	public HubController(JointNodesInfo.ExtendedHub hub) {
		this.hub = hub;
		view = new VBox();

		nameLabel = new Label(hub.name);
		nameLabel.setFont(FONT);

		view.getChildren().add(nameLabel);

		for (String prefix : Arrays.asList("A", "B")) {
			HBox row = new HBox();
			PortController controller = registerAndCreateController(hub.ports.port(prefix + "in"));
			row.getChildren().add(controller.getView());
			for (int i = 1; i <= 8; i++) {
				controller = registerAndCreateController(hub.ports.port(prefix + i));
				row.getChildren().add(controller.getView());
			}
			view.getChildren().add(row);
		}
		PortController controller = registerAndCreateController(hub.ports.port("CLU"));
		view.getChildren().add(controller.getView());
		HBox row = new HBox();
		view.getChildren().add(row);
		for (String name : Arrays.asList("A", "B", "C", "D", "E", "X", "Y", "SAU")) {
			controller = registerAndCreateController(hub.ports.port(name));
			row.getChildren().add(controller.getView());
		}
		controller = registerAndCreateController(hub.undefined());
		view.getChildren().add(controller.getView());

		setBackgroundColor(RbsVisual.SHU_COLOR);
	}

	@Override
	public Type getType() {
		return Type.HUB;
	}

	@Override
	public Component getUnit() {
		return hub;
	}

	@Override
	public String getName() {
		return hub.name;
	}

	@Override
	protected Pane getEcPortContainer() {
		return view;
	}
}
