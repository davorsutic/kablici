package hr.hashcode.cables.visual.huge.units;

import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.visual.huge.Controller;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

/**
 * @author Fran Stanic
 */
public class RadioUnitSimpleController extends UnitVisualController {

	private RadioUnitBase radio;

	private StackPane stackPane = new StackPane();
	private HBox labels;
	private Label nameLabel;

	public RadioUnitSimpleController(RadioUnitBase radio) {
		this.radio = radio;

		view = new BorderPane();

		addPorts(radio.riPorts());
		nameLabel = new Label(radio.type());
		nameLabel.setFont(FONT);
		labels = new HBox(nameLabel);
		labels.setAlignment(Pos.CENTER);
		labels.setSpacing(10);

		stackPane = new StackPane();
		stackPane.getChildren().addAll(labels);
		((BorderPane) view).setCenter(labels);

		Controller.setSize(view, RbsVisual.UNIT_LENGTH, 2 * RbsVisual.UNIT_HEIGHT);
		Controller.setSize(labels, RbsVisual.UNIT_LENGTH,
				2 * RbsVisual.UNIT_HEIGHT);
		setBackgroundColor(RbsVisual.RU_COLOR);
	}

	@Override
	public Type getType() {
		return Type.RADIO;
	}

	@Override
	public Component getUnit() {
		return radio;
	}

	@Override
	public String getName() {
		return radio.type();
	}

	public RadioUnitBase getRadio() {
		return radio;
	}
}
