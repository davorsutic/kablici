package hr.hashcode.cables.visual.huge.units;

import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author Fran Stanic
 */
public class R503SimpleController extends UnitVisualController {

	private R503 r503;

	private Label nameLabel;

	public R503SimpleController(R503 r503) {
		this.r503 = r503;

		view = new HBox();

		((HBox) view).setAlignment(Pos.CENTER);

		nameLabel = new Label("R503");
		nameLabel.setFont(FONT);
		view.getChildren().addAll(nameLabel);
		((HBox) view).setSpacing(30);
		setSize(RbsVisual.UNIT_LENGTH, RbsVisual.UNIT_HEIGHT);
		setBackgroundColor(RbsVisual.R503_COLOR);

		addPorts(r503.riPorts());
		addPorts(r503.dcPorts());
		addPorts(r503.ecPorts());
	}

	@Override
	public Type getType() {
		return Type.R503;
	}

	@Override
	public Component getUnit() {
		return r503;
	}

	@Override
	public String getName() {
		return r503.name();
	}

	public R503 getR503() {
		return r503;
	}
}
