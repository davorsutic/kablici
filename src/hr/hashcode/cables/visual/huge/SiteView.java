package hr.hashcode.cables.visual.huge;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.MiniRbs;
import hr.hashcode.cables.equip.comp.Port;
import hr.hashcode.cables.equip.comp.RemoteRadioUnit;
import hr.hashcode.cables.equip.comp.RiPort;
import hr.hashcode.cables.equip.comp.Site;
import hr.hashcode.cables.equip.comp.SiteContext;
import hr.hashcode.cables.visual.huge.cabinets.RBS6102;
import hr.hashcode.cables.visual.huge.cabinets.RBS6131;
import hr.hashcode.cables.visual.huge.cabinets.RBS6201;
import hr.hashcode.cables.visual.huge.cabinets.RBS6202;
import hr.hashcode.cables.visual.huge.cabinets.RBS6601;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import hr.hashcode.cables.visual.huge.connections.Connection;
import hr.hashcode.cables.visual.huge.units.ExternalRadioUnitSimpleController;
import hr.hashcode.cables.visual.huge.units.PortController;
import hr.hashcode.cables.visual.huge.units.UnitVisualController;
import hr.hashcode.cables.visual.utility.ViewUtility;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.util.Pair;

public class SiteView extends HBox {

	private Map<Port, Node> portToNodeMap = new HashMap<>();
	private Map<Port, PortController> portToPortController = new HashMap<>();
	private Map<Node, Set<Band>> nodeToBands = new HashMap<>();

	private static RbsVisual createRbs(MacroRbs cabinet) {
		if (cabinet.type().equals("RBS6102")) {
			return new RBS6102(cabinet);
		} else if (cabinet.type().equals("RBS6131")) {
			return new RBS6131(cabinet);
		} else if (cabinet.type().equals("RBS6201")) {
			return new RBS6201(cabinet);
		} else if (cabinet.type().equals("RBS6202")) {
			return new RBS6202(cabinet);
		}
		throw new IllegalArgumentException();
	}

	@FXML
	private Group cables;

	@FXML
	private ScrollPane scrollPane;

	@FXML
	private StackPane scrollPaneContent;

	@FXML
	private Group group;

	@FXML
	private HBox cabinetBox;

	@FXML
	private Group scrollPaneGroup;

	@FXML
	private CheckBox showCablesCheckBox;

	@FXML
	private Button zoomToFitButton;

	private List<RbsVisual> cabinets = new ArrayList<>();

	private List<Pair<Node, Node>> connections = new ArrayList<>();

	private void addConnector(Connector<RiPort> connector) {
		Node u1 = portToNodeMap.get(connector.first());
		Node u2 = portToNodeMap.get(connector.second());
		if (u1 != null && u2 != null) {
			connections.add(new Pair<>(u1, u2));
		}
	}

	private void registerNodeToBand(SiteContext siteContext, Node viewToUse, UnitVisualController unitVisualController) {
		Component unit = unitVisualController.getUnit();
		List<Band> bands = siteContext.getCoveredBandsFor(unit);
		nodeToBands.put(viewToUse, new HashSet<>(bands));
	}

	private void registerNodeToBand(RbsVisual rbs, SiteContext siteContext) {
		rbs.getSlots().stream().filter(s -> s.getUnitVisualController() != null)
				.forEach(s -> registerNodeToBand(siteContext, s.getView(), s.getUnitVisualController()));

		rbs.getExternalRadios().stream().flatMap(e -> e.getUnitControllers().stream())
				.forEach(unitVisualController -> registerNodeToBand(siteContext, unitVisualController.getView(), unitVisualController));
	}

	private void registerCabinetPorts(RbsVisual rbs) {
		rbs.getSlots().stream().filter(s -> s.getUnitVisualController() != null)
				.forEach(slot -> {
					Map<Port, PortController> portNodes = slot.getUnitVisualController().getPortNodes();
					portNodes.keySet().forEach(port -> portToNodeMap.put(port, slot.getView()));
					portToPortController.putAll(portNodes);
				});

		rbs.getExternalRadios().forEach(unit -> {
			Map<Port, PortController> portNodes = unit.getPortNodes();
			portNodes.keySet().forEach(c -> portToNodeMap.put(c, unit.getView()));
			portToPortController.putAll(portNodes);
		});
	}

	private void registerExternalRadioPorts(ExternalRadioUnitSimpleController radio) {
		radio.getUnitControllers().forEach(c -> c.getPortNodes().keySet().forEach(p -> portToNodeMap.put(p, c.getView())));
	}

	private void loadFromFXML() {
		URL resource = SiteView.class.getResource("site_view.fxml");
		FXMLLoader loader = new FXMLLoader(resource);
		loader.setController(this);
		loader.setRoot(this);
		try {
			loader.load();
		} catch (IOException ex) {
			// throw as cause of RuntimeException
			throw new RuntimeException(ex);
		}

		ViewUtility.makePannableZoomableScrollPane(scrollPane, scrollPaneContent, group, scrollPaneGroup);

		zoomToFitButton.setOnAction(m -> ViewUtility.zoomToFit(scrollPane, scrollPaneGroup));
	}

	private Connector<RiPort> getConnector(Site site, RiPort port) {
		return site.riConnectors.stream().filter(c -> c.first() == port || c.second() == port).findFirst().orElse(null);
	}

	private RbsVisual getCabinet(UnitVisualController unitVisualController) {
		for (RbsVisual cabinet : cabinets) {
			if (cabinet.getInternalUnitControllers().contains(unitVisualController)) {
				return cabinet;
			}
		}
		throw new IllegalArgumentException();
	}

	private RbsVisual getRbsForRadioUnit(Site site, RemoteRadioUnit unit) {
		for (RiPort riPort : unit.riPorts().ports()) {
			Connector<RiPort> connector = getConnector(site, riPort);
			RiPort other;

			if (connector == null)
				continue; // Happens if one of the ports in RRU is free

			if (connector.first() == riPort) {
				other = connector.second();
			} else {
				other = connector.first();
			}
			PortController portController = portToPortController.get(other);
			UnitVisualController otherUnit = portController.getUnit();
			return getCabinet(otherUnit);
		}
		throw new IllegalArgumentException();
	}

	public SiteView(Site site) {

		loadFromFXML();

		Connection connection = new Connection(scrollPaneGroup);
		SiteContext siteContext = SiteContext.from(site);

		Map<Band, List<RemoteRadioUnit>> groupedRadios = siteContext.getGroupedExternalRadios();

		for (MacroRbs rbs : site.rbses) {
			RbsVisual rbsVisual = createRbs(rbs);
			cabinets.add(rbsVisual);
			cabinetBox.getChildren().add(rbsVisual.getView());
		}

		List<ExternalRadioUnitSimpleController> radios = new ArrayList<>();

		for (Map.Entry<Band, List<RemoteRadioUnit>> entry : groupedRadios.entrySet()) {
			ExternalRadioUnitSimpleController radio = new ExternalRadioUnitSimpleController(entry.getValue());
			radios.add(radio);
			registerExternalRadioPorts(radio);
			connection.registerExternalRadioConfiguration(radio);
		}

		Collection<Set<MiniRbs>> minis = siteContext.getPartitionedMiniRbses();// partitionMiniRbs(site, radios, site.riConnectors);

		for (Collection<MiniRbs> mini : minis) {
			RbsVisual rbsVisual = new RBS6601(mini);
			cabinets.add(rbsVisual);
			cabinetBox.getChildren().add(rbsVisual.getView());
		}

		for (RbsVisual rbs : cabinets) {
			registerCabinetPorts(rbs);
			connection.registerConfiguration(rbs);
		}

		for (ExternalRadioUnitSimpleController radio : radios) {
			List<RemoteRadioUnit> units = radio.getRemoteRadioUnits();
			RbsVisual rbs = null;
			for (RemoteRadioUnit radioUnit : units) {
				RbsVisual r = getRbsForRadioUnit(site, radioUnit);
				assert (rbs == null || rbs == r);
				rbs = r;
			}
			assert (rbs != null);
			rbs.addRemote(radio);
			registerExternalRadioPorts(radio);
			connection.registerExternalRadioConfiguration(radio);
		}

		for (RbsVisual rbs : cabinets) {
			registerNodeToBand(rbs, siteContext);
		}
		connection.setNodeToBands(nodeToBands);

		for (Connector<RiPort> connector : site.riConnectors) {
			addConnector(connector);
		}

		showCablesCheckBox.setOnAction(e -> {
			connection.defineConfigurations();
			connection.connect(cables, connections, showCablesCheckBox.selectedProperty());
		});
		Platform.runLater(() -> showCablesCheckBox.fire());
		Platform.runLater(() -> ViewUtility.zoomToFit(scrollPane, scrollPaneGroup));
	}

}
