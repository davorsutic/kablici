package hr.hashcode.cables.visual.huge.cabinets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import hr.hashcode.cables.equip.comp.DigitalUnitBaseband;
import hr.hashcode.cables.equip.comp.DigitalUnitStandard;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.MiniRbs;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.visual.huge.Controller;
import hr.hashcode.cables.visual.huge.units.DigitalUnitController;
import hr.hashcode.cables.visual.huge.units.DigitalUnitSimpleController;
import hr.hashcode.cables.visual.huge.units.R503Controller;
import hr.hashcode.cables.visual.huge.units.R503SimpleController;
import hr.hashcode.cables.visual.huge.units.UnitVisualController;
import hr.hashcode.cables.visual.utility.ViewUtility;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author Fran Stanic
 */
public class RBS6601 extends RbsVisual {
	private List<Node> subunits = new ArrayList<>();

	public RBS6601(Collection<MiniRbs> rbses) {
		Controller.setBorder(cabinetView, 0);
		List<MiniRbs> rbsList = new ArrayList<>(rbses);
		for (int i = 0; i < rbsList.size(); i++) {
			Subunit subunit = new Subunit(i + 1, rbsList.get(i));
			subunits.add(subunit.getView());
			cabinetView.getChildren().add(subunit.getView());
		}
	}

	private static final boolean simple = true;

	private class Subunit extends Controller {
		private HBox root;
		private VBox uSlots;
		private Label scuLabel;

		private String getSlotId(int rbsOrdinal, String slotName) {
			return "#" + rbsOrdinal + "#" + slotName;
		}

		public Subunit(int ordinal, MiniRbs rbs) {
			root = new HBox(20);
			root.setPadding(new Insets(SPACING));
			root.setAlignment(Pos.CENTER_LEFT);
			Controller.setBorder(root, BORDER_WIDTH);

			uSlots = (VBox) RBS6601.this.getSlotGroup(new SlotGroupDefinition(true, rbs.slots().size(),
					x -> getSlotId(ordinal, rbs.slots().get(x)),
					SLOT_WIDTH,
					SLOT_HEIGHT / 2));

			scuLabel = createLabel("SCU", RbsVisual.SHU_COLOR, SHU_WIDTH * 1.5, SCU_HEIGHT);

			DigitalUnitWcdma duw = rbs.duw();
			if (duw != null) {
				UnitVisualController controller = simple
						? new DigitalUnitSimpleController(duw)
						: new DigitalUnitController(duw);
				SlotController slot = getSlot(getSlotId(ordinal, rbs.slots().get(0)));
				Controller.setBorder(slot.getUnitPane(), RbsVisual.BORDER_WIDTH);
				slot.setUnitVisual(controller);

				slot = getSlot(getSlotId(ordinal, rbs.slots().get(1)));
				slot.getView().setVisible(false);
				slot.getView().setManaged(false);
			}

			for (String slot : rbs.slots()) {
				R503 r503 = rbs.r503(slot);
				DigitalUnitStandard unit = rbs.dus(slot);
				DigitalUnitBaseband bb = rbs.bb(slot);
				UnitVisualController controller;
				if (r503 != null)
					controller = simple ? new R503SimpleController(r503) : new R503Controller(r503);
				else if (unit != null)
					controller = simple ? new DigitalUnitSimpleController(unit) : new DigitalUnitController(unit);
				else if (bb != null)
					controller = simple ? new DigitalUnitSimpleController(bb) : new DigitalUnitController(bb);
				else
					controller = null;
				if (controller != null)
					getSlot(getSlotId(ordinal, slot)).setUnitVisual(controller);
			}

			ViewUtility.createSeparators(uSlots, BORDER_WIDTH);
			root.getChildren().addAll(new Group(uSlots), scuLabel);
		}

		@Override
		public Node getView() {
			return root;
		}
	}

	public List<Node> getSubunits() {
		return subunits;
	}
}
