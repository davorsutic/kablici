package hr.hashcode.cables.visual.huge.cabinets;

import hr.hashcode.cables.visual.huge.Controller;
import hr.hashcode.cables.visual.huge.units.UnitVisualController;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * @author Fran Stanic
 */
public class SlotController extends Controller {
	public static final Color SLOT_COLOR = Color.grayRgb(191);

	private UnitVisualController unitVisualController;
	private String slotName;
	private Group view;
	private VBox unitPane;
	private boolean vertical;
	private Label nameLabel;

	private double width;
	private double height;

	public UnitVisualController getUnitVisualController() {
		return unitVisualController;
	}

	public void setColor(Color color) {
		unitPane.setBackground(new Background(new BackgroundFill(color, null, null)));
	}

	public void expandSlot(double w, double h) {
		this.width = w;
		this.height = h;
	}

	public void setUnitVisual(UnitVisualController unitVisual) {
		this.unitVisualController = unitVisual;
		unitPane.getChildren().clear();
		Controller.setSize(unitPane, width, height);
		unitPane.setRotate(vertical ? -90 : 0);
		if (unitVisual != null) {
			unitPane.getChildren().add(unitVisual.view);
		} else {
			unitPane.setBackground(new Background(new BackgroundFill(SLOT_COLOR, null, null)));
			unitPane.getChildren().add(nameLabel);
			nameLabel.setPrefSize(width, height);
		}
	}

	public SlotController(String slotName, String dummyText, boolean vertical, double width, double height) {
		this.slotName = slotName;
		this.vertical = vertical;
		this.width = width;
		this.height = height;
		view = new Group();
		nameLabel = new Label(dummyText);
		nameLabel.setFont(UnitVisualController.FONT);
		nameLabel.setAlignment(Pos.CENTER);
		unitPane = new VBox();
		unitPane.setAlignment(Pos.CENTER);
		view.getChildren().addAll(unitPane);
		setUnitVisual(null);
	}

	@Override
	public Node getView() {
		return view;
	}

	public Pane getUnitPane() {
		return unitPane;
	}

	public String getSlotName() {
		return slotName;
	}
}
