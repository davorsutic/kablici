package hr.hashcode.cables.visual.huge.cabinets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.hashcode.cables.equip.comp.ExtensionRack;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.Subrack;
import hr.hashcode.cables.visual.huge.Controller;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * @author Fran Stanic
 */
public class RBS6102 extends RbsVisual {

	private HBox lSlots;
	private VBox lXSlots;

	private HBox uSlots;
	private VBox uXSlots;

	private Label externalFanLabel;
	private Label internalFan1Label;
	private Label scuLabel;
	private Label internalFan2Label;

	private List<String> uSlotNames;
	private List<String> lSlotNames;
	private List<String> uXSlotNames;
	private List<String> lXSlotNames;

	private static double SPACING = 60;

	public RBS6102(MacroRbs rbs) {

		externalFanLabel = createLabel("External FAN", SlotController.SLOT_COLOR, -1, EXTENRAL_FAN_HEIGHT);
		externalFanLabel.setBorder(null);
		internalFan1Label =
				createLabel("Internal FAN1", SlotController.SLOT_COLOR, INTERNAL_FAN_WIDTH, INTERNAL_FAN_HEIGHT);
		scuLabel = createLabel("SCU", RbsVisual.SHU_COLOR, SCU_WIDTH, SCU_HEIGHT);
		internalFan2Label =
				createLabel("Internal FAN2", SlotController.SLOT_COLOR, INTERNAL_FAN_WIDTH, INTERNAL_FAN_HEIGHT);

		HBox internalFansHbox = new HBox(SPACING, internalFan1Label, scuLabel, internalFan2Label);
		internalFansHbox.setAlignment(Pos.CENTER);

		List<HBox> hboces = new ArrayList<>();

		for (Subrack subrack : rbs.subracks()) {
			HBox box = (HBox) getSlotGroup(new SlotGroupDefinition(false, subrack.slots().size() + 1, x -> {
				if (x == 0)
					return duwId(subrack.name());
				else
					return radioId(subrack.name(), subrack.slots().get(x - 1));
			}, SLOT_WIDTH, SLOT_HEIGHT));
			hboces.add(box);
		}

		uSlots = hboces.get(0);
		lSlots = hboces.get(1);

		uSlotNames = getSubrackName(rbs.subracks().get(0));
		lSlotNames = getSubrackName(rbs.subracks().get(1));

		lXSlots = new VBox(SPACING);
		lXSlotNames = new ArrayList<>();

		lXSlots.setAlignment(Pos.CENTER);
		for (int i = 3; i < 6; i++) {
			ExtensionRack extension = rbs.extensions().get(i);
			List<String> slots = extension.slots();
			lXSlots.getChildren().add(getSlotGroup(new SlotGroupDefinition(true, slots.size(), x -> extensionId(extension.name(), slots.get(x)),
																				  SLOT_WIDTH, SLOT_HEIGHT / 2)));
			lXSlotNames.addAll(getExtensionName(extension));
		}

		uXSlots = new VBox(SPACING);
		uXSlotNames = new ArrayList<>();

		for (int i = 0; i < 3; i++) {
			ExtensionRack extension = rbs.extensions().get(i);
			List<String> slots = extension.slots();
			uXSlots.getChildren().add(getSlotGroup(new SlotGroupDefinition(true, slots.size(), x -> extensionId(extension.name(), slots.get(x)),
																				  SLOT_WIDTH, SLOT_HEIGHT / 2)));
			uXSlotNames.addAll(getExtensionName(extension));
		}

		VBox left = new VBox(SPACING, new Group(lSlots), new Group(lXSlots));
		left.setAlignment(Pos.CENTER);

		HBox slotsAndPDUHbox = new HBox(SPACING);
		slotsAndPDUHbox.setAlignment(Pos.TOP_CENTER);

		VBox right = new VBox(SPACING, new Group(uSlots), new Group(uXSlots));
		right.setAlignment(Pos.CENTER);

		List<String> centerNames = Arrays.asList("PDU3", "PDU2", "PDU1", "SHU-01", "PFU");

		VBox center = (VBox) getSlotGroup(new SlotGroupDefinition(true, centerNames.size(), x -> fixedId(centerNames.get(x)), CENTER_SLOT_WIDTH,
																		 CENTER_SLOT_HEIGHT)
		.setDummyTextFunction(centerNames::get)
		.setSlotColorFunction(x -> x ==3 ? SHU_COLOR : EC_OTHERS_COLOR));
		center.setAlignment(Pos.TOP_CENTER);

		slotsAndPDUHbox.getChildren().addAll(left, new Group(center), right);

		slotsAndPDUHbox.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				slotsAndPDUHbox.widthProperty().removeListener(this);
				internalFan1Label.setMinWidth((slotsAndPDUHbox.getWidth() - SCU_WIDTH - 2 * SPACING) / 2);
				internalFan2Label.setMinWidth((slotsAndPDUHbox.getWidth() - SCU_WIDTH - 2 * SPACING) / 2);
			}
		});

		//externalFanLabel.prefWidthProperty().bind(cabinetView.widthProperty());
		/*cabinetView.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				cabinetView.widthProperty().removeListener(this);
				externalFanLabel.setPrefWidth(newValue.doubleValue());
			}
		});*/

		place(rbs);

		VBox inner = new VBox(SPACING, internalFansHbox, slotsAndPDUHbox);
		inner.setPadding(new Insets(SPACING));
		Pane p = new StackPane(externalFanLabel);
		Controller.setBorder(p, 0, 0, BORDER_WIDTH, 0);
		p.setBackground(new Background(new BackgroundFill(SlotController.SLOT_COLOR, null, null)));
		cabinetView.getChildren().addAll(p, inner);
	}

	public HBox getuSlots() {
		return uSlots;
	}

	public List<String> getuSlotNames() {
		return uSlotNames;
	}

	public HBox getlSlots() {
		return lSlots;
	}

	public List<String> getlSlotNames() {
		return lSlotNames;
	}

	public VBox getlXSlots() {
		return lXSlots;
	}

	public List<String> getlXSlotNames() {
		return lXSlotNames;
	}

	public List<String> getuXSlotNames() {
		return uXSlotNames;
	}

	public VBox getuXSlots() {
		return uXSlots;
	}

	public Label getScuLabel() {
		return scuLabel;
	}
}
