package hr.hashcode.cables.visual.huge.cabinets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.ExtensionRack;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.Subrack;
import hr.hashcode.cables.visual.huge.Controller;
import hr.hashcode.cables.visual.huge.units.DigitalUnitController;
import hr.hashcode.cables.visual.huge.units.DigitalUnitSimpleController;
import hr.hashcode.cables.visual.huge.units.ExternalRadioUnitSimpleController;
import hr.hashcode.cables.visual.huge.units.PortController;
import hr.hashcode.cables.visual.huge.units.R503Controller;
import hr.hashcode.cables.visual.huge.units.R503SimpleController;
import hr.hashcode.cables.visual.huge.units.RadioUnitController;
import hr.hashcode.cables.visual.huge.units.RadioUnitSimpleController;
import hr.hashcode.cables.visual.huge.units.UnitVisualController;
import hr.hashcode.cables.visual.utility.ViewUtility;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * @author Fran Stanic
 */
public abstract class RbsVisual extends Controller {

	public static final Color DUW_COLOR = Color.rgb(206, 255, 255);
	public static final Color RU_COLOR = Color.rgb(255, 254, 159);
	public static final Color DUS_COLOR = Color.rgb(254, 205, 254);
	public static final Color R503_COLOR = Color.rgb(154, 156, 252);
	public static final Color EC_OTHERS_COLOR = Color.rgb(253, 191, 45);
	public static final Color SHU_COLOR = Color.rgb(205, 253, 158);

	public static final double SCALE = 0.75;

	public static final double UNIT_LENGTH = (int) (600 * 0.7 * 1.4 * SCALE);
	public static final double UNIT_HEIGHT = (int) (50 * 0.8 * 1.4 * SCALE);

	public static final double PORT_WIDTH = (int) (40 * SCALE);
	public static final double PORT_HEIGHT = (int) (20 * SCALE);
	public static final double EXTENRAL_FAN_HEIGHT = UNIT_HEIGHT * 2.5;
	public static final double INTERNAL_FAN_WIDTH = UNIT_LENGTH * 1.5;
	public static final double INTERNAL_FAN_HEIGHT = EXTENRAL_FAN_HEIGHT;

	public static final double SCU_HEIGHT = INTERNAL_FAN_HEIGHT;
	public static final double SCU_WIDTH = SCU_HEIGHT * 0.9;

	public static final double SHU_WIDTH = SCU_WIDTH;
	public static final double SHU_HEIGHT = SCU_HEIGHT * 0.7;
	public static final double SLOT_WIDTH = UNIT_LENGTH;
	public static final double SLOT_HEIGHT = UNIT_HEIGHT * 2;

	public static final double CENTER_SLOT_WIDTH = SCU_WIDTH * 1.4;
	public static final double CENTER_SLOT_HEIGHT = SLOT_HEIGHT * 0.65;

	public static final double SAU_WIDTH = 7 * SLOT_HEIGHT;
	public static final double SAU_HEIGHT = SLOT_HEIGHT / 2;

	public static final double SPACING = 100 * SCALE;

	public static final double NON_SIMPLE_SCALE = 1.1;

	public static final double BORDER_WIDTH = 4 * SCALE;
	public static final double PORT_BORDER_WIDTH = BORDER_WIDTH / 2;
	protected VBox cabinetAndExternalRadioView;
	protected HBox externalRadioHbox;
	protected VBox cabinetView;

	protected List<SlotController> slots = new ArrayList<>();
	protected List<ExternalRadioUnitSimpleController> externalRadios = new ArrayList<>();

	public RbsVisual() {
		cabinetAndExternalRadioView = new VBox(100);
		cabinetView = new VBox(20);
		externalRadioHbox = new HBox(50);
		cabinetView.setAlignment(Pos.CENTER);
		cabinetAndExternalRadioView.getChildren().addAll(externalRadioHbox, cabinetView);
		cabinetAndExternalRadioView.setAlignment(Pos.BOTTOM_CENTER);
		Controller.setBorder(cabinetView, BORDER_WIDTH);
	}

	public List<ExternalRadioUnitSimpleController> getExternalRadios() {
		return externalRadios;
	}

	public void addRemote(ExternalRadioUnitSimpleController externalController) {
		externalRadioHbox.getChildren().add(new Group(externalController.view));
		externalRadios.add(externalController);
	}

	public List<UnitVisualController> getInternalUnitControllers(){
		return slots.stream().filter(s -> s.getUnitVisualController() != null)
					   .map(SlotController::getUnitVisualController)
					   .collect(Collectors.toList());
	}

	@Override
	public Node getView() {
		return cabinetAndExternalRadioView;
	}

	private static final String duwId = "DUW";
	private static final String extId = "EXTENSION";
	private static final String radioId = "RADIO";
	private static final String fixedId = "FIXED";

	static String duwId(String subrack) {
		return merge(duwId, subrack);
	}

	static String extensionId(String subrack, String slot) {
		return merge(extId, subrack, slot);
	}

	static String radioId(String subrack, String slot) {
		return merge(radioId, subrack, slot);
	}

	static String fixedId(String unit) {
		return merge(fixedId, unit);
	}

	private static String merge(String... comps) {
		return String.join(".", comps);
	}

	private static final boolean simple = true;

	void place(MacroRbs rbs) {
		for (Subrack subrack : rbs.subracks()) {
			DigitalUnitWcdma duw = subrack.digital();
			if (duw != null) {
				UnitVisualController controller =
						simple ? new DigitalUnitSimpleController(duw) : new DigitalUnitController(duw);
				getSlot(duwId(subrack.name())).setUnitVisual(controller);
			}

			for (String slot : subrack.slots()) {
				RadioUnitBase radio = subrack.radio(slot);
				if (radio != null) {
					UnitVisualController controller =
							simple ? new RadioUnitSimpleController(radio) : new RadioUnitController(radio);
					getSlot(radioId(subrack.name(), slot)).setUnitVisual(controller);
				}
			}
		}

		for (ExtensionRack extension : rbs.extensions()) {
			for (String slot : extension.slots()) {
				R503 r503 = extension.r503(slot);
				DigitalUnit unit = extension.unit(slot);
				UnitVisualController controller;
				if (r503 != null)
					controller = simple ? new R503SimpleController(r503) : new R503Controller(r503);
				else if (unit != null)
					controller = simple ? new DigitalUnitSimpleController(unit) : new DigitalUnitController(unit);
				else
					controller = null;
				if (controller != null)
					getSlot(extensionId(extension.name(), slot)).setUnitVisual(controller);
			}
		}
	}

	protected class SlotGroupDefinition{
		private boolean isVbox;
		private int num;
		private Function<Integer, String> nameFunction;
		private Function<Integer, String> dummyTextFunction = x -> "Dummy";
		private Function<Integer, Color> slotColorFunction = x -> SlotController.SLOT_COLOR;
		private double slotWidth;
		private double slotHeight;

		public SlotGroupDefinition(boolean isVbox, int num, Function<Integer, String> nameFunction, double slotWidth, double slotHeight) {
			this.isVbox = isVbox;
			this.num = num;
			this.nameFunction = nameFunction;
			this.slotWidth = slotWidth;
			this.slotHeight = slotHeight;
		}

		public SlotGroupDefinition setDummyTextFunction(Function<Integer, String> dummyTextFunction) {
			this.dummyTextFunction = dummyTextFunction;
			return this;
		}

		public SlotGroupDefinition setSlotColorFunction(Function<Integer, Color> slotColorFunction) {
			this.slotColorFunction = slotColorFunction;
			return this;
		}

		public SlotGroupDefinition setSlotColor(Color slotColor) {
			this.slotColorFunction = x -> slotColor;
			return this;
		}
	}

	protected Pane getSlotGroup(SlotGroupDefinition def) {
		Pane pane = def.isVbox ? new VBox() : new HBox();
		if (def.isVbox) {
			((VBox) pane).setAlignment(Pos.CENTER);
		} else {
			((HBox) pane).setAlignment(Pos.CENTER);
		}
		Controller.setBorder(pane, BORDER_WIDTH);

		for (int i = 0; i < def.num; i++) {
			SlotController slotController = addSlot(pane, def.nameFunction.apply(i), def.dummyTextFunction.apply(i), !def.isVbox, def.slotWidth,
					def.slotHeight);
			slotController.setColor(def.slotColorFunction.apply(i));
		}
		ViewUtility.createSeparators(pane, BORDER_WIDTH);
		return pane;
	}

	protected Label createLabel(String text, Color backgroundColor, double width, double height) {
		Label label = new Label(text);
		label.setFont(UnitVisualController.FONT);
		label.setBackground(new Background(new BackgroundFill(backgroundColor, null, null)));
		label.setAlignment(Pos.CENTER);
		Controller.setBorder(label, BORDER_WIDTH);
		label.setPrefSize(width, height);
		// Controller.setSize(label, width, height);
		return label;
	}

	protected SlotController addSlot(Pane pane, String slotName, String dummyText, boolean vertical, double width, double height) {
		SlotController controller = createSlotController(slotName, dummyText, vertical, width, height);
		pane.getChildren().add(controller.getView());
		return controller;
	}

	protected SlotController createSlotController(String slotName, String dummyText, boolean vertical, double width, double height) {
		SlotController controller = new SlotController(slotName, dummyText, vertical, width, height);
		slots.add(controller);
		return controller;
	}

	public SlotController getSlot(String slotName) {
		Optional<SlotController> slot = slots.stream().filter(s -> s.getSlotName().equals(slotName)).findFirst();
		if (slot.isPresent()) {
			return slot.get();
		}
		return null;
	}

	public List<SlotController> getSlotsByName(String... slotNames) {
		return Arrays.stream(slotNames).map(this::getSlot).collect(Collectors.toList());
	}

	public List<SlotController> getSlots() {
		return slots;
	}

	public PortController getPortController(String slotName, String portName) {
		return getSlot(slotName).getUnitVisualController().getPortController(portName);
	}

	public VBox getCabinetView() {
		return cabinetView;
	}

	public List<String> getSubrackName(Subrack subrack) {
		List<String> names = new ArrayList<>();
		names.add(duwId(subrack.name()));
		for (String slot : subrack.slots()) {
			names.add(radioId(subrack.name(), slot));
		}
		return names;
	}

	public List<String> getExtensionName(ExtensionRack extensionRack) {
		List<String> names = new ArrayList<>();
		for (String slot : extensionRack.slots()) {
			names.add(extensionId(extensionRack.name(), slot));
		}
		return names;
	}
}
