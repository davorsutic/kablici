package hr.hashcode.cables.visual.huge.cabinets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.hashcode.cables.equip.comp.ExtensionRack;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.Subrack;
import hr.hashcode.cables.visual.huge.Controller;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author Fran Stanic
 */
public class RBS6131 extends RbsVisual {
	private static double CABINET_SPACING = 50;

	public HBox lSlots;
	public HBox uSlots;
	public HBox mbSlots;
	public GridPane gridPane;

	private List<String> uSlotNames;
	private List<String> lSlotNames;
	private List<String> extensionNames;

	private Label externalFanLabel;

	public RBS6131(MacroRbs rbs) {

		externalFanLabel = createLabel("FAN", SlotController.SLOT_COLOR, -1, EXTENRAL_FAN_HEIGHT);
		Controller.setBorder(externalFanLabel, BORDER_WIDTH);

		SlotController scuLabel = createSlotController("SCU", "SCU", false, SCU_WIDTH, SCU_HEIGHT);
		scuLabel.setColor(RbsVisual.SHU_COLOR);
		Controller.setBorder(scuLabel.getUnitPane(), BORDER_WIDTH);
		SlotController shuLabel = createSlotController("SHU","SHU", false, SHU_WIDTH, SHU_HEIGHT);
		shuLabel.setColor(RbsVisual.SHU_COLOR);
		Controller.setBorder(shuLabel.getUnitPane(), BORDER_WIDTH);

		List<HBox> boces = new ArrayList<>();

		for (ExtensionRack extension : rbs.extensions()) {
			List<String> slots = extension.slots();
			HBox box = (HBox) getSlotGroup(new SlotGroupDefinition(false, slots.size(), x -> extensionId(extension.name(), slots.get(x)),
																		  SLOT_WIDTH, SLOT_HEIGHT / 2));
			boces.add(box);
		}

		extensionNames = getExtensionName(rbs.extensions().get(0));

		mbSlots = boces.get(0);

		List<HBox> hboces = new ArrayList<>();

		for (Subrack subrack : rbs.subracks()) {
			HBox box = (HBox) getSlotGroup(new SlotGroupDefinition(false, subrack.slots().size() + 1, x -> {
				if (x == 0)
					return duwId(subrack.name());
				else
					return radioId(subrack.name(), subrack.slots().get(x - 1));
			}, SLOT_WIDTH, SLOT_HEIGHT));
			hboces.add(box);
		}

		uSlots = hboces.get(0);
		lSlots = hboces.get(1);

		uSlotNames = getSubrackName(rbs.subracks().get(0));
		lSlotNames = getSubrackName(rbs.subracks().get(1));

		Label sau = createLabel("SAU", RbsVisual.EC_OTHERS_COLOR, SAU_WIDTH, SAU_HEIGHT);
		List<String> arr = Arrays.asList("PFU", "", "PDU2", "PDU1");
		VBox pfu = (VBox) getSlotGroup(new SlotGroupDefinition(true, 4,arr::get,
																	  SAU_WIDTH, SAU_HEIGHT)
		.setSlotColor(RbsVisual.EC_OTHERS_COLOR)
		.setDummyTextFunction(arr::get));

		gridPane = new GridPane();
		gridPane.setHgap(100);
		gridPane.setVgap(140);

		GridPane.setHalignment(scuLabel.getView(), HPos.CENTER);
		GridPane.setValignment(scuLabel.getView(), VPos.BOTTOM);
		GridPane.setValignment(shuLabel.getView(), VPos.BOTTOM);
		GridPane.setValignment(sau, VPos.BOTTOM);
		// gridPane.setGridLinesVisible(true);

		gridPane.add(scuLabel.getView(), 1, 0);
		gridPane.add(shuLabel.getView(), 2, 0);

		gridPane.add(sau, 0, 1);
		gridPane.add(new Group(pfu), 2, 1);

		gridPane.add(new Group(mbSlots), 1, 2);
		gridPane.add(new Group(uSlots), 2, 2);
		gridPane.add(new Group(lSlots), 2, 3);

		cabinetView.setPadding(new Insets(SPACING));
		cabinetView.getChildren().addAll(externalFanLabel, gridPane);

		// externalFanLabel.prefWidthProperty().bind(gridPane.widthProperty());

		place(rbs);

		gridPane.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				Platform.runLater(() -> {
					externalFanLabel.setPrefWidth(newValue.doubleValue());
				});
				gridPane.widthProperty().removeListener(this);
			}
		});
		cabinetView.setSpacing(CABINET_SPACING);
	}

	public HBox getlSlots() {
		return lSlots;
	}

	public HBox getuSlots() {
		return uSlots;
	}

	public Label getExternalFanLabel() {
		return externalFanLabel;
	}

	public List<String> getuSlotNames() {
		return uSlotNames;
	}

	public List<String> getlSlotNames() {
		return lSlotNames;
	}

	public List<String> getExtensionNames() {
		return extensionNames;
	}
}
