package hr.hashcode.cables.visual.huge.cabinets;

import java.util.ArrayList;
import java.util.List;

import hr.hashcode.cables.equip.comp.ExtensionRack;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.Subrack;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author Fran Stanic
 */
public class RBS6202 extends RbsVisual {

	private HBox uSlots;
	private VBox xSlots;

	private Label scuLabel;

	private Label pduLabel;

	private Label pfuLabel;

	private static final double CABINET_SPACING = 55;

	private List<String> uSlotNames;
	private List<String> extensionNames;

	private static final double SAU_WIDTH = 5 * SLOT_HEIGHT;
	private static final double SAU_HEIGHT = SLOT_HEIGHT / 2;

	public RBS6202(MacroRbs rbs) {
		scuLabel = createLabel("SCU", RbsVisual.SHU_COLOR, SHU_WIDTH * 1.5, SHU_HEIGHT);

		HBox scuHolder = new HBox(scuLabel);
		scuHolder.setAlignment(Pos.CENTER_RIGHT);

		pduLabel = createLabel("PDU", RbsVisual.EC_OTHERS_COLOR, SAU_WIDTH, SAU_HEIGHT);
		pfuLabel = createLabel("PFU", RbsVisual.EC_OTHERS_COLOR, SAU_WIDTH, SAU_HEIGHT);

		List<HBox> hboces = new ArrayList<>();

		for (Subrack subrack : rbs.subracks()) {
			HBox box = (HBox) getSlotGroup(new SlotGroupDefinition(false, subrack.slots().size() + 1, x -> {
				if (x == 0)
					return duwId(subrack.name());
				else
					return radioId(subrack.name(), subrack.slots().get(x - 1));
			}, SLOT_WIDTH, SLOT_HEIGHT));
			hboces.add(box);
		}

		uSlots = hboces.get(0);
		uSlotNames = getSubrackName(rbs.subracks().get(0));

		List<VBox> boces = new ArrayList<>();

		for (ExtensionRack extension : rbs.extensions()) {
			List<String> slots = extension.slots();
			VBox box = (VBox) getSlotGroup(new SlotGroupDefinition(true, slots.size(), x -> extensionId(extension.name(), slots.get(x)),
																		  SLOT_WIDTH, SLOT_HEIGHT / 2));
			boces.add(box);
		}

		xSlots = boces.get(0);
		extensionNames = getExtensionName(rbs.extensions().get(0));

		place(rbs);

		cabinetView.setPadding(new Insets(SPACING));
		cabinetView.getChildren().addAll(scuHolder, pduLabel, new Group(uSlots), pfuLabel, new Group(xSlots));
		cabinetView.setSpacing(CABINET_SPACING);
	}

	public List<String> getuSlotNames() {
		return uSlotNames;
	}

	public HBox getuSlots() {
		return uSlots;
	}

	public List<String> getExtensionNames() {
		return extensionNames;
	}

	public VBox getxSlots() {
		return xSlots;
	}

	public Label getPfuLabel() {
		return pfuLabel;
	}
}
