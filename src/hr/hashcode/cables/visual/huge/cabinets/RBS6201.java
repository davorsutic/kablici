package hr.hashcode.cables.visual.huge.cabinets;

import java.util.ArrayList;
import java.util.List;

import hr.hashcode.cables.equip.comp.ExtensionRack;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.Subrack;
import hr.hashcode.cables.visual.huge.Controller;
import hr.hashcode.cables.visual.huge.units.ExternalRadioUnitSimpleController;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author Fran Stanic
 */
public class RBS6201 extends RbsVisual {

	private final HBox lSlots;
	private final HBox uSlots;
	private final VBox xSlots;

	private Label externalFanLabel;
	private Label pcfLabel;

	private Label pdu1Label;

	private Label pdu2Label;

	private Label pfuLabel;

	private List<String> uSlotNames;
	private List<String> lSlotNames;
	private List<String> extensionNames;

	private static final double SAU_WIDTH = 5 * SLOT_HEIGHT;
	private static final double SAU_HEIGHT = SLOT_HEIGHT / 2;

	private static final double CABINET_SPACING = 55;

	public RBS6201(MacroRbs rbs) {

		externalFanLabel = createLabel("FAN", SlotController.SLOT_COLOR, SAU_WIDTH, SLOT_HEIGHT);
		Controller.setBorder(externalFanLabel, BORDER_WIDTH);

		pcfLabel = createLabel("PCF", SlotController.SLOT_COLOR, SHU_WIDTH, SHU_HEIGHT);

		SlotController scuLabel = createSlotController("SCU", "SCU", false, SHU_WIDTH, SHU_HEIGHT);
		scuLabel.setColor(RbsVisual.SHU_COLOR);
		Controller.setBorder(scuLabel.getUnitPane(), BORDER_WIDTH);
		SlotController shuLabel = createSlotController("SHU", "SHU", false, SHU_WIDTH, SHU_HEIGHT);
		shuLabel.setColor(RbsVisual.SHU_COLOR);
		Controller.setBorder(shuLabel.getUnitPane(), BORDER_WIDTH);

		pdu1Label = createLabel("PDU1", RbsVisual.EC_OTHERS_COLOR, SAU_WIDTH, SAU_HEIGHT);
		pdu2Label = createLabel("PDU2", RbsVisual.EC_OTHERS_COLOR, SAU_WIDTH, SAU_HEIGHT);
		pfuLabel = createLabel("PFU", RbsVisual.EC_OTHERS_COLOR, SAU_WIDTH, SAU_HEIGHT);

		HBox h1 = new HBox(CABINET_SPACING, shuLabel.getView(), scuLabel.getView());
		AnchorPane row = new AnchorPane();
		AnchorPane.setLeftAnchor(pcfLabel, 0.);
		AnchorPane.setRightAnchor(h1, 0.);
		row.getChildren().addAll(pcfLabel, h1);

		List<HBox> hboces = new ArrayList<>();
		for (Subrack subrack : rbs.subracks()) {
			HBox box = (HBox) getSlotGroup(new SlotGroupDefinition(false, subrack.slots().size() + 1, x -> {
				if (x == 0)
					return duwId(subrack.name());
				else
					return radioId(subrack.name(), subrack.slots().get(x - 1));
			}, SLOT_WIDTH, SLOT_HEIGHT
			));
			hboces.add(box);
		}

		uSlots = hboces.get(0);
		lSlots = hboces.get(1);

		uSlotNames = getSubrackName(rbs.subracks().get(0));
		lSlotNames = getSubrackName(rbs.subracks().get(1));

		List<VBox> boces = new ArrayList<>();

		for (ExtensionRack extension : rbs.extensions()) {
			List<String> slots = extension.slots();
			VBox box = (VBox)getSlotGroup(new SlotGroupDefinition(true, slots.size(), x -> extensionId(extension.name(), slots.get(x)),
																		 SLOT_WIDTH, SLOT_HEIGHT / 2));
			boces.add(box);
		}

		xSlots = boces.get(0);
		extensionNames = getExtensionName(rbs.extensions().get(0));

		place(rbs);

		cabinetView.setPadding(new Insets(SPACING));
		cabinetView.getChildren().addAll(externalFanLabel,
				row, pdu1Label, new Group(uSlots), pdu2Label,
				new Group(lSlots), pfuLabel, new Group(xSlots));
		cabinetView.setSpacing(CABINET_SPACING);
	}

	public HBox getlSlots() {
		return lSlots;
	}

	public HBox getuSlots() {
		return uSlots;
	}

	public VBox getxSlots() {
		return xSlots;
	}

	public Label getExternalFanLabel() {
		return externalFanLabel;
	}

	public Label getPcfLabel() {
		return pcfLabel;
	}

	public Label getPdu1Label() {
		return pdu1Label;
	}

	public Label getPdu2Label() {
		return pdu2Label;
	}

	public Label getPfuLabel() {
		return pfuLabel;
	}

	public List<String> getuSlotNames() {
		return uSlotNames;
	}

	public List<String> getlSlotNames() {
		return lSlotNames;
	}

	public List<String> getExtensionNames() {
		return extensionNames;
	}
}
