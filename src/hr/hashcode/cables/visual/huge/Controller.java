package hr.hashcode.cables.visual.huge;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;

/**
 * @author Fran Stanic
 */
public abstract class Controller {

	protected Insets desiredBorder = Insets.EMPTY;
	protected double desiredWidth = -1, desiredHeight = -1;

	public abstract Node getView();

	public static void setBorder(Region region, double width) {
		setBorder(region, width, width, width, width);
	}

	public static void setBorder(Region region, double widthT, double widthR, double widthB, double widthL) {
		BorderWidths borderWidths = new BorderWidths(widthT, widthR, widthB, widthL);
		Border border = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, borderWidths));
		region.setBorder(border);
	}

	public static void setBorder(Region region, Insets widths) {
		setBorder(region, widths.getTop(), widths.getRight(), widths.getBottom(), widths.getLeft());
	}

	public void setSize(double width, double height) {
		this.desiredWidth = width;
		this.desiredHeight = height;
		Region view = (Region) getView();
		setSize(view,
				width - desiredBorder.getLeft() - desiredBorder.getRight(),
				height - desiredBorder.getTop() - desiredBorder.getBottom()
		);
	}

	public static void setSize(Region region, double width, double height) {
		region.setMinSize(width, height);
		region.setMaxSize(width, height);
	}

}
