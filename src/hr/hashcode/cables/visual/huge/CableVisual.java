package hr.hashcode.cables.visual.huge;

import java.util.Objects;

import hr.hashcode.cables.visual.huge.units.PortController;
import hr.hashcode.cables.visual.huge.units.UnitVisualController;

/**
 * @author Fran Stanic
 */
public class CableVisual {

	public final PortController port1;
	public final PortController port2;

	public CableVisual(PortController port1, PortController port2) {
		this.port1 = Objects.requireNonNull(port1);
		this.port2 = Objects.requireNonNull(port2);
	}

	public boolean isConnectedToUnit(UnitVisualController unit) {
		return port1.getUnit() == unit || port2.getUnit() == unit;
	}
}
