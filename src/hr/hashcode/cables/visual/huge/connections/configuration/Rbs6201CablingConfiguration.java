package hr.hashcode.cables.visual.huge.connections.configuration;

import hr.hashcode.cables.visual.huge.cabinets.RBS6201;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.scene.Node;

public class Rbs6201CablingConfiguration extends CablingConfiguration {
	private static final double CABLE_OFFSET = 8;
	private static final double OUTSIDE_OFFSET = 10;

	public Rbs6201CablingConfiguration(RbsVisual cabinet) {
		super(cabinet);
	}

	@Override
	public void defineCables(Node scene) {
		if(!(cabinet instanceof RBS6201)) {
			return;
		}
		RBS6201 rbs = (RBS6201) cabinet;
		String u1 = rbs.getuSlotNames().get(0);
		String l1 = rbs.getlSlotNames().get(0);
		defineSameSlotCables(scene, u1, rbs.getuSlotNames());
		defineSameSlotCables(scene, l1, rbs.getlSlotNames());
		defineU1L1Cable(scene, u1, l1);
		defineRuInterconnections(scene, rbs.getuSlotNames());
		defineRuInterconnections(scene, rbs.getlSlotNames());
		defineXInterconnections(scene, rbs.getExtensionNames(), 0.13);
		defineOppositeSlotCables(scene, l1, rbs.getuSlotNames(), rbs.getuSlots(), Cable.Direction.UP,  i -> CABLE_OFFSET * i);
		defineOppositeSlotCables(scene, u1, rbs.getlSlotNames(), rbs.getPdu2Label(), Cable.Direction.DOWN,
				i -> CABLE_OFFSET * (rbs.getuSlotNames().size() - i));
		String x1 = rbs.getExtensionNames().get(0);
		defineX1UCables(scene, rbs, x1);
		defineX1LCables(scene, rbs, x1);
		String x4 = rbs.getExtensionNames().get(3);
		defineX4UCables(scene, rbs, x4);
		defineX4LCables(scene, rbs, x4);
		String x2 = rbs.getExtensionNames().get(1);
		defineXMiddleUCables(scene, rbs, x2);
		defineXMiddleLCables(scene, rbs, x2);
		String x3 = rbs.getExtensionNames().get(2);
		defineXMiddleUCables(scene, rbs, x3);
		defineXMiddleLCables(scene, rbs, x3);

		rbs.getExtensionNames().forEach(n -> xPlacement.add(cabinet.getSlot(n).getView()));
	}

	private void defineX1UCables(Node scene, RBS6201 cabinet, String unit) {
		int n = cabinet.getuSlotNames().size();
		for (int i = 1;  i < n; i++) {
			defineCable(scene, getNodeColor(cabinet.getuSlotNames().get(i)),
					unit,Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (i + n)),
					Cable.UPPER_Y,
					cabinet.getuSlotNames().get(i), Cable.MIDDLE_X, Cable.LOWER_Y,
					Cable.Direction.LEFT,
					new Cable.ActionInput(CABLE_OFFSET * (n - i), Cable.Direction.UP),
					new Cable.ActionInput(cabinet.getPdu2Label(), CABLE_OFFSET * (n - i), Cable.Direction.RIGHT),
					new Cable.ActionInput(cabinet.getlSlots(), CABLE_OFFSET * i, Cable.Direction.RIGHT, Cable.Direction.UP),
					new Cable.ActionInput(cabinet.getPdu2Label(), CABLE_OFFSET * i, Cable.Direction.UP));
		}
	}

	private void defineX1LCables(Node scene, RBS6201 cabinet, String unit) {
		int n = cabinet.getlSlotNames().size();
		for (int i = 1;  i < n; i++) {
			defineCable(scene, getNodeColor(cabinet.getlSlotNames().get(i)),
					unit,Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * i),
					Cable.UPPER_Y,
					cabinet.getlSlotNames().get(i), Cable.MIDDLE_X, Cable.LOWER_Y,
					Cable.Direction.RIGHT,
					new Cable.ActionInput(CABLE_OFFSET * i, Cable.Direction.UP),
					new Cable.ActionInput(cabinet.getSlot(unit).getView(), CABLE_OFFSET * (n-i), Cable.Direction.LEFT),
					new Cable.ActionInput(cabinet.getPfuLabel(), CABLE_OFFSET * (n-i), Cable.Direction.UP));
		}
	}

	private void defineX4UCables(Node scene, RBS6201 cabinet, String unit) {
		int n = cabinet.getuSlotNames().size();
		for (int i = 1;  i < n; i++) {
			defineCable(scene, getNodeColor(cabinet.getuSlotNames().get(i)),
					unit,Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * ((n-i) + n)),
					Cable.LOWER_Y,
					cabinet.getuSlotNames().get(i), Cable.MIDDLE_X, Cable.LOWER_Y,
					Cable.Direction.LEFT,
					new Cable.ActionInput(CABLE_OFFSET * i, Cable.Direction.DOWN),
					new Cable.ActionInput(cabinet.getlSlots(), CABLE_OFFSET * i, Cable.Direction.RIGHT, Cable.Direction.UP),
					new Cable.ActionInput(cabinet.getPdu2Label(), CABLE_OFFSET * i, Cable.Direction.UP));
		}
	}

	private void defineX4LCables(Node scene, RBS6201 cabinet, String unit) {
		int n = cabinet.getlSlotNames().size();
		for (int i = 1;  i < n; i++) {
			defineCable(scene, getNodeColor(cabinet.getlSlotNames().get(i)),
					unit,Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (n-i)),
					Cable.LOWER_Y,
					cabinet.getlSlotNames().get(i), Cable.MIDDLE_X, Cable.LOWER_Y,
					Cable.Direction.RIGHT,
					new Cable.ActionInput(CABLE_OFFSET * (n-i), Cable.Direction.DOWN),
					new Cable.ActionInput(cabinet.getSlot(unit).getView(), CABLE_OFFSET * (n-i), Cable.Direction.LEFT),
					new Cable.ActionInput(cabinet.getPfuLabel(), CABLE_OFFSET * (n-i), Cable.Direction.UP));
		}
	}

	private void defineXMiddleUCables(Node scene, RBS6201 cabinet, String unit) {
		int n = cabinet.getuSlotNames().size();
		for (int i = 1;  i < n; i++) {
			defineCable(scene, getNodeColor(cabinet.getuSlotNames().get(i)),
					unit, Cable.RIGHT_X,
					Cable.Y_ALPHA((1.0 / (MIN_PORTS_PER_NODE + 1)) * i),
					cabinet.getuSlotNames().get(i), Cable.MIDDLE_X, Cable.LOWER_Y,
					Cable.Direction.LEFT,
					new Cable.ActionInput(CABLE_OFFSET * i, Cable.Direction.RIGHT),
					new Cable.ActionInput(cabinet.getlSlots(), CABLE_OFFSET * i, Cable.Direction.RIGHT, Cable.Direction.UP),
					new Cable.ActionInput(cabinet.getPdu2Label(), CABLE_OFFSET * i, Cable.Direction.UP));
		}
	}

	private void defineXMiddleLCables(Node scene, RBS6201 cabinet, String unit) {
		int n = cabinet.getlSlotNames().size();
		for (int i = 1;  i < n; i++) {
			defineCable(scene, getNodeColor(cabinet.getlSlotNames().get(i)),
					unit, Cable.LEFT_X,
					Cable.Y_ALPHA((1.0 / (MIN_PORTS_PER_NODE + 1)) * (n-i)),
					cabinet.getlSlotNames().get(i), Cable.MIDDLE_X, Cable.LOWER_Y,
					Cable.Direction.RIGHT,
					new Cable.ActionInput(CABLE_OFFSET * (n-i), Cable.Direction.LEFT),
					new Cable.ActionInput(cabinet.getPfuLabel(), CABLE_OFFSET * (n-i), Cable.Direction.UP));
		}
	}

	@Override
	public void startOutSideCabinetConnection(Node node, Cable cable) {
		if(isRu(node)) {
			startOutsideCabinetConnectionFromRu(node, cable);
		} else {
			super.startOutSideCabinetConnection(node, cable);
			int alpha = externalConnections.get(node);
			cable.startConnection(node, Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (PORTS_PER_NODE - alpha)), Cable.LOWER_Y);
			cable.goAroundCollisionNode(cabinet.getCabinetView(), OUTSIDE_OFFSET * alpha, Cable.Direction.DOWN);
		}
	}

	@Override
	public void finishOutSideCabinetConnection(Node node, Cable cable) {
		super.finishOutSideCabinetConnection(node, cable);
		int alpha = externalConnections.get(node);
		cable.finishConnection(Cable.Direction.RIGHT, node, Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * alpha), Cable.LOWER_Y);
	}
}
