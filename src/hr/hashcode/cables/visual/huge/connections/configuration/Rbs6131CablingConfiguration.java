package hr.hashcode.cables.visual.huge.connections.configuration;

import java.util.List;

import hr.hashcode.cables.visual.huge.cabinets.RBS6131;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.scene.Node;

public class Rbs6131CablingConfiguration extends CablingConfiguration {
	private static final double CABLE_OFFSET = 8;
	private static final double OUTSIDE_OFFSET = 20;

	// Special connection to external node
	private Node duw1Placement;

	public Rbs6131CablingConfiguration(RbsVisual cabinet) {
		super(cabinet);
	}

	@Override
	public void defineCables(Node scene) {
		RBS6131 rbs = getCabinet();
		if(cabinet == null) {
			return;
		}
		String u1 = rbs.getuSlotNames().get(0);
		duw1Placement = cabinet.getSlot(u1).getView();
		String l1 = rbs.getlSlotNames().get(0);
		defineSameSlotCables(scene, u1, rbs.getuSlotNames());
		defineSameSlotCables(scene, l1, rbs.getlSlotNames());
		defineU1L1Cable(scene, u1, l1);
		defineRuInterconnections(scene, rbs.getuSlotNames());
		defineRuInterconnections(scene, rbs.getlSlotNames());
		defineXInterconnections(scene, rbs.getExtensionNames());
		defineOppositeSlotCables(scene, l1, rbs.getuSlotNames(), rbs.getuSlots(), Cable.Direction.UP,  i -> CABLE_OFFSET * i);
		defineOppositeSlotCables(scene, u1, rbs.getlSlotNames(), rbs.getuSlots(), Cable.Direction.DOWN,
				i -> CABLE_OFFSET * (rbs.getuSlotNames().size() - i));
		String x1 = rbs.getExtensionNames().get(0);
		defineX1SlotCables(scene, x1, rbs.getuSlotNames(), null, 0, CABLE_OFFSET * 1.7);
		defineX1SlotCables(scene, x1, rbs.getlSlotNames(), rbs.getlSlots(), rbs.getuSlotNames().size(), CABLE_OFFSET);
		String x2 = rbs.getExtensionNames().get(1);
		defineX2SlotCables(scene, x2, rbs.getuSlotNames(), rbs.getuSlots(), 0, CABLE_OFFSET * 1.7);
		defineX2SlotCables(scene, x2, rbs.getlSlotNames(), rbs.getlSlots(), rbs.getuSlotNames().size(), CABLE_OFFSET);
		rbs.getExtensionNames().forEach(n -> xPlacement.add(cabinet.getSlot(n).getView()));
	}

	private RBS6131 getCabinet() {
		if (!(cabinet instanceof RBS6131)) {
			return null;
		}
		return (RBS6131) cabinet;
	}

	private void defineX2SlotCables(Node scene, String unit, List<String> slotNames, Node collisionNode, int portOffset,
										double cableOffset) {
		int n = slotNames.size();
		for (int i = 1; i < n; i++) {
			defineCable(scene, getNodeColor(slotNames.get(i)),
					unit,
					Cable.RIGHT_X,
					Cable.Y_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (i + portOffset)),
					slotNames.get(i), Cable.MIDDLE_X, Cable.LOWER_Y,
					Cable.Direction.RIGHT,
					new Cable.ActionInput(CABLE_OFFSET * (n - i) + portOffset, Cable.Direction.RIGHT),
					new Cable.ActionInput(collisionNode, cableOffset * i, Cable.Direction.DOWN));
		}
	}

	private void defineX1SlotCables(Node scene, String unit, List<String> slotNames, Node collisionNode, int portOffset,
										double cableOffset) {
		int n = slotNames.size();
		for (int i = 1; i < n; i++) {
			defineCable(scene, getNodeColor(slotNames.get(i)),
					unit,
					Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * ((n - i) + portOffset)),
					Cable.LOWER_Y,
					slotNames.get(i), Cable.MIDDLE_X, Cable.LOWER_Y,
					Cable.Direction.RIGHT,
					new Cable.ActionInput(collisionNode, cableOffset * i, Cable.Direction.DOWN));
		}
	}

	@Override
	public void startOutSideCabinetConnection(Node node, Cable cable) {
		if(isRu(node)) {
			startOutsideCabinetConnectionFromRu(node, cable);
		} else {
			super.startOutSideCabinetConnection(node, cable);
			int alpha = externalConnections.get(node);
			cable.startConnection(node, Cable.LEFT_X, Cable.Y_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (PORTS_PER_NODE - alpha)));
			cable.applyOffset(OUTSIDE_OFFSET * alpha, Cable.Direction.LEFT);
			cable.goAroundCollisionNode(cabinet.getCabinetView(), OUTSIDE_OFFSET * alpha, Cable.Direction.DOWN);
		}
	}

	@Override
	public void finishOutSideCabinetConnection(Node node, Cable cable) {
		super.finishOutSideCabinetConnection(node, cable);
		int alpha = externalConnections.get(node);
		cable.avoidCollisionNode(cabinet.getCabinetView(), CABLE_OFFSET * (MIN_PORTS_PER_NODE - alpha), Cable.Direction.RIGHT);
		cable.finishConnection(Cable.Direction.UP, node, Cable.LEFT_X, Cable.Y_ALPHA((1.0 / (PORTS_PER_NODE  + 1)) * alpha));
	}

	protected void defineXInterconnections(Node scene, List<String> xNames) {
		int n = xNames.size();
		for (int i = 0; i < n - 1; i++) {
			for (int j = i + 1; j < n; j++) {
				String unit1 = xNames.get(i);
				String unit2 = xNames.get(j);
				defineCable(scene, getNodeColor(unit1, unit2), unit1, Cable.MIDDLE_X, Cable.UPPER_Y,
						unit2, Cable.MIDDLE_X, Cable.UPPER_Y, Cable.Direction.RIGHT,
						new Cable.ActionInput(2 * CABLE_OFFSET, Cable.Direction.UP));
			}
		}
	}

	@Override
	protected Node getStartExternalNode(Node node) {
		if (node == duw1Placement) {
			return node;
		} else {
			return cabinet.getCabinetView();
		}
	}
}
