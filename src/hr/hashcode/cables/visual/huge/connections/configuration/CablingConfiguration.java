package hr.hashcode.cables.visual.huge.connections.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import hr.hashcode.cables.visual.huge.units.DigitalUnitSimpleController;
import hr.hashcode.cables.visual.huge.units.RadioUnitSimpleController;
import javafx.beans.property.BooleanProperty;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.util.Pair;

public abstract class CablingConfiguration {
	public static final double CABLE_OFFSET = 8;
	public static final double OUTSIDE_OFFSET = 10;
	public static final double CABLE_WIDTH = 2.5;

	public static final int MIN_PORTS_PER_NODE = 6;
	public static final int MAX_PORTS_PER_NODE = 12;


	public static final Color DEFAULT_COLOR = Color.BLACK;
	public static final Color U1L1_COLOR = Color.ROSYBROWN;
	public static final Map<Band, Color> BAND_COLORS;

	static {
		BAND_COLORS = new HashMap<>();
		BAND_COLORS.put(Band.B1, Color.BLUE);
		BAND_COLORS.put(Band.B8, Color.GREEN);
		BAND_COLORS.put(Band.B11, Color.ORANGE);
		BAND_COLORS.put(Band.B3, Color.RED);
	}

	public static int PORTS_PER_NODE = MAX_PORTS_PER_NODE;

	protected Map<Node, Integer> externalConnections = new HashMap<>();
	protected RbsVisual cabinet;
	protected Map<Node, String> nodeToUnitName = new HashMap<>();
	protected Map<Pair<String, String>, Cable> unitsToCable = new HashMap<>();
	private Map<Node, Set<Band>> nodeToBands;
	protected List<Node> xPlacement = new ArrayList<>();

	public CablingConfiguration(RbsVisual cabinet) {
		this.cabinet = cabinet;
	}

	public abstract void defineCables(Node scene);

	public Cable getCable(Node scene, Color color, Node unit1, Function<Bounds, Double> x1, Function<Bounds, Double> y1,
								 Node unit2, Function<Bounds, Double> x2, Function<Bounds, Double> y2,
								 Cable.Direction finishDirection, Cable.ActionInput... actions) {
		Cable cable = new Cable(scene, color);
		cable.startConnection(unit1, x1, y1);
		cable.applyActions(actions);
		if (finishDirection != null) {
			cable.finishConnection(finishDirection, unit2, x2, y2);
		} else {
			cable.finishConnection(unit2, x2, y2);
		}
		return cable;
	}

	protected Pair<String, String> getOrderedUnitNames(String unit1, String unit2) {
		return unit1.compareTo(unit2) < 0 ? new Pair<>(unit1, unit2) : new Pair<>(unit2, unit1);
	}

	public void defineCable(Node scene, Color color, String name1, Function<Bounds, Double> x1, Function<Bounds, Double> y1,
								   String name2, Function<Bounds, Double> x2, Function<Bounds, Double> y2,
								   Cable.Direction finishDirection, Cable.ActionInput... actions) {
		unitsToCable.put(getOrderedUnitNames(name1, name2),
				getCable(scene, color, cabinet.getSlot(name1).getView(), x1, y1, cabinet.getSlot(name2).getView(),
						x2, y2, finishDirection, actions));

		nodeToUnitName.put(cabinet.getSlot(name1).getView(), name1);
		nodeToUnitName.put(cabinet.getSlot(name2).getView(), name2);
	}

	public boolean containsNode(Node n) {
		return nodeToUnitName.containsKey(n);
	}

	public void registerAllCables(Group group, BooleanProperty switach) {
		for (Cable c : unitsToCable.values()) {
			c.registerCable(group, switach, CABLE_WIDTH);
		}
	}

	public boolean registerCable(Group group, BooleanProperty switach, Node unit1, Node unit2) {
		String name1 = nodeToUnitName.get(unit1);
		String name2 = nodeToUnitName.get(unit2);
		if (name1 != null && name2 != null) {
			Cable cable = unitsToCable.get(getOrderedUnitNames(name1, name2));
			if (cable == null) {
				return false;
			}
			cable.registerCable(group, switach, CABLE_WIDTH);
			return true;
		}
		return false;
	}

	public Node getCabinetNode() {
		return cabinet.getCabinetView();
	}

	public void registerNodeToBand(Map<Node, Set<Band>> nodeToBand) {
		this.nodeToBands = nodeToBand;
	}

	protected Color getNodeColor(String unitName) {
		Node node = cabinet.getSlot(unitName).getView();
		return getNodeColor(node);
	}

	protected Color getNodeColor(Node node) {
		if (nodeToBands != null && nodeToBands.containsKey(node)) {
			Set<Band> bands = nodeToBands.get(node);
			if (!bands.isEmpty()) {
				return BAND_COLORS.get(bands.iterator().next());
			}
		}

		return DEFAULT_COLOR;
	}

	protected Color getNodeColor(String unit1, String unit2) {
		Color c1 = getNodeColor(unit1);
		return c1 == DEFAULT_COLOR ? getNodeColor(unit2) : c1;
	}

	protected Color getNodeColor(Node unit1, Node unit2) {
		Color c1 = getNodeColor(unit1);
		return c1 == DEFAULT_COLOR ? getNodeColor(unit2) : c1;
	}

	protected void defineSameSlotCables(Node scene, String unit, List<String> rbsNames) {
		for (int i = 1; i < rbsNames.size(); i++) {
			defineCable(scene, getNodeColor(rbsNames.get(i)), rbsNames.get(i), Cable.MIDDLE_X, Cable.UPPER_Y,
					unit, Cable.X_ALPHA((1.0 / (MIN_PORTS_PER_NODE + 1)) * (rbsNames.size() - i)), Cable.UPPER_Y,
					Cable.Direction.LEFT,
					new Cable.ActionInput(CABLE_OFFSET * i, Cable.Direction.UP));
		}
	}

	protected void defineU1L1Cable(Node scene, String u1, String l1) {
		defineCable(scene, U1L1_COLOR, u1, Cable.LEFT_X, Cable.Y_ALPHA(0.9), l1, Cable.LEFT_X, Cable.Y_ALPHA(0.1),
				Cable.Direction.DOWN,
				new Cable.ActionInput(2 * CABLE_OFFSET, Cable.Direction.LEFT));
	}

	protected void defineRuInterconnections(Node scene, List<String> ruNames) {
		for (int i = 1; i < ruNames.size() - 1; i += 2) {
			defineCable(scene, getNodeColor(ruNames.get(i)), ruNames.get(i), Cable.X_ALPHA(0.7), Cable.Y_ALPHA(0.8),
					ruNames.get(i + 1), Cable.X_ALPHA(0.3), Cable.Y_ALPHA(0.7), null);
		}
	}

	protected void defineXInterconnections(Node scene, List<String> xNames, double offset) {
		int n = xNames.size();
		for (int i = 0; i < n - 1; i++) {
			for (int j = i + 1; j < n; j++) {
				String unit1 = xNames.get(i);
				String unit2 = xNames.get(j);
				defineCable(scene, getNodeColor(unit1, unit2), unit1, Cable.RIGHT_X, Cable.MIDDLE_Y,
						unit2, Cable.RIGHT_X, Cable.MIDDLE_Y, Cable.Direction.DOWN,
						new Cable.ActionInput(2 * CABLE_OFFSET, Cable.Direction.RIGHT));
			}
		}
	}

	protected void defineOppositeSlotCables(Node scene, String unit, List<String> rbsNames, Node collisionNode, Cable.Direction direction,
												   Function<Integer, Double> offsetFunction) {
		for (int i = 1; i < rbsNames.size(); i++) {
			defineCable(scene, getNodeColor(rbsNames.get(i)), unit, Cable.LEFT_X, Cable.Y_ALPHA((1.0 / (MIN_PORTS_PER_NODE + 1)) * i),
					rbsNames.get(i), Cable.MIDDLE_X, Cable.UPPER_Y,
					Cable.Direction.RIGHT,
					new Cable.ActionInput(offsetFunction.apply(i), Cable.Direction.LEFT),
					new Cable.ActionInput(collisionNode, offsetFunction.apply(i), direction));
		}
	}

	public void startOutSideCabinetConnection(Node node, Cable cable) {
		externalConnections.computeIfAbsent(node, n -> 0);
		externalConnections.computeIfPresent(node, (n, i) -> i + 1);
	}

	public void finishOutSideCabinetConnection(Node node, Cable cable) {
		externalConnections.computeIfAbsent(node, n -> 0);
		externalConnections.computeIfPresent(node, (n, i) -> i + 1);
	}

	public boolean isDu(Node node) {
		return cabinet.getSlots().stream()
					   .filter(s -> s.getUnitVisualController() != null && s.getUnitVisualController() instanceof DigitalUnitSimpleController)
					   .map(s -> s.getView())
					   .filter(v -> v == node).count() > 0;

	}

	public boolean isRu(Node node) {
		return cabinet.getSlots().stream()
					   .filter(s -> s.getUnitVisualController() != null && (s.getUnitVisualController() instanceof RadioUnitSimpleController))
					   .map(s -> s.getView())
					   .filter(n -> n == node).count() > 0;

	}

	public List<Node> getAllRus() {
		return cabinet.getSlots().stream().filter(s -> isRu(s.getView())).map(s -> s.getView()).collect(Collectors.toList());
	}

	protected void startOutsideCabinetConnectionFromRu(Node ru, Cable cable) {
		int offset1 = getNumberOfNodesInDirectionHorizontal(ru, getAllRus(), cable, Cable.Direction.RIGHT) + 1;
		int offset2 = getNumberOfNodesInDirectionHorizontal(ru, getAllRus(), cable, Cable.Direction.LEFT) + 1;
		cable.startConnection(ru, Cable.MIDDLE_X, Cable.LOWER_Y);
		cable.applyAction(new Cable.ActionInput(CABLE_OFFSET * offset1, Cable.Direction.DOWN));
		cable.goAroundCollisionNode(cabinet.getCabinetView(), OUTSIDE_OFFSET * offset2, Cable.Direction.RIGHT);
		cable.goAroundCollisionNode(cabinet.getCabinetView(), OUTSIDE_OFFSET * offset1, Cable.Direction.DOWN);
	}

	public int getNumberOfNodesInDirectionHorizontal(Node node, List<Node> nodes, Cable cable, Cable.Direction direction) {
		int counter = 0;
		for(Node n : nodes) {
			if(cable.getNodeRelationHorizontal(node, n) == direction) {
				++counter;
			}
		}
		return counter;
	}

	// for opposite ending directions, we observe opposite collision directions
	// if lower side of node -> end direction is up
	public void startExternalCabinetConnection(Node node, Cable cable, int numberOfCollisionLeftNodes, int numberOfCollisionRightNodes,
													  Cable.Direction endDirection) {
		externalConnections.computeIfAbsent(node, n -> 0);
		externalConnections.computeIfPresent(node, (n, i) -> i + 1);
		cable.startConnection(node, getStartExternalX(node, numberOfCollisionLeftNodes, numberOfCollisionRightNodes, endDirection),
				getStartExternalY(node, numberOfCollisionLeftNodes, numberOfCollisionRightNodes, endDirection));
		cable.goAroundCollisionNode(getStartExternalNode(node),
				getExternalOffset(node, cable, numberOfCollisionLeftNodes, numberOfCollisionRightNodes, endDirection),
				getStartExternalDirection(node));
		cable.goAroundCollisionNode(cabinet.getCabinetView(), OUTSIDE_OFFSET, Cable.Direction.UP);
	}

	protected Node getStartExternalNode(Node startNode) {
		return cabinet.getCabinetView();
	}

	protected Cable.Direction getStartExternalDirection(Node startNode) {
		return Cable.Direction.LEFT;
	}

	protected Function<Bounds, Double> getStartExternalX(Node startNode, int numberOfCollisionLeftNodes, int numberOfCollisionRightNodes,
																Cable.Direction endDirection) {
		return Cable.LEFT_X;
	}

	protected Function<Bounds, Double> getStartExternalY(Node startNode, int numberOfCollisionLeftNodes, int numberOfCollisionRightNodes,
																Cable.Direction endDirection) {
		if(endDirection == Cable.Direction.UP) {
			return Cable.Y_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (numberOfCollisionRightNodes + 1));
		} else if(endDirection == Cable.Direction.DOWN) {
			return Cable.Y_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (numberOfCollisionLeftNodes + 1));
		}
		return Cable.MIDDLE_Y;
	}


	protected int getNumberOfNodesInDirectionVertical(Node node, Cable cable, List<Node> compareNodes, Cable.Direction direction) {
		int counter = 0;
		for(Node n : compareNodes) {
			if(cable.getNodeRelationVertical(node, n) == direction) {
				++counter;
			}
		}
		return counter;
	}


	public double getExternalOffset(Node n, Cable cable, int numberOfCollisionLeftNodes, int numberOfCollisionRightNodes, Cable.Direction
																													 endDirection) {
		// offset is switched by number of nodes bellow in case cables appear in both nodes
		int numberOfNodesAbove = getNumberOfNodesAbove(n, cable) * MIN_PORTS_PER_NODE;
		if(endDirection == Cable.Direction.UP) {
			return (numberOfCollisionRightNodes + numberOfNodesAbove + 1) * CABLE_OFFSET ;
		} else if(endDirection == Cable.Direction.DOWN) {
			return (numberOfCollisionLeftNodes + numberOfNodesAbove + 1) * CABLE_OFFSET;
		}

		return CABLE_OFFSET * (1 + numberOfNodesAbove);
	}

	protected int getNumberOfNodesAbove(Node node, Cable cable) {
		if(xPlacement.contains(node)) {
			return getNumberOfNodesInDirectionVertical(node, cable, xPlacement, Cable.Direction.UP);
		}
		return 0;
	}
}
