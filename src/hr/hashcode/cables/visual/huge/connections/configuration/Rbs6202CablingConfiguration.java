package hr.hashcode.cables.visual.huge.connections.configuration;

import hr.hashcode.cables.visual.huge.cabinets.RBS6202;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.scene.Node;

public class Rbs6202CablingConfiguration extends CablingConfiguration {
	private static final double CABLE_OFFSET = 8;
	private static final double OUTSIDE_OFFSET = 20;


	public Rbs6202CablingConfiguration(RbsVisual cabinet) {
		super(cabinet);
	}

	@Override
	public void defineCables(Node scene) {
		if(!(cabinet instanceof RBS6202)) {
			return;
		}
		RBS6202 rbs = (RBS6202) cabinet;
		String u1 = rbs.getuSlotNames().get(0);
		defineSameSlotCables(scene, u1, rbs.getuSlotNames());
		defineRuInterconnections(scene, rbs.getuSlotNames());
		defineXInterconnections(scene, rbs.getExtensionNames(), 0.3);
		String x1 = rbs.getExtensionNames().get(0);
		defineX1UCables(scene, rbs, x1);
		String x4 = rbs.getExtensionNames().get(1);
		defineX4UCables(scene, rbs, x4);

		rbs.getExtensionNames().forEach(n -> xPlacement.add(cabinet.getSlot(n).getView()));
	}


	protected void defineX1UCables(Node scene, RBS6202 cabinet, String unit) {
		int n = cabinet.getuSlotNames().size();
		for (int i = 1;  i < n; i++) {
			defineCable(scene, getNodeColor(cabinet.getuSlotNames().get(i)),
					unit,Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * i),
					Cable.UPPER_Y,
					cabinet.getuSlotNames().get(i), Cable.MIDDLE_X, Cable.LOWER_Y,
					Cable.Direction.RIGHT,
					new Cable.ActionInput(CABLE_OFFSET * i, Cable.Direction.UP),
					new Cable.ActionInput(cabinet.getPfuLabel(), CABLE_OFFSET * (n-i), Cable.Direction.LEFT, Cable.Direction.UP));
		}
	}

	protected void defineX4UCables(Node scene, RBS6202 cabinet, String unit) {
		int n = cabinet.getuSlotNames().size();
		for (int i = 1;  i < n; i++) {
			defineCable(scene, getNodeColor(cabinet.getuSlotNames().get(i)),
					unit,Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (n-i)),
					Cable.LOWER_Y,
					cabinet.getuSlotNames().get(i), Cable.MIDDLE_X, Cable.LOWER_Y,
					Cable.Direction.RIGHT,
					new Cable.ActionInput(CABLE_OFFSET * (n-i), Cable.Direction.DOWN),
					new Cable.ActionInput(cabinet.getSlot(unit).getView(), CABLE_OFFSET * (n - i), Cable.Direction.LEFT),
					new Cable.ActionInput(cabinet.getPfuLabel(), CABLE_OFFSET * (n-i), Cable.Direction.UP));
		}
	}


	@Override
	public void startOutSideCabinetConnection(Node node, Cable cable) {
		if(isRu(node)) {
			startOutsideCabinetConnectionFromRu(node, cable);
		} else {
			super.startOutSideCabinetConnection(node, cable);
			int alpha = externalConnections.get(node);
			cable.startConnection(node, Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (PORTS_PER_NODE - alpha)), Cable.LOWER_Y);
			cable.goAroundCollisionNode(cabinet.getCabinetView(), OUTSIDE_OFFSET * alpha, Cable.Direction.DOWN);
		}
	}

	@Override
	public void finishOutSideCabinetConnection(Node node, Cable cable) {
		super.finishOutSideCabinetConnection(node, cable);
		int alpha = externalConnections.get(node);
		cable.finishConnection(Cable.Direction.RIGHT, node, Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * alpha), Cable.LOWER_Y);
	}
}
