package hr.hashcode.cables.visual.huge.connections.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import hr.hashcode.cables.visual.huge.cabinets.RBS6102;
import hr.hashcode.cables.visual.huge.cabinets.RBS6131;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.geometry.Bounds;
import javafx.scene.Node;

public class Rbs6102CablingConfiguration extends CablingConfiguration {
	private static final double CABLE_OFFSET = 8;
	private static final double OUTSIDE_OFFSET = 20;

	// Special connection to external node
	private Node duw1Placement;
	private List<Node> uXPlacement = new ArrayList<>();
	private List<Node> lXPlacement = new ArrayList<>();

	public Rbs6102CablingConfiguration(RbsVisual cabinet) {
		super(cabinet);
	}

	@Override
	public void defineCables(Node scene) {
		RBS6102 rbs = getCabinet();
		if(rbs == null) {
			return;
		}
		String u1 = rbs.getuSlotNames().get(0);
		duw1Placement = cabinet.getSlot(u1).getView();
		String l1 = rbs.getlSlotNames().get(0);
		defineSameSlotCables(scene, u1, rbs.getuSlotNames());
		defineSameSlotCables(scene, l1, rbs.getlSlotNames());
		defineU1L1Cable(scene, u1, l1);
		defineRuInterconnections(scene, rbs.getuSlotNames());
		defineRuInterconnections(scene, rbs.getlSlotNames());
		defineXInterconnections(scene, rbs.getlXSlotNames(), 0.1);
		defineXInterconnections(scene, rbs.getuXSlotNames(), 0.1);
		defineOppositeSlotCables(scene, l1, rbs.getuSlotNames(), Cable.Direction.RIGHT, i -> CABLE_OFFSET * i, i -> CABLE_OFFSET * i);
		defineOppositeSlotCables(scene, u1, rbs.getlSlotNames(), Cable.Direction.LEFT,
				i -> CABLE_OFFSET * i, i -> CABLE_OFFSET * (rbs.getuSlotNames().size() - i));
		defineX1SlotCables(scene, rbs.getlXSlotNames().get(0), rbs.getlSlotNames());
		defineX1SlotCables(scene, rbs.getuXSlotNames().get(0), rbs.getuSlotNames());
		defineXSameSideCables(scene, rbs);
		defineXOppositeSideCables(scene, rbs);
		defineOppositeXInterconnection(scene, rbs.getuXSlotNames(), rbs.getlXSlotNames(), Cable.Direction.LEFT, Cable.Direction.RIGHT,
				((RBS6102) cabinet).getScuLabel());
		defineOppositeXInterconnection(scene, rbs.getlXSlotNames(), rbs.getuXSlotNames(), Cable.Direction.RIGHT, Cable.Direction.LEFT,
				((RBS6102) cabinet).getScuLabel());
		rbs.getuXSlotNames().forEach(n -> uXPlacement.add(cabinet.getSlot(n).getView()));
		rbs.getlXSlotNames().forEach(n -> lXPlacement.add(cabinet.getSlot(n).getView()));
	}

	private RBS6102 getCabinet() {
		if (!(cabinet instanceof RBS6102)) {
			return null;
		}
		return  (RBS6102) cabinet;
	}


	@Override
	protected void defineU1L1Cable(Node scene, String u1, String l1) {
		RBS6102 rbs = getCabinet();
		if(cabinet == null) {
			return;
		}
		defineCable(scene, U1L1_COLOR, u1, Cable.LEFT_X, Cable.Y_ALPHA(0.9), l1, Cable.LEFT_X, Cable.Y_ALPHA(0.9),
				Cable.Direction.UP,
				new Cable.ActionInput(rbs.getlSlots(), CABLE_OFFSET, Cable.Direction.RIGHT, Cable.Direction.DOWN, Cable.Direction.LEFT));
	}

	private void defineXSameSideCables(Node scene, RBS6102 rbs) {
		for (int i = 1; i < rbs.getlXSlotNames().size(); i++) {
			defineXSameSideCables(scene, rbs.getuXSlotNames().get(i), rbs.getuXSlotNames().get(0), rbs.getuSlotNames(),
					Cable.Direction.RIGHT, Cable.Direction.LEFT);
			defineXSameSideCables(scene, rbs.getlXSlotNames().get(i), rbs.getlXSlotNames().get(0), rbs.getlSlotNames(),
					Cable.Direction.LEFT, Cable.Direction.RIGHT);
		}
	}

	private void defineXOppositeSideCables(Node scene, RBS6102 rbs) {
		for (int i = 0; i < rbs.getlXSlotNames().size(); i++) {
			defineXOppositeSideCables(scene, rbs.getlXSlotNames().get(i), rbs.getuSlotNames(), rbs.getuXSlotNames().get(0),
					Cable.Direction.RIGHT);
			defineXOppositeSideCables(scene, rbs.getuXSlotNames().get(i), rbs.getlSlotNames(), rbs.getlXSlotNames().get(0),
					Cable.Direction.LEFT);
			break;
		}
	}

	protected void defineOppositeSlotCables(Node scene, String unit, List<String> rbsNames, Cable.Direction direction,
												   Function<Integer, Double> offsetFunction1, Function<Integer, Double> offsetFunction2) {
		for (int i = 1; i < rbsNames.size(); i++) {
			defineCable(scene, getNodeColor(rbsNames.get(i)), unit, Cable.LEFT_X, Cable.Y_ALPHA((1.0 / (MIN_PORTS_PER_NODE + 1)) * i),
					rbsNames.get(i), Cable.MIDDLE_X, Cable.UPPER_Y,
					direction,
					new Cable.ActionInput(offsetFunction1.apply(i), Cable.Direction.LEFT),
					new Cable.ActionInput(cabinet.getSlot(unit).getView(), offsetFunction2.apply(i), Cable.Direction.UP));
		}
	}

	private void defineX1SlotCables(Node scene, String unit, List<String> rbsNames) {
		for (int i = 1; i < rbsNames.size(); i++) {
			defineCable(scene, getNodeColor(rbsNames.get(i)),
					unit, Cable.X_ALPHA((1.0 / (MIN_PORTS_PER_NODE + 1)) * i),
					Cable.UPPER_Y,
					rbsNames.get(i), Cable.MIDDLE_X, Cable.LOWER_Y,
					i == 1 ? Cable.Direction.LEFT : Cable.Direction.RIGHT,
					new Cable.ActionInput(CABLE_OFFSET * i, Cable.Direction.UP));
		}
	}

	private void defineXSameSideCables(Node scene, String unit, String upperUnit, List<String> rbsNames,
											  Cable.Direction directionSlot, Cable.Direction directionRu) {
		for (int i = 1; i < rbsNames.size(); i++) {
			String ru = directionSlot == Cable.Direction.LEFT ? rbsNames.get(i) : rbsNames.get(rbsNames.size() - i);
			defineCable(scene, getNodeColor(ru),
					unit, directionSlot == Cable.Direction.LEFT ? Cable.LEFT_X : Cable.RIGHT_X,
					Cable.Y_ALPHA((1.0 / (MIN_PORTS_PER_NODE + 1)) * (rbsNames.size() - i)),
					ru, Cable.MIDDLE_X, Cable.LOWER_Y,
					directionRu,
					new Cable.ActionInput(CABLE_OFFSET * (rbsNames.size() - i), directionSlot),
					new Cable.ActionInput(cabinet.getSlot(upperUnit).getView(), CABLE_OFFSET * (rbsNames.size() - i), Cable.Direction.UP));
		}
	}

	private void defineXOppositeSideCables(Node scene, String unit, List<String> rbsNames, String cornerUnit, Cable.Direction direction) {
		for (int i = 1; i < rbsNames.size(); i++) {
			String ru = direction == Cable.Direction.RIGHT ? rbsNames.get(i) : rbsNames.get(rbsNames.size() - i);
			defineCable(scene, getNodeColor(ru),
					unit,
					direction == Cable.Direction.RIGHT ? Cable.RIGHT_X : Cable.LEFT_X,
					Cable.Y_ALPHA((1.0 / (MIN_PORTS_PER_NODE + 1) * i)),
					ru, Cable.MIDDLE_X, Cable.LOWER_Y,
					direction,
					new Cable.ActionInput(3 * CABLE_OFFSET * i, direction),
					new Cable.ActionInput(cabinet.getSlot(cornerUnit).getView(), CABLE_OFFSET * (rbsNames.size() - i), Cable.Direction
																															   .UP));
		}
	}

	private void defineOppositeXInterconnection(Node scene, List<String> xFirst, List<String> xSecond,
													   Cable.Direction directionFirst, Cable.Direction directionSecond,
													   Node corner) {
		for (int i = 0; i < xFirst.size(); i++) {
			for (int j = 0; j < xSecond.size(); j++) {
				String unit1 = xFirst.get(i);
				String unit2 = xSecond.get(j);
				defineCable(scene, getNodeColor(unit1, unit2),
						unit1,
						directionFirst == Cable.Direction.RIGHT ? Cable.RIGHT_X : Cable.LEFT_X, Cable.MIDDLE_Y,
						unit2,
						directionSecond == Cable.Direction.RIGHT ? Cable.RIGHT_X : Cable.LEFT_X, Cable.MIDDLE_Y,
						Cable.Direction.DOWN,
						new Cable.ActionInput(corner, i * CABLE_OFFSET, directionFirst));
			}
		}
	}

	@Override
	public void startOutSideCabinetConnection(Node node, Cable cable) {
		super.startOutSideCabinetConnection(node, cable);
		if(isRu(node)) {
			startOutsideCabinetConnectionFromRu(node, cable);
		} else {
			int alpha = externalConnections.get(node);
			cable.startConnection(node, Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (PORTS_PER_NODE - alpha)), Cable.LOWER_Y);
			cable.goAroundCollisionNode(cabinet.getCabinetView(), OUTSIDE_OFFSET * alpha, Cable.Direction.DOWN);
		}
	}

	@Override
	public void finishOutSideCabinetConnection(Node node, Cable cable) {
		super.finishOutSideCabinetConnection(node, cable);
		int alpha = externalConnections.get(node);
		cable.finishConnection(Cable.Direction.RIGHT, node, Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * alpha), Cable.LOWER_Y);
	}

	@Override
	protected Node getStartExternalNode(Node node) {
		if (node == duw1Placement) {
			return node;
		} else {
			return cabinet.getCabinetView();
		}
	}

	@Override
	protected Cable.Direction getStartExternalDirection(Node startNode) {
		return uXPlacement.contains(startNode) ? Cable.Direction.RIGHT : Cable.Direction.LEFT;
	}

	@Override
	protected Function<Bounds, Double> getStartExternalY(Node startNode, int numberOfCollisionLeftNodes,
																int numberOfCollisionRightNodes, Cable.Direction endDirection) {
		if (uXPlacement.contains(startNode)) {
			if(endDirection == Cable.Direction.UP) {
				return Cable.Y_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (numberOfCollisionLeftNodes + 1));
			} else if(endDirection == Cable.Direction.DOWN) {
				return Cable.Y_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (numberOfCollisionRightNodes + 1));
			}
			return Cable.MIDDLE_Y;
		}
		return super.getStartExternalY(startNode, numberOfCollisionLeftNodes, numberOfCollisionRightNodes, endDirection);

	}

	@Override
	protected Function<Bounds, Double> getStartExternalX(Node startNode, int numberOfCollisionLeftNodes, int numberOfCollisionRightNodes,
																Cable.Direction endDirection) {
		return uXPlacement.contains(startNode) ? Cable.RIGHT_X : Cable.LEFT_X;
	}

	@Override
	public double getExternalOffset(Node n, Cable cable, int numberOfCollisionLeftNodes, int numberOfCollisionRightNodes,
											   Cable.Direction endDirection) {
		if (endDirection == Cable.Direction.UP) {
			return (numberOfCollisionLeftNodes + 1) * CABLE_OFFSET;
		} else if (endDirection == Cable.Direction.DOWN) {
			return (numberOfCollisionRightNodes + 1) * CABLE_OFFSET;
		}
		return super.getExternalOffset(n, cable, numberOfCollisionLeftNodes, numberOfCollisionRightNodes, endDirection);
	}

	@Override
	protected int getNumberOfNodesAbove(Node node, Cable cable) {
		if(uXPlacement.contains(node)) {
			return getNumberOfNodesInDirectionVertical(node, cable, uXPlacement, Cable.Direction.UP);
		} else if(lXPlacement.contains(node)) {
			return getNumberOfNodesInDirectionVertical(node, cable, lXPlacement, Cable.Direction.UP);
		}
		return 0;
	}
}
