package hr.hashcode.cables.visual.huge.connections.configuration;

import static hr.hashcode.cables.visual.huge.connections.configuration.Cable.Direction.DOWN;
import static hr.hashcode.cables.visual.huge.connections.configuration.Cable.Direction.LEFT;
import static hr.hashcode.cables.visual.huge.connections.configuration.Cable.Direction.RIGHT;
import static hr.hashcode.cables.visual.huge.connections.configuration.Cable.Direction.UP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import hr.hashcode.cables.equip.comp.RadioUnit;
import hr.hashcode.cables.visual.huge.units.ExternalRadioUnitSimpleController;
import javafx.scene.Node;

public class ExternalRadioCablingConfiguration {
	public static double CABLE_OFFSET = 10;
	List<ExternalRadioUnitSimpleController> radioUnitSimpleControllers = new ArrayList<>();
	Map<Node, ExternalRadioUnitSimpleController> radioNodeToExternalRadioUnit = new HashMap<>();
	Set<Node> connectedRadios = new HashSet<>();

	public void registerExternalRadio(ExternalRadioUnitSimpleController controller) {
		radioUnitSimpleControllers.add(controller);
		addRadios(controller);
	}

	public void finishConnection(Node radio, Cable cable) {
		finishConnection(radio, cable, getNumberOfNodesInDirectionHorizontal(radio, cable, LEFT),
				getNumberOfNodesInDirectionHorizontal(radio, cable, RIGHT),
				getEndDirection(radio));

	}

	public void finishConnection(Node radio, Cable cable, int numberOfCollisionLeftNodes, int numberOfCollisionRightNodes, Cable.Direction endDirection) {
		ExternalRadioUnitSimpleController externalRadio = radioNodeToExternalRadioUnit.get(radio);
		if (externalRadio == null) {
			return;
		}
		// radio can be above or bellow current position
		Cable.Direction radioPosition = cable.getCurrentRelationToNodeVertical(radio);
		if(radioPosition == UP) {
			double distanceToRadio = cable.getCurrentRelationToNodeHorizontal(radio) == Cable.Direction.LEFT ?
											 numberOfCollisionLeftNodes * CABLE_OFFSET :
											 numberOfCollisionRightNodes * CABLE_OFFSET;
			cable.applyOffset(distanceToRadio, UP);
			cable.finishConnectionHorizontally(Cable.Direction.DOWN, radio, Cable.MIDDLE_X, Cable.UPPER_Y);
		} else {
			if (endDirection == DOWN) {
				// connect upper port
				connectToUpperPort(radio, cable, numberOfCollisionLeftNodes, numberOfCollisionLeftNodes);
			} else if (endDirection == UP) {
				// connect lower port
				double distanceToRadio = cable.getCurrentRelationToNodeHorizontal(radio) == Cable.Direction.LEFT ?
												 numberOfCollisionRightNodes * CABLE_OFFSET :
												 numberOfCollisionLeftNodes * CABLE_OFFSET;
				cable.applyOffset(distanceToRadio, UP);
				// if there are too many cables force going up
				if (cable.getCurrentRelationToNodeVertical(radio, Cable.LOWER_Y) == UP) {
					connectToUpperPort(radio, cable, numberOfCollisionLeftNodes, numberOfCollisionLeftNodes);
				} else {
					cable.finishConnectionHorizontally(Cable.Direction.UP, radio, Cable.MIDDLE_X, Cable.LOWER_Y);
					connectedRadios.add(radio);
				}
			}
		}
	}

	private void connectToUpperPort(Node radio, Cable cable, int numberOfCollisionLeftNodes, int numberOfCollisionRightNodes) {
		double distanceToRadio = cable.getCurrentRelationToNodeHorizontal(radio) == Cable.Direction.LEFT ?
										 numberOfCollisionLeftNodes * CABLE_OFFSET :
										 numberOfCollisionRightNodes * CABLE_OFFSET;
		cable.goAroundCollisionNode(radio, CABLE_OFFSET, UP);
		cable.applyOffset(distanceToRadio, UP);
		cable.finishConnectionHorizontally(Cable.Direction.DOWN, radio, Cable.MIDDLE_X, Cable.UPPER_Y);
	}

	public Cable.Direction getEndDirection(Node radio) {
		return connectedRadios.contains(radio) ? DOWN : UP;
	}

	public int getNumberOfNodesInDirectionHorizontal(Node node, Cable cable, Cable.Direction direction) {
		int counter = 0;
		for(Node n : radioNodeToExternalRadioUnit.keySet()) {
			if(cable.getNodeRelationHorizontal(node, n) == direction) {
				++counter;
			}
		}
		return counter;
	}


	private void addRadios(ExternalRadioUnitSimpleController controller) {
		controller.getUnitControllers().forEach(r -> radioNodeToExternalRadioUnit.put(r.getView(), controller));
	}

	private int getRadioOffset(Node radio, Cable cable, Cable.Direction direction) {
		return getNumberOfNodesInDirectionHorizontal(radio, cable, direction);
	}
}
