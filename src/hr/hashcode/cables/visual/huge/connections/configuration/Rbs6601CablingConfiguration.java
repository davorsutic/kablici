package hr.hashcode.cables.visual.huge.connections.configuration;

import hr.hashcode.cables.visual.huge.cabinets.RBS6601;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.scene.Node;
import javafx.scene.paint.Color;

public class Rbs6601CablingConfiguration extends CablingConfiguration {
	private static final double CABLE_OFFSET = 8;
	private static final double OUTSIDE_OFFSET = 10;

	public Rbs6601CablingConfiguration(RbsVisual cabinet) {
		super(cabinet);
	}


	@Override
	public void startOutSideCabinetConnection(Node node, Cable cable) {
		super.startOutSideCabinetConnection(node, cable);
		int offset = externalConnections.get(node);
		cable.startConnection(node, Cable.X_ALPHA((1.0 / (PORTS_PER_NODE + 1)) * (PORTS_PER_NODE - offset)), Cable.LOWER_Y);
		Node collisionNode = getSubUnitNode(node, cable);
		collisionNode = collisionNode == null ? cabinet.getCabinetView() : collisionNode;
		cable.goAroundCollisionNode(collisionNode, OUTSIDE_OFFSET * offset, Cable.Direction.DOWN);
	}

	@Override
	public void finishOutSideCabinetConnection(Node node, Cable cable) {
		super.finishOutSideCabinetConnection(node, cable);
		int alpha = externalConnections.get(node);
		cable.avoidCollisionNode(cabinet.getCabinetView(), CABLE_OFFSET * alpha, Cable.Direction.RIGHT);
		Cable.Direction endDirection = Cable.Direction.UP;
		if(cable.getCurrentRelationToNodeVertical(node) == Cable.Direction.UP) {
			endDirection = Cable.Direction.DOWN;
		}
		cable.finishConnection(endDirection, node, Cable.LEFT_X, Cable.Y_ALPHA(1-(1.0 / (PORTS_PER_NODE + 1)) * alpha));
	}


	@Override
	public void defineCables(Node scene) {
		if(!(cabinet instanceof RBS6601)) {
			return;
		}
		RBS6601 rbs = (RBS6601) cabinet;
		defineXInterconnections(scene, rbs);

		rbs.getSlots().forEach(s -> xPlacement.add(s.getView()));
	}

	private void defineXInterconnections(Node scene, RBS6601 rbs) {
		int n = rbs.getSlots().size();
		for (int i = 0;  i < n - 1; i++) {
			Node n1 = rbs.getSlots().get(i).getView();
			for(int j = i+1; j < n; j++) {
				Node n2 = rbs.getSlots().get(j).getView();
				unitsToCable.put(getOrderedUnitNames(Integer.toString(i), Integer.toString(j)),
						getCable(scene, getNodeColor(n1, n2), n1, Cable.LEFT_X, Cable.MIDDLE_Y,
								n2, Cable.LEFT_X, Cable.MIDDLE_Y, Cable.Direction.DOWN,
								new Cable.ActionInput(2 * CABLE_OFFSET, Cable.Direction.LEFT)));

				nodeToUnitName.put(n1, Integer.toString(i));
				nodeToUnitName.put(n2, Integer.toString(j));
			}
		}
	}

	protected Node getStartExternalNode(Node startNode) {
		return cabinet.getCabinetView();
	}

	protected Cable.Direction getStartExternalDirection(Node startNode) {
		return Cable.Direction.LEFT;
	}

	private Node getSubUnitNode(Node node, Cable cable) {
		if(!(cabinet instanceof RBS6601)) {
			return null;
		}
		RBS6601 rbs = (RBS6601) cabinet;
		if(xPlacement.contains(node)) {
			for (Node subUnit : rbs.getSubunits()) {
				if(cable.nodeInsideNode(subUnit, node)) {
					return subUnit;
				}
			}
		}
		return null;
	}

}
