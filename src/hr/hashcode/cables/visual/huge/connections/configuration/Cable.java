package hr.hashcode.cables.visual.huge.connections.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class Cable {
	public static double EPSILON = 10e-3;
	public static final double BORDER_WIDTH = RbsVisual.BORDER_WIDTH;
	public static final Function<Bounds, Double> UPPER_Y = bounds -> bounds.getMinY() - BORDER_WIDTH;
	public static final Function<Bounds, Double> LOWER_Y = bounds -> bounds.getMaxY() + BORDER_WIDTH;
	public static final Function<Bounds, Double> LEFT_X = bounds -> bounds.getMinX() - BORDER_WIDTH;
	public static final Function<Bounds, Double> RIGHT_X = bounds -> bounds.getMaxX() + BORDER_WIDTH;
	public static final Function<Bounds, Double> MIDDLE_X = X_ALPHA(0.5);
	public static final Function<Bounds, Double> MIDDLE_Y = Y_ALPHA(0.5);

	public static final Color DEFAULT_COLOR = Color.BLACK;

	public static Function<Bounds, Double> X_ALPHA(double alpha) {
		return bounds ->  bounds.getMinX() + alpha * (bounds.getMaxX() - bounds.getMinX());
	}

	public static Function<Bounds, Double> Y_ALPHA(double alpha) {
		return bounds ->  bounds.getMinY() + alpha * (bounds.getMaxY() - bounds.getMinY());
	}


	private double currentX;
	private double currentY;
	private Color currentColor;
	private List<Line> polyLine;
	private Node scene;
	private boolean connectionStarted;

	public enum Direction {
		LEFT,
		RIGHT,
		UP,
		DOWN,
		NON_STRAIGHT
	}

	public static class ActionInput {
		Node node;
		double offset;
		List<Direction> directions;

		public ActionInput(Node node, double offset, Direction... directions) {
			this.node = node;
			this.offset = offset;
			if(directions.length > 0) {
				this.directions = new ArrayList<>();
				for(Direction d : directions) {
					this.directions.add(d);
				}
			}
		}

		public ActionInput(double offset, Direction... directions) {
			this(null, offset, directions);
		}
	}

	public interface ActionMovement {
		void apply(Node node, double offset, Direction... directions);
	}

	public Cable(Node scene, Color color) {
		polyLine = new ArrayList<>();
		this.scene = scene;
		this.currentColor = color;
	}

	public Cable(Node scene) {
		this(scene, DEFAULT_COLOR);
	}

	public void setColor(Color color) {
		currentColor = color;
		if(polyLine != null && !polyLine.isEmpty()) {
			polyLine.forEach(l -> l.setStroke(color));
		}
	}

	public boolean inCollision(List<Node> units) {
		for(Node unit : units) {
			Bounds bounds = bounds(unit);
			if(polyLineIntersectsNode(polyLine, bounds)) {
					return true;
			}
		}
		return false;
	}

	public boolean polyLineIntersectsNode(List<Line> polyLine, Bounds bounds) {
		for(Line line : polyLine) {
			if(bounds.contains(line.getStartX(), line.getStartY()) || bounds.contains(line.getEndX(), line.getStartY())) {
				return true;
			} else if(lineVertical(line)) {
				return pointBetweenPoints(line.getStartX(), bounds.getMinX(), bounds.getMaxX());
			} else if(lineHorizontal(line)) {
				return pointBetweenPoints(line.getStartY(), bounds.getMinY(), bounds.getMaxY());
			}
		}
		return false;
	}


	public void registerCable(Group g, BooleanProperty switach, double lineWidth) {
		for (Line l : polyLine) {
			l.strokeWidthProperty().bind(Bindings.when(switach).then(lineWidth).otherwise(0));
		}
		boolean add = true;
		for(Line line : polyLine) {
			if(g.getChildren().contains(line)) {
				add = false;
			}
		}
		if(add) {
			g.getChildren().addAll(polyLine);
		} else {
			int a = 0;
		}
	}

	public void startConnection(double x, double y) {
		polyLine = new ArrayList<>();
		currentX = x;
		currentY = y;
		connectionStarted = true;
	}

	public void startConnection(Node node, Function<Bounds, Double> startingXProvider, Function<Bounds, Double> startingYProvider) {
		Bounds bounds = bounds(node);
		if(bounds == null) {
			startConnection(0,0);
		} else {
			startConnection(startingXProvider.apply(bounds), startingYProvider.apply(bounds));
		}
	}

	public void applyActions(ActionInput... input) {
		for(ActionInput a : input) {
			applyAction(a);
		}
	}

	public void applyActions(List<ActionInput> input) {
		for(ActionInput a : input) {
			applyAction(a);
		}
	}

	public void applyAction(ActionInput input) {
		if(input.node == null && input.directions != null && input.directions.size() == 1) {
			applyOffset(input.offset, input.directions.get(0));
		} else {
			goAroundCollisionNode(input.node,  input.offset, input.directions);
		}
	}

	public void applyOffset(double offset, Direction direction) {
		if(!connectionStarted) {
			return;
		}
		connectFromLastPoint(direction, offset);
	}

	// TODO(ana): merge with goAroundCollisionNOde
	public void avoidCollisionNode(Node node, double offset, Direction direction) {
		if(!connectionStarted) {
			return;
		}
		Bounds bounds = bounds(node);
		double endX = currentX;
		double endY  = currentY;
		if(direction == Direction.LEFT) {
			endX = currentX >= bounds.getMaxX() + offset ? bounds.getMaxX() + offset : currentX;
		} else if (direction == Direction.RIGHT) {
			endX = currentX <= bounds.getMinX() - offset ? bounds.getMinX() - offset : currentX;
		} else if (direction == Direction.UP) {
			endY = currentY >= bounds.getMaxY() + offset ? bounds.getMaxY() + offset : currentY;
		} else if (direction == Direction.DOWN) {
			endY = currentY <= bounds.getMinY() - offset ? bounds.getMinY() - offset : currentY;
		}
		connectFromLastPoint(direction, endX, endY);
	}

	public void goAroundCollisionNode(Node node, double offset, List<Direction> directions) {
		if(!connectionStarted) {
			return;
		}
		for(Direction direction : directions) {
			Bounds bounds = bounds(node);
			double endX = getCornerX(direction, bounds, currentX, offset);
			double endY = getCornerY(direction, bounds, currentY, offset);
			// todo
			if(endX == currentX && endY == currentY) {
				avoidCollisionNode(node, offset, getOppositeDirection(direction));
			} else {
				connectFromLastPoint(direction, endX, endY);
			}
		}
	}

	public Direction getOppositeDirection(Direction direction) {
		switch (direction) {
			case LEFT:
				return Direction.RIGHT;
			case RIGHT:
				return Direction.LEFT;
			case UP:
				return Direction.DOWN;
			case DOWN:
				return Direction.UP;
		}
		return null;
	}

	public void goAroundCollisionNode(Node node, double offset, Direction... directions) {
		List<Direction> listDirections = new ArrayList<>();
		for(Direction d : directions) {
			listDirections.add(d);
		}
		goAroundCollisionNode(node, offset, listDirections);
	}

	private void finishConnection() {
		connectionStarted = false;
	}

	public void finishConnection(Direction direction, double x, double y) {
		connectFromLastPoint(direction, x, y);
		finishConnection();
	}

	public void finishConnection(Direction direction, Node node, Function<Bounds, Double> startingXProvider, Function<Bounds, Double> startingYProvider) {
		Bounds bounds = bounds(node);
		if(bounds == null) {
			finishConnection();
		} else {
			finishConnection(direction, startingXProvider.apply(bounds), startingYProvider.apply(bounds));
		}
	}

	public void finishConnectionHorizontally(Direction direction, Node node, Function<Bounds, Double> startingXProvider, Function<Bounds, Double> startingYProvider) {
		Bounds bounds = bounds(node);
		double endX = startingXProvider.apply(bounds);
		if(direction == Direction.UP || direction == Direction.DOWN) {
			 polyLine.add(getLine(currentX, currentY, endX, currentY));
			 currentX = endX;
		}

		finishConnection(direction, currentX, startingYProvider.apply(bounds));
	}


	// connects last point directly: non straight line is possible
	public void finishConnection(Node node, Function<Bounds, Double> startingXProvider, Function<Bounds, Double> startingYProvider) {
		Bounds bounds = bounds(node);
		if(bounds != null) {
			double endX = startingXProvider.apply(bounds);
			double endY = startingYProvider.apply(bounds);
			polyLine.add(getLine(currentX, currentY, endX, endY));
			currentX = endX;
			currentY = endY;
		}
		finishConnection();
	}

	public Node getLeftNode(Node n1, Node n2) {
		Bounds bounds1 = bounds(n1);
		Bounds bounds2 = bounds(n2);
		return MIDDLE_X.apply(bounds1) < MIDDLE_X.apply(bounds2) ? n1 : n2;
	}

	public Direction getCurrentRelationToNodeHorizontal(Node n) {
		Bounds bounds = bounds(n);
		if(currentX  <= MIDDLE_X.apply(bounds)) {
			return Direction.LEFT;
		} else {
			return Direction.RIGHT;
		}
	}

	public Direction getCurrentRelationToNodeVertical(Node n) {
		return getCurrentRelationToNodeVertical(n, MIDDLE_Y);
	}

	public Direction getCurrentRelationToNodeVertical(Node n, Function<Bounds, Double> y) {
		Bounds bounds = bounds(n);
		if(currentY  <= y.apply(bounds)) {
			return Direction.UP;
		} else {
			return Direction.DOWN;
		}
	}

	private double getCornerX(Direction direction, Bounds bounds, double x, double offset) {
		if(direction == Direction.LEFT) {
			return x >= bounds.getMinX() ? bounds.getMinX()  - offset : x;
		} else if(direction == Direction.RIGHT) {
			return x <= bounds.getMaxX() ? bounds.getMaxX() + offset : x;
		}
		return x;
	}

	private double getCornerY(Direction direction, Bounds bounds, double y, double offset) {
		if(direction == Direction.UP) {
			return y >= bounds.getMinY() ? bounds.getMinY() - offset: y;
		} else if(direction == Direction.DOWN) {
			return y <= bounds.getMaxY() ? bounds.getMaxY() + offset: y;
		}
		return y;
	}

	private boolean lineVertical(Line line) {
		return line.getStartX() == line.getEndX();
	}

	private boolean lineHorizontal(Line line) {
		return line.getStartY() == line.getEndY();
	}

	private boolean pointBetweenPoints(double x, double x1, double x2) {
		return x <= Math.max(x1, x2) && x >= Math.min(x1, x2);
	}

	private Bounds bounds(javafx.scene.Node n) {
		return scene.sceneToLocal(n.localToScene(n.getBoundsInLocal()));
	}

	private boolean connectFromLastPoint(Direction direction, double x, double y) {
		if(direction == Direction.LEFT || direction == Direction.RIGHT) {
			if(direction == Direction.LEFT  && currentX < x || direction == Direction.RIGHT && currentX > x) {
				return false;
			}
			appendLine(currentX, currentY, x, currentY);
			appendLine(x, currentY, x, y);
		} else {
			if(direction == Direction.UP  && currentY < y || direction == Direction.DOWN && currentY > y) {
				return false;
			}
			appendLine(currentX, currentY, currentX, y);
			appendLine(currentX, y, x, y);
		}
		return true;
	}

	private boolean connectFromLastPoint(Direction direction, double offset) {
		if(direction == Direction.LEFT || direction == Direction.RIGHT) {
			double endX = direction == Direction.LEFT ? currentX - offset : currentX + offset;
			appendLine(currentX, currentY, endX, currentY);
		} else {
			double endY = direction == Direction.UP ? currentY - offset : currentY + offset;
			appendLine(currentX, currentY, currentX, endY);
		}
		return true;
	}

	private void appendLine(double startX, double startY, double endX, double endY) {
		if(startX == endX && startY == endY) {
			return;
		}
		if (polyLine.isEmpty()) {
			polyLine.add(getLine(startX, startY, endX, endY));
			currentX = endX;
			currentY = endY;
		} else {
			// line is added only if it is continued to previous line in straight direction
			Line last = polyLine.get(polyLine.size() - 1);
			if (last.getEndX() == startX && last.getEndY() == startY) {
				Line newLine = getLine(startX, startY, endX, endY);
				// if same direction, merge lines
				if(getDirection(last) == getDirection(newLine)) {
					last.setEndX(newLine.getEndX());
					last.setEndY(newLine.getEndY());
				} else if(!oppositeDirections(getDirection(last), getDirection(newLine))) {
					polyLine.add(newLine);
				}
				currentX = endX;
				currentY = endY;
			}
		}

	}

	private Direction getDirection(Line line) {
		if (line.getStartX() == line.getEndX()) {
			return line.getStartY() - line.getEndY() < 0 ? Direction.DOWN : Direction.UP;
		} else if (line.getStartY() == line.getEndY()) {
			return line.getStartX() - line.getEndX() < 0 ? Direction.RIGHT : Direction.LEFT;
		}
		return Direction.NON_STRAIGHT;
	}

	private Line getLine(double startX, double startY, double endX, double endY) {
		Line line = new Line();
		line.setStartX(startX);
		line.setStartY(startY);
		line.setEndX(endX);
		line.setEndY(endY);
		line.setStroke(currentColor);
		return line;
	}

	private boolean oppositeDirections(Direction d1, Direction d2) {
		return (d1 == Direction.LEFT && d2 == Direction.RIGHT) || (d2 == Direction.LEFT && d1 == Direction.RIGHT) ||
					   (d1 == Direction.DOWN && d2 == Direction.UP) || (d1 == Direction.UP && d2 == Direction.DOWN);
	}

	public Direction getNodeRelationHorizontal(Node n1, Node n2) {
		Bounds b1 = bounds(n1);
		Bounds b2 = bounds(n2);
		if(n1 != n2 && pointsEqual(MIDDLE_Y.apply(b1), MIDDLE_Y.apply(b2))) {
			return MIDDLE_X.apply(b1) < MIDDLE_X.apply(b2) ? Direction.RIGHT : Direction.LEFT;
		}
		return null;
	}


	public Direction getNodeRelationVertical(Node n1, Node n2) {
		Bounds b1 = bounds(n1);
		Bounds b2 = bounds(n2);
		if(n1 != n2 && pointsEqual(MIDDLE_X.apply(b1), MIDDLE_X.apply(b2))) {
			return MIDDLE_Y.apply(b1) < MIDDLE_Y.apply(b2) ? Direction.DOWN : Direction.UP;
		}
		return null;
	}

	public boolean nodeInsideNode(Node n1, Node n2) {
		Bounds b1 = bounds(n1);
		Bounds b2 = bounds(n2);
		return b1.contains(b2);
	}


	public static boolean pointsEqual(double x, double y) {
		return Math.abs(x - y) < EPSILON;
	}

}
