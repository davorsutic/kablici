package hr.hashcode.cables.visual.huge.connections;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.visual.huge.cabinets.RBS6102;
import hr.hashcode.cables.visual.huge.cabinets.RBS6131;
import hr.hashcode.cables.visual.huge.cabinets.RBS6201;
import hr.hashcode.cables.visual.huge.cabinets.RBS6202;
import hr.hashcode.cables.visual.huge.cabinets.RBS6601;
import hr.hashcode.cables.visual.huge.cabinets.RbsVisual;
import hr.hashcode.cables.visual.huge.connections.configuration.Cable;
import hr.hashcode.cables.visual.huge.connections.configuration.CablingConfiguration;
import hr.hashcode.cables.visual.huge.connections.configuration.ExternalRadioCablingConfiguration;
import hr.hashcode.cables.visual.huge.connections.configuration.Rbs6102CablingConfiguration;
import hr.hashcode.cables.visual.huge.connections.configuration.Rbs6131CablingConfiguration;
import hr.hashcode.cables.visual.huge.connections.configuration.Rbs6201CablingConfiguration;
import hr.hashcode.cables.visual.huge.connections.configuration.Rbs6202CablingConfiguration;
import hr.hashcode.cables.visual.huge.connections.configuration.Rbs6601CablingConfiguration;
import hr.hashcode.cables.visual.huge.units.ExternalRadioUnitSimpleController;
import javafx.beans.property.BooleanProperty;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.util.Pair;

public class Connection {
	private static final double INTERCONNECTION_WIDTH = 2.5;
	private boolean connected = false;

	private List<CablingConfiguration> rbs6201 = new ArrayList<>();
	private List<CablingConfiguration> rbs6102 = new ArrayList<>();
	private List<CablingConfiguration> rbs6202 = new ArrayList<>();
	private List<CablingConfiguration> rbs6131 = new ArrayList<>();
	private List<CablingConfiguration> rbs6601 = new ArrayList<>();
	private ExternalRadioCablingConfiguration externalRadioCablingConfiguration;
	private Node scene;
	private Map<Node, Set<Band>> nodeToBands;

	public Connection(Node scene) {
		this.scene = scene;
	}

	public void registerConfiguration(RbsVisual cabinet) {
		if(cabinet instanceof RBS6201) {
			rbs6201.add(new Rbs6201CablingConfiguration(cabinet));
		} else if(cabinet instanceof RBS6601) {
			rbs6601.add(new Rbs6601CablingConfiguration(cabinet));
		} else if(cabinet instanceof RBS6131) {
			rbs6131.add(new Rbs6131CablingConfiguration(cabinet));
		} else if(cabinet instanceof RBS6102) {
			rbs6102.add(new Rbs6102CablingConfiguration(cabinet));
		}  else if(cabinet instanceof RBS6202) {
			rbs6202.add(new Rbs6202CablingConfiguration(cabinet));
		}
	}

	public void registerExternalRadioConfiguration(ExternalRadioUnitSimpleController controller) {
		if(externalRadioCablingConfiguration == null) {
			externalRadioCablingConfiguration = new ExternalRadioCablingConfiguration();
		}
		externalRadioCablingConfiguration.registerExternalRadio(controller);
	}

	public void setNodeToBands(Map<Node, Set<Band>> nodeToBands) {
		this.nodeToBands = nodeToBands;
	}

	public void defineConfigurations() {
		defineConfiguration(rbs6201, scene);
		defineConfiguration(rbs6202, scene);
		defineConfiguration(rbs6131, scene);
		defineConfiguration(rbs6601, scene);
		defineConfiguration(rbs6102, scene);
	}

	public void connect(Group group, List<Pair<Node, Node>> connections, BooleanProperty property) {
		if(connected) {
			return;
		}
		for (Pair<Node, Node> connection : connections) {
			CablingConfiguration configuration = getMatchingConfiguration(connection.getKey(), connection.getValue());
			if(configuration == null) {
				interconnect(group, property, connection.getKey(), connection.getValue());
			} else {
				configuration.registerCable(group, property, connection.getKey(), connection.getValue());
			}
		}

		connected = true;
	}

	private Color getInterconnectionColor(Node n1, CablingConfiguration c1, Node n2, CablingConfiguration c2) {
		if(c1.isRu(n1)) {
			return getNodeColor(n1, false);
		} else if(c2.isRu(n2)) {
			return getNodeColor(n2, false);
		}
		return c1.isDu(n1) ? getNodeColor(n2, true) : getNodeColor(n1, true);
	}

	private Color getNodeColor(Node node, boolean remove) {
		if(nodeToBands != null && nodeToBands.containsKey(node)) {
			Set<Band> bands = nodeToBands.get(node);
			if(!bands.isEmpty()) {
				Band band = bands.iterator().next();
				if(remove) {
					// for R503 dus connection
					bands.remove(band);
				}
				return CablingConfiguration.BAND_COLORS.get(band);
			}
		}
		return Cable.DEFAULT_COLOR;
	}

	private void interconnect(Group group,  BooleanProperty property, Node n1, Node n2) {
		CablingConfiguration c1 = getMatchingConfiguration(n1);
		CablingConfiguration c2 = getMatchingConfiguration(n2);
		if(c1 == null && c2 == null) {
			return;
		}
		Cable cable;
		double width = CablingConfiguration.CABLE_WIDTH;
		if(c1 == null) {
			cable = connectExternalNode(n2, c2, n1);
		} else if(c2 == null) {
			cable = connectExternalNode(n1, c1, n2);
		} else {
			cable = connectExternalCabinet(n1, c1, n2, c2);
			width = INTERCONNECTION_WIDTH;
		}
		if (cable != null) {
			cable.registerCable(group, property, width);
		}
	}

	private Cable connectExternalCabinet(Node n1, CablingConfiguration c1, Node n2, CablingConfiguration c2) {
		Cable cable = new Cable(scene, getInterconnectionColor(n1, c1, n2, c2));
		Node left = cable.getLeftNode(n1, n2);
		Node right = left == n1 ? n2 : n1;
		getMatchingConfiguration(left).startOutSideCabinetConnection(left, cable);
		getMatchingConfiguration(right).finishOutSideCabinetConnection(right, cable);
		return cable;
	}

	private Cable connectExternalNode(Node cabinetNode, CablingConfiguration cabinetConfiguration, Node externalNode) {
		Cable cable = new Cable(scene, getNodeColor(externalNode, false));
		if(externalRadioCablingConfiguration != null) {
			int leftNodes = externalRadioCablingConfiguration.getNumberOfNodesInDirectionHorizontal(externalNode, cable, Cable.Direction.LEFT);
			int rightNodes = externalRadioCablingConfiguration.getNumberOfNodesInDirectionHorizontal(externalNode, cable, Cable.Direction.RIGHT);
			CablingConfiguration.PORTS_PER_NODE = Math.max(CablingConfiguration.PORTS_PER_NODE, leftNodes + rightNodes);
			Cable.Direction endDirection = externalRadioCablingConfiguration.getEndDirection(externalNode);
			cabinetConfiguration.startExternalCabinetConnection(cabinetNode, cable, leftNodes, rightNodes, endDirection);
			externalRadioCablingConfiguration.finishConnection(externalNode, cable, leftNodes, rightNodes, endDirection);
			CablingConfiguration.PORTS_PER_NODE = CablingConfiguration.MAX_PORTS_PER_NODE;
		}
		return cable;
	}

	private CablingConfiguration getMatchingConfiguration(Node n1, Node n2) {
		CablingConfiguration c1 = getMatchingConfiguration(n1);
		CablingConfiguration c2 = getMatchingConfiguration(n2);
		if(c1 == c2) {
			return c1;
		}
		return null;
	}

	private CablingConfiguration getMatchingConfiguration(Node n) {
		CablingConfiguration conf = getMatchingConfiguration(rbs6201, n);
		if(conf != null) {
			return conf;
		}
		conf = getMatchingConfiguration(rbs6131, n);
		if(conf != null) {
			return conf;
		}
		conf = getMatchingConfiguration(rbs6601, n);
		if(conf != null) {
			return conf;
		}
		conf = getMatchingConfiguration(rbs6102, n);
		if(conf != null) {
			return conf;
		}
		conf = getMatchingConfiguration(rbs6202, n);
		if(conf != null) {
			return conf;
		}
		return null;
	}

	private CablingConfiguration getMatchingConfiguration(List<CablingConfiguration> configurations, Node n) {
		for(CablingConfiguration configuration : configurations) {
			if(configuration.containsNode(n)) {
				return configuration;
			}
		}
		return null;
	}

	private void defineConfiguration(List<CablingConfiguration> configurations, Node scene) {
		for(CablingConfiguration configuration : configurations) {
			if(nodeToBands != null) {
				configuration.registerNodeToBand(nodeToBands);
			}
			configuration.defineCables(scene);
		}
	}
}
