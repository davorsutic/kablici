package hr.hashcode.cables.equip.comp;

import java.util.Objects;

public class Connector<T extends Port> {

	private final T first;
	private final T second;

	public Connector(T first, T second) {
		this.first = Objects.requireNonNull(first);
		this.second = Objects.requireNonNull(second);
	}

	public T first() {
		return first;
	}

	public T second() {
		return second;
	}

	@Override
	public String toString() {
		return first + "-" + second;
	}

}
