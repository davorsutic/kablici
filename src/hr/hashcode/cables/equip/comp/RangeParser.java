package hr.hashcode.cables.equip.comp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RangeParser {

	public static List<String> parse(String string) {
		if (string == null)
			return null;
		String[] elems = string.split("(?<=\\))|(?=\\()");
		List<String> list = Collections.singletonList("");
		for (int i = elems.length - 1; i >= 0; i--) {
			String elem = elems[i];
			if (elem.endsWith(")")) {
				if (!elem.startsWith("("))
					throw new IllegalArgumentException(string);
				elem = elem.substring(1, elem.length() - 1);
			}
			List<String> temp = new ArrayList<>();
			List<String> parsed = parse2(elem);
			for (String first : parsed)
				for (String second : list)
					temp.add(first + second);
			list = temp;
		}
		return list;
	}

	private static List<String> parse2(String string) {
		List<String> result = new ArrayList<>();
		for (String elem : string.split(","))
			result.addAll(parse3(elem));
		return result;
	}

	private static final Pattern prefixNumber = Pattern.compile("(\\D*)(\\d+)-\\1(\\d+)");
	private static final Pattern charNumber = Pattern.compile("([A-Z])(\\d+)-([A-Z])(\\d+)");
	private static final Pattern charInterval = Pattern.compile("[A-Z]-[A-Z]");

	private static List<String> range(int from, int to) {
		List<String> list = new ArrayList<>();
		if (from <= to)
			for (int i = from; i <= to; i++)
				list.add(String.valueOf(i));
		else
			for (int i = to; i >= from; i--)
				list.add(String.valueOf(i));

		return list;
	}

	private static List<String> range(char from, char to) {
		List<String> list = new ArrayList<>();
		if (from <= to)
			for (char c = from; c <= to; c++)
				list.add(String.valueOf(c));
		else
			for (char c = to; c >= from; c--)
				list.add(String.valueOf(c));

		return list;
	}

	private static List<String> parse3(String string) {
		List<String> result = new ArrayList<>();

		int index = string.indexOf('-');
		if (index == -1)
			return Collections.singletonList(string);
		else {
			Matcher matcher = prefixNumber.matcher(string);
			if (matcher.matches()) {
				String prefix = matcher.group(1);
				int first = Integer.parseInt(matcher.group(2));
				int last = Integer.parseInt(matcher.group(3));
				for (String elem : range(first, last))
					result.add(prefix + elem);
			} else {
				matcher = charNumber.matcher(string);
				if (matcher.matches()) {

					char ch1 = matcher.group(1).charAt(0);
					char ch2 = matcher.group(3).charAt(0);
					int n1 = Integer.parseInt(matcher.group(2));
					int n2 = Integer.parseInt(matcher.group(4));

					for (String first : range(ch1, ch2))
						for (String second : range(n1, n2))
							result.add(first + second);
				} else {
					matcher = charInterval.matcher(string);
					if (matcher.matches()) {
						char ch1 = string.charAt(0);
						char ch2 = string.charAt(2);
						result.addAll(range(ch1, ch2));
					} else
						throw new IllegalArgumentException(string);
				}
			}
		}
		return result;
	}
}
