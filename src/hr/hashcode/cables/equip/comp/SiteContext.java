package hr.hashcode.cables.equip.comp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import hr.hashcode.cables.util.Util;

public class SiteContext {

	private final List<DigitalUnitWcdma> duws;
	private final List<DigitalUnitStandard> duses;
	private final List<DigitalUnitBaseband> bbs;
	private final List<R503> r503s;
	private final List<List<RadioUnit>> radios;
	private final List<RemoteRadioUnit> remotes;
	private final Map<Component, List<Band>> componentToCoveredBands;
	private final List<Set<MiniRbs>> partitionedMiniRbses;

	public SiteContext(List<DigitalUnitWcdma> duws, List<DigitalUnitStandard> duses, List<DigitalUnitBaseband> bbs,
							  List<R503> r503s, List<List<RadioUnit>> radios, List<RemoteRadioUnit> remotes,
							  Map<Component, List<Band>> componentToCoveredBands,
							  List<Set<MiniRbs>> partitionedMiniRbses) {
		this.duws = duws;
		this.duses = duses;
		this.bbs = bbs;
		this.r503s = r503s;
		this.radios = radios;
		this.remotes = remotes;
		this.componentToCoveredBands = componentToCoveredBands;
		this.partitionedMiniRbses = partitionedMiniRbses;
	}

	private static List<Band> getBandsFrom(Component unit, Site site) {
		List<Band> coveredBands = null;
		if (unit instanceof DigitalUnit) {
			coveredBands = new ArrayList<>(site.duToBands.get(unit));
		} else if (unit instanceof R503) {
			coveredBands = new ArrayList<>(site.r503ToBands.get(unit));
		} else {
			assert (unit instanceof RadioUnitBase);
			Band band = site.radios.entrySet().stream()
								.filter(e -> e.getValue().contains(unit))
								.map(Map.Entry::getKey)
								.findFirst()
								.orElse(null);
			if (band == null) {
				coveredBands = Collections.emptyList();
			} else {
				coveredBands = Collections.singletonList(band);
			}
		}
		coveredBands.sort(Band.comparator);
		return coveredBands;
	}

	private static List<Set<MiniRbs>> partitionMiniRbs(Site site, Map<Component, List<Band>> componentToCoveredBands) {

		Map<Band, Set<MiniRbs>> rbs = new HashMap<>();
		Set<MiniRbs> addedRbses = Util.identitySet();
		Map<Component, MiniRbs> miniRbsComponents = new IdentityHashMap<>();
		for (MiniRbs miniRbs : site.miniRbses) {
			for (Component component : miniRbs.getComponents()) {
				miniRbsComponents.put(component, miniRbs);
			}
		}

		for (Connector<RiPort> connector : site.riConnectors) {
			RemoteRadioUnit radio = site.remotes.stream().filter(r -> {
				List<RiPort> ports = r.riPorts().ports();
				return ports.contains(connector.first()) || ports.contains(connector.second());
			}).findFirst().orElse(null);
			if (radio == null) {
				continue;
			}
			List<Band> bands = componentToCoveredBands.get(radio);
			if (bands.isEmpty()) {
				continue;
			}
			Band band = bands.get(0);
			for (Component component : miniRbsComponents.keySet()) {
				if (!(component instanceof RiPort.RiPortable)) {
					continue;
				}
				List<RiPort> ports = ((RiPort.RiPortable) component).riPorts().ports();
				boolean target = ports.contains(connector.first()) || ports.contains(connector.second());
				if (target) {
					MiniRbs miniRbs = miniRbsComponents.get(component);
					rbs.computeIfAbsent(band, b -> Util.identitySet()).add(miniRbs);
					addedRbses.add(miniRbs);
				}
			}
		}

		List<Set<MiniRbs>> list = new ArrayList<>(rbs.values());
		list.sort(Comparator.comparingInt(l -> -l.size()));
		for (int i = 0; i < list.size() - 1; i++) {
			for (int j = i + 1; j < list.size(); j++) {
				Set<MiniRbs> l1 = list.get(i);
				Set<MiniRbs> l2 = list.get(j);
				if(!Util.intersection(l1, l2).isEmpty()){
					l1.addAll(l2);
					list.remove(j);
					i--;
					break;
				}
			}
		}

		for(MiniRbs miniRbs : site.miniRbses){
			if(!addedRbses.contains(miniRbs)){
				list.add(Collections.singleton(miniRbs));
			}
		}

		assert (list.stream().mapToInt(Collection::size).sum() == site.miniRbses.size());

		return list;
	}

	public static SiteContext from(Site site) {

		// ************************ SUBRACK - DUW, RADIOS ************************//
		List<DigitalUnitWcdma> duws = new ArrayList<>();
		List<List<RadioUnit>> radios = new ArrayList<>();
		for (MacroRbs rbs : site.rbses) {
			for (Subrack subrack : rbs.subracks()) {
				if (subrack.digital() != null)
					duws.add(subrack.digital());
				if (!subrack.radios().isEmpty())
					radios.add(subrack.radios());
			}
		}
		for (MiniRbs mini : site.miniRbses) {
			if (mini.duw() != null)
				duws.add(mini.duw());
		}

		// ************************ EXTENSION RACK ************************//
		List<DigitalUnitStandard> duses = new ArrayList<>();
		List<DigitalUnitBaseband> bbs = new ArrayList<>();
		List<R503> r503s = new ArrayList<>();

		for (MacroRbs rbs : site.rbses) {
			for (ExtensionRack rack : rbs.extensions()) {
				for (String slot : rack.slots()) {
					if (rack.unit(slot) != null && DigitalUnitStandard.class.isAssignableFrom(rack.unit(slot).getClass())) {
						duses.add((DigitalUnitStandard) rack.unit(slot));
					}
					if (rack.unit(slot) != null && DigitalUnitBaseband.class.isAssignableFrom(rack.unit(slot).getClass())) {
						bbs.add((DigitalUnitBaseband) rack.unit(slot));
					}
					if (rack.r503(slot) != null && R503.class.isAssignableFrom(rack.r503(slot).getClass())) {
						r503s.add((R503) rack.r503(slot));
					}
				}
			}
		}
		for (MiniRbs miniRbs : site.miniRbses) {
			for (String slot : miniRbs.slots()) {
				if (miniRbs.dus(slot) != null)
					duses.add(miniRbs.dus(slot));
				if (miniRbs.bb(slot) != null)
					bbs.add(miniRbs.bb(slot));
				if (miniRbs.r503(slot) != null)
					r503s.add(miniRbs.r503(slot));
			}
		}

		List<RemoteRadioUnit> remotes = new ArrayList<>(site.remotes);

		Map<Component, List<Band>> componentToCoveredBands = new HashMap<>();
		List<Component> components = new ArrayList<>();
		components.addAll(duws);
		components.addAll(duses);
		components.addAll(bbs);
		components.addAll(r503s);
		components.addAll(radios.stream().flatMap(Collection::stream).filter(Objects::nonNull).collect(Collectors.toList()));
		components.addAll(remotes);

		for(Map.Entry<Component, List<Band>> entry : componentToCoveredBands.entrySet()){
			Component component = entry.getKey();
			List<Band> bands = entry.getValue();
			if(component instanceof RemoteRadioUnit){
				if(bands.size() >1){
					throw new IllegalArgumentException(String.format("Too many bands (%s) given for remote radio unit %s, max 1 band "
																			 + "allowed", bands, component));
				}
			}else{
				if(bands.size() ==0){
					throw new IllegalArgumentException(String.format("No bands given for radio unit %s, max 1 band "
																			 + "allowed", component));
				}
			}
		}

		for (Component component : components) {
			componentToCoveredBands.put(component, getBandsFrom(component, site));
		}

		List<Set<MiniRbs>> partitionedMiniRbses = partitionMiniRbs(site, componentToCoveredBands);

		return new SiteContext(duws, duses, bbs, r503s, radios, remotes, componentToCoveredBands, partitionedMiniRbses);
	}

	public Map<Band, List<RemoteRadioUnit>> getGroupedExternalRadios() {
		Map<Band, List<RemoteRadioUnit>> groupedRadios = new HashMap<>();
		for (RemoteRadioUnit u : remotes) {
			List<Band> coveredBands = getCoveredBandsFor(u);
			if (coveredBands.isEmpty()) {
				//TODO handle this situation. This happens, for example, when the type of the unit is "ARETU" - this is a tilt controller
				//TODO for the antenna, and should be ignored
				continue;
			}
			assert (coveredBands.size() == 1);
			Band band = coveredBands.get(0);
			groupedRadios.computeIfAbsent(band, b -> new ArrayList<>()).add(u);
		}
		return groupedRadios;
	}

	public List<DigitalUnitWcdma> duws() {
		return duws;
	}

	public List<DigitalUnitStandard> duses() {
		return duses;
	}

	public List<DigitalUnitBaseband> bbs() {
		return bbs;
	}

	public List<R503> r503s() {
		return r503s;
	}

	public List<RadioUnit> radiosFlat() {
		return radios.stream().flatMap(Collection::stream).filter(Objects::nonNull).collect(Collectors.toList());
	}

	public List<RemoteRadioUnit> remotes() {
		return remotes;
	}

	public List<Band> getCoveredBandsFor(Component component) {
		return componentToCoveredBands.get(component);
	}

	public List<Component> getInternalComponents() {
		List<Component> components = new ArrayList<>();
		components.addAll(duws);
		components.addAll(duses);
		components.addAll(bbs);
		components.addAll(r503s);
		components.addAll(radiosFlat());
		return components;
	}

	public List<Component> getAllComponents() {
		List<Component> components = getInternalComponents();
		components.addAll(remotes);
		return components;
	}

	public List<Set<MiniRbs>> getPartitionedMiniRbses() {
		return partitionedMiniRbses;
	}
}
