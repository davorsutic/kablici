package hr.hashcode.cables.equip.comp;

import hr.hashcode.cables.equip.comp.PortSet.SinglePortSet;

public class DcPort extends Port {

	DcPort(String name) {
		super(name);
	}

	public interface DcPortable {
		PortSet<DcPort> dcPorts();

		default DcPort dcPort(String name) {
			return dcPorts().port(name);
		}
	}

	public interface SingleDcPortable extends DcPortable {
		@Override
		SinglePortSet<DcPort> dcPorts();

		default DcPort dcPort() {
			return dcPorts().port();
		}
	}

}
