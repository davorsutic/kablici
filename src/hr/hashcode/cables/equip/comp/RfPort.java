package hr.hashcode.cables.equip.comp;

public class RfPort extends Port {

	RfPort(String name) {
		super(name);
	}

	public interface RfPortable {
		PortSet<RfPort> rfPorts();

		default RfPort rfPort(String name) {
			return rfPorts().port(name);
		}
	}

}
