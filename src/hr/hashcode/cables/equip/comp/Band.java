package hr.hashcode.cables.equip.comp;

import java.util.Comparator;

public enum Band {
	// Bands should be listed by priority here
	B1(1), B8(8), B11(11), B3(3, 9);

	private final int lte;
	private final int wcdma;

	public static Comparator<Band> comparator = Comparator.comparingInt(Band::ordinal);

	Band(int mark) {
		this.lte = mark;
		this.wcdma = mark;
	}

	Band(int lte, int wcdma) {
		this.lte = lte;
		this.wcdma = wcdma;
	}

	public static Band lte(int mark) {
		for (Band band : values())
			if (band.lte == mark)
				return band;
		return null;
	}

	public static Band wcdma(int mark) {
		for (Band band : values())
			if (band.wcdma == mark)
				return band;
		return null;
	}
}
