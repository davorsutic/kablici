package hr.hashcode.cables.equip.comp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Set;

import hr.hashcode.cables.equip.bind.CabinetSystem;
import hr.hashcode.cables.equip.bind.DumpUtil;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Extended;
import hr.hashcode.cables.equip.comp.RiPort.RiPortable;

public class SystemContext {

	private final CabinetSystem system;
	private final List<Extended> hw;
	private final List<Connector<?>> cables;
	private final IdentityHashMap<Port, Component> portToUnit;
	private final IdentityHashMap<DigitalUnit, Set<Band>> digitalToBands;

	IdentityHashMap<Port, Port> portToPort = new IdentityHashMap<>();

	public SystemContext(CabinetSystem system) {
		this.system = system;
		this.hw = DumpUtil.getAllHardware(system);
		this.cables = DumpUtil.getCablesInDump(system);
		this.portToUnit = DumpUtil.calculatePortToUnit(system, hw);
		this.digitalToBands = DumpUtil.digitalToBands(system);

		for (Connector<?> cable : this.cables) {
			Port a = portToPort.put(cable.first(), cable.second());
			Port b = portToPort.put(cable.second(), cable.first());
			if (a != null && a != cable.second()) {
				Component start = portToUnit.get(cable.first());
				Component alreadyInside = portToUnit.get(a);
				Component nowInside = portToUnit.get(cable.second());
				// This situation happens for antennas (cause unknown) and for ec hub (likely, unknown port)
				// If debugging this, it might be useful to enable 'debug' option in Bind

//				throw new IllegalArgumentException(start + "->" + alreadyInside + "," + nowInside);
//				System.out.println("DUPLICATE " + start + "->" + alreadyInside + "," + nowInside);
			}
			if (b != null && b != cable.first()) {
				Component start = portToUnit.get(cable.second());
				Component alreadyInside = portToUnit.get(b);
				Component nowInside = portToUnit.get(cable.first());
//				throw new IllegalArgumentException(start + "->" + alreadyInside + "," + nowInside);
//				System.out.println("DUPLICATE " + start + "->" + alreadyInside + "," + nowInside);
			}
		}
	}

	private Set<Port> allPorts(List<? extends Component> components) {
		List<Port> ports = new ArrayList<>();
		for (Component component : components) {
			if (DigitalUnit.class.isAssignableFrom(component.getClass())) {
				DigitalUnit du = (DigitalUnit) component;

				ports.addAll(du.riPorts().ports());
				ports.addAll(du.ecPorts().ports());
				ports.addAll(du.idlPorts().ports());
				ports.addAll(du.dcPorts().ports());
			} else if (R503.class.isAssignableFrom(component.getClass())) {
				R503 r = (R503) component;

				ports.addAll(r.riPorts().ports());
				ports.addAll(r.ecPorts().ports());
				ports.addAll(r.dcPorts().ports());
			} else if (RadioUnitBase.class.isAssignableFrom(component.getClass())) {
				RadioUnitBase r = (RadioUnitBase) component;

				ports.addAll(r.riPorts().ports());
				ports.addAll(r.rfPorts().ports());
			}
		}
		Set<Port> portSet = Collections.newSetFromMap(new IdentityHashMap<>());
		portSet.addAll(ports);
		return portSet;
	}

	public boolean areConnected(RiPort port, List<? extends Component> components) {
		Set<Port> portSet = allPorts(components);
		if (port == null)
			return false;

		Port neighbor = portToPort.get(port);
		if (neighbor != null && portSet.contains(neighbor))
			return true;
		else
			return false;
	}

	boolean areConnected(RiPort port, Set<Port> portSet) {
		if (port == null)
			return false;

		Port neighbor = portToPort.get(port);
		return neighbor != null && portSet.contains(neighbor);
	}

	public boolean areConnected(RiPortable home, List<? extends Component> components) {
		if (home == null)
			return false;

		Set<Port> portSet = allPorts(components);
		return home.riPorts().ports().stream().anyMatch(port -> this.areConnected(port, portSet));
	}

	public IdentityHashMap<DigitalUnit, Set<Band>> digitalToBands() {
		return new IdentityHashMap<>(digitalToBands);
	}
}
