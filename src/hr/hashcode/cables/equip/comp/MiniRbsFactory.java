package hr.hashcode.cables.equip.comp;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hr.hashcode.cables.equip.comp.MiniRbs.Blueprint;
import hr.hashcode.cables.util.Xml;

public class MiniRbsFactory {

	private final List<MiniRbs.Blueprint> blueprints;

	private MiniRbsFactory(List<Blueprint> blueprints) {
		this.blueprints = blueprints;
	}

	public MiniRbs create(String type, String subtype) {
		return blueprint(type, subtype).create();
	}

	public Blueprint blueprint(String type, String subtype) {
		for (Blueprint bp : blueprints)
			if (bp.type().equals(type) && Objects.equals(subtype, bp.subtype()))
				return bp;
		throw new IllegalArgumentException(type + "," + subtype);
	}

	public static MiniRbsFactory of(InputStream input) {
		Xml xml = Xml.read(input);
		Xml rbses = xml.child("rbses");
		List<MiniRbs.Blueprint> blueprints = new ArrayList<>();
		for (Xml mini : rbses.children("mini")) {
			for (Xml rbs : mini.children("rbs")) {
				String type = rbs.attribute("type");
				String subtype = rbs.attribute("subtype");
				List<String> slots = names(rbs, "slot");
				List<String> ec = names(rbs, "ec");
				List<String> dc = names(rbs, "dc");
				Blueprint bp = new Blueprint(type, subtype, slots, dc, ec);
				blueprints.add(bp);
			}
		}
		return new MiniRbsFactory(blueprints);
	}

	private static List<String> names(Xml xml, String attr) {
		return RangeParser.parse(xml.child(attr).attribute("name"));
	}

}
