package hr.hashcode.cables.equip.comp;

public class Port {

	private final String name;

	Port(String name) {
		this.name = name;
	}

	public String name() {
		return name;
	}

	public String toString() {
		return name;
	}

}
