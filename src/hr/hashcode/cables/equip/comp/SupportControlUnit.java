package hr.hashcode.cables.equip.comp;

import java.util.List;

import hr.hashcode.cables.equip.comp.DcPort.DcPortable;
import hr.hashcode.cables.equip.comp.EcPort.EcPortable;

public class SupportControlUnit implements Component, DcPortable, EcPortable {

	private final String name;
	private final PortSet<DcPort> dcPorts;
	private final PortSet<EcPort> ecPorts;

	SupportControlUnit(String name, List<String> dcPorts, List<String> ecPorts) {
		this.name = name;
		this.ecPorts = PortSet.ecFactory.ports(ecPorts);
		this.dcPorts = PortSet.dcFactory.ports(dcPorts);
	}

	SupportControlUnit(List<String> dcPorts, List<String> ecPorts) {
		this("SCU", dcPorts, ecPorts);
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public PortSet<DcPort> dcPorts() {
		return dcPorts;
	}

	@Override
	public PortSet<EcPort> ecPorts() {
		return ecPorts;
	}

	@Override
	public String toString() {
		return name;
	}
}
