package hr.hashcode.cables.equip.comp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class Site {

	public final Map<Band, Set<RadioUnitBase>> radios = new EnumMap<>(Band.class);
	public final Map<DigitalUnit, Set<Band>> duToBands = new IdentityHashMap<>();
	public final Map<R503, Set<Band>> r503ToBands = new IdentityHashMap<>();

	public final List<Antenna> antennas = new ArrayList<>();
	public final List<RemoteRadioUnit> remotes = new ArrayList<>();
	public final List<MacroRbs> rbses = new ArrayList<>();
	public final List<MiniRbs> miniRbses = new ArrayList<>();
	public final List<Connector<IdlPort>> idlConnectors = new ArrayList<>();
	public final List<Connector<RiPort>> riConnectors = new ArrayList<>();
	public final List<Connector<EcPort>> ecConnectors = new ArrayList<>();
	public final List<Connector<DcPort>> dcConnectors = new ArrayList<>();
	public final List<Connector<RfPort>> rfConnectors = new ArrayList<>();

	public final Inventory inventory = new Inventory();

	public static class UnitQuantity {
		private final String type;
		private final String name;
		private final int count;

		private UnitQuantity(String name, String type, int count) {
			this.name = name;
			this.type = type;
			this.count = count;
		}

		public String name() {
			return name;
		}

		public String type() {
			return type;
		}

		public int count() {
			return count;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof UnitQuantity))
				return false;
			UnitQuantity other = (UnitQuantity) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			if (type == null) {
				if (other.type != null)
					return false;
			} else if (!type.equals(other.type))
				return false;
			return true;
		}

	}

	public static class Inventory {

		private final List<UnitQuantity> quantities = new ArrayList<>();

		public void add(String name, String type) {
			quantities.add(new UnitQuantity(name, type, 0));
		}

		public List<UnitQuantity> quantities() {
			Map<UnitQuantity, Long> group = quantities.stream()
					.collect(Collectors.groupingBy(x -> x, Collectors.counting()));
			List<UnitQuantity> result = new ArrayList<>();
			group.forEach((x, y) -> result.add(new UnitQuantity(x.name, x.type, y.intValue())));
			Collections.sort(result, Comparator.comparing(x -> x.name));
			return result;
		}
	}

}
