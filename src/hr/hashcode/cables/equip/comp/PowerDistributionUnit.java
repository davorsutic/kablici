package hr.hashcode.cables.equip.comp;

import java.util.List;

import hr.hashcode.cables.equip.comp.DcPort.DcPortable;
import hr.hashcode.cables.equip.comp.EcPort.SingleEcPortable;
import hr.hashcode.cables.equip.comp.PortSet.SinglePortSet;

public class PowerDistributionUnit implements Component, SingleEcPortable, DcPortable {

	private final String name;
	private final SinglePortSet<EcPort> ecPort;
	private final PortSet<DcPort> dcPorts;

	public PowerDistributionUnit(String name, int count) {
		this.name = name;
		this.ecPort = PortSet.ecFactory.port("EC");
		this.dcPorts = PortSet.dcFactory.ports(count, x -> String.valueOf(x + 1));
	}

	public PowerDistributionUnit(String name, List<String> ports) {
		this.name = name;
		this.ecPort = PortSet.ecFactory.port("EC");
		this.dcPorts = PortSet.dcFactory.ports(ports);
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public SinglePortSet<EcPort> ecPorts() {
		return ecPort;
	}

	@Override
	public PortSet<DcPort> dcPorts() {
		return dcPorts;
	}

	@Override
	public String toString() {
		return name;
	}
}
