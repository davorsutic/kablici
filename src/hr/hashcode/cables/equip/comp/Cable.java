package hr.hashcode.cables.equip.comp;

public class Cable<T extends Port> {

	private final String name;
	private final Connector<T> connector;

	public Cable(Connector<T> connector, String name) {
		this.name = name;
		this.connector = connector;
	}

	public String name() {
		return name;
	}

	public Connector<T> connector() {
		return connector;
	}
}
