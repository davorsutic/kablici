package hr.hashcode.cables.equip.comp;

import hr.hashcode.cables.equip.comp.DcPort.SingleDcPortable;
import hr.hashcode.cables.equip.comp.EcPort.SingleEcPortable;
import hr.hashcode.cables.equip.comp.IdlPort.SingleIdlPortable;
import hr.hashcode.cables.equip.comp.PortSet.SinglePortSet;
import hr.hashcode.cables.equip.comp.RiPort.RiPortable;

public class DigitalUnit implements Component, RiPortable, SingleDcPortable, SingleEcPortable, SingleIdlPortable {

	private static final int riPortsCount = 6;

	private final String name;
	private final String type;

	private final PortSet<RiPort> riPorts;
	private final SinglePortSet<DcPort> dcPort;
	private final SinglePortSet<EcPort> ecPort;
	private final SinglePortSet<IdlPort> idlPort;

	public DigitalUnit(String name, String type) {
		this.name = name;
		this.type = type;
		this.dcPort = PortSet.dcFactory.port("DC");
		this.ecPort = PortSet.ecFactory.port("EC");
		this.idlPort = PortSet.idlFactory.port("IDL");
		this.riPorts = PortSet.riFactory.ports(riPortsCount, x -> String.valueOf((char) ('A' + x)));
	}

	public String type() {
		return type;
	}

	@Override
	public SinglePortSet<DcPort> dcPorts() {
		return dcPort;
	}

	@Override
	public SinglePortSet<EcPort> ecPorts() {
		return ecPort;
	}

	@Override
	public PortSet<RiPort> riPorts() {
		return riPorts;
	}

	@Override
	public SinglePortSet<IdlPort> idlPorts() {
		return idlPort;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}
}
