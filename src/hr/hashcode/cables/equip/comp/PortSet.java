package hr.hashcode.cables.equip.comp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import hr.hashcode.cables.util.Util;

public class PortSet<T extends Port> {

	private final List<T> ports;
	private final Map<String, T> byName;

	private PortSet(List<T> ports) {
		this.ports = Collections.unmodifiableList(new ArrayList<>(ports));
		this.byName = Util.toMap(this.ports, Port::name);
	}

	public List<T> ports() {
		return ports;
	}

	public T port(String name) {
		return byName.get(name);
	}

	public static class SinglePortSet<T extends Port> extends PortSet<T> {

		private final T port;

		private SinglePortSet(T port) {
			super(Collections.singletonList(port));
			this.port = port;
		}

		public T port() {
			return port;
		}
	}

	public static final Factory<EcPort> ecFactory = new Factory<>(EcPort::new);
	public static final Factory<DcPort> dcFactory = new Factory<>(DcPort::new);
	public static final Factory<IdlPort> idlFactory = new Factory<>(IdlPort::new);
	public static final Factory<RiPort> riFactory = new Factory<>(RiPort::new);
	public static final Factory<RfPort> rfFactory = new Factory<>(RfPort::new);

	public static class Factory<T extends Port> {

		private final Function<String, T> constructor;

		Factory(Function<String, T> constructor) {
			this.constructor = constructor;
		}

		PortSet<T> ports(int count, IntFunction<String> naming) {
			List<String> names = IntStream.range(0, count)
					.mapToObj(naming)
					.collect(Collectors.toList());
			return ports(names);
		}

		PortSet<T> ports(List<String> names) {
			List<T> ports = Util.map(names, constructor);
			return new PortSet<>(ports);
		}

		public PortSet<T> ports(String... names) {
			return ports(Arrays.asList(names));
		}

		public SinglePortSet<T> port(String name) {
			T port = constructor.apply(name);
			return new SinglePortSet<>(port);
		}

	}

}
