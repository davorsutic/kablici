package hr.hashcode.cables.equip.comp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Subrack implements Component {

	private final String name;
	private DigitalUnitWcdma digital;

	private final Map<String, Integer> indices;
	private final List<String> slots;

	private final RadioUnit[] radios;

	Subrack(String name, List<String> radios) {
		this.name = name;
		this.slots = Collections.unmodifiableList(new ArrayList<>(radios));
		Map<String, Integer> indices = new HashMap<>();
		for (String radio : radios) {
			if (indices.containsKey(radio))
				throw new IllegalArgumentException(radios.toString());
			indices.put(radio, indices.size());
		}
		this.indices = Collections.unmodifiableMap(indices);
		this.radios = new RadioUnit[indices.size()];
	}

	@Override
	public String name() {
		return name;
	}

	public List<String> slots() {
		return slots;
	}

	public void setDigital(DigitalUnitWcdma digital) {
		this.digital = digital;
	}

	private int index(String slot) {
		Integer index = indices.get(slot);
		if (index == null)
			throw new IllegalArgumentException(slot);
		return index;
	}

	@Deprecated
	public void setRadio(int slot, RadioUnit radio) {
		this.radios[slot] = radio;
	}

	public void setRadio(String slot, RadioUnit radio) {
		this.radios[index(slot)] = radio;
	}

	public DigitalUnitWcdma digital() {
		return digital;
	}

	@Deprecated
	public RadioUnit radio(int slot) {
		return radios[slot];
	}

	public RadioUnit radio(String slot) {
		return radios[index(slot)];
	}

	public List<RadioUnit> radios() {
		return new ArrayList<>(Arrays.asList(radios));
	}

	@Override
	public String toString() {
		return name;
	}
}
