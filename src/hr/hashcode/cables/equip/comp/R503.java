package hr.hashcode.cables.equip.comp;

import hr.hashcode.cables.equip.comp.DcPort.SingleDcPortable;
import hr.hashcode.cables.equip.comp.EcPort.SingleEcPortable;
import hr.hashcode.cables.equip.comp.PortSet.SinglePortSet;
import hr.hashcode.cables.equip.comp.RiPort.RiPortable;

public class R503 implements Component, RiPortable, SingleDcPortable, SingleEcPortable {

	private static final int riPortsCount = 16;

	private final String name;
	private final PortSet<RiPort> riPorts;
	private final SinglePortSet<DcPort> dcPort;
	private final SinglePortSet<EcPort> ecPort;

	public R503(String name) {
		this.name = name;
		this.riPorts = PortSet.riFactory.ports(riPortsCount, x -> String.valueOf(x + 1));
		this.dcPort = PortSet.dcFactory.port("DC");
		this.ecPort = PortSet.ecFactory.port("EC");
	}

	public R503() {
		this("R503");
	}

	public String name() {
		return name;
	}

	public PortSet<RiPort> riPorts() {
		return riPorts;
	}

	public SinglePortSet<DcPort> dcPorts() {
		return dcPort;
	}

	public SinglePortSet<EcPort> ecPorts() {
		return ecPort;
	}

	public String toString() {
		return name;
	}
}
