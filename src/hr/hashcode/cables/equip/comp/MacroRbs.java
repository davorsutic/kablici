package hr.hashcode.cables.equip.comp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MacroRbs implements Rbs {

	private abstract static class Desc<T> implements Supplier<T> {
		final String name;

		Desc(String name) {
			this.name = Objects.requireNonNull(name);
		}

		static List<String> protect(List<String> list) {
			Objects.requireNonNull(list);

			if (list.contains(null))
				throw new IllegalArgumentException(list.toString());

			if (new HashSet<>(list).size() != list.size())
				throw new IllegalArgumentException(list.toString());

			return Collections.unmodifiableList(new ArrayList<>(list));
		}
	}

	private static class PduDesc extends Desc<PowerDistributionUnit> {

		private final List<String> ports;

		PduDesc(String name, List<String> ports) {
			super(name);
			this.ports = protect(ports);
		}

		@Override
		public PowerDistributionUnit get() {
			return new PowerDistributionUnit(name, ports);
		}
	}

	private static class ScuDesc extends Desc<SupportControlUnit> {

		private final List<String> dcPorts;
		private final List<String> ecPorts;

		ScuDesc(String name, List<String> dcPorts, List<String> ecPorts) {
			super(name);
			this.dcPorts = protect(dcPorts);
			this.ecPorts = protect(ecPorts);
		}

		@Override
		public SupportControlUnit get() {
			return new SupportControlUnit(name, dcPorts, ecPorts);
		}
	}

	private static class ShuDesc extends Desc<SupportHubUnit> {

		private final List<String> ports;

		ShuDesc(String name, List<String> ports) {
			super(name);
			this.ports = protect(ports);
		}

		@Override
		public SupportHubUnit get() {
			return new SupportHubUnit(name, ports);
		}
	}

	private static class SubrackDesc extends Desc<Subrack> {

		private final List<String> radioSlots;

		SubrackDesc(String name, List<String> radioSlots) {
			super(name);
			this.radioSlots = protect(radioSlots);
		}

		@Override
		public Subrack get() {
			return new Subrack(name, radioSlots);
		}

	}

	private static class ExtensionDesc extends Desc<ExtensionRack> {

		private final List<String> slots;

		ExtensionDesc(String name, List<String> slots) {
			super(name);
			this.slots = protect(slots);
		}

		@Override
		public ExtensionRack get() {
			return new ExtensionRack(name, slots);
		}

	}

	public static class RbsBlueprint implements Supplier<MacroRbs> {

		private final String type;
		private final String subtype;
		private final ShuDesc shu;
		private final ScuDesc scu;
		private final List<PduDesc> pdus;
		private final List<SubrackDesc> subracks;
		private final List<ExtensionDesc> extensions;

		private RbsBlueprint(Builder builder) {
			this.type = builder.type;
			this.subtype = builder.subtype;
			this.shu = builder.shu.value;
			this.scu = builder.scu.value;
			this.pdus = builder.pdus.values();
			this.subracks = builder.subracks.values();
			this.extensions = builder.extensions.values();
		}

		public String type() {
			return type;
		}

		public String subtype() {
			return subtype;
		}

		@Override
		public MacroRbs get() {
			return new MacroRbs(this);
		}

	}

	static Builder newBuilder(String type) {
		return newBuilder(type, null);
	}

	static Builder newBuilder(String type, String subtype) {
		return new Builder(type, subtype);
	}

	private static class DescMap<T extends Desc<?>> {
		private final Map<String, T> map = new LinkedHashMap<>();

		void register(T value) {
			String name = value.name;
			if (map.containsKey(name))
				throw new IllegalArgumentException(name);
			map.put(name, value);
		}

		List<T> values() {
			return Collections.unmodifiableList(new ArrayList<>(map.values()));
		}
	}

	private static class UniqueValue<T extends Desc<?>> {
		private T value;

		void register(T value) {
			if (this.value != null)
				throw new IllegalStateException(this.value.name + "," + value.name);
			this.value = Objects.requireNonNull(value);
		}

	}

	static class Builder {

		private final String type;
		private final String subtype;

		private final UniqueValue<ScuDesc> scu = new UniqueValue<>();
		private final UniqueValue<ShuDesc> shu = new UniqueValue<>();

		private final DescMap<PduDesc> pdus = new DescMap<>();
		private final DescMap<SubrackDesc> subracks = new DescMap<>();
		private final DescMap<ExtensionDesc> extensions = new DescMap<>();

		private Builder(String type, String subtype) {
			this.type = Objects.requireNonNull(type);
			this.subtype = subtype;
		}

		private static List<String> enumerate(int count) {
			return IntStream.rangeClosed(1, count)
					.mapToObj(String::valueOf)
					.collect(Collectors.toList());
		}

		private static List<String> enumerate(String prefix, int count) {
			return IntStream.rangeClosed(1, count)
					.mapToObj(x -> prefix + x)
					.collect(Collectors.toList());
		}

		Builder scu(List<String> dcPorts, List<String> ecPorts) {
			return scu("SCU", dcPorts, ecPorts);
		}

		Builder scu(String name, List<String> dcPorts, List<String> ecPorts) {
			ScuDesc scu = new ScuDesc(name, dcPorts, ecPorts);
			this.scu.register(scu);
			return this;
		}

		Builder shu(List<String> ports) {
			return shu("SHU", ports);
		}

		Builder shu(String name, List<String> ports) {
			ShuDesc shu = new ShuDesc(name, ports);
			this.shu.register(shu);
			return this;
		}

		Builder pdu(String name, List<String> ports) {
			PduDesc pdu = new PduDesc(name, ports);
			pdus.register(pdu);
			return this;
		}

		Builder pdu(List<String> names, List<String> ports) {
			for (String name : names)
				pdu(name, ports);
			return this;
		}

		Builder pdu(int count, int ports) {
			return pdu(enumerate("PDU", count), enumerate(ports));
		}

		Builder subrack(String name, List<String> slots) {
			SubrackDesc subrack = new SubrackDesc(name, slots);
			subracks.register(subrack);
			return this;
		}

		Builder subrack(List<String> names, List<String> slots) {
			for (String name : names)
				subrack(name, slots);
			return this;
		}

		Builder subrack(int count, int slots) {
			return subrack(enumerate(count), enumerate(slots));
		}

		Builder extension(String name, List<String> slots) {
			ExtensionDesc extension = new ExtensionDesc(name, slots);
			extensions.register(extension);
			return this;
		}

		Builder extension(List<String> names, List<String> slots) {
			for (String name : names)
				extension(name, slots);
			return this;
		}

		Builder extension(int count, int slots) {
			return extension(enumerate(count), enumerate(slots));
		}

		RbsBlueprint build() {

			return new RbsBlueprint(this);
		}

	}

	private static class Named<T extends Component> {
		private final Map<String, T> map;
		private final List<T> list;

		Named(List<? extends Supplier<T>> supps) {
			Map<String, T> map = new HashMap<>();
			List<T> list = new ArrayList<>();
			for (Supplier<T> supp : supps) {
				T value = supp.get();
				map.put(value.name(), value);
				list.add(value);
			}
			this.map = Collections.unmodifiableMap(map);
			this.list = Collections.unmodifiableList(list);
		}

		T get(String name) {
			return map.get(name);
		}

		List<T> values() {
			return list;
		}

		@Override
		public String toString() {
			return list.toString();
		}
	}

	private final String type;
	private final String subtype;
	private final SupportControlUnit scu;
	private final SupportHubUnit hub;
	private final Named<PowerDistributionUnit> pdus;
	private final Named<Subrack> subracks;
	private final Named<ExtensionRack> extensions;

	MacroRbs(RbsBlueprint blueprint) {
		this.type = blueprint.type;
		this.subtype = blueprint.subtype;
		this.scu = extract(blueprint.scu);
		this.hub = extract(blueprint.shu);
		this.pdus = new Named<>(blueprint.pdus);
		this.subracks = new Named<>(blueprint.subracks);
		this.extensions = new Named<>(blueprint.extensions);
	}

	private static <T> T extract(Supplier<T> supp) {
		if (supp == null)
			return null;
		return supp.get();
	}

	public String type() {
		return type;
	}

	public String subtype() {
		return subtype;
	}

	public SupportControlUnit scu() {
		return scu;
	}

	public SupportHubUnit shu() {
		return hub;
	}

	public PowerDistributionUnit pdu(String name) {
		return pdus.get(name);
	}

	public List<PowerDistributionUnit> pdus() {
		return pdus.values();
	}

	public Subrack subrack(String name) {
		return subracks.get(name);
	}

	public List<Subrack> subracks() {
		return subracks.values();
	}

	public ExtensionRack extension(String name) {
		return extensions.get(name);
	}

	public List<ExtensionRack> extensions() {
		return extensions.values();
	}

}
