package hr.hashcode.cables.equip.comp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExtensionRack implements Component {

	private final String name;
	private final Rack<Object> rack;
	private final Map<String, Integer> indices;
	private final List<String> slots;

	ExtensionRack(String name, List<String> slots) {
		this.name = name;
		this.slots = Collections.unmodifiableList(new ArrayList<>(slots));
		Map<String, Integer> indices = new HashMap<>();
		for (String slot : slots) {
			if (indices.containsKey(slot))
				throw new IllegalArgumentException(slots.toString());
			indices.put(slot, indices.size());
		}
		this.indices = Collections.unmodifiableMap(indices);
		this.rack = new Rack<>(indices.size());
	}

	private int index(String slot) {
		Integer index = indices.get(slot);
		if (index == null)
			throw new IllegalArgumentException(slot);
		return index;
	}

	@Override
	public String name() {
		return name;
	}

	public List<String> slots() {
		return slots;
	}

	@Deprecated
	void setUnit(int slot, DigitalUnitStandard unit) {
		rack.set(slot, unit);
	}

	public void setUnit(String slot, DigitalUnitStandard unit) {
		rack.set(index(slot), unit);
	}

	public void setUnit(String slot, DigitalUnitBaseband unit) {
		rack.set(index(slot), unit);
	}

	@Deprecated
	void setR503(int slot, R503 r503) {
		rack.set(slot, r503);
	}

	public void setR503(String slot, R503 r503) {
		rack.set(index(slot), r503);
	}

	public int length() {
		return rack.length();
	}

	public DigitalUnit unit(String slot) {
		return rack.get(index(slot), DigitalUnit.class);
	}

	@Deprecated
	public DigitalUnit unit(int slot) {
		return rack.get(slot, DigitalUnit.class);
	}

	public R503 r503(String slot) {
		return rack.get(index(slot), R503.class);
	}

	@Deprecated
	public R503 r503(int index) {
		return rack.get(index, R503.class);
	}

}
