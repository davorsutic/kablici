package hr.hashcode.cables.equip.comp;

import hr.hashcode.cables.equip.comp.PortSet.SinglePortSet;

public class EcPort extends Port {

	EcPort(String name) {
		super(name);
	}

	public interface EcPortable {
		PortSet<EcPort> ecPorts();

		default EcPort ecPort(String name) {
			return ecPorts().port(name);
		}

	}

	public interface SingleEcPortable extends EcPortable {
		@Override
		SinglePortSet<EcPort> ecPorts();

		default EcPort ecPort() {
			return ecPorts().port();
		}
	}

}
