package hr.hashcode.cables.equip.comp;

import java.util.List;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.comp.EcPort.EcPortable;

public class SupportHubUnit implements Component, EcPortable {

	private final String name;
	private final PortSet<EcPort> ecPorts;

	SupportHubUnit(String name, List<String> prefices, List<String> suffices) {
		this.name = name;
		List<String> names = prefices.stream()
				.flatMap(x -> suffices.stream().map(y -> x + y))
				.collect(Collectors.toList());
		this.ecPorts = PortSet.ecFactory.ports(names);
	}

	public SupportHubUnit(List<String> prefices, List<String> suffices) {
		this("SHU", prefices, suffices);
	}

	public SupportHubUnit(String name, List<String> ports) {
		this.name = name;
		this.ecPorts = PortSet.ecFactory.ports(ports);
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public PortSet<EcPort> ecPorts() {
		return ecPorts;
	}

	@Override
	public String toString() {
		return name;
	}
}
