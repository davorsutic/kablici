package hr.hashcode.cables.equip.comp;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hr.hashcode.cables.equip.comp.MacroRbs.RbsBlueprint;
import hr.hashcode.cables.util.Xml;

public class RbsFactory {

	private static List<String> list(Xml xml, String attr) {
		return RangeParser.parse(xml.attribute(attr));
	}

	private static List<String> ec(Xml xml) {
		return list(xml, "ec");
	}
	private static List<String> dc(Xml xml) {
		return list(xml, "dc");
	}
	private static List<String> slots(Xml xml) {
		return list(xml, "slots");
	}
	private static List<String> names(Xml xml) {
		return list(xml, "name");
	}

	private static String name(Xml xml) {
		List<String> list = list(xml, "name");
		if (list.size() != 1)
			throw new IllegalArgumentException();
		return list.get(0);
	}

	private final List<RbsBlueprint> blueprints;

	private RbsFactory(List<RbsBlueprint> blueprints) {
		this.blueprints = blueprints;
	}

	public RbsBlueprint blueprint(String type) {
		return blueprint(type, null);
	}

	public RbsBlueprint blueprint(String type, String subtype) {
		for (RbsBlueprint bp : blueprints)
			if (bp.type().equals(type) && Objects.equals(bp.subtype(), subtype))
				return bp;
		throw new IllegalArgumentException(type + "," + subtype);
	}

	public MacroRbs create(String type) {
		return create(type, null);
	}

	public MacroRbs create(String type, String subtype) {
		return blueprint(type, subtype).get();
	}

	public static RbsFactory of(InputStream stream) {
		List<RbsBlueprint> blueprints = new ArrayList<>();

		Xml xml = Xml.read(stream);
		Xml rbses = xml.child("rbses");

		for (Xml macro : rbses.children("macro"))
			for (Xml rbs : macro.children("rbs")) {
				String type = rbs.attribute("type");
				String subtype = rbs.attribute("subtype");

				MacroRbs.Builder builder = MacroRbs.newBuilder(type, subtype);

				for (Xml pdu : rbs.children("pdu"))
					builder.pdu(names(pdu), dc(pdu));

				for (Xml scu : rbs.children("scu"))
					builder.scu(name(scu), dc(scu), ec(scu));

				for (Xml shu : rbs.children("shu"))
					builder.shu(name(shu), ec(shu));

				for (Xml subrack : rbs.children("subrack"))
					builder.subrack(names(subrack), slots(subrack));

				for (Xml extension : rbs.children("extension"))
					builder.extension(names(extension), slots(extension));

				blueprints.add(builder.build());
			}

		return new RbsFactory(blueprints);
	}

}
