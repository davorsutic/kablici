package hr.hashcode.cables.equip.comp;

import hr.hashcode.cables.equip.comp.PortSet.SinglePortSet;

public class IdlPort extends Port {

	IdlPort(String name) {
		super(name);
	}

	public interface IdlPortable {
		PortSet<IdlPort> idlPorts();

		default IdlPort idlPort(String name) {
			return idlPorts().port(name);
		}
	}

	public interface SingleIdlPortable extends IdlPortable {
		@Override
		SinglePortSet<IdlPort> idlPorts();

		default IdlPort idlPort() {
			return idlPorts().port();
		}
	}
}
