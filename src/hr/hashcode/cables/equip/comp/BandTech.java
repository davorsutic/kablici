package hr.hashcode.cables.equip.comp;

import java.util.Objects;

public class BandTech {
	public static BandTech lte(int mark) {
		return new BandTech(Band.lte(mark), Technology.LTE);
	}

	public static BandTech wcdma(int mark) {
		return new BandTech(Band.wcdma(mark), Technology.WCDMA);
	}

	private final Band band;
	private final Technology tech;

	public BandTech(Band band, Technology tech) {
		this.band = Objects.requireNonNull(band);
		this.tech = Objects.requireNonNull(tech);
	}

	public Band band() {
		return band;
	}

	public Technology technology() {
		return tech;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + band.hashCode();
		result = prime * result + tech.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof BandTech))
			return false;
		BandTech other = (BandTech) obj;
		return band == other.band && tech == other.tech;
	}

	@Override
	public String toString() {
		return tech + ":" + band;
	}
}
