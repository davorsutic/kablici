package hr.hashcode.cables.equip.comp;

public class RiPort extends Port {

	RiPort(String name) {
		super(name);
	}

	public interface RiPortable {
		PortSet<RiPort> riPorts();

		default RiPort riPort(String name) {
			return riPorts().port(name);
		}
	}

}
