package hr.hashcode.cables.equip.comp;

import hr.hashcode.cables.equip.comp.RfPort.RfPortable;
import hr.hashcode.cables.equip.comp.RiPort.RiPortable;

public class RadioUnitBase implements Component, RiPortable, RfPortable {

	private final String name;
	private final String type;
	private final PortSet<RiPort> riPorts;
	private final PortSet<RfPort> rfPorts;

	public RadioUnitBase(String name, String type) {
		this.name = name;
		this.type = type;
		this.riPorts = PortSet.riFactory.ports("DATA_1", "DATA_2");
		this.rfPorts = PortSet.rfFactory.ports("A", "B", "RXA_IO", "RXB_IO");
	}

	public String type() {
		return type;
	}

	@Override
	public PortSet<RiPort> riPorts() {
		return riPorts;
	}

	@Override
	public PortSet<RfPort> rfPorts() {
		return rfPorts;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}

}
