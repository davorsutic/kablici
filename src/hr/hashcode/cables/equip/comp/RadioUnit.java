package hr.hashcode.cables.equip.comp;

import hr.hashcode.cables.equip.comp.DcPort.SingleDcPortable;
import hr.hashcode.cables.equip.comp.PortSet.SinglePortSet;

public class RadioUnit extends RadioUnitBase implements SingleDcPortable {

	private final SinglePortSet<DcPort> dcPort;

	public RadioUnit(String name, String type) {
		super(name, type);
		this.dcPort = PortSet.dcFactory.port("DC");
	}

	@Override
	public SinglePortSet<DcPort> dcPorts() {
		return dcPort;
	}

}
