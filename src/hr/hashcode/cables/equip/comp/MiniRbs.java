package hr.hashcode.cables.equip.comp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MiniRbs implements Rbs {

	public static class Blueprint {
		private final String type;
		private final String subtype;
		private final List<String> slots;
		private final List<String> dcPorts;
		private final List<String> ecPorts;

		public Blueprint(String type, String subtype, List<String> slots, List<String> dcPorts, List<String> ecPorts) {
			this.type = type;
			this.subtype = subtype;
			this.slots = slots;
			this.dcPorts = dcPorts;
			this.ecPorts = ecPorts;
		}

		public String type() {
			return type;
		}

		public String subtype() {
			return subtype;
		}

		public MiniRbs create() {
			return new MiniRbs(type, subtype, slots, dcPorts, ecPorts);
		}

	}

	private final Map<String, Integer> indices;
	private final PortSet<DcPort> dcPorts;
	private final PortSet<EcPort> ecPorts;
	private final String type;
	private final String subtype;
	private final Rack<Object> rack;
	private final List<String> slots;

	private MiniRbs(String type, String subtype, List<String> slots, List<String> dcPorts, List<String> ecPorts) {
		this.slots = Collections.unmodifiableList(slots);
		this.type = type;
		this.subtype = subtype;
		this.dcPorts = PortSet.dcFactory.ports(dcPorts);
		this.ecPorts = PortSet.ecFactory.ports(ecPorts);
		this.rack = new Rack<>(slots.size());
		Map<String, Integer> indices = new HashMap<>();
		for (String slot : slots)
			indices.put(slot, indices.size());
		this.indices = indices;
	}

	public List<String> slots() {
		return slots;
	}

	private int index(String slot) {
		Integer index = indices.get(slot);
		if (index == null)
			throw new IllegalArgumentException(slot);
		return index;
	}

	public List<Component> getComponents() {
		List<Component> c = new ArrayList<>();
		if (duw() != null) {
			c.add(duw());
		}
		for (String slot : slots) {
			Component component = dus(slot);
			if (component != null) {
				c.add(component);
			}
			component = bb(slot);
			if (component != null) {
				c.add(component);
			}
			component = r503(slot);
			if (component != null) {
				c.add(component);
			}
		}
		return c;
	}

	public String type() {
		return type;
	}

	public String subtype() {
		return subtype;
	}

	public PortSet<DcPort> dcPorts() {
		return dcPorts;
	}

	public PortSet<EcPort> ecPorts() {
		return ecPorts;
	}

	public void set(DigitalUnitWcdma unit) {
		rack.set(0, 2, unit);
	}

	public DigitalUnitWcdma duw() {
		return rack.get(0, 2, DigitalUnitWcdma.class);
	}

	@Deprecated
	public DigitalUnit du(int index) {
		return rack.get(index, DigitalUnit.class);
	}

	@Deprecated
	public R503 r503(int index) {
		return rack.get(index, R503.class);
	}

	@Deprecated
	public void set(int index, DigitalUnitStandard dus) {
		rack.set(index, dus);
	}

	@Deprecated
	public void set(int index, DigitalUnitBaseband bb) {
		rack.set(index, bb);
	}

	@Deprecated
	public void set(int index, R503 r503) {
		rack.set(index, r503);
	}

	public DigitalUnitStandard dus(String slot) {
		return rack.get(index(slot), DigitalUnitStandard.class);
	}

	public R503 r503(String slot) {
		return rack.get(index(slot), R503.class);
	}

	public DigitalUnitBaseband bb(String slot) {
		return rack.get(index(slot), DigitalUnitBaseband.class);
	}

	public void set(String slot, DigitalUnitStandard dus) {
		rack.set(index(slot), dus);
	}

	public void set(String slot, DigitalUnitBaseband bb) {
		rack.set(index(slot), bb);
	}

	public void set(String slot, DigitalUnitLte dul) {
		rack.set(index(slot), dul);
	}

	public void set(String slot, R503 r503) {
		rack.set(index(slot), r503);
	}
}
