package hr.hashcode.cables.equip.comp;

import java.util.Arrays;

class Rack<T> {

	private final T[] objects;

	Rack(int slots) {
		@SuppressWarnings("unchecked")
		T[] objects = (T[]) new Object[slots];
		this.objects = objects;
	}

	void set(int start, int slots, T unit) {
		if (!isEmpty(start, slots))
			throw new IllegalStateException(start + "," + slots + "," + Arrays.toString(objects));
		for (int i = 0; i < slots; i++)
			objects[start + i] = unit;
	}

	void set(int slot, T unit) {
		set(slot, 1, unit);
	}

	<U extends T> U get(int slot, Class<U> clazz) {
		T unit = objects[slot];
		if (clazz.isInstance(unit))
			return clazz.cast(unit);
		else
			return null;
	}

	<U extends T> U get(int start, int slots, Class<U> clazz) {
		U first = get(start, clazz);
		if (first == null)
			return null;
		for (int i = 1; i < slots; i++)
			if (objects[start + i] != first)
				return null;
		return first;
	}

	int length() {
		return objects.length;
	}

	boolean isEmpty(int index) {
		return objects[index] == null;
	}

	boolean isEmpty(int start, int slots) {
		for (int i = 0; i < slots; i++)
			if (!isEmpty(start + i))
				return false;
		return true;
	}

}
