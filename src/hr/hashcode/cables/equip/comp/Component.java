package hr.hashcode.cables.equip.comp;

public interface Component {

	String name();

	public default boolean is(Class<?> clazz) {
		return clazz.isAssignableFrom(this.getClass());
	}

}
