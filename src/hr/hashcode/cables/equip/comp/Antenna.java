package hr.hashcode.cables.equip.comp;

import hr.hashcode.cables.equip.comp.RfPort.RfPortable;

public class Antenna implements Component, RfPortable {

	private final PortSet<RfPort> rfPorts = PortSet.rfFactory.ports("A", "B");

	private final String name;

	public Antenna(String name) {
		this.name = name;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public PortSet<RfPort> rfPorts() {
		return rfPorts;
	}

	@Override
	public String toString() {
		return name;
	}
}
