package hr.hashcode.cables.equip.rules;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.comp.Cable;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DcPort;
import hr.hashcode.cables.equip.comp.DcPort.SingleDcPortable;
import hr.hashcode.cables.equip.comp.DigitalUnitStandard;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.MiniRbs;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.util.Xml;

public class DcRules {

	public List<Cable<DcPort>> connect(MacroRbs rbs) {
		for (RbsRules rule : rules)
			if (rule.fits(rbs))
				return rule.connectors(rbs);
		return Collections.emptyList();
	}

	public List<Cable<DcPort>> connect(MiniRbs rbs) {
		for (MiniRbsRules rule : miniRules)
			if (rule.fits(rbs))
				return rule.connectors(rbs);
		return Collections.emptyList();
	}

	private final List<RbsRules> rules;
	private final List<MiniRbsRules> miniRules;

	private DcRules(List<RbsRules> rules, List<MiniRbsRules> miniRules) {
		this.rules = rules;
		this.miniRules = miniRules;
	}

	private static class RbsRules {
		private final String type;
		private final String subtype;
		private final List<DcRuleDesc> rules;

		RbsRules(String type, String subtype, List<DcRuleDesc> rules) {
			this.type = type;
			this.subtype = subtype;
			this.rules = rules;
		}

		boolean fits(MacroRbs rbs) {
			return rbs.type().equals(type) && (subtype == null || Objects.equals(rbs.subtype(), subtype));
		}

		List<Cable<DcPort>> connectors(MacroRbs rbs) {
			return rules.stream()
					.map(x -> x.connector(rbs))
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
		}
	}

	private static class MiniRbsRules {
		private final String type;
		private final String subtype;
		private final List<MiniRuleDesc> rules;

		private MiniRbsRules(String type, String subtype, List<MiniRuleDesc> rules) {
			this.type = type;
			this.subtype = subtype;
			this.rules = rules;
		}

		boolean fits(MiniRbs rbs) {
			return rbs.type().equals(type) && (subtype == null || Objects.equals(rbs.subtype(), subtype));
		}

		List<Cable<DcPort>> connectors(MiniRbs rbs) {
			return rules.stream()
					.map(x -> x.connector(rbs))
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
		}

	}

	private interface MiniRuleDesc {
		Cable<DcPort> connector(MiniRbs rbs);
	}

	private static class MiniR503Desc implements MiniRuleDesc {
		private final String slot;
		private final String port;
		private final String cable;

		MiniR503Desc(String slot, String port, String cable) {
			this.slot = slot;
			this.port = port;
			this.cable = cable;
		}

		@Override
		public Cable<DcPort> connector(MiniRbs rbs) {
			R503 r503 = rbs.r503(slot);
			if (r503 != null) {
				DcPort first = r503.dcPort();
				DcPort second = rbs.dcPorts().port(port);
				Connector<DcPort> connector = new Connector<>(first, second);
				return new Cable<>(connector, cable);
			}
			return null;
		}
	}

	private static class MiniDuwDesc implements MiniRuleDesc {
		private final String port;
		private final String cable;

		private MiniDuwDesc(String port, String cable) {
			this.port = port;
			this.cable = cable;
		}

		@Override
		public Cable<DcPort> connector(MiniRbs rbs) {
			DigitalUnitWcdma duw = rbs.duw();
			if (duw != null) {
				DcPort first = duw.dcPort();
				DcPort second = rbs.dcPorts().port(port);
				Connector<DcPort> connector = new Connector<>(first, second);
				return new Cable<>(connector, cable);
			}
			return null;
		}

	}

	private static class MiniDusDesc implements MiniRuleDesc {
		private final String slot;
		private final String port;
		private final String cable;

		MiniDusDesc(String slot, String port, String cable) {
			this.slot = slot;
			this.port = port;
			this.cable = cable;
		}

		@Override
		public Cable<DcPort> connector(MiniRbs rbs) {
			DigitalUnitStandard dus = rbs.dus(slot);
			if (dus != null) {
				DcPort first = dus.dcPort();
				DcPort second = rbs.dcPorts().port(port);
				Connector<DcPort> connector = new Connector<>(first, second);
				return new Cable<>(connector, cable);
			}
			return null;
		}
	}

	private static class DcRuleDesc {

		private final AddressPdu source;
		private final DcTarget target;
		private final String cable;

		DcRuleDesc(AddressPdu source, DcTarget target, String cable) {
			this.source = source;
			this.target = target;
			this.cable = cable;
		}

		Cable<DcPort> connector(MacroRbs rbs) {
			DcPort first = source.get(rbs);
			DcPort second = target.get(rbs);
			if (second == null)
				return null;
			Connector<DcPort> connector = new Connector<>(first, second);
			return new Cable<>(connector, cable);
		}
	}

	private interface DcComponent extends DcTarget {
		SingleDcPortable getComponent(MacroRbs rbs);

		@Override
		default DcPort get(MacroRbs rbs) {
			SingleDcPortable portable = getComponent(rbs);
			if (portable == null)
				return null;
			return portable.dcPort();
		}
	}

	private interface DcTarget {
		DcPort get(MacroRbs rbs);
	}

	private static class AddressRadio implements DcComponent {

		private final String subrack;
		private final String slot;

		AddressRadio(String subrack, String slot) {
			this.subrack = subrack;
			this.slot = slot;
		}

		@Override
		public SingleDcPortable getComponent(MacroRbs rbs) {
			return rbs.subrack(subrack).radio(slot);
		}
	}

	private static class AddressPdu {
		private final String pdu;
		private final String port;

		AddressPdu(String pdu, String port) {
			this.pdu = pdu;
			this.port = port;
		}

		public DcPort get(MacroRbs rbs) {
			return rbs.pdu(pdu).dcPort(port);
		}
	}

	private static class AddressDus implements DcComponent {
		private final String extension;
		private final String slot;

		AddressDus(String extension, String slot) {
			this.extension = Objects.requireNonNull(extension);
			this.slot = Objects.requireNonNull(slot);
		}

		@Override
		public SingleDcPortable getComponent(MacroRbs rbs) {
			return rbs.extension(this.extension).unit(slot);
		}
	}

	private static class AddressDuw implements DcComponent {
		private final String subrack;

		AddressDuw(String subrack) {
			this.subrack = Objects.requireNonNull(subrack);
		}

		@Override
		public SingleDcPortable getComponent(MacroRbs rbs) {
			return rbs.subrack(subrack).digital();
		}
	}

	private static class AddressR503 implements DcComponent {
		private final String extension;
		private final String slot;

		AddressR503(String extension, String slot) {
			this.extension = extension;
			this.slot = slot;
		}

		@Override
		public SingleDcPortable getComponent(MacroRbs rbs) {
			return rbs.extension(this.extension).r503(slot);
		}
	}

	private static class AddressScu implements DcTarget {
		private final String port;

		AddressScu(String port) {
			this.port = port;
		}

		@Override
		public DcPort get(MacroRbs rbs) {
			return rbs.scu().dcPort(port);
		}
	}

	private static String attr(Xml xml, String attribute) {
		return xml.attribute(attribute);
	}

	private static String subrack(Xml xml) {
		return attr(xml, "subrack");
	}
	private static String slot(Xml xml) {
		return attr(xml, "slot");
	}
	private static String pdu(Xml xml) {
		return attr(xml, "pdu");
	}

	private static String port(Xml xml) {
		return attr(xml, "port");
	}

	private static String extension(Xml xml) {
		return attr(xml, "extension");
	}

	private static String dc(Xml xml) {
		return attr(xml, "dc");
	}

	private static String cable(Xml xml) {
		return attr(xml, "cable");
	}

	public static DcRules of(InputStream input) {
		Xml xml = Xml.read(input);
		if (xml == null)
			throw new IllegalArgumentException();

		Xml dcRules = xml.child("dc_rules");
		List<RbsRules> rules = new ArrayList<>();

		for (Xml macro : dcRules.children("macro")) {
			for (Xml rbs : macro.children("rbs")) {
				String type = rbs.attribute("type");
				String subtype = rbs.attribute("subtype");

				List<DcRuleDesc> descs = new ArrayList<>();

				for (Xml radio : rbs.children("radio")) {
					AddressRadio target = new AddressRadio(subrack(radio), slot(radio));
					AddressPdu pdu = new AddressPdu(pdu(radio), port(radio));
					String cable = cable(radio);
					descs.add(new DcRuleDesc(pdu, target, cable));
				}

				for (Xml duw : rbs.children("duw")) {
					AddressDuw target = new AddressDuw(subrack(duw));
					AddressPdu pdu = new AddressPdu(pdu(duw), port(duw));
					String cable = cable(duw);
					descs.add(new DcRuleDesc(pdu, target, cable));
				}

				for (Xml dus : rbs.children("dus")) {
					AddressDus target = new AddressDus(extension(dus), slot(dus));
					AddressPdu pdu = new AddressPdu(pdu(dus), port(dus));
					String cable = cable(dus);
					descs.add(new DcRuleDesc(pdu, target, cable));
				}

				for (Xml r503 : rbs.children("r503")) {
					AddressR503 target = new AddressR503(extension(r503), slot(r503));
					AddressPdu pdu = new AddressPdu(pdu(r503), port(r503));
					String cable = cable(r503);
					descs.add(new DcRuleDesc(pdu, target, cable));
				}

				for (Xml scu : rbs.children("scu")) {
					AddressScu target = new AddressScu(dc(scu));
					AddressPdu pdu = new AddressPdu(pdu(scu), port(scu));
					String cable = cable(scu);
					descs.add(new DcRuleDesc(pdu, target, cable));
				}
				rules.add(new RbsRules(type, subtype, descs));
			}
		}

		List<MiniRbsRules> miniRules = new ArrayList<>();

		for (Xml mini : dcRules.children("mini")) {
			for (Xml rbs : mini.children("rbs")) {
				String type = rbs.attribute("type");
				String subtype = rbs.attribute("subtype");

				List<MiniRuleDesc> descs = new ArrayList<>();

				for (Xml duw : rbs.children("duw")) {
					String port = port(duw);
					String cable = cable(duw);
					descs.add(new MiniDuwDesc(port, cable));
				}

				for (Xml dus : rbs.children("dus")) {
					String port = port(dus);
					String slot = slot(dus);
					String cable = cable(dus);
					descs.add(new MiniDusDesc(slot, port, cable));
				}

				for (Xml r503 : rbs.children("r503")) {
					String port = port(r503);
					String slot = slot(r503);
					String cable = cable(r503);
					descs.add(new MiniR503Desc(slot, port, cable));
				}

				miniRules.add(new MiniRbsRules(type, subtype, descs));
			}
		}

		return new DcRules(rules, miniRules);
	}
}
