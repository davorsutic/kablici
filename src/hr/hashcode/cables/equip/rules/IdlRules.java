package hr.hashcode.cables.equip.rules;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.IdlPort;
import hr.hashcode.cables.util.Xml;

public class IdlRules {

	private final boolean connect;

	private IdlRules(boolean connect) {
		this.connect = connect;
	}

	public List<Connector<IdlPort>> connect(DigitalUnitWcdma primary, DigitalUnitWcdma secondary) {
		if (connect)
			return Collections.singletonList(new Connector<>(primary.idlPort(), secondary.idlPort()));
		else
			return Collections.emptyList();
	}

	public static IdlRules of(InputStream input) {
		Xml xml = Xml.read(input);

		String value = xml.child("idl_rules").child("connect").attribute("value");

		return new IdlRules(Boolean.parseBoolean(value));
	}

}
