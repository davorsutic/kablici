package hr.hashcode.cables.equip.rules;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import hr.hashcode.cables.equip.comp.DigitalUnitBaseband;
import hr.hashcode.cables.equip.comp.DigitalUnitStandard;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.util.Xml;

public class PlacementRules {

	private static class RbsRules {

		private final String type;
		private final String subtype;

		private final DuwLocation primary;
		private final DuwLocation secondary;

		private final DusLocation dus;

		private final R503Location primaryR503;
		private final R503Location secondaryR503;
		public final BbLocation bb;

		private RbsRules(String type, String subtype, DuwLocation primary, DuwLocation secondary, DusLocation dus, BbLocation bb,
		                 R503Location primaryR503, R503Location secondaryR503) {
			this.type = type;
			this.subtype = subtype;
			this.primary = primary;
			this.secondary = secondary;
			this.dus = dus;
			this.bb = bb;
			this.primaryR503 = primaryR503;
			this.secondaryR503 = secondaryR503;
		}
	}

	private static class DuwLocation {
		private final String subrack;

		DuwLocation(String subrack) {
			this.subrack = subrack;
		}

		void set(DigitalUnitWcdma unit, MacroRbs rbs) {
			rbs.subrack(subrack).setDigital(unit);
		}
	}

	private static class DusLocation {
		private final String subrack;
		private final String slot;

		DusLocation(String subrack, String slot) {
			this.subrack = subrack;
			this.slot = slot;
		}

		void set(DigitalUnitStandard unit, MacroRbs rbs) {
			rbs.extension(subrack).setUnit(slot, unit);
		}
	}

	private static class BbLocation {
		private final String subrack;
		private final String slot;

		BbLocation(String subrack, String slot) {
			this.subrack = subrack;
			this.slot = slot;
		}

		void set(DigitalUnitBaseband unit, MacroRbs rbs) {
			rbs.extension(subrack).setUnit(slot, unit);
		}
	}

	private static class R503Location {
		private final String subrack;
		private final String slot;

		R503Location(String subrack, String slot) {
			this.subrack = subrack;
			this.slot = slot;
		}

		void set(R503 unit, MacroRbs rbs) {
			rbs.extension(subrack).setR503(slot, unit);
		}
	}

	private final List<RbsRules> rbsRules;

	private PlacementRules(List<RbsRules> rbsRules) {
		this.rbsRules = rbsRules;
	}

	private RbsRules findRules(MacroRbs rbs) {
		for (RbsRules rules : rbsRules)
			if (rules.type.equals(rbs.type()) && (rules.subtype == null || rules.subtype.equals(rbs.subtype())))
				return rules;
		return null;
	}

	public void placePrimaryDuw(DigitalUnitWcdma duw, MacroRbs rbs) {
		RbsRules rules = findRules(rbs);
		if (rules != null)
			rules.primary.set(duw, rbs);
	}

	public void placeSecondaryDuw(DigitalUnitWcdma duw, MacroRbs rbs) {
		RbsRules rules = findRules(rbs);
		if (rules != null)
			rules.secondary.set(duw, rbs);
	}

	public void placeDus(DigitalUnitStandard dus, MacroRbs rbs) {
		RbsRules rules = findRules(rbs);
		if (rules != null)
			rules.dus.set(dus, rbs);
	}

	public void placeBb(DigitalUnitBaseband bb, MacroRbs rbs) {
		RbsRules rules = findRules(rbs);
		if (rules != null)
			rules.bb.set(bb, rbs);
	}

	public void placeR503(R503 r503, MacroRbs rbs) {
		RbsRules rules = findRules(rbs);
		if (rules != null)
			rules.primaryR503.set(r503, rbs);
	}

	public void placeSecondaryR503(R503 r503, MacroRbs rbs) {
		RbsRules rules = findRules(rbs);
		if (rules != null)
			rules.secondaryR503.set(r503, rbs);
	}

	public static PlacementRules of(InputStream input) {
		Xml xml = Xml.read(input);

		Xml placement = xml.child("placement_rules");
		Xml macro = placement.child("macro");
		List<RbsRules> allRules = new ArrayList<>();
		for (Xml rbs : macro.children("rbs")) {
			String type = rbs.attribute("type");
			String subtype = rbs.attribute("subtype");
			Xml duwNode = rbs.child("duw");
			DuwLocation primary = duwFrom(duwNode.child("primary"));
			DuwLocation secondary = duwFrom(duwNode.child("secondary"));

			Xml dusNode = rbs.child("dus");
			DusLocation dusLocation = new DusLocation(dusNode.attribute("subrack"), dusNode.attribute("slot"));

			Xml bbNode = rbs.child("bb");
			BbLocation bbLocation = new BbLocation(bbNode.attribute("subrack"), bbNode.attribute("slot"));

			Xml r503Node = rbs.child("r503");
			Xml r503SecondaryNode = rbs.child("r503_secondary");

			R503Location r503Location = new R503Location(r503Node.attribute("subrack"), r503Node.attribute("slot"));
			R503Location r503SecondaryLocation = r503SecondaryNode == null
					? null
					: new R503Location(r503SecondaryNode.attribute("subrack"), r503SecondaryNode.attribute("slot"));
			RbsRules rules =
					new RbsRules(type, subtype, primary, secondary, dusLocation, bbLocation, r503Location, r503SecondaryLocation);
			allRules.add(rules);
		}

		return new PlacementRules(allRules);
	}

	private static DuwLocation duwFrom(Xml xml) {
		if (xml == null)
			return null;
		return new DuwLocation(xml.attribute("subrack"));
	}

}
