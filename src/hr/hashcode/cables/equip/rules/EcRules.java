package hr.hashcode.cables.equip.rules;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.comp.Cable;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DigitalUnitStandard;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.EcPort;
import hr.hashcode.cables.equip.comp.EcPort.SingleEcPortable;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.MiniRbs;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.util.Xml;

public class EcRules {

	public List<Cable<EcPort>> connect(MacroRbs rbs) {
		for (RbsRules rule : rules)
			if (rule.fits(rbs))
				return rule.connectors(rbs);
		return Collections.emptyList();
	}

	public List<Cable<EcPort>> connect(MiniRbs rbs) {
		for (MiniRbsRules rule : miniRules)
			if (rule.fits(rbs))
				return rule.connectors(rbs);
		return Collections.emptyList();
	}

	private final List<RbsRules> rules;
	private final List<MiniRbsRules> miniRules;

	private EcRules(List<RbsRules> rules, List<MiniRbsRules> miniRules) {
		this.rules = rules;
		this.miniRules = miniRules;
	}

	private static class RbsRules {
		private final String type;
		private final String subtype;
		private final List<EcRuleDesc> rules;

		RbsRules(String type, String subtype, List<EcRuleDesc> rules) {
			this.type = type;
			this.subtype = subtype;
			this.rules = rules;
		}

		boolean fits(MacroRbs rbs) {
			return rbs.type().equals(type) && Objects.equals(rbs.subtype(), subtype);
		}

		List<Cable<EcPort>> connectors(MacroRbs rbs) {
			return rules.stream()
					.map(x -> x.connect(rbs))
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
		}
	}

	private static class MiniRbsRules {
		private final String type;
		private final String subtype;
		private final List<MiniRuleDesc> rules;

		private MiniRbsRules(String type, String subtype, List<MiniRuleDesc> rules) {
			this.type = type;
			this.subtype = subtype;
			this.rules = rules;
		}

		boolean fits(MiniRbs rbs) {
			return rbs.type().equals(type) && (subtype == null || Objects.equals(rbs.subtype(), subtype));
		}

		List<Cable<EcPort>> connectors(MiniRbs rbs) {
			return rules.stream()
					.map(x -> x.connector(rbs))
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
		}

	}

	private interface MiniRuleDesc {
		Cable<EcPort> connector(MiniRbs rbs);
	}

	private static class MiniR503Desc implements MiniRuleDesc {
		private final String slot;
		private final String port;
		private final String cable;

		MiniR503Desc(String slot, String port, String cable) {
			this.slot = slot;
			this.port = port;
			this.cable = cable;
		}

		@Override
		public Cable<EcPort> connector(MiniRbs rbs) {
			R503 r503 = rbs.r503(slot);
			if (r503 != null) {
				EcPort first = r503.ecPort();
				EcPort second = rbs.ecPorts().port(port);

				Connector<EcPort> connector = new Connector<>(first, second);
				return new Cable<>(connector, cable);
			}
			return null;
		}
	}

	private static class MiniDuwDesc implements MiniRuleDesc {
		private final String port;
		private final String cable;

		private MiniDuwDesc(String port, String cable) {
			this.port = port;
			this.cable = cable;
		}

		@Override
		public Cable<EcPort> connector(MiniRbs rbs) {
			DigitalUnitWcdma duw = rbs.duw();
			if (duw != null) {
				EcPort first = duw.ecPort();
				EcPort second = rbs.ecPorts().port(port);
				Connector<EcPort> connector = new Connector<>(first, second);
				return new Cable<>(connector, cable);
			}
			return null;
		}

	}

	private static class MiniDusDesc implements MiniRuleDesc {
		private final String slot;
		private final String port;
		private final String cable;

		MiniDusDesc(String slot, String port, String cable) {
			this.slot = slot;
			this.port = port;
			this.cable = cable;
		}

		@Override
		public Cable<EcPort> connector(MiniRbs rbs) {
			DigitalUnitStandard dus = rbs.dus(slot);
			if (dus != null) {
				EcPort first = dus.ecPort();
				EcPort second = rbs.ecPorts().port(port);
				Connector<EcPort> connector = new Connector<>(first, second);
				return new Cable<>(connector, cable);
			}
			return null;
		}
	}

	private static class EcRuleDesc {

		private final EcPortDesc first;
		private final EcPortDesc second;
		private final String cable;

		EcRuleDesc(EcPortDesc first, EcPortDesc second, String cable) {
			this.first = first;
			this.second = second;
			this.cable = cable;
		}

		Cable<EcPort> connect(MacroRbs rbs) {
			EcPort port1 = first.target(rbs);
			EcPort port2 = second.target(rbs);
			if (port1 == null)
				return null;
			Connector<EcPort> connector = new Connector<>(port1, port2);
			return new Cable<>(connector, cable);
		}

	}

	private interface EcPortDesc {
		EcPort target(MacroRbs rbs);
	}

	private interface EcComponentDesc extends EcPortDesc {
		SingleEcPortable targetComponent(MacroRbs rbs);

		@Override
		default EcPort target(MacroRbs rbs) {
			SingleEcPortable component = targetComponent(rbs);
			if (component == null)
				return null;
			return component.ecPort();
		}
	}

	private static class TargetShu implements EcPortDesc {
		private final String port;

		TargetShu(String port) {
			this.port = port;
		}

		@Override
		public EcPort target(MacroRbs rbs) {
			return rbs.shu().ecPort(port);
		}

	}

	private static class TargetScu implements EcPortDesc {
		private final String port;

		TargetScu(String port) {
			this.port = port;
		}

		@Override
		public EcPort target(MacroRbs rbs) {
			return rbs.scu().ecPort(port);
		}
	}

	private static class TargetDus implements EcComponentDesc {
		private final String extension;
		private final String slot;

		TargetDus(String extension, String slot) {
			this.extension = extension;
			this.slot = slot;
		}

		@Override
		public SingleEcPortable targetComponent(MacroRbs rbs) {
			return rbs.extension(extension).unit(slot);
		}
	}

	private static class TargetPdu implements EcComponentDesc {
		private final String name;

		TargetPdu(String name) {
			this.name = name;
		}

		@Override
		public SingleEcPortable targetComponent(MacroRbs rbs) {
			return rbs.pdu(name);
		}
	}

	private static class TargetDuw implements EcComponentDesc {
		private final String subrack;

		TargetDuw(String subrack) {
			this.subrack = subrack;
		}

		@Override
		public SingleEcPortable targetComponent(MacroRbs rbs) {
			return rbs.subrack(subrack).digital();
		}
	}

	private static class AddressR503 {
		private final String extension;
		private final String slot;

		AddressR503(String extension, String slot) {
			this.extension = extension;
			this.slot = slot;
		}

		public SingleEcPortable targetComponent(MacroRbs rbs) {
			return rbs.extension(extension).r503(slot);
		}
	}

	private static String attr(Xml xml, String attribute) {
		return xml.attribute(attribute);
	}

	private static String subrack(Xml xml) {
		return attr(xml, "subrack");
	}

	private static String slot(Xml xml) {
		return attr(xml, "slot");
	}

	private static String name(Xml xml) {
		return attr(xml, "name");
	}

	private static String port(Xml xml) {
		return attr(xml, "port");
	}

	private static String extension(Xml xml) {
		return attr(xml, "extension");
	}

	private static String ec(Xml xml) {
		return attr(xml, "ec");
	}

	private static String cable(Xml xml) {
		return attr(xml, "cable");
	}

	private static EcPortDesc target(Xml xml) {
		String target = attr(xml, "target");
		if ("SHU".equals(target))
			return new TargetShu(port(xml));
		else if ("SCU".equals(target))
			return new TargetScu(port(xml));
		else
			throw new IllegalArgumentException(xml.name() + "," + target);
	}

	public static EcRules of(InputStream input) {
		Xml xml = Xml.read(input);
		if (xml == null)
			throw new IllegalArgumentException();

		Xml dcRules = xml.child("ec_rules");
		List<RbsRules> rules = new ArrayList<>();

		for (Xml macro : dcRules.children("macro")) {
			for (Xml rbs : macro.children("rbs")) {
				String type = rbs.attribute("type");
				String subtype = rbs.attribute("subtype");

				List<EcRuleDesc> descs = new ArrayList<>();

				for (Xml scu : rbs.children("scu")) {
					TargetScu sscu = new TargetScu(ec(scu));
					String cable = cable(scu);
					descs.add(new EcRuleDesc(sscu, target(scu), cable));
				}

				for (Xml pdu : rbs.children("pdu")) {

					TargetPdu first = new TargetPdu(name(pdu));
					String cable = cable(pdu);
					descs.add(new EcRuleDesc(first, target(pdu), cable));
				}

				for (Xml duw : rbs.children("duw")) {
					TargetDuw target = new TargetDuw(subrack(duw));
					String cable = cable(duw);
					descs.add(new EcRuleDesc(target, target(duw), cable));
				}

				for (Xml dus : rbs.children("dus")) {
					TargetDus target = new TargetDus(extension(dus), slot(dus));
					String cable = cable(dus);
					descs.add(new EcRuleDesc(target, target(dus), cable));
				}

				rules.add(new RbsRules(type, subtype, descs));
			}
		}

		List<MiniRbsRules> miniRules = new ArrayList<>();

		for (Xml mini : dcRules.children("mini")) {
			for (Xml rbs : mini.children("rbs")) {
				String type = rbs.attribute("type");
				String subtype = rbs.attribute("subtype");

				List<MiniRuleDesc> descs = new ArrayList<>();

				for (Xml duw : rbs.children("duw")) {
					String port = port(duw);
					String cable = cable(duw);
					descs.add(new MiniDuwDesc(port, cable));
				}

				for (Xml dus : rbs.children("dus")) {
					String port = port(dus);
					String slot = slot(dus);
					String cable = cable(dus);
					descs.add(new MiniDusDesc(slot, port, cable));
				}

				for (Xml r503 : rbs.children("r503")) {
					String port = port(r503);
					String slot = slot(r503);
					String cable = cable(r503);
					descs.add(new MiniR503Desc(slot, port, cable));
				}

				miniRules.add(new MiniRbsRules(type, subtype, descs));
			}
		}

		return new EcRules(rules, miniRules);
	}

}
