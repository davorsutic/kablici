package hr.hashcode.cables.equip.rules;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import hr.hashcode.cables.equip.comp.Antenna;
import hr.hashcode.cables.equip.comp.Cable;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.RfPort;
import hr.hashcode.cables.util.Xml;

public class RfRules {

	private static class Rule {
		private final String radioPort;
		private final String antennaPort;
		private final String cable;

		Rule(String radioPort, String antennaPort, String cable) {
			this.radioPort = radioPort;
			this.antennaPort = antennaPort;
			this.cable = cable;
		}

		Cable<RfPort> connect(RadioUnitBase radio, Antenna antenna) {
			RfPort first = radio.rfPort(radioPort);
			RfPort second = antenna.rfPort(antennaPort);
			Connector<RfPort> connector = new Connector<>(first, second);
			return new Cable<>(connector, cable);
		}
	}

	private static class SectorRule {
		private final Rule first;
		private final Rule second;

		SectorRule(Rule first, Rule second) {
			this.first = first;
			this.second = second;
		}

		List<Cable<RfPort>> connect(RadioUnitBase radio1, RadioUnitBase radio2, Antenna antenna) {
			List<Cable<RfPort>> connectors = new ArrayList<>();
			connectors.add(first.connect(radio1, antenna));
			connectors.add(second.connect(radio2, antenna));
			return connectors;
		}
	}

	private final SectorRule cascade;
	private final SectorRule noCascade;

	private RfRules(SectorRule cascade, SectorRule noCascade) {
		this.cascade = cascade;
		this.noCascade = noCascade;
	}

	public List<Cable<RfPort>> cascade(RadioUnitBase first, RadioUnitBase second, Antenna antenna) {
		return cascade.connect(first, second, antenna);
	}

	public List<Cable<RfPort>> noCascade(RadioUnitBase radio, Antenna antenna) {
		return noCascade.connect(radio, radio, antenna);
	}

	public static RfRules of(InputStream input) {
		Xml xml = Xml.read(input);

		Xml rfRules = xml.child("rf_rules");
		Xml cascade = rfRules.child("cascade");
		Xml noCascade = rfRules.child("no_cascade");

		Xml connectFirst = cascade.child("connect_first");
		Xml connectSecond = cascade.child("connect_second");

		SectorRule cascadeRule = from(connectFirst, connectSecond);

		List<Xml> connect = noCascade.children("connect");
		SectorRule noCascadeRule = from(connect.get(0), connect.get(1));
		return new RfRules(cascadeRule, noCascadeRule);
	}

	private static SectorRule from(Xml first, Xml second) {
		return new SectorRule(from(first), from(second));
	}

	private static Rule from(Xml xml) {
		String port = xml.attribute("port");
		String antenna = xml.attribute("antenna");
		String cable = xml.attribute("cable");

		return new Rule(port, antenna, cable);
	}

}
