package hr.hashcode.cables.equip.rules;

import java.io.InputStream;

import hr.hashcode.cables.util.Xml;

public class OtherRules {

	private final String crossConnect;
	private final String cascade;

	private OtherRules(String crossConnect, String cascade) {
		this.crossConnect = crossConnect;
		this.cascade = cascade;
	}

	public String cascade() {
		return cascade;
	}

	public String crossConnect() {
		return crossConnect;
	}

	public static OtherRules of(InputStream input) {
		Xml xml = Xml.read(input);

		Xml other = xml.child("other");
		String crossConnect = other.child("x-connect").attribute("cable");
		String cascade = other.child("cascade").attribute("cable");

		return new OtherRules(crossConnect, cascade);
	}
}
