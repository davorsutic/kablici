package hr.hashcode.cables.equip.rules;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.hashcode.cables.equip.build.DusPortAssigner;
import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.RangeParser;
import hr.hashcode.cables.equip.comp.RiPort;
import hr.hashcode.cables.util.Xml;

public class RiRules {

	private static class Rule {
		private final String port;

		Rule(String port) {
			this.port = port;
		}

		Connector<RiPort> connect(RadioUnitBase radio, RiPort other) {
			RiPort port = radio.riPort(this.port);
			if (port == null)
				throw new IllegalArgumentException(this.port);
			if (other == null)
				throw new IllegalArgumentException(this.port);
			return new Connector<>(port, other);
		}
	}

	private static class CrossRule {
		private final String port1;
		private final String port2;

		CrossRule(String port1, String port2) {
			this.port1 = port1;
			this.port2 = port2;
		}

		Connector<RiPort> connect(RadioUnitBase first, RadioUnitBase second) {
			return new Connector<>(first.riPort(port1), second.riPort(port2));
		}
	}

	private static class LteRuleSet {
		private final Rule remote;
		private final Rule noCascade;

		private final Rule cascadeFirst;
		private final Rule cascadeSecond;
		private final CrossRule cross;

		LteRuleSet(Rule remote, Rule noCascade, Rule cascadeFirst, Rule cascadeSecond, CrossRule cross) {
			this.remote = remote;
			this.noCascade = noCascade;
			this.cascadeFirst = cascadeFirst;
			this.cascadeSecond = cascadeSecond;
			this.cross = cross;
		}

		List<Connector<RiPort>> remote(RadioUnitBase radio, RiPort other) {
			return Collections.singletonList(remote.connect(radio, other));
		}

		List<Connector<RiPort>> cascade(RadioUnitBase first, RadioUnitBase second, RiPort port) {
			List<Connector<RiPort>> result = new ArrayList<>();
			if (cascadeFirst != null)
				result.add(cascadeFirst.connect(first, port));
			if (cascadeSecond != null)
				result.add(cascadeSecond.connect(second, port));
			result.add(cross.connect(first, second));
			return result;
		}

		List<Connector<RiPort>> noCascade(RadioUnitBase radio, RiPort other) {
			return Collections.singletonList(noCascade.connect(radio, other));
		}
	}

	private static class MixedRule {
		private final Rule wcdma;
		private final Rule lte;

		private MixedRule(Rule wcdma, Rule lte) {
			this.wcdma = wcdma;
			this.lte = lte;
		}

		List<Connector<RiPort>> connect(RadioUnitBase wcdma, RadioUnitBase lte, RiPort wcdmaPort, RiPort ltePort) {
			List<Connector<RiPort>> result = new ArrayList<>();
			if (this.wcdma != null)
				result.add(this.wcdma.connect(wcdma, wcdmaPort));
			if (this.lte != null)
				result.add(this.lte.connect(lte, ltePort));
			return result;
		}
	}

	private static class MixedRuleSet {
		private final MixedRule remote;
		private final MixedRule noCascade;
		private final MixedRule cascade;
		private final CrossRule cascadeCross;

		MixedRuleSet(MixedRule remote, MixedRule noCascade, MixedRule cascade, CrossRule cascadeCross) {
			this.remote = remote;
			this.noCascade = noCascade;
			this.cascade = cascade;
			this.cascadeCross = cascadeCross;
		}

		List<Connector<RiPort>> remote(RadioUnitBase radio, RiPort wcdmaPort, RiPort ltePort) {
			return remote.connect(radio, radio, wcdmaPort, ltePort);
		}

		List<Connector<RiPort>> noCascade(RadioUnitBase radio, RiPort wcdmaPort, RiPort ltePort) {
			return noCascade.connect(radio, radio, wcdmaPort, ltePort);
		}

		List<Connector<RiPort>> cascade(RadioUnitBase wcdmaRadio, RadioUnitBase lteRadio, RiPort wcdmaPort,
				RiPort ltePort) {
			List<Connector<RiPort>> result = new ArrayList<>();
			result.addAll(cascade.connect(wcdmaRadio, lteRadio, wcdmaPort, ltePort));
			result.add(cascadeCross.connect(wcdmaRadio, lteRadio));
			return result;
		}

	}

	private static class InterDuw {
		private final String primary;
		private final String secondary;

		InterDuw(String primary, String secondary) {
			this.primary = primary;
			this.secondary = secondary;
		}

		Connector<RiPort> connect(DigitalUnitWcdma primary, DigitalUnitWcdma secondary) {
			return new Connector<>(primary.riPort(this.primary), secondary.riPort(this.secondary));
		}

	}

	private final LteRuleSet lte;
	private final MixedRuleSet both;
	private final InterDuw interDuw;
	private final Map<Band, String> byBand;
	private final List<String> directWithR503;
	private final List<String> directWithoutR503;
	private final List<String> duw;

	private RiRules(LteRuleSet lte, MixedRuleSet both, InterDuw interDuw, Map<Band, String> byBand,
			List<String> directWithR503, List<String> directWithoutR503, List<String> duw) {
		this.lte = lte;
		this.both = both;
		this.interDuw = interDuw;
		this.byBand = byBand;
		this.directWithR503 = directWithR503;
		this.directWithoutR503 = directWithoutR503;
		this.duw = duw;
	}

	public List<Connector<RiPort>> remote(RadioUnitBase radio, RiPort wcdmaPort, RiPort ltePort) {
		if (wcdmaPort == null)
			return lte.remote(radio, ltePort);
		else
			return both.remote(radio, wcdmaPort, ltePort);
	}

	public List<Connector<RiPort>> noCascade(RadioUnitBase radio, RiPort wcdmaPort, RiPort ltePort) {
		if (wcdmaPort == null)
			return lte.noCascade(radio, ltePort);
		else
			return both.noCascade(radio, wcdmaPort, ltePort);
	}

	public List<Connector<RiPort>> cascadeLte(RadioUnitBase first, RadioUnitBase second, RiPort port) {
		return lte.cascade(first, second, port);
	}

	public List<Connector<RiPort>> cascadeMixed(RadioUnitBase wcdmaRadio, RadioUnitBase lteRadio, RiPort wcdmaPort,
			RiPort ltePort) {
		return both.cascade(wcdmaRadio, lteRadio, wcdmaPort, ltePort);
	}

	public List<Connector<RiPort>> interDuw(DigitalUnitWcdma primary, DigitalUnitWcdma secondary) {
		if (interDuw != null)
			return Collections.singletonList(interDuw.connect(primary, secondary));
		return Collections.emptyList();
	}

	public DusPortAssigner withoutR503() {
		return new DusPortAssigner(Collections.emptyMap(), directWithoutR503);
	}

	public DusPortAssigner withR503() {
		return new DusPortAssigner(byBand, directWithR503);
	}

	public DusPortAssigner duw() {
		return new DusPortAssigner(Collections.emptyMap(), duw);
	}

	public static RiRules of(InputStream input) {

		Xml xml = Xml.read(input);

		Xml riRules = xml.child("ri_rules");
		Xml radio = riRules.child("radio");
		Xml lte = radio.child("lte");
		Xml both = radio.child("lte_wcdma");

		Xml lteRemote = lte.child("remote").child("lte");
		Xml lteNoCascade = lte.child("no_cascade").child("lte");

		Xml lteCascadeFirst = lte.child("cascade").child("first");
		Xml lteCascadeSecond = lte.child("cascade").child("second");
		Xml lteCascadeCross = lte.child("cascade").child("cross");

		Rule lteRemoteRule = new Rule(lteRemote.attribute("port"));
		Rule lteNoCascadeRule = new Rule(lteNoCascade.attribute("port"));
		Rule lteCascadeFirstRule = lteCascadeFirst == null ? null : new Rule(lteCascadeFirst.attribute("port"));
		Rule lteCascadeSecondRule = lteCascadeSecond == null ? null : new Rule(lteCascadeSecond.attribute("port"));
		CrossRule lteCross = new CrossRule(lteCascadeCross.attribute("first"), lteCascadeCross.attribute("second"));

		LteRuleSet lteSet =
				new LteRuleSet(lteRemoteRule, lteNoCascadeRule, lteCascadeFirstRule, lteCascadeSecondRule, lteCross);

		Xml bothRemoteWcdma = both.child("remote").child("wcdma");
		Xml bothRemoteLte = both.child("remote").child("lte");

		Xml bothNoCascadeWcdma = both.child("no_cascade").child("wcdma");
		Xml bothNoCascadeLte = both.child("no_cascade").child("lte");

		Xml bothCascadeWcdma = both.child("cascade").child("wcdma");
		Xml bothCascadeLte = both.child("cascade").child("lte");
		Xml bothCascadeCross = both.child("cascade").child("cross");

		Rule bothRemoteWcdmaRule = new Rule(bothRemoteWcdma.attribute("port"));
		Rule bothRemoteLteRule = new Rule(bothRemoteLte.attribute("port"));
		MixedRule mixedRemoteRule = new MixedRule(bothRemoteWcdmaRule, bothRemoteLteRule);

		Rule bothNoCascadeWcdmaRule = new Rule(bothNoCascadeWcdma.attribute("port"));
		Rule bothNoCascadeLteRule = new Rule(bothNoCascadeLte.attribute("port"));
		MixedRule mixedNoCascadeRule = new MixedRule(bothNoCascadeWcdmaRule, bothNoCascadeLteRule);

		Rule bothCascadeWcdmaRule = new Rule(bothCascadeWcdma.attribute("port"));
		Rule bothCascadeLteRule = new Rule(bothCascadeLte.attribute("port"));
		MixedRule mixedCascadeRule = new MixedRule(bothCascadeWcdmaRule, bothCascadeLteRule);

		CrossRule mixedCascadeCrossRule =
				new CrossRule(bothCascadeCross.attribute("wcdma"), bothCascadeCross.attribute("lte"));

		MixedRuleSet mixedSet =
				new MixedRuleSet(mixedRemoteRule, mixedNoCascadeRule, mixedCascadeRule, mixedCascadeCrossRule);

		Xml interDuwConnect = riRules.child("inter_duw").child("connect");
		InterDuw interDuw = interDuwConnect == null
				? null
				: new InterDuw(interDuwConnect.attribute("primary"), interDuwConnect.attribute("secondary"));

		String duw = riRules.child("duw").child("direct").attribute("values");
		List<String> duwDirect = RangeParser.parse(duw);

		Xml dus = riRules.child("dus");

		Xml to503 = dus.child("to_r503");
		Map<Band, String> bandPort = new HashMap<>();

		for (Xml entry : to503.child("strategy").children("band")) {
			String name = entry.attribute("name");
			String port = entry.attribute("port");

			Band band = Band.valueOf(name);
			bandPort.put(band, port);
		}

		String directDusPorts = to503.child("strategy").child("direct").attribute("ports");
		List<String> directDus = RangeParser.parse(directDusPorts);

		String directDusNo503 = dus.child("no_r503").child("direct").attribute("ports");
		List<String> directDusNo503Ports = RangeParser.parse(directDusNo503);

		return new RiRules(lteSet, mixedSet, interDuw, bandPort, directDus, directDusNo503Ports, duwDirect);
	}

}
