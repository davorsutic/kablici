package hr.hashcode.cables.equip.bind;

import static hr.hashcode.cables.equip.bind.CabinetSystemVisual.getPointDistance;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

// todo delete class
public class CableVisual extends Shape {
	public static int LINE_OFFSET = 6;
	public static int HARDWARE_OFFSET = 8;
	public static Color DEFAULT_COLOR = Color.BLACK;

	Line startLine;
	double endX;
	double endY;
	List<Line> polyShape = new ArrayList<>();
	Parent cabinet;
	Color color;

	public enum Direction {
		LEFT,
		RIGHT,
		UP,
		DOWN,
		STAY
	}

	@Override
	public com.sun.javafx.geom.Shape impl_configShape() {
		return null;
	}

	public CableVisual(Parent cabinet, Node n1, Node n2) {
		this(cabinet, n1, n2, DEFAULT_COLOR);
	}

	public CableVisual(Parent cabinet, Node n1, Node n2, Color color) {
		this.cabinet = cabinet;
		this.color = color;
		connect(n1, n2);
	}

	private void connect(Node n1, Node n2) {
		Bounds b1 = cabinet.sceneToLocal(n1.localToScene(n1.getBoundsInLocal()));
		double startX = x(b1);
		double startY = y(b1);


		Bounds b2 = cabinet.sceneToLocal(n2.localToScene(n2.getBoundsInLocal()));
		endX = x(b2);
		endY = y(b2);

		// these lines use for getting out of ports
		//startLine = getLineOutSideNodeShortest(startX, startY, endX, endY, cabinet, getPortHardwareNode(n1));
		//connectStartToEndLine(getPortHardwareNode(n2));
		polyShape.add(0, startLine);
	}

	private double x(Bounds bounds) {
		return (bounds.getMaxX() + bounds.getMinX()) / 2;
	}

	private double y(Bounds bounds) {
		return (bounds.getMaxY() + bounds.getMinY()) / 2;
	}

	public void addLineToEnd(double x, double y) {
		addLine(x, y, endX, endY);
	}

	public void addLine(double startX, double startY, double endX, double endY) {
		if (polyShape.isEmpty()) {
			polyShape.add(getLine(startX, startY, endX, endY));
		} else {
			Line last = polyShape.get(polyShape.size() - 1);
			if (last.getEndX() == startX && last.getEndY() == startY) {
				polyShape.add(getLine(startX, startY, endX, endY));
			}
		}
	}

	public Line getLine(double startX, double startY, double endX, double endY) {
		Line line = new Line();
		line.setStartX(startX);
		line.setStartY(startY);
		line.setEndX(endX);
		line.setEndY(endY);
		line.setStroke(color);
		return line;
	}

	private Line getLineOutSideNodeShortest(double startX, double startY, double endX, double endY, Node cabinet, Node n) {
		Bounds b = cabinet.sceneToLocal(n.localToScene(n.getBoundsInLocal()));
		double dUp = getPointDistance(startX, startY, startX, b.getMinY());
		double dDown = getPointDistance(startX, startY, startX, b.getMaxY());
		double dLeft = getPointDistance(startX, startY, b.getMinX(), startY);
		double dRight = getPointDistance(startX, startY, b.getMaxX(), startY);
		if (Math.max(dUp, dDown) < Math.max(dLeft, dRight)) {
			//go vertical to the direction of endpoint
			double endToY = startY - endY < 0 ? startY + dDown : startY - dUp;
			return getLine(startX, startY, startX, endToY);
		} else {
			//go horizontal to the direction of endpoint
			double endToX = startX - endX < 0 ? startX + dRight : startX - dLeft;
			return getLine(startX, startY, endToX, startY);
		}
	}

	private boolean addLinesAroundHardwareOrConnect(Direction prevDirection, Direction mustGoDirection, double x, double y, Node cabinet,
														   Node hw) {
		double connectionLenght = (x == endX || y == endY) && !polyShape.isEmpty() ? getPointDistance(x, y, endX, endY)  : Double.MAX_VALUE;
		Bounds b = cabinet.sceneToLocal(hw.localToScene(hw.getBoundsInLocal()));
		double dUp = getPointDistance(x, y, x, b.getMinY());
		double dDown = getPointDistance(x, y, x, b.getMaxY());
		double dLeft = getPointDistance(x, y, b.getMinX(), y);
		double dRight = getPointDistance(x, y, b.getMaxX(), y);
		if (mustGoDirection == Direction.DOWN && prevDirection == Direction.DOWN && dDown < connectionLenght) {
			double newX = dLeft < dRight ? x - dLeft : x + dRight;
			addLine(x, y, newX, y);
			addLine(newX, y, newX, y + dDown + HARDWARE_OFFSET);
		} else if (mustGoDirection == Direction.UP && prevDirection != Direction.UP && dUp < connectionLenght) {
			double newX = dLeft < dRight ? x - dLeft : x + dRight;
			addLine(x, y, newX, y);
			addLine(newX, y, newX, y - dUp - HARDWARE_OFFSET);
		} else if (mustGoDirection == Direction.RIGHT && prevDirection == Direction.RIGHT && dRight < connectionLenght) {
			double newY = dUp < dDown ? y - dUp : y + dDown;
			addLine(x, y, x, newY);
			addLine(x, newY, x + dRight + HARDWARE_OFFSET, newY);
		} else if (mustGoDirection == Direction.LEFT && prevDirection == Direction.LEFT && dLeft < connectionLenght) {
			double newY = dUp < dDown ? y - dUp : y + dDown;
			addLine(x, y, x, newY);
			addLine(x, newY, x - dLeft - HARDWARE_OFFSET, newY);
		} else {
			if(polyShape.isEmpty()) {
				if(mustGoDirection == Direction.DOWN && prevDirection != Direction.UP) {
					addLine(x, y, x, y + dDown);
				} else if(mustGoDirection == Direction.UP && prevDirection != Direction.DOWN) {
					addLine(x, y, x, y - dUp);
				} else if(mustGoDirection == Direction.LEFT && prevDirection != Direction.RIGHT) {
					addLine(x, y, x - dLeft, y);
				} else if(mustGoDirection == Direction.RIGHT && prevDirection != Direction.LEFT) {
					addLine(x, y, x + dRight, y);
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		return true;
	}

	public void connectDirect(Direction mustGoDirection, Direction previousDirection, double x, double y) {
		if (mustGoDirection == Direction.DOWN && mustGoDirection == Direction.RIGHT) {
			if (previousDirection == Direction.LEFT) {
				addLine(x, y, x, endY);
				addLineToEnd(x, endY);
			} else {
				addLine(x, y, endX, y);
				addLineToEnd(endX, y);
			}
		} else {
			if (previousDirection == Direction.RIGHT) {
				addLine(x, y, x, endY);
				addLineToEnd(x, endY);
			} else {
				addLine(x, y, endX, y);
				addLineToEnd(endX, y);
			}
		}

	}

	private Node getHardwareCollision(double x, double y) {
		for (Node child : cabinet.getChildrenUnmodifiable()) {
			if (cabinet.sceneToLocal(child.localToScene(child.getBoundsInLocal())).contains(x, y)) {
				return child;
			}
		}
		return null;
	}

	private Direction getMustGoDirectionX(double x) {
		if (x == endX) {
			return Direction.STAY;
		} else if (x < endX) {
			return Direction.RIGHT;
		} else {
			return Direction.LEFT;
		}
	}

	private Direction getMustGoDirectionY(double y) {
		if (y == endY) {
			return Direction.STAY;
		} else if (y < endY) {
			return Direction.DOWN;
		} else {
			return Direction.UP;
		}
	}

	private boolean directionHorizontal(Direction direction) {
		return direction == Direction.LEFT || direction == Direction.RIGHT;
	}

	private boolean directionVertical(Direction direction) {
		return direction == Direction.UP || direction == Direction.DOWN;
	}

	private double getDistanceToClosestHardware(Direction direction, double x, double y) {
		double distance = Double.MAX_VALUE;
		for (Node hw : cabinet.getChildrenUnmodifiable()) {
			Bounds bounds = cabinet.sceneToLocal(hw.localToScene(hw.getBoundsInLocal()));
			if (directionHorizontal(direction)) {
				if (direction == Direction.RIGHT && bounds.getMinX() > x && bounds.getMaxY() > y && bounds.getMinY() < y) {
					distance = Math.min(distance, bounds.getMinX() - x);
				} else if (direction == Direction.LEFT && bounds.getMaxX() < x && bounds.getMaxY() > y && bounds.getMinY() < y) {
					distance = Math.min(distance, x - bounds.getMaxX());
				}
			} else if (directionVertical(direction)) {
				if (direction == Direction.DOWN && bounds.getMinY() > y && bounds.getMaxX() > x && bounds.getMinX() < x) {
					distance = Math.min(distance, bounds.getMinY() - y);
				} else if (direction == Direction.UP && bounds.getMaxY() < y && bounds.getMaxX() > x && bounds.getMinX() < x) {
					distance = Math.min(distance, y - bounds.getMaxY());
				}
			}
		}
		return distance;
	}

	private double getLineLength(Direction direction, double x, double y) {
		double lenght = 0;
		if (directionHorizontal(direction)) {
			lenght = Math.min(getDistanceToClosestHardware(direction, x, y), Math.abs(x - endX));
		} else if (directionVertical(direction)) {
			lenght = Math.min(getDistanceToClosestHardware(direction, x, y), Math.abs(y - endY));
		}
		return lenght;
	}

	private boolean oppositeDirections(Direction d1, Direction d2) {
		return (d1 == Direction.LEFT && d2 == Direction.RIGHT) || (d2 == Direction.LEFT && d1 == Direction.RIGHT) ||
					   (d1 == Direction.DOWN && d2 == Direction.UP) || (d1 == Direction.UP && d2 == Direction.DOWN);
	}

	private Direction getDirection(Line line) {
		if (line.getStartX() == line.getEndX()) {
			return line.getStartY() - line.getEndY() < 0 ? Direction.DOWN : Direction.UP;
		} else if (line.getStartY() == line.getEndY()) {
			return line.getStartX() - line.getEndX() < 0 ? Direction.RIGHT : Direction.LEFT;
		}
		return Direction.STAY;
	}

	private Direction getPreviousDirection() {
		if (polyShape.isEmpty()) {
			return Direction.STAY;
		}
		return getDirection(polyShape.get(polyShape.size() - 1));
	}

	private double getCurrentX() {
		if (polyShape.isEmpty()) {
			return startLine.getEndX();
		}
		return polyShape.get(polyShape.size() - 1).getEndX();
	}

	private double getCurrentY() {
		if (polyShape.isEmpty()) {
			return startLine.getEndY();
		}
		return polyShape.get(polyShape.size() - 1).getEndY();
	}

	private boolean finished(double x, double y) {
		return x == endX && y == endY;
	}

	private void connectStartToEndLine(Node endHardware) {
		Direction prevDirection = getDirection(startLine);

		double currentX = startLine.getEndX();
		double currentY = startLine.getEndY();

		Node currentHardware = null;

		while (!finished(currentX, currentY)) {
			boolean moved = false;
			// get horizontal direction
			Direction horizontal = getMustGoDirectionX(currentX);
			if (horizontal != Direction.STAY && !oppositeDirections(horizontal, prevDirection)) {
				// check if can go to direction and go
				currentHardware = getHardwareCollision(currentX, currentY);
				if (currentHardware == null) {
					double length = getLineLength(horizontal, currentX, currentY);
					if (length > 0) {
						double endX = horizontal == Direction.RIGHT ? currentX + length : currentX - length;
						addLine(currentX, currentY, endX, currentY);
						currentX = endX;
						prevDirection = getPreviousDirection();
						moved = true;
					}
				} else if (currentHardware == endHardware.getParent()) {
					connectDirect(horizontal, prevDirection, currentX, currentY);
					break;
				}
			}
			Direction vertical = getMustGoDirectionY(currentY);
			if (vertical != Direction.STAY && !oppositeDirections(vertical, prevDirection)) {
				// check if can go to direction and go
				currentHardware = getHardwareCollision(currentX, currentY);
				if (currentHardware == null) {
					double length = getLineLength(vertical, currentX, currentY);
					if (length > 0) {
						double endY = vertical == Direction.DOWN ? currentY + length : currentY - length;
						addLine(currentX, currentY, currentX, endY);
						currentY = endY;
						prevDirection = getPreviousDirection();
						moved = true;
					}
				} else if (currentHardware == endHardware.getParent()) {
					connectDirect(vertical, prevDirection, currentX, currentY);
					break;
				}
			}

			if (!moved && currentHardware != null) {
				// go around hardware
				if (addLinesAroundHardwareOrConnect(prevDirection, horizontal, currentX, currentY, cabinet, currentHardware) ||
							addLinesAroundHardwareOrConnect(prevDirection, vertical, currentX, currentY, cabinet, currentHardware)) {
					prevDirection = getPreviousDirection();
					currentX = getCurrentX();
					currentY = getCurrentY();
					moved = true;
				}
			}

			if (!moved) {
				// connect to the end
				connectDirect(horizontal == Direction.STAY ? vertical : horizontal, prevDirection, currentX, currentY);
				break;
			}
		}
	}

	private void swapDirection(Line l) {
		Double temp = l.getStartX();
		l.setStartX(l.getEndX());
		l.setEndX(temp);
		temp = l.getStartY();
		l.setStartY(l.getEndY());
		l.setEndY(temp);
	}

	public void addShapes(Group g, BooleanProperty switach) {
		for (Line l : polyShape) {
			l.strokeWidthProperty().bind(Bindings.when(switach).then(2).otherwise(0));
		}
		g.getChildren().addAll(polyShape);
	}

	public boolean lineHorizontal(Line line) {
		return line.getStartY() == line.getEndY();
	}

	public boolean lineVertical(Line line) {
		return line.getStartX() == line.getEndX();
	}

	public boolean linesOverlapHorizontal(Line line1, Line line2) {
		return lineHorizontal(line1) && lineHorizontal(line2) && line1.getEndY() == line2.getEndY() &&
					   !(Math.max(line1.getStartX(), line1.getEndX()) <= Math.min(line2.getStartX(), line2.getEndX()) ||
								 Math.max(line2.getStartX(), line2.getEndX()) <= Math.min(line1.getStartX(), line1.getEndX()));
	}

	public boolean linesOverlapVertical(Line line1, Line line2) {
		return lineVertical(line1) && lineVertical(line2) && line1.getEndX() == line2.getEndX() &&
					   !(Math.max(line1.getStartY(), line1.getEndY()) <= Math.min(line2.getStartY(), line2.getEndY()) ||
								 Math.max(line2.getStartY(), line2.getEndY()) <= Math.min(line1.getStartY(), line1.getEndY()));
	}

	private void adjustLines(int index, double value, Function<Line, Boolean> lineDirection, BiConsumer<Line, Double> setStart,
									BiConsumer<Line, Double> setEnd) {
		int i = index;
		Line line = polyShape.get(i);
		while (i > 0 && lineDirection.apply(line)) {
			setStart.accept(line, value);
			if(i < polyShape.size() - 1) {
				setEnd.accept(line, value);
			}
			--i;
			line = polyShape.get(i);
		}
		//polyShape.get(i).setEndY(value);
		setEnd.accept(line, value);
		i = index + 1;
		if(i < polyShape.size()) {
			line = polyShape.get(i);
			while (i < polyShape.size() - 1 && lineDirection.apply(line)) {
				setStart.accept(line, value);
				setEnd.accept(line, value);
				++i;
				line = polyShape.get(i);
			}
			setStart.accept(line, value);
		}
	}

	private void adjustLinesHorizontal(int index, double value) {
		adjustLines(index, value, line -> lineHorizontal(line),  (line, v) -> line.setStartY(v), (line, v) -> line.setEndY(v));
	}

	private void adjustLinesVertical(int index, double value) {
		adjustLines(index, value, line -> lineVertical(line), (line, v) -> line.setStartX(v), (line, v) -> line.setEndX(v));
	}

	public void adjust(Line line) {
		for (int i = 0; i < polyShape.size(); i++) {
			Line l = polyShape.get(i);
			if (linesOverlapHorizontal(line, l)) {
				adjustLinesHorizontal(i, l.getStartY() + LINE_OFFSET);
			} else if (linesOverlapVertical(line, l)) {
				adjustLinesVertical(i, l.getStartX() + LINE_OFFSET);
			}
		}
	}

	public void adjust(List<CableVisual> connections) {
		for (CableVisual c : connections) {
			adjust(c);
		}
	}

	public void adjust(CableVisual connection) {
		for (Line line : connection.polyShape) {
			adjust(line);
		}
	}

}