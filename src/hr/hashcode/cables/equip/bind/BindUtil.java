package hr.hashcode.cables.equip.bind;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRadio;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRuw;

public final class BindUtil {

	static String productName(String productName, String hint, String managedElementProductName, int cabinetsNum) {

		// Determine product name:
		// 1) Try if cabinet's product name is fine
		// 2) Try if hint has RBS6601 's product number
		// 3) Try to get product name from managed element. The string there can be in form "RBS6131W". Remove "W" part first.
		//
		// Note: if hint is not for RBS6601, it has form like: BGM 136 1006/1_CD34706071. The first part "BGM 136 1006" appears both in
		// 6131 and 6102, so we cannot conclude anything. The other part, 1_CD... appears to be an ID

		if (productName != null && productName.length() >= "RBS".length()) {
			return productName;
		} else if (hint.contains(DumpToSite.Rbs6601_1) || hint.contains(DumpToSite.Rbs6601_2)) {
			return "RBS6601";
		} else if (cabinetsNum == 1) {
			if (managedElementProductName != null || managedElementProductName.length() > "RBS".length()) {
				if (managedElementProductName.endsWith("W") || managedElementProductName.endsWith("L")) {
					// Remove last W or L
					return managedElementProductName.substring(0, managedElementProductName.length() - 1);
				} else {
					// Return string as-is
					return managedElementProductName;
				}
			}
		}
		return ""; // empty string is kind-of OK here, perhaps product name will be there after cabinet merge
	}

	private static String radioDescription(RawRadio radio) {
		List<String> list = Arrays.asList(
				radio.getClass().getSimpleName(),
				"#" + (radio.name.isEmpty() ? "<empty>" : radio.name) + "#",
				(radio instanceof RawRuw ? ((RawRuw) radio).auType : "unknown"),
				String.valueOf(radio.bands.size()),
				String.valueOf(radio.cells.size()),
				String.valueOf(radio.location),
				String.valueOf(radio.positionRef),
				radio.dumpType.toString()
		);
		return list.stream().collect(Collectors.joining("\t"));
	}
}
