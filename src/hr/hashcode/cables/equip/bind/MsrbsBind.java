package hr.hashcode.cables.equip.bind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.IdentityHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.bind.Extractor.Corset;
import hr.hashcode.cables.equip.bind.Extractor.CorsetStruct;
import hr.hashcode.cables.equip.bind.JointNodesInfo.DumpType;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.EcBusConn;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawAntFeederCable;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawAntenna;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawBb;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawCabinet;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawCoordinates;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawDigitalCable;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawEUtranCell;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawEcBus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawEquipmentSupportFunction;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawExternalNode;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawHw;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawManagedElement;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawR503;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRadio;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRemoteRus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RiUnit;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.SerialProductInfo;
import hr.hashcode.cables.equip.comp.BandTech;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.Version;

public class MsrbsBind {

	private static SerialProductInfo productInfo(ProductData productData) {
		String name = productData.productName();
		String number = productData.productNumber();
		String serial = productData.serialNumber();
		return new SerialProductInfo(name, number, serial);
	}

	private final RefKeep<EcBus, RawEcBus> ecBuses = new RefKeep<>();
	private final RefKeep<Cabinet, RawCabinet> cabinets = new RefKeep<>();
	private final RefKeep<Corset, RiUnit> riUnits = new RefKeep<>();

	private EcBusConn ecConn(Corset corset) {
		List<EcPort> ecPorts = corset.children(EcPort.class);
		if (ecPorts.size() == 0)
			return null;
		if (ecPorts.size() > 1)
			throw new IllegalArgumentException(corset.toString());
		EcPort ecPort = ecPorts.get(0);
		EcBus ecBus = ecPort.ecBusRef();
		return new EcBusConn(ecBuses.get(ecBus), ecPort.hubPosition());
	}

	private RiUnit findRiParent(Corset corset) {
		for (Corset temp = corset; temp != null; temp = temp.parent())
			if (riUnits.isPresent(temp))
				return riUnits.get(temp);

		throw new IllegalArgumentException(corset.toString());
	}

	SingleNodeInfo start(NodeState state) {

		Extractor extractor = new Extractor(state, MsrbsBind.class);

		SingleNodeInfo info = new SingleNodeInfo();
		info.type = Version.Type.MSRBS;

		for (EcBus bus : extractor.extract(EcBus.class)) {
			RawEcBus raw = new RawEcBus();
			info.ecBuses.add(raw);
			raw.name = bus.name();
			raw.connectionType = bus.ecBusConnectionType();
			raw.ecBusConnectorRef = String.valueOf(bus.ecBusConnectorRef());
			ecBuses.register(bus, raw);
		}

		for (ManagedElement me : extractor.extract(ManagedElement.class)) {
			info.node = new RawManagedElement();
			info.node.fdn = state.fdn;
			info.node.productName = me.managedElementType();
			info.name = me.userLabel();
		}

		for (EquipmentSupportFunction eq : extractor.extract(EquipmentSupportFunction.class)) {
			info.eqSupport = new RawEquipmentSupportFunction(eq.supportSystemControl());
		}

		for (Cabinet cabinet : extractor.extract(Cabinet.class)) {
			String sharedId = cabinet.sharedCabinetIdentifier();
			String identifier = cabinet.cabinetIdentifier();
			SerialProductInfo prodInfo = productInfo(cabinet.productData());
			RawCabinet raw = new RawCabinet(identifier, sharedId, prodInfo);
			info.cabinets.add(raw);
			this.cabinets.register(cabinet, raw);

		}

		for (ExternalNode ext : extractor.extract(ExternalNode.class)) {
			RawExternalNode raw = new RawExternalNode();
			raw.fdn = ext.fullDistinguishedName();
			raw.informationOnly = ext.informationOnly();
			raw.logicalName = ext.logicalName();
			raw.radioAccessTechnology = String.valueOf(ext.radioAccessTechnology());
			raw.supportSystemControl = String.valueOf(ext.supportSystemControl());
			info.extNodes.add(raw);
			raw.ecBusConn = ecConn(ext);
		}

		for (FieldReplaceableUnit unit : units(extractor, "Baseband 5216")) {
			RawBb raw = new RawBb();
			raw.ecConn = ecConn(unit);
			raw.positionRef = this.cabinets.get(unit.positionRef());
			ProductData prodData = unit.productData();
			raw.productInfo = productInfo(prodData);
			riUnits.register(unit, raw);
			info.bbs.add(raw);
		}

		for (FieldReplaceableUnit unit : units(extractor, "Baseband R503", "R503")) {
			RawR503 raw = new RawR503();
			raw.ecConn = ecConn(unit);
			raw.positionRef = this.cabinets.get(unit.positionRef());
			ProductData prodData = unit.productData();
			raw.productInfo = productInfo(prodData);
			riUnits.register(unit, raw);
			info.r503s.add(raw);

		}

		Map<FieldReplaceableUnit, RawRadio> radios = new IdentityHashMap<>();

		for (FieldReplaceableUnit unit : units(extractor, "RU")) {
			RawRus raw = new RawRus();
			raw.name = unit.name();
			raw.positionRef = cabinets.get(unit.positionRef());
			ProductData prodData = unit.productData();
			raw.productInfo = productInfo(prodData);
			riUnits.register(unit, raw);
			info.ruses.add(raw);
			radios.put(unit, raw);
			raw.dumpType = DumpType.MSRBS;
		}

		Set<FieldReplaceableUnit> units = new LinkedHashSet<>();
		units.addAll(units(extractor, "Radio"));
		units.addAll(units(extractor, "RR", "RRU"));
		for (FieldReplaceableUnit unit : units) {
			RawRemoteRus raw = new RawRemoteRus();
			raw.name = unit.name();
			raw.positionRef = cabinets.get(unit.positionRef());
			ProductData prodData = unit.productData();
			raw.productInfo = productInfo(prodData);
			riUnits.register(unit, raw);
			info.remoteRuses.add(raw);
			radios.put(unit, raw);
			raw.dumpType = DumpType.MSRBS;
		}

		List<FieldReplaceableUnit> other = new ArrayList<>();
		for (String type : Arrays.asList("PDU", "CLU", "SAU", "SCU", "SUP"))
			other.addAll(units(extractor, type));

		for (FieldReplaceableUnit unit : other) {
			RawHw raw = new RawHw();
			raw.positionRef = this.cabinets.get(unit.positionRef());
			ProductData prodData = unit.productData();
			raw.productInfo = productInfo(prodData);

			raw.ecConn = ecConn(unit);
			info.hws.add(raw);
		}

		for (AntennaUnitGroup group : extractor.extract(AntennaUnitGroup.class)) {
			RawAntenna antenna = new RawAntenna();
			info.rawAntennas.add(antenna);
			antenna.name = group.name();
			for (RfBranch branch : group.children(RfBranch.class)) {
				antenna.branches.add(branch.name());
				RawAntFeederCable cable = new RawAntFeederCable();
				cable.antenna = antenna;
				cable.antennaPort = branch.name();
				RfPort rfPort = branch.rfPortRef();
				FieldReplaceableUnit unit = null;
				for (Corset temp = rfPort.parent(); temp != null; temp = temp.parent()) {
					if (temp instanceof FieldReplaceableUnit) {
						unit = (FieldReplaceableUnit) temp;
						break;
					}
				}
				cable.radio = radios.get(unit);
				cable.radioPort = rfPort.name();
				info.antennaCables.add(cable);
			}
		}

		for (EUtranCellFDD cell : extractor.extract(EUtranCellFDD.class)) {
			RawEUtranCell raw = new RawEUtranCell();
			raw.coordinates = new RawCoordinates(cell.longitude() / 1e6, cell.latitude() / 1e6);
			raw.earfcndl = cell.earfcndl();
			raw.freqBand = cell.freqBand();

			raw.sectorCarrier =
					Arrays.stream(cell.sectorCarrierRef()).map(String::valueOf).collect(Collectors.toList());
			info.cells.add(raw);
			for (SectorCarrier sectorCarrier : cell.sectorCarrierRef()) {
				for (RfBranch branch : sectorCarrier.sectorFunctionRef().rfBranchRef()) {
					RfPort rfPort = branch.rfPortRef();
					for (Corset temp = rfPort.parent(); temp != null; temp = temp.parent())
						if (temp instanceof FieldReplaceableUnit) {
							RawRadio rawRadio = radios.get(temp);
							rawRadio.bands.add(BandTech.lte(cell.freqBand()));
							rawRadio.cells.add(cell.name());
							break;
						}
				}
			}
		}

		for (RiLink riLink : extractor.extract(RiLink.class)) {
			RiPort port1 = riLink.riPortRef1();
			RiPort port2 = riLink.riPortRef2();

			RiUnit unit1 = findRiParent(port1);
			RiUnit unit2 = findRiParent(port2);

			RawDigitalCable raw = new RawDigitalCable(unit1, port1.name(), unit2, port2.name());

			info.digitalCables.add(raw);
		}

		return info;
	}

	public static BindTrace trace(NodeState state) {
		List<String> radioSerials = new ArrayList<>();
		List<String> externalFdns = new ArrayList<>();

		Extractor extractor = new Extractor(state, MsrbsBind.class);

		for (ExternalNode ext : extractor.extract(ExternalNode.class)) {
			String fdn = ext.fullDistinguishedName();
			externalFdns.add(fdn);
		}

		Set<FieldReplaceableUnit> units = new LinkedHashSet<>();
		units.addAll(units(extractor, "Radio"));
		units.addAll(units(extractor, "RR", "RRU"));
		for (FieldReplaceableUnit unit : units) {
			ProductData prodData = unit.productData();
			if (prodData != null)
				radioSerials.add(prodData.serialNumber());
		}

		return new BindTrace(radioSerials, externalFdns);
	}

	private static List<FieldReplaceableUnit> units(Extractor extractor, String type) {
		List<FieldReplaceableUnit> result = new ArrayList<>();
		for (FieldReplaceableUnit unit : extractor.extract(FieldReplaceableUnit.class)) {
			if (unit.productData().productName().startsWith(type))
				result.add(unit);
		}
		return result;
	}

	private static List<FieldReplaceableUnit> units(Extractor extractor, String type, String name) {
		List<FieldReplaceableUnit> result = new ArrayList<>();
		for (FieldReplaceableUnit unit : extractor.extract(FieldReplaceableUnit.class)) {
			if (unit.productData().productName().startsWith(type) || unit.name().startsWith(name))
				result.add(unit);
		}
		return result;
	}

	private interface RiLink extends Corset {
		RiPort riPortRef1();

		RiPort riPortRef2();
	}

	private interface EquipmentSupportFunction extends Corset {
		Boolean supportSystemControl();
	}

	private enum RadioAccessTechnology {
		NOT_AVAILABLE, GSM, WCDMA, LTE, MULTI_STANDARD, TRANSMISSION;
	}

	private interface ManagedElement extends Corset {
		String userLabel();

		String managedElementType();
	}

	private interface ExternalNode extends Corset {

		String fullDistinguishedName();

		String logicalName();

		RadioAccessTechnology radioAccessTechnology();

		Boolean informationOnly();

		SupportSystemControl supportSystemControl();

	}

	private enum SupportSystemControl {
		FALSE, TRUE, NOT_AVAILABLE
	}

	private interface Cabinet extends Corset {
		String sharedCabinetIdentifier();

		String cabinetIdentifier();

		ProductData productData();
	}

	private interface SectorCarrier extends Corset {
		SectorEquipmentFunction sectorFunctionRef();
	}

	private interface SectorEquipmentFunction extends Corset {
		RfBranch[] rfBranchRef();
	}

	private interface HwItem extends Corset {
		String hwModel();

		String hwType();

		ProductData productData();

		String serialNumber();

		Corset[] equipmentMoRef();
	}

	private interface FieldReplaceableUnit extends Corset {

		Cabinet positionRef();

		ProductData productData();

	}

	private interface EUtranCellFDD extends Corset {
		SectorCarrier[] sectorCarrierRef();

		Integer freqBand();

		Integer longitude();

		Integer latitude();

		Integer earfcndl();

		FieldReplaceableUnit hostingDigitalUnit();

	}

	private interface RiPort extends Corset {

	}

	private interface EcBus extends Corset {
		String ecBusConnectionType();

		Corset ecBusConnectorRef();
	}

	private interface EcPort extends Corset {
		Integer cascadingOrder();

		EcBus ecBusRef();

		String hubPosition();

	}

	private interface ProductData extends CorsetStruct {
		String productName();

		String productNumber();

		String serialNumber();
	}

	private interface RfPort extends Corset {
		String dlFrequencyRanges();
	}

	private interface RfBranch extends Corset {

		RfPort rfPortRef();
	}

	private interface AntennaUnitGroup extends Corset {

	}

}
