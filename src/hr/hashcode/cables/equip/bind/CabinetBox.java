package hr.hashcode.cables.equip.bind;

import java.util.ArrayList;
import java.util.List;

import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnit;

class CabinetBox {

	final List<DigitalUnit> units = new ArrayList<>();
	final List<R503> r503s = new ArrayList<>();
	final List<RadioUnit> radios = new ArrayList<>();
	final String type;

	CabinetBox(String type) {
		this.type = type;
	}
}
