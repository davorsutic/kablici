package hr.hashcode.cables.equip.bind;

import java.util.List;

import hr.hashcode.cables.equip.bind.JointNodesInfo.Container;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawBb;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawCabinet;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawDus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawDuw;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawEcBus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawR503;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRus;

// Class to help debugging
public final class DumpPrinter {

	public static void printRaw(CabinetSystem system) {

		for (SingleNodeInfo info : system.infos) {
			System.out.println("\t" + info.name);

			for (RawCabinet cab : info.cabinets) {
				if (cab != null) writeCab("CAB", 2, cab);
			}

			RawBb bb = getSingle(info.bbs);
			if (bb != null) writeCab("BB", 2, bb.positionRef);

			RawDus dus = getSingle(info.duses);
			if (dus != null) writeCab("DUS", 2, dus.positionRef);

			for (RawDuw duw : info.duws) {
				if (duw != null) writeCab("DUW", 2, duw.positionRef);
			}

			for (RawR503 r503 : info.r503s) {
				if (r503 != null) writeCab("R503", 2, r503.positionRef);
			}

			for (RawRus rus : info.ruses) {
				if (rus != null) writeCab("RUS", 2, rus.positionRef);
			}

			for (RawEcBus ecBus : info.ecBuses) {
				if (ecBus != null) System.out.println(mult("\t", 2) + "EC_BUS" + "\t" + ecBus.name);
			}

			System.out.println();
		}
	}

	public static void printExtended(CabinetSystem system) {

		Container con = system.joint.container;
		System.out.println("BB " + con.bbs.size());
		System.out.println("DUSES " + con.duses.size());
		System.out.println("DUWS " + con.duws.size());
		System.out.println("R503 " + con.r503s.size());
		System.out.println("RADIOS " + con.radios.size());
		System.out.println("REMOTES " + con.remotes.size());
		System.out.println();
	}

	private static void writeCab(String str, int i, RawCabinet positionRef) {
		if (positionRef != null)
			System.out.println(mult("\t", i) + str + "\t" + positionRef.identifier + "-->" + positionRef.sharedId);
		else
			System.out.println(mult("\t", i) + str + "\t" + "NULL");
	}

	private static String mult(String x, int num) {
		StringBuilder b = new StringBuilder();
		for (int i = 0; i < num; i++) {
			b.append(x);
		}
		return b.toString();
	}

	private static <T> T getSingle(List<T> list) {
		if (list.size() == 1)
			return list.get(0);
		else
			throw new IllegalArgumentException(list.toString());
	}
}
