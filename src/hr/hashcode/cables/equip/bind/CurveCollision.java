package hr.hashcode.cables.equip.bind;

import java.util.Arrays;
import java.util.function.Function;

public class CurveCollision {

	public static class BezierCubicCurve {
		int[] pointsX;
		int[] pointsY;

		BezierCubicCurve(int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3) {
			pointsX = new int[] {x0, x1, x2, x3};
			pointsY = new int[] {y0, y1, y2, y3};
		}

		private double getValue(double t, int v0, int v1, int v2, int v3) {
			return (1 - t) * (1 - t) * (1 - t) * v0 + 3 * t * (1 - t) * (1 - t) * v1 + 3 * t * t * (1 - t) * v2 + t * t * t * v3;
		}

		public double getValueX(double t) {
			return getValue(t, pointsX[0], pointsX[1], pointsX[2], pointsX[3]);
		}

		public double getValueY(double t) {
			return getValue(t, pointsY[0], pointsY[1], pointsY[2], pointsY[3]);
		}

		private void solveCubic(Cube c, int[] points, int val) {
			c.solve(-points[0] + 3 * points[1] - 3 * points[2] + points[3], 3 * points[0] - 6 * points[1] + points[2],
					-3 * points[0] + 3 * points[1], points[0] - val);
		}

		private double getIntersection(int[] points, int val, Function<Double, Double> pointFunction) {
			Cube c = new Cube();
			solveCubic(c, points, val);
			double[] roots = new double[] {c.x1, c.x2, c.x3};
			for(int i = 0; i < roots.length; i++) {
				if(!Double.isNaN(roots[i])) {
					double point = pointFunction.apply(roots[i]);
					if (point >= Math.min(points[0], points[1]) && point <= Math.max(points[0], points[1])) {
						return point;
					}
				}
			}
			return Double.NaN;
		}

		public double getIntersectionX(int x) {
			return getIntersection(pointsX, x, t -> getValueX(t));
		}

		public double getIntersectionY(int y) {
			return getIntersection(pointsY, y, t -> getValueY(t));
		}
	}

	public static class Cube {

		private static final double TWO_PI = 2.0 * Math.PI;
		private static final double FOUR_PI = 4.0 * Math.PI;

		public int nRoots;
		public double x1;
		public double x2;
		public double x3;

		public void solve(double a, double b, double c, double d) {
			if (a == 0.0) {
				throw new RuntimeException("CurveCollision.solve(): a = 0");
			}

			// normalize coefficients
			double denom = a;
			a = b / denom;
			b = c / denom;
			c = d / denom;

			double aOver3 = a / 3.0;
			double q = (3 * b - a * a) / 9.0;
			double qCube = q * q * q;
			double r = (9 * a * b - 27 * c - 2 * a * a * a) / 54.0;
			double rSqr = r * r;
			double D = qCube + rSqr;

			if (D < 0.0) {
				// Three unequal real roots.
				nRoots = 3;
				double theta = Math.acos(r / Math.sqrt(-qCube));
				double sqrtQ = Math.sqrt(-q);
				x1 = 2.0 * sqrtQ * Math.cos(theta / 3.0) - aOver3;
				x2 = 2.0 * sqrtQ * Math.cos((theta + TWO_PI) / 3.0) - aOver3;
				x3 = 2.0 * sqrtQ * Math.cos((theta + FOUR_PI) / 3.0) - aOver3;
				sortRoots();
			} else if (D > 0.0) {
				// one real root
				nRoots = 1;
				double SqRT_D = Math.sqrt(D);
				double S = Math.cbrt(r + SqRT_D);
				double T = Math.cbrt(r - SqRT_D);
				x1 = (S + T) - aOver3;
				x2 = Double.NaN;
				x3 = Double.NaN;
			} else {
				// three real roots, at least two equal
				nRoots = 3;
				double cbrtR = Math.cbrt(r);
				x1 = 2 * cbrtR - aOver3;
				x2 = x3 = cbrtR - aOver3;
				sortRoots();
			}
		}

		private void sortRoots() {
			double[] values = new double[] { x1, x2, x3 };
			Arrays.sort(values);
			x1 = values[0];
			x2 = values[1];
			x3 = values[2];
		}
	}

	public static void main(String[] args) {
		CurveCollision.BezierCubicCurve curve = new BezierCubicCurve(0, 0, 1, 3, 3, 3, 4, 3);
		System.out.println(curve.getIntersectionX(1));
		System.out.println(curve.getIntersectionY(1));
	}
}
