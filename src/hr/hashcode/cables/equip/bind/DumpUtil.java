package hr.hashcode.cables.equip.bind;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

import hr.hashcode.cables.equip.bind.JointNodesInfo.Cabinet;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Container;
import hr.hashcode.cables.equip.bind.JointNodesInfo.DumpType;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Extended;
import hr.hashcode.cables.equip.bind.JointNodesInfo.ExtendedHub;
import hr.hashcode.cables.equip.bind.JointNodesInfo.ExtendedRadio;
import hr.hashcode.cables.equip.bind.JointNodesInfo.InCabinetExtendedRadio;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.CabinetLocation;
import hr.hashcode.cables.equip.comp.Antenna;
import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DcPort.DcPortable;
import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.DigitalUnitBaseband;
import hr.hashcode.cables.equip.comp.DigitalUnitLte;
import hr.hashcode.cables.equip.comp.DigitalUnitStandard;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.EcPort;
import hr.hashcode.cables.equip.comp.EcPort.EcPortable;
import hr.hashcode.cables.equip.comp.ExtensionRack;
import hr.hashcode.cables.equip.comp.IdlPort.IdlPortable;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.MiniRbs;
import hr.hashcode.cables.equip.comp.Port;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.Rbs;
import hr.hashcode.cables.equip.comp.RfPort.RfPortable;
import hr.hashcode.cables.equip.comp.RiPort;
import hr.hashcode.cables.equip.comp.RiPort.RiPortable;
import hr.hashcode.cables.equip.comp.Site;
import hr.hashcode.cables.equip.comp.Subrack;

// Utility functions
public final class DumpUtil {

	// Used to order radios inside same radio group, not arbitrary radios
	// Assumes that bands order corresponds with cell order
	// Sort by cell name on same technology, if radios both support LTE or both WCDMA
	// If tied, sort by location on common technology
	// If previous two fail, sort in some deterministic order (by concatenating location strings and comparing that)
	final static Comparator<InCabinetExtendedRadio> RadioInGroupComparator = (Comparator<InCabinetExtendedRadio>) (o1, o2) -> {
		// If both contain LTE cells, compare by LTE cell name, then by location
		if (o1.lte() != null && o2.lte() != null) {

			int cmp = o1.lte().normalizedCell.compareTo(o2.lte().normalizedCell);
			if (cmp != 0) return cmp;

			if (o1.lte().dumpType == DumpType.ERBS && o2.lte().dumpType == DumpType.ERBS) {
				CabinetLocation loc1 = o1.lte().location.get(); // Get is safe because ERBS and RBS must have location set
				CabinetLocation loc2 = o2.lte().location.get();
				return loc1.compareTo(loc2);
			}

		}

		// If both contain WCDAM cells, compare by WCDMA cell name, then by location
		if (o1.wcdma() != null && o2.wcdma() != null) {
			int cmp = o1.wcdma().normalizedCell.compareTo(o2.wcdma().normalizedCell);
			if (cmp != 0) return cmp;

			CabinetLocation loc1 = o1.wcdma().location.get();
			CabinetLocation loc2 = o2.wcdma().location.get();
			return loc1.compareTo(loc2);
		}

		// In case one is LTE-only, the other is WCDMA-only
		String sign1 = (o1.wcdma() != null ? o1.wcdma().radioName : "") + "," + (o1.lte() != null ? o1.lte().radioName : "");
		String sign2 = (o2.wcdma() != null ? o2.wcdma().radioName : "") + "," + (o2.lte() != null ? o2.lte().radioName : "");
		return sign1.compareTo(sign2);
	};

	static void require(boolean b, Object o) {
		if (!b)
			throw new IllegalArgumentException(o.toString());
	}

	// Returns true if any ec port's name contains given string
	static boolean ecPortsContain(CabinetSystem system, String str) {
		boolean subtypeFirst = false;
		for (Connector<EcPort> cable : system.joint.ecCables) {
			EcPort port1 = cable.first();
			EcPort port2 = cable.second();

			if (port1.name().contains(str) || port2.name().contains(str)) {
				subtypeFirst = true;
				break;
			}
		}
		return subtypeFirst;
	}

	// Returns mapping from port to unit
	public static IdentityHashMap<Port, Component> calculatePortToUnit(CabinetSystem system, List<Extended> allHw) {
		IdentityHashMap<Port, Component> portToUnit = new IdentityHashMap<>();
		for (Extended unit : allHw) {
			Component component = (Component) unit.item;
			List<Port> ports = ports(component);
			ports.forEach(p -> portToUnit.put(p, component));
		}
		for (ExtendedHub hub : system.joint.hubs) {
			hub.ports.ports().forEach(p -> portToUnit.put(p, hub));
		}
		for (Antenna antenna : system.joint.antennas) {
			antenna.rfPorts().ports().forEach(p -> portToUnit.put(p, antenna));
		}
		return portToUnit;
	}

	// Returns all cables found in dump (dc cables are not there)
	public static List<Connector<?>> getCablesInDump(CabinetSystem system) {
		List<Connector<?>> cables = new ArrayList<>();
		cables.addAll(system.joint.ecCables);
		cables.addAll(system.joint.idlCables);
		cables.addAll(system.joint.riCables);
		cables.addAll(system.joint.rfCables);
		return cables;
	}

	// Returns all hardware in the system
	public static List<Extended> getAllHardware(CabinetSystem system) {
		// Prepare all hw
		List<Extended> allHw = new ArrayList<>();

		// Collect all containers
		List<Container> containers = new ArrayList<>();
		containers.add(system.joint.container);
		system.joint.cabinets.forEach(cab -> containers.add(cab.container));

		for (Container c : containers) {
			allHw.addAll(c.bbs);
			allHw.addAll(c.duses);
			allHw.addAll(c.duws);
			allHw.addAll(c.r503s);
			allHw.addAll(c.radios);
			allHw.addAll(c.remotes);
		}
		return allHw;
	}

	// Creates a mapping from digital units to their bands
	public static IdentityHashMap<DigitalUnit, Set<Band>> digitalToBands(CabinetSystem system) {
		IdentityHashMap<DigitalUnit, Set<Band>> result = new IdentityHashMap<>();

		// Find LTE BB
		DigitalUnit lte = null;
		for (Cabinet cabinet : system.joint.cabinets) {
			if (cabinet.container.duses.size() == 1) lte = cabinet.container.duses.get(0).item;
			if (cabinet.container.bbs.size() == 1) lte = cabinet.container.bbs.get(0).item;
		}

		// Find all LTE bands
		Set<Band> lteBands = new HashSet<>();
		for (Cabinet cabinet : system.joint.cabinets) {
			ArrayList<ExtendedRadio<?>> allRadios = new ArrayList<>();
			allRadios.addAll(cabinet.container.radios);
			allRadios.addAll(cabinet.container.remotes);

			for (ExtendedRadio<?> radio : allRadios) {
				if (radio.lte() != null)
					lteBands.add(radio.band);
			}
		}
		for (ExtendedRadio<?> remote : system.joint.container.remotes) {
			if (remote.lte() != null)
				lteBands.add(remote.band);
		}

		// LTE is done
		if (lte != null) // Happens for unpaired dumps, when we have only WCDMA
			result.put(lte, lteBands);

		// Create a map from WCDMA radios to their DUW. Assign DUW bands based on radio bands
		List<Extended> allHw = getAllHardware(system);
		IdentityHashMap<Component, Extended<?>> componentToExtended = new IdentityHashMap<>();
		allHw.forEach(e -> componentToExtended.put(e.item, e));

		IdentityHashMap<Port, Component> portToUnit = calculatePortToUnit(system, allHw);
		for (Connector<RiPort> riCable : system.joint.riCables) {
			Component first = portToUnit.get(riCable.first());
			Component second = portToUnit.get(riCable.second());

			ComponentPair<DigitalUnitWcdma, RadioUnitBase> pair = cableMatch(
					CableType.of(DigitalUnitWcdma.class, RadioUnitBase.class),
					RawComponentPair.of(first, second));
			if (pair != null) {
				DigitalUnitWcdma duw = pair.first;
				RadioUnitBase radioUnit = pair.second;
				Band band = ((ExtendedRadio<?>) componentToExtended.get(radioUnit)).band;
				result.computeIfAbsent(duw, x -> new HashSet<>()).add(band);
			}
		}

		return result;
	}

	public static IdentityHashMap<R503, Set<Band>> r503ToBands(CabinetSystem system) {
		IdentityHashMap<R503, Set<Band>> result = new IdentityHashMap<>();

		// Prepare structures
		List<Extended> allHw = getAllHardware(system);
		IdentityHashMap<Component, Extended<?>> componentToExtended = new IdentityHashMap<>();
		allHw.forEach(e -> componentToExtended.put(e.item, e));

		IdentityHashMap<Port, Component> portToUnit = calculatePortToUnit(system, allHw);
		for (Connector<RiPort> riCable : system.joint.riCables) {
			Component first = portToUnit.get(riCable.first());
			Component second = portToUnit.get(riCable.second());

			ComponentPair<R503, RadioUnitBase> pair = cableMatch(
					CableType.of(R503.class, RadioUnitBase.class),
					RawComponentPair.of(first, second)
			);

			if (pair != null) {
				R503 r503 = pair.first;
				RadioUnitBase radioUnit = pair.second;
				Band band = ((ExtendedRadio<?>) componentToExtended.get(radioUnit)).band;
				result.computeIfAbsent(r503, x -> new HashSet<>()).add(band);
			}
		}

		return result;
	}

	public static void doForCabinetUnits(Container container, Consumer<Component> foo) {
		container.bbs.forEach(x -> foo.accept(x.item));
		container.duses.forEach(x -> foo.accept(x.item));
		container.duws.forEach(x -> foo.accept(x.item));
		container.r503s.forEach(x -> foo.accept(x.item));
		container.radios.forEach(x -> foo.accept(x.item));
	}

	public static IdentityHashMap<Component, Extended<?>> itemsToExtended(List<Extended> allHw) {
		IdentityHashMap<Component, Extended<?>> map = new IdentityHashMap<>();
		allHw.forEach(x -> map.put(x.item, x));
		return map;
	}

	public static void moveUnit(Container from, Container to, Extended<?> unit) {
		if (unit.item instanceof DigitalUnitBaseband) {
			Extended<DigitalUnitBaseband> typed = (Extended<DigitalUnitBaseband>) unit;
			from.bbs.remove(typed);
			to.bbs.add(typed);
		} else if (unit.item instanceof DigitalUnitStandard || unit.item instanceof DigitalUnitLte) {
			Extended<DigitalUnitStandard> typed = (Extended<DigitalUnitStandard>) unit;
			from.duses.remove(typed);
			to.duses.add(typed);
		} else if (unit.item instanceof DigitalUnitWcdma) {
			Extended<DigitalUnitWcdma> typed = (Extended<DigitalUnitWcdma>) unit;
			from.duws.remove(typed);
			to.duws.add(typed);
		} else if (unit.item instanceof R503) {
			Extended<R503> typed = (Extended<R503>) unit;
			from.r503s.remove(typed);
			to.r503s.add(typed);
		} else if (unit instanceof InCabinetExtendedRadio) {
			InCabinetExtendedRadio typed = (InCabinetExtendedRadio) unit;
			from.radios.remove(typed);
			to.radios.add(typed);
		}
	}

	public static IdentityHashMap<Component, Rbs> componentToRbs(Site site) {
		IdentityHashMap<Component, Rbs> result = new IdentityHashMap<>();
		for (MacroRbs macroRbs : site.rbses) {
			for (ExtensionRack rack : macroRbs.extensions()) {
				// TODO continue here
			}
			for (Subrack subrack : macroRbs.subracks()) {
				// TODO continue here
			}
		}
		for (MiniRbs miniRbs : site.miniRbses) {
			miniRbs.duw();
			miniRbs.dus("1");
			miniRbs.dus("2");
			miniRbs.r503("1");
			miniRbs.r503("2");
		}
		return result;
	}

	public static EnumMap<Band, Set<RadioUnitBase>> radiosByBand(CabinetSystem system) {
		EnumMap<Band, Set<RadioUnitBase>> result = new EnumMap<>(Band.class);
		for (Extended extended : getAllHardware(system)) {
			if (RadioUnitBase.class.isAssignableFrom(extended.item.getClass())) {
				ExtendedRadio<?> er = (ExtendedRadio<?>) extended;
				result.computeIfAbsent(er.band, x -> new HashSet<>()).add(er.item);
			}
		}
		return result;
	}

	public static <F extends Component, S extends Component> ComponentPair<F, S> cableMatch(CableType<F, S> link, RawComponentPair
			pair) {
		if (link.first.isAssignableFrom(pair.first.getClass()) && link.second.isAssignableFrom(pair.second.getClass())) {
			return ComponentPair.of(link.first.cast(pair.first), link.second.cast(pair.second));
		} else if (link.second.isAssignableFrom(pair.first.getClass()) && link.first.isAssignableFrom(pair.second.getClass())) {
			return ComponentPair.of(link.first.cast(pair.second), link.second.cast(pair.first));
		} else {
			return null;
		}
	}

	static List<Port> ports(Component component) {
		List<Port> ret = new ArrayList<>();
		if (component instanceof DcPortable) {
			DcPortable c = (DcPortable) component;
			ret.addAll(c.dcPorts().ports());
		}
		if (component instanceof EcPortable) {
			EcPortable c = (EcPortable) component;
			ret.addAll(c.ecPorts().ports());
		}
		if (component instanceof IdlPortable) {
			IdlPortable c = (IdlPortable) component;
			ret.addAll(c.idlPorts().ports());
		}
		if (component instanceof RiPortable) {
			RiPortable c = (RiPortable) component;
			ret.addAll(c.riPorts().ports());
		}
		if (component instanceof RfPortable) {
			RfPortable c = (RfPortable) component;
			ret.addAll(c.rfPorts().ports());
		}
		return ret;
	}

	static class RawComponentPair {
		final Component first;
		final Component second;

		private RawComponentPair(Component first, Component second) {
			this.first = Objects.requireNonNull(first);
			this.second = Objects.requireNonNull(second);
		}

		public static RawComponentPair of(Component from, Component to) {
			return new RawComponentPair(from, to);
		}
	}

	static class ComponentPair<F extends Component, S extends Component> {
		final F first;
		final S second;

		private ComponentPair(F first, S second) {
			this.first = Objects.requireNonNull(first);
			this.second = Objects.requireNonNull(second);
		}

		public static <F extends Component, S extends Component> ComponentPair<F, S> of(F from, S to) {
			return new ComponentPair<>(from, to);
		}
	}

	static class CableType<F extends Component, S extends Component> {
		final Class<F> first;
		final Class<S> second;

		private CableType(Class<F> first, Class<S> second) {
			this.first = first;
			this.second = second;
		}

		public static <F extends Component, S extends Component> CableType<F, S> of(Class<F> first, Class<S> second) {
			return new CableType<>(first, second);
		}
	}
}
