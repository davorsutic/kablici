package hr.hashcode.cables.equip.bind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.bind.DumpUtil.CableType;
import hr.hashcode.cables.equip.bind.DumpUtil.RawComponentPair;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Cabinet;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Extended;
import hr.hashcode.cables.equip.bind.JointNodesInfo.InCabinetExtendedRadio;
import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.DigitalUnitBaseband;
import hr.hashcode.cables.equip.comp.DigitalUnitLte;
import hr.hashcode.cables.equip.comp.DigitalUnitStandard;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.Port;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnit;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.RfPort;
import hr.hashcode.cables.equip.comp.RiPort;
import hr.hashcode.cables.equip.comp.SupportHubUnit;

public class DumpNormalize {

	private static boolean debug = false;

	// Assumption: there are some cables that are inside same cabinet only
	public static void normalize(CabinetSystem system) {

		List<Extended> allHw = DumpUtil.getAllHardware(system);

		// Prepare all cables
		List<Connector<?>> cables = DumpUtil.getCablesInDump(system);

		// Map ports to HW
		IdentityHashMap<Port, Component> portToUnit = DumpUtil.calculatePortToUnit(system, allHw);

		// Which connections are made only inside cabinet?
		// Note: this is 'logical cabinet'. Even if 2 DUWs are inside separate 6601 cabinets, they are in same logical cabinet
		List<CableType<?, ?>> links = Arrays.asList(
				CableType.of(Component.class, SupportHubUnit.class),
				CableType.of(DigitalUnit.class, RadioUnit.class),
				CableType.of(R503.class, RadioUnit.class),
				CableType.of(RadioUnit.class, RadioUnit.class),
				CableType.of(DigitalUnitWcdma.class, DigitalUnitWcdma.class)
		);

		// Create link map. Map is reflexive and transitive, meaning that HW is in the same cabinet
		IdentityHashMap<Component, List<Component>> neighbors = new IdentityHashMap<>();
		for (Connector<?> cable : cables) {
			Component from = portToUnit.get(cable.first());
			Component to = portToUnit.get(cable.second());

			if (from == null || to == null) {
				// This happens for EC ports for units that go under ExtendedEcOther: SAU, SAP, ...
				continue;
			}

			for (CableType<?, ?> link : links) {
				if (DumpUtil.cableMatch(link, RawComponentPair.of(from, to)) != null) {
					neighbors.computeIfAbsent(from, x -> new ArrayList<>()).add(to);
					neighbors.computeIfAbsent(to, x -> new ArrayList<>()).add(from);
				}
			}
		}

		// Hw to cabinet, what we know
		final IdentityHashMap<Component, Cabinet> hwToCabinet = new IdentityHashMap<>();
		for (Cabinet c : system.joint.cabinets) {
			DumpUtil.doForCabinetUnits(c.container, x -> hwToCabinet.put(x, c));
		}

		IdentityHashMap<Component, Extended<?>> itemToExtended = DumpUtil.itemsToExtended(allHw);

		// The actual stuff happens bellow in three steps

		unassignedUnitsToCabinets(system, neighbors, hwToCabinet, itemToExtended);

		mergeCabinets(system, neighbors, hwToCabinet);

		normalizeRadios(system);
	}

	private static void unassignedUnitsToCabinets(CabinetSystem system, IdentityHashMap<Component, List<Component>> neighbors,
	                                              IdentityHashMap<Component, Cabinet> hwToCabinet,
	                                              IdentityHashMap<Component, Extended<?>> itemToExtended) {
		// Unassigned HW
		List<Component> tempUnassigned = new ArrayList<>();
		DumpUtil.doForCabinetUnits(system.joint.container, tempUnassigned::add);

		// Place unassigned hardware in turns. It is possible that placing HW in one turn 'unlocks' placing the other in next turn
		List<Component> unassigned = tempUnassigned;
		for (int turn = 1; unassigned.size() > 0 && turn < 10; turn++) {
			List<Component> stillUnassigned = new ArrayList<>();

			// Try to place unassigned hardware
			for (Component una : unassigned) {
				List<Component> nei = neighbors.get(una);
				if (nei == null) {
					if (debug) System.out.println("no neighbors: " + una.name());
					stillUnassigned.add(una);
					continue;
				}

				// Check, all neighbors must be in the same cabinet
				Set<Cabinet> cabinets = nei.stream().map(hwToCabinet::get).filter(Objects::nonNull).collect(Collectors.toSet());
				if (cabinets.size() > 1) {
					throw new IllegalArgumentException(cabinets.toString());
				} else if (cabinets.size() == 0) {
					if (debug) System.out.println(una.name() + "My neighbors don't have cabinets: " + nei.toString());
					stillUnassigned.add(una);
					continue;
				}

				// Do the actual move
				Cabinet myCabinet = cabinets.iterator().next();
				Extended<?> myExtended = itemToExtended.get(una);
				DumpUtil.moveUnit(system.joint.container, myCabinet.container, myExtended);
				hwToCabinet.put(una, myCabinet);

				if (debug) System.out.println(una);
			}
			unassigned = stillUnassigned;
		}

		// Step two.
		// Motivation: there are only external RUs and two DUWs, neither with positionRef attribute. There is no way to connect
		// them to cabinets. Only if all cabinets are 6601, we place DUWs into first cabinet (currenly still logical cabinet, will be
		// split to several real cabinets. If cabinets are anything else, we don't know DUWs' location and exception happens.
		boolean all6601 = !system.joint.cabinets.isEmpty();
		for (Cabinet cabinet : system.joint.cabinets) {
			if (!cabinet.type.equals("RBS6601")) {
				all6601 = false;
			}
		}
		ArrayList<Component> stillUnassigned = new ArrayList<>();
		if (all6601) {
			Cabinet anyCabinet = system.joint.cabinets.get(0);
			for (Component component : unassigned) {
				boolean fits6601 = component.is(DigitalUnit.class) || component.is(R503.class);

				Extended<?> myExtended = itemToExtended.get(component);
				if (fits6601 && myExtended != null) {
					DumpUtil.moveUnit(system.joint.container, anyCabinet.container, myExtended);
				} else {
					stillUnassigned.add(component);
				}
			}
		}

		DumpUtil.require(stillUnassigned.isEmpty(), stillUnassigned);
	}

	private static void mergeCabinets(CabinetSystem system, IdentityHashMap<Component, List<Component>> neighbors,
	                                  IdentityHashMap<Component, Cabinet> hwToCabinet) {
		// Figure out which cabinets to merge
		// Assumption: it is enough to merge in just 1 turn. In theory it is possible that 3 cabinets are merged into one, but here I assume
		// that 1 merge is enough, as LTE and DUW cabinets get merged
		Map<Cabinet, Cabinet> cabinetsToMerge = new HashMap<>();
		for (Entry<Component, List<Component>> entry : neighbors.entrySet()) {
			Component home = entry.getKey();
			for (Component nei : entry.getValue()) {
				Cabinet homeCab = hwToCabinet.get(home);
				Cabinet neiCab = hwToCabinet.get(nei);

				if (homeCab != neiCab) {
					// Cabinets must be of the same type
					if (!homeCab.type.equals(neiCab.type)) {
						return;
					}

					if (homeCab.name.compareTo(neiCab.name) <= 0) // Comparing by name might be fragile, in rare cases name can be empty
						cabinetsToMerge.put(homeCab, neiCab);
					else
						cabinetsToMerge.put(neiCab, homeCab);
				}
			}
		}

		// Do the actual merge
		HashSet<Cabinet> alreadyMerged = new HashSet<>();
		for (Entry<Cabinet, Cabinet> cabinetPair : cabinetsToMerge.entrySet()) {
			Cabinet to = cabinetPair.getKey();
			Cabinet from = cabinetPair.getValue();
			if (!alreadyMerged.contains(from)) {
				if (debug) System.out.println("Merging cabinets: " + to.name + " and " + from.name);
				alreadyMerged.add(from);

				to.container.duses.addAll(from.container.duses);
				to.container.duws.addAll(from.container.duws);
				to.container.bbs.addAll(from.container.bbs);
				to.container.r503s.addAll(from.container.r503s);

				to.container.radios.addAll(from.container.radios);
				to.container.remotes.addAll(from.container.remotes);

				to.container.ecOthers.addAll(from.container.ecOthers);
			}
			system.joint.cabinets.remove(from);
		}
	}

	private static void normalizeRadios(CabinetSystem system) {

		// Establish alpha radios on LTE
		Set<Component> alphaRadios = new HashSet<>();

		List<Extended> allHw = DumpUtil.getAllHardware(system);
		IdentityHashMap<Port, Component> portToUnits = DumpUtil.calculatePortToUnit(system, allHw);

		for (Connector<RiPort> riCable : system.joint.riCables) {
			RiPort firstPort = riCable.first();
			RiPort secondPort = riCable.second();

			Component firstUnit = portToUnits.get(firstPort);
			Component secondUnit = portToUnits.get(secondPort);

			boolean firstIsDigital = isLteBaseband(firstUnit) || isR503(firstUnit);
			boolean secondIsDigital = isLteBaseband(secondUnit) || isR503(secondUnit);

			boolean firstIsRadio = RadioUnit.class.isAssignableFrom(firstUnit.getClass());
			boolean secondIsRadio = RadioUnit.class.isAssignableFrom(secondUnit.getClass());

			if (firstIsDigital && secondIsRadio) alphaRadios.add(secondUnit);
			if (firstIsRadio && secondIsDigital) alphaRadios.add(firstUnit);
		}

		for (Cabinet cabinet : system.joint.cabinets) {
			List<InCabinetExtendedRadio> radios = cabinet.container.radios;

			Map<InCabinetExtendedRadio, InCabinetExtendedRadio> mergeResult = mergeCabinetRadios(radios, alphaRadios);
			if (!mergeResult.isEmpty()) {

				// Debug printing
				if (debug) System.out.println("MERGED: ");
				for (Entry<InCabinetExtendedRadio, InCabinetExtendedRadio> entry : mergeResult.entrySet()) {
					if (debug) System.out.println("\t" + entry.getKey().item.name() + "\t" + entry.getValue().item.name());
				}

				// Do the actual replacement
				for (InCabinetExtendedRadio extendedRadio : mergeResult.keySet()) {
					boolean removeResult = cabinet.container.radios.remove(extendedRadio);
					DumpUtil.require(removeResult, extendedRadio);
				}

				cabinet.container.radios.addAll(mergeResult.values());

				// Prepare map for cable switching
				Map<Component, InCabinetExtendedRadio> forSwitching = new HashMap<>();
				for (Entry<InCabinetExtendedRadio, InCabinetExtendedRadio> entry : mergeResult.entrySet()) {
					RadioUnitBase from = entry.getKey().item;
					InCabinetExtendedRadio to = entry.getValue();
					forSwitching.put(from, to);
				}

				// Rewire cables - go through all of them (RI and RF for radio units) and reswitch
				for (int i = 0; i < system.joint.riCables.size(); i++) {
					Connector<RiPort> cable = system.joint.riCables.get(i);

					Component from = portToUnits.get(cable.first());
					Component to = portToUnits.get(cable.second());

					if (forSwitching.containsKey(from) || forSwitching.containsKey(to)) {

						String firstName = cable.first().name();
						RiPort newFirst = forSwitching.containsKey(from) ? forSwitching.get(from).item.riPort(firstName) : cable.first();
						String secondName = cable.second().name();
						RiPort newSecond = forSwitching.containsKey(to) ? forSwitching.get(to).item.riPort(secondName) : cable.second();

						if (debug) System.out.println("Swapping cables for " + cable.first() + " and " + cable.second());
						Connector<RiPort> newCable = new Connector<>(newFirst, newSecond);
						system.joint.riCables.set(i, newCable);
					}
				}
				for (int i = 0; i < system.joint.rfCables.size(); i++) {
					Connector<RfPort> cable = system.joint.rfCables.get(i);

					Component from = portToUnits.get(cable.first());
					Component to = portToUnits.get(cable.second());

					if (forSwitching.containsKey(from) || forSwitching.containsKey(to)) {

						String firstName = cable.first().name();
						RfPort newFirst = forSwitching.containsKey(from) ? forSwitching.get(from).item.rfPort(firstName) : cable.first();
						String secondName = cable.second().name();
						RfPort newSecond = forSwitching.containsKey(to) ? forSwitching.get(to).item.rfPort(secondName) : cable.second();

						Connector<RfPort> newCable = new Connector<>(newFirst, newSecond);
						system.joint.rfCables.set(i, newCable);
					}
				}
			}
		}
	}

	private static boolean isLteBaseband(Component component) {
		return DigitalUnitStandard.class.isAssignableFrom(component.getClass()) || DigitalUnitLte.class
				.isAssignableFrom(component.getClass()) || DigitalUnitBaseband.class.isAssignableFrom(component.getClass());
	}

	private static boolean isR503(Component component) {
		return R503.class.isAssignableFrom(component.getClass());
	}

	/**
	 * Merges LTE and WCDMA radios that do not have serial ID set (correctly).
	 * Conditions:
	 * a) there are two units visible on LTE, alpha (connected to DUS or R503) and beta (linked through radio interconnect)
	 *
	 * Two radio units (WCDMA and LTE beta) are merged if they :
	 * 1) Represent same bands
	 * 2) Represent same sectors (WCDMA sector are determined by lexicographical order inside band, not cell name)
	 */
	private static Map<InCabinetExtendedRadio, InCabinetExtendedRadio> mergeCabinetRadios(List<InCabinetExtendedRadio> radios,
	                                                                                      Set<Component> alphaRadios) {
		// result map
		Map<InCabinetExtendedRadio, InCabinetExtendedRadio> result = new HashMap<>();

		// all bands
		Map<Band, Set<InCabinetExtendedRadio>> bandToRadios = bandsToRadios(radios);

		// Go through radios band by band
		for (Entry<Band, Set<InCabinetExtendedRadio>> bandAndRadios : bandToRadios.entrySet()) {

			// Split radios to LTE and WCDMA
			List<InCabinetExtendedRadio> onLte = new ArrayList<>();
			List<InCabinetExtendedRadio> onWcdma = new ArrayList<>();
			for (InCabinetExtendedRadio radio : bandAndRadios.getValue()) {

				if (radio.lte() != null)
					onLte.add(radio);
				else if (radio.wcdma() != null)
					onWcdma.add(radio);
				else
					throw new IllegalArgumentException(radio.toString());
			}

			// Sort radios by location - this determines sector
			onLte.sort(DumpUtil.RadioInGroupComparator);
			onWcdma.sort(DumpUtil.RadioInGroupComparator);

			if (onLte.size() != 2 * onWcdma.size())
				continue;

			for (int i = 0; i < onWcdma.size(); i++) {
				InCabinetExtendedRadio firstLte = onLte.get(2 * i);
				InCabinetExtendedRadio secondLte = onLte.get(2 * i + 1);

				boolean sameCells = firstLte.lte().normalizedCell.equals(secondLte.lte().normalizedCell);
				if (!sameCells) continue;

				boolean alphaAndBeta = alphaRadios.contains(firstLte.item) ^ alphaRadios.contains(secondLte.item);
				if (!alphaAndBeta) continue;

				InCabinetExtendedRadio beta = alphaRadios.contains(firstLte.item) ? secondLte : firstLte;

				InCabinetExtendedRadio wcdmaRadio = onWcdma.get(i);

				boolean alreadyMerged = beta == wcdmaRadio;
				if (alreadyMerged) continue;

				// Do the merge
				InCabinetExtendedRadio merge = mergeRadios(beta, wcdmaRadio);

				result.put(beta, merge);
				result.put(wcdmaRadio, merge);
			}
		}
		return result;
	}

	static Map<Band, Set<InCabinetExtendedRadio>> bandsToRadios(List<InCabinetExtendedRadio> radios) {
		Map<Band, Set<InCabinetExtendedRadio>> bandToRadios = new HashMap<>();
		for (InCabinetExtendedRadio radio : radios) {
			bandToRadios.computeIfAbsent(radio.band, x -> new HashSet<>()).add(radio);
		}
		return bandToRadios;
	}

	// Merges two radios
	private static InCabinetExtendedRadio mergeRadios(InCabinetExtendedRadio lte, InCabinetExtendedRadio wcdma) {
		InCabinetExtendedRadio merge = new InCabinetExtendedRadio(lte.band, Arrays.asList(lte.lte(), wcdma.wcdma()));
		merge.item = lte.item;
		return merge;
	}

}
