package hr.hashcode.cables.equip.bind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.bind.Extractor.Corset;
import hr.hashcode.cables.equip.bind.Extractor.CorsetStruct;
import hr.hashcode.cables.equip.bind.JointNodesInfo.DumpType;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.EcBusConn;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawAntFeederCable;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawAntenna;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawAntennaBranch;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawBoardPositions;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawCabinet;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawCoordinates;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawDigitalCable;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawDuw;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawEcBus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawEquipmentSupportFunction;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawExternalNode;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawHwUnit;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawIub;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawManagedElement;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawPiu;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawPiuLink;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRadio;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRbsSubrack;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRbsSynchronization;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRemoteRuw;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRuw;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawSector;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawSectorAntenna;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawSubrack;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawSubrackProdType;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RbsSubrackLocation;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RiUnit;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.SerialProductInfo;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.SubrackLocation;
import hr.hashcode.cables.equip.comp.BandTech;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.Version;
import hr.hashcode.cables.util.Util;

public class RbsBind {

	private static final int factor = 1 << 24;

	private static double round(double value) {
		int digits = 6;
		double factor = Math.pow(10, digits);
		return (long) (value * factor) / factor;
	}

	private static double longitude(int longitude) {
		return round(longitude * 360. / factor);
	}

	private static double latitude(int latitude, LatHemisphere hemisphere) {
		return round(hemisphere.sign * latitude * 180. / factor);
	}

	private static SerialProductInfo productInfo(ProductData productData) {
		String name = productData.productName();
		String number = productData.productNumber();
		String serial = productData.serialNumber();
		return new SerialProductInfo(name, number, serial);
	}

	private final RefKeep<EcBus, RawEcBus> ecBuses = new RefKeep<>();
	private final RefKeep<Cabinet, RawCabinet> cabinets = new RefKeep<>();
	private final RefKeep<Slot, RawDuw> slottedDuws = new RefKeep<>();
	private final RefKeep<PlugInUnit, RawDuw> duws = new RefKeep<>();
	private final RefKeep<Corset, RiUnit> riUnits = new RefKeep<>();

	private EcBusConn ecConn(Corset corset) {
		List<EcPort> ecPorts = corset.children(EcPort.class);
		if (ecPorts.size() == 0)
			return null;
		if (ecPorts.size() > 1)
			throw new IllegalArgumentException(corset.toString());
		EcPort ecPort = ecPorts.get(0);
		EcBus ecBus = ecPort.ecBusRef();
		return new EcBusConn(ecBuses.get(ecBus), ecPort.hubPosition());
	}

	private static RawPiu piuType(PiuType piuType) {
		RawPiu rawPiu = new RawPiu();
		rawPiu.productName = piuType.productData().productName();
		rawPiu.productNumber = piuType.productData().productNumber();
		rawPiu.role = String.valueOf(piuType.role());
		rawPiu.boardWidth = piuType.boardWidth();
		return rawPiu;
	}

	public SingleNodeInfo start(NodeState state) {

		Extractor extractor = new Extractor(state, RbsBind.class);

		SingleNodeInfo info = new SingleNodeInfo();
		info.type = Version.Type.RBS;
		for (EcBus bus : extractor.extract(EcBus.class)) {
			RawEcBus raw = new RawEcBus();
			info.ecBuses.add(raw);
			raw.name = bus.name();
			raw.connectionType = bus.connectionType();
			raw.ecBusConnectorRef = String.valueOf(bus.ecBusConnectorRef());
			ecBuses.register(bus, raw);
		}

		String managedElementProductName = null;
		for (ManagedElement me : extractor.extract(ManagedElement.class)) {
			info.node = new RawManagedElement();
			info.node.fdn = state.fdn;
			info.node.productName = me.productName();
			info.node.productNumber = me.productNumber();
			managedElementProductName = me.productName();
			info.node.site = me.site();
			info.node.logicalName = me.logicalName();
			info.name = me.userLabel();
		}

		for (EquipmentSupportFunction eq : extractor.extract(EquipmentSupportFunction.class))
			info.eqSupport = new RawEquipmentSupportFunction(eq.supportSystemControl());

		List<Cabinet> myCabinets = extractor.extract(Cabinet.class);
		for (Cabinet cabinet : myCabinets) {
			String sharedId = cabinet.sharedCabinetIdentifier();

			ProductData data = cabinet.productData();
			String identifier = cabinet.cabinetIdentifier();
			String productName = BindUtil.productName(data.productName(), identifier, managedElementProductName, myCabinets.size());
			SerialProductInfo prodInfo = new SerialProductInfo(productName, data.productNumber(), data.serialNumber());

			RawCabinet raw = new RawCabinet(identifier, sharedId, prodInfo);
			info.cabinets.add(raw);
			this.cabinets.register(cabinet, raw);
		}

		for (PlugInUnit unit : extractor.extract(PlugInUnit.class)) {
			Cabinet cabinet = unit.positionRef();
			RawDuw raw = new RawDuw();
			raw.ecConn = ecConn(unit);

			raw.positionRef = this.cabinets.get(cabinet);
			Slot slot = unit.parent();

			SubrackLocation location = new SubrackLocation(slot.parent().name(), slot.name());
			raw.location = location;
			raw.productInfo = productInfo(slot.productData());
			raw.piu = piuType(unit.piuType());

			this.slottedDuws.register(slot, raw);
			this.duws.register(unit, raw);
			this.riUnits.register(unit, raw);
			info.duws.add(raw);
		}

		for (Subrack subrack : extractor.extract(Subrack.class)) {
			RawSubrack raw = new RawSubrack();
			info.subracks.add(raw);
			raw.cabinetPosition = subrack.cabinetPosition();
			raw.subrackNumber = subrack.subrackNumber();
			raw.subrackPosition = subrack.subrackPosition();
			raw.subrackProdType = new RawSubrackProdType();
			AdminProductData prodData = subrack.subrackProdTypeRef().productData();
			raw.subrackProdType.productInfo = prodData.productInfo();
			raw.subrackProdType.productName = prodData.productName();
			raw.subrackProdType.productNumber = prodData.productNumber();
			raw.transmisionType = String.valueOf(subrack.subrackProdTypeRef().transmissionType());

			RawBoardPositions rawBoard = new RawBoardPositions();
			raw.subrackProdType.boardPositions = rawBoard;
			BoardTypePositions board = subrack.subrackProdTypeRef().boardPositions();
			rawBoard.cmxbPositionA = board.cmxbPositionA();
			rawBoard.cmxbPositionB = board.cmxbPositionB();
			rawBoard.scuPositionA = board.scuPositionA();
			rawBoard.scuPositionB = board.scuPositionB();
			rawBoard.tuPositionA = board.tuPositionA();
			rawBoard.tuPositionB = board.tuPositionB();
		}

		for (RbsSubrack subrack : extractor.extract(RbsSubrack.class)) {
			RawRbsSubrack raw = new RawRbsSubrack();
			info.rbsSubracks.add(raw);
			raw.noOfSlots = subrack.noOfSlots();
			raw.subrackPosition = subrack.subrackPosition();
		}

		Map<AuxPlugInUnit, RawRadio> unitToRadio = new IdentityHashMap<>();
		for (AuxPlugInUnit unit : extractor.extract(AuxPlugInUnit.class)) {
			AuType auType = unit.auType();
			if (!auType.radio)
				continue;

			RawRuw radio;
			if (auType.remote) {
				RawRemoteRuw remote = new RawRemoteRuw();
				radio = remote;
				info.remotes.add(remote);
			} else {
				RawRuw ruw = new RawRuw();
				radio = ruw;
				info.radios.add(ruw);
			}
			radio.name = unit.name();
			radio.auType = String.valueOf(auType);
			radio.plugInUnit = String.valueOf(unit.plugInUnitRef1());
			radio.position = unit.position();
			radio.productInfo = new SerialProductInfo(unit.productName(), unit.productNumber(), unit.serialNumber());
			radio.dumpType = DumpType.RBS;

			radio.piu = piuType(unit.piuType());

			if (unit.positionCoordinates() != null) {
				double longitude = 0.000001 * unit.positionCoordinates().longitude();
				double latitude = 0.000001 * unit.positionCoordinates().latitude();
				radio.positionCoordinates = new RawCoordinates(longitude, latitude);
			}

			radio.positionRef = cabinets.get(unit.positionRef());
			radio.uniqueHwId = unit.uniqueHwId();

			Corset parent = unit.parent();
			if (parent instanceof RbsSlot) {
				RbsSlot rbsSlot = (RbsSlot) parent;
				radio.slotPosition = rbsSlot.slotPosition();
				radio.location = new RbsSubrackLocation(rbsSlot.parent().name(), rbsSlot.name());
			}
			this.riUnits.register(unit, radio);
			unitToRadio.put(unit, radio);
		}

		for (InterPiuLink link : extractor.extract(InterPiuLink.class)) {
			Slot slot1 = link.primaryPiuSlot();
			Slot slot2 = link.secondaryPiuSlot();

			RawPiuLink raw = new RawPiuLink(slottedDuws.get(slot1), slottedDuws.get(slot2));
			info.piuLinks.add(raw);
		}

		for (ExternalNode ext : extractor.extract(ExternalNode.class)) {
			RawExternalNode raw = new RawExternalNode();
			raw.fdn = ext.fullDistinguishedName();
			raw.informationOnly = ext.informationOnly();
			raw.logicalName = ext.logicalName();
			raw.radioAccessTechnology = String.valueOf(ext.radioAccessTechnology());
			raw.supportSystemControl = String.valueOf(ext.supportSystemControl());
			info.extNodes.add(raw);
			raw.ecBusConn = ecConn(ext);
		}

		for (Sector sector : extractor.extract(Sector.class)) {
			RawSector raw = new RawSector();
			info.sectors.add(raw);
			raw.band = sector.band();
			raw.beamDirection = sector.beamDirection();
			double latitude = latitude(sector.latitude(), sector.latHemisphere());
			double longitude = longitude(sector.longitude());
			raw.coordinates = new RawCoordinates(longitude, latitude);
			raw.mixedModeRadio = sector.mixedModeRadio();
			for (Carrier carrier : sector.children(Carrier.class))
				raw.dlBandwidth = carrier.dlBandwidth();
			raw.sectorAntennas = Arrays.stream(sector.sectorAntennasRef())
					.map(String::valueOf)
					.collect(Collectors.toList());
		}

		for (SectorAntenna antenna : extractor.extract(SectorAntenna.class)) {
			RawSectorAntenna rawAntenna = new RawSectorAntenna();
			rawAntenna.antennaType = antenna.antennaType();
			rawAntenna.beamDirection = antenna.beamDirection();
			rawAntenna.branches = new ArrayList<>();
			for (AntennaBranch branch : antenna.children(AntennaBranch.class)) {
				RawAntennaBranch rawBranch = new RawAntennaBranch();
				rawAntenna.branches.add(rawBranch);
				rawBranch.branchName = branch.branchName();
				rawBranch.fqBandHighEdge = branch.fqBandHighEdge();
				rawBranch.fqBandLowEdge = branch.fqBandLowEdge();
			}
			info.antennas.add(rawAntenna);
		}

		Map<SectorAntenna, RawAntenna> rawAntennas = new IdentityHashMap<>();
		for (SectorAntenna antenna : extractor.extract(SectorAntenna.class)) {
			RawAntenna raw = new RawAntenna();
			raw.name = antenna.name();
			for (AntennaBranch branch : antenna.children(AntennaBranch.class))
				raw.branches.add(branch.branchName());
			info.rawAntennas.add(raw);
			rawAntennas.put(antenna, raw);
		}

		for (DigitalCable cable : extractor.extract(DigitalCable.class)) {
			RiUnit unit1 = riUnits.get(cable.connectedToObjectARef());
			RiUnit unit2 = riUnits.get(cable.connectedToObjectBRef());
			String port1 = cable.objectAConnector().name;
			String port2 = cable.objectBConnector().name;

			RawDigitalCable raw = new RawDigitalCable(unit1, port1, unit2, port2);
			info.digitalCables.add(raw);
		}

		Map<AntennaBranch, Set<Sector>> branchToSector = new IdentityHashMap<>();

		for (Sector sector : extractor.extract(Sector.class))
			for (SectorAntenna antenna : sector.sectorAntennasRef())
				for (AntennaBranch branch : antenna.children(AntennaBranch.class))
					branchToSector.computeIfAbsent(branch, x -> Util.identitySet()).add(sector);

		Map<Sector, Set<RawRadio>> sectorToRadio = new IdentityHashMap<>();
		for (AntFeederCable cable : extractor.extract(AntFeederCable.class)) {
			AntennaBranch branch = cable.antennaBranchRef();
			Corset other = cable.connectedToObjectARef();
			if (other instanceof AuxPlugInUnit) {
				AuxPlugInUnit unit = (AuxPlugInUnit) other;
				RawRadio radio = unitToRadio.get(unit);
				Set<Sector> sectors = branchToSector.getOrDefault(branch, Collections.emptySet());
				for (Sector sector : sectors) {
					radio.bands.add(BandTech.wcdma(sector.band()));
					sectorToRadio.computeIfAbsent(sector, x -> Util.identitySet()).add(radio);
				}
			}
		}

		for (RbsLocalCell cell : extractor.extract(RbsLocalCell.class)) {
			List<Carrier> carriers = new ArrayList<>();
			if (cell.carriersRef() != null)
				carriers.addAll(Arrays.asList(cell.carriersRef()));
			else if (cell.carrierRef() != null)
				carriers.add(cell.carrierRef());
			for (Carrier carrier : carriers) {
				Sector sector = carrier.parent();
				for (RawRadio radio : sectorToRadio.getOrDefault(sector, Collections.emptySet())) {
					radio.cells.add(cell.name());
				}
			}
		}

		for (AntFeederCable cable : extractor.extract(AntFeederCable.class)) {
			RawAntFeederCable raw = new RawAntFeederCable();
			info.antennaCables.add(raw);
			raw.antenna = rawAntennas.get(cable.antennaBranchRef().parent());
			raw.antennaPort = cable.antennaBranchRef().branchName();
			raw.radio = unitToRadio.get(cable.connectedToObjectARef());
			raw.radioPort = cable.objectAConnector().name;
			info.antennaCables.add(raw);
		}

		for (HwUnit unit : extractor.extract(HwUnit.class)) {
			RawHwUnit raw = new RawHwUnit();
			raw.piu = piuType(unit.piuType());

			raw.position = unit.position();
			raw.positionRef = this.cabinets.get(unit.positionRef());
			raw.productInfo = productInfo(unit.productData());
			info.hwUnits.add(raw);
			raw.ecConn = ecConn(unit);
		}

		for (Iub iub : extractor.extract(Iub.class)) {
			RawIub raw = new RawIub();
			raw.rbsId = iub.rbsId();
			raw.userLabel = iub.userLabel();
			info.iubs.add(raw);
		}

		for (RbsSynchronization sync : extractor.extract(RbsSynchronization.class)) {
			RawRbsSynchronization raw = new RawRbsSynchronization();
			raw.masterTu = String.valueOf(sync.masterTu());
			raw.piu1 = this.duws.get(sync.plugInUnitRef1());
			raw.piu2 = this.duws.get(sync.plugInUnitRef2());
			raw.tim1 = String.valueOf(sync.timDeviceRef1());
			raw.tim2 = String.valueOf(sync.timDeviceRef2());
			info.syncs.add(raw);
		}

		return info;
	}

	public static BindTrace trace(NodeState state) {
		List<String> radioSerials = new ArrayList<>();
		List<String> externalFdns = new ArrayList<>();

		Extractor extractor = new Extractor(state, RbsBind.class);

		for (ExternalNode ext : extractor.extract(ExternalNode.class)) {
			String fdn = ext.fullDistinguishedName();
			externalFdns.add(fdn);
		}

		for (AuxPlugInUnit unit : extractor.extract(AuxPlugInUnit.class)) {
			AuType auType = unit.auType();
			if (auType.radio) {
				String serial = unit.serialNumber();
				radioSerials.add(serial);
			}
		}

		return new BindTrace(radioSerials, externalFdns);
	}

	private interface InterPiuLink extends Corset {
		Slot primaryPiuSlot();

		Slot secondaryPiuSlot();
	}

	private interface EquipmentSupportFunction extends Corset {
		Boolean supportSystemControl();
	}

	private enum RadioAccessTechnology {
		NOT_AVAILABLE, GSM, WCDMA, LTE, MULTI_STANDARD, TRANSMISSION;
	}

	private interface DigitalCable extends Corset {
		Corset connectedToObjectARef();

		Corset connectedToObjectBRef();

		ObjectAConnector objectAConnector();

		ObjectBConnector objectBConnector();
	}

	private enum ObjectAConnector {

		DATA_A, DATA_B, DATA_C, DATA_D, DATA_E, DATA_F,
		DATA_1, DATA_2,
		RI_A("A"), RI_B("B"), RI_C("C"), RI_D("D"), RI_E("E"), RI_F("F");

		private final String name;

		ObjectAConnector() {
			this.name = name();
		}

		ObjectAConnector(String name) {
			this.name = name;
		}

	}

	private interface Carrier extends Corset {
		@Override
		Sector parent();

		Corset[] dbccDeviceRef();

		Integer dlBandwidth();
	}

	private enum ObjectBConnector {
		DATA_1, DATA_2, RI_F("F"), RI_E("E");

		private final String name;

		ObjectBConnector() {
			this.name = name();
		}

		ObjectBConnector(String name) {
			this.name = name;
		}

	}

	private interface ManagedElement extends Corset {

		String logicalName();

		String userLabel();

		String productName();

		String productNumber();

		String site();
	}

	private interface ExternalNode extends Corset {

		String fullDistinguishedName();

		String logicalName();

		RadioAccessTechnology radioAccessTechnology();

		Boolean informationOnly();

		SupportSystemControl supportSystemControl();
	}

	private enum SupportSystemControl {
		FALSE, TRUE, NOT_AVAILABLE
	}

	private interface Sector extends Corset {
		Integer band();

		String beamDirection();

		Integer latitude();

		Integer longitude();

		SectorAntenna[] sectorAntennasRef();

		Boolean mixedModeRadio();

		LatHemisphere latHemisphere();

	}

	private enum LatHemisphere {
		NORTH(1), SOUTH(-1);

		private final int sign;

		private LatHemisphere(int sign) {
			this.sign = sign;
		}

	}

	private interface SectorAntenna extends Corset {

		Integer antennaType();

		String beamDirection();

	}

	private interface AntennaBranch extends Corset {

		@Override
		SectorAntenna parent();

		Integer fqBandHighEdge();

		String branchName();

		Integer fqBandLowEdge();

	}

	private interface Cabinet extends Corset {
		String sharedCabinetIdentifier();

		String cabinetIdentifier();

		ProductData productData();

	}

	private interface Slot extends Corset {
		@Override
		Subrack parent();

		ProductData productData();

		SlotState slotState();
	}

	private enum SlotState {
		FREE, USED, COVERED_BY_PIU;
	}

	private interface Subrack extends Corset {

		String cabinetPosition();

		SubrackType subrackType();

		SubrackProdType subrackProdTypeRef();

		Integer subrackNumber();

		String subrackPosition();

	}

	private interface RbsSubrack extends Corset {

		Integer noOfSlots();

		String subrackPosition();
	}

	private interface RbsSlot extends Corset {
		Integer slotPosition();
	}

	private interface SubrackProdType extends Corset {

		AdminProductData productData();

		BoardTypePositions boardPositions();

		Integer defNumberOfSlots();

		TransmissionType transmissionType();
	}

	private enum SubrackType {
		HUB, DEVICE;
	}

	private interface EcBus extends Corset {
		String connectionType();

		Corset ecBusConnectorRef();
	}

	private interface EcPort extends Corset {
		Integer cascadingOrder();

		EcBus ecBusRef();

		String hubPosition();

	}

	private enum AuType {
		/** Multi-Carrier Power Amplifier */
		MCPA,

		/** Capacitor Unit */
		CU,

		/** Fan unit */
		FAN,

		/** Power Control Unit */
		PCU,

		/** Antenna System Control unit */
		ASC,

		/** External Alarm Unit */
		XALM,

		/** Remote Electrical Tilt Unit */
		RETU(true),

		/** Power Amplifier Unit */
		PAU,

		/** Single carrier Antenna Interface Unit */
		SAIU,

		/** Remote Radio Unit */
		RRU(true),

		/** Radio Unit */
		RU(false),

		/** Filter Unit */
		FU,

		/** Fan Control Unit */
		FCU,

		/** Power Supply Unit */
		PSU,

		/** Battery Fuse Unit */
		BFU,

		/** Climate Unit */
		CLU,

		/** AISG Tower Mounted Amplifier Unit */
		ATMAU,

		/** AISG Remote Electrical Tilt Unit */
		ARETU(true),

		/** Radio Unit, WCDMA. Also represents a multi-standard RU (RUS) */
		RUW(false),

		/** Remote Radio Unit, WCDMA. Also represents a multi-standard RRU (RRUS) */
		RRUW(true),

		/** auXiliary Converter Unit */
		XCU,

		/** (AISG) Tower Mounted Frequency shifting amplifier */
		TMF,

		/** Antenna Integrated Radio */
		AIR(true),

		/** Indoor Radio Unit */
		IRU(true),

		/** Radio Dot */
		RD;

		private final boolean radio;
		private final boolean remote;

		AuType() {
			this.radio = false;
			this.remote = false;
		}

		AuType(boolean remote) {
			this.radio = true;
			this.remote = remote;
		}

	}

	private interface RbsLocalCell extends Corset {
		Carrier[] carriersRef();

		Carrier carrierRef();
	}

	private interface AuxPlugInUnit extends Corset {

		AuType auType();

		String uniqueHwId();

		PiuType piuType();

		String productNumber();

		String positionInformation();

		Integer position();

		HubPosition hubPosition();

		String productName();

		String serialNumber();

		String unitType();

		Cabinet positionRef();

		PositionStruct positionCoordinates();

		PlugInUnit plugInUnitRef1();
	}

	private enum HubPosition {
		A, B, C, E, D, F, G, H, I, J, A1, A2, A3, A4, A5, A6, A7, A8, B1, B2, B3, B4, B5, B6, B7, B8, NA;
	}

	private interface PlugInUnit extends Corset {

		@Override
		Slot parent();

		Cabinet positionRef();

		String unitType();

		PiuType piuType();

	}

	private interface HwUnit extends Corset {
		PiuType piuType();

		Integer position();

		Cabinet positionRef();

		ProductData productData();

	}

	private enum PiuRole {
		MP, BP, OTHERS, CMXB, GP;
	}

	private interface PiuType extends Corset {
		AdminProductData productData();

		Integer boardWidth();

		PiuRole role();
	}

	private interface ProductData extends CorsetStruct {
		String productName();

		String productNumber();

		String serialNumber();
	}

	private interface AdminProductData extends CorsetStruct {
		String productName();

		String productNumber();

		String productInfo();
	}

	private interface BoardTypePositions extends CorsetStruct {
		Integer cmxbPositionA();

		Integer cmxbPositionB();

		Integer scuPositionA();

		Integer scuPositionB();

		Integer tuPositionA();

		Integer tuPositionB();
	}

	private interface PositionStruct extends CorsetStruct {
		Integer longitude();

		Integer latitude();

		Integer altitude();
	}

	private enum TransmissionType {
		ATM_TRANSMISSION, ETHERNET10G, ETHERNET_CABLE, ETHERNET_1G_10G, ETHERNET_1G_10G_40G;
	}

	private interface AntFeederCable extends Corset {
		AntennaBranch antennaBranchRef();

		Corset connectedToObjectARef();

		AntFeederObjectAConnector objectAConnector();

	}

	private enum AntFeederObjectAConnector {

		NOT_CONNECTED, ANT_A("A"), ANT_B("B"), J1, H1, J2, H2, J3, H3, A1, A2, A3, A4, A5, A6, J1_AND_K1, J3_AND_K2, J4,
		J5_AND_K3, J6, RXA_IO, RXB_IO, INTERNAL, ANT_C, ANT_D;

		private final String name;

		private AntFeederObjectAConnector() {
			this.name = name();
		}

		private AntFeederObjectAConnector(String name) {
			this.name = name;
		}
	}

	private interface Iub extends Corset {
		Integer rbsId();

		String userLabel();
	}

	private interface RbsSynchronization extends Corset {
		MasterTu masterTu();

		PlugInUnit plugInUnitRef1();

		PlugInUnit plugInUnitRef2();

		Corset timDeviceRef1();

		Corset timDeviceRef2();
	}

	private enum MasterTu {
		NO_ACTIVE, PLUG_IN_UNIT_REF1, PLUG_IN_UNIT_REF2
	}

}
