package hr.hashcode.cables.equip.bind;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

import hr.hashcode.cables.equip.bind.JointNodesInfo.Cabinet;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Container;
import hr.hashcode.cables.equip.bind.JointNodesInfo.DumpType;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Extended;
import hr.hashcode.cables.equip.bind.JointNodesInfo.ExtendedEcOther;
import hr.hashcode.cables.equip.bind.JointNodesInfo.InCabinetExtendedRadio;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.CabinetLocation;
import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.DigitalUnitBaseband;
import hr.hashcode.cables.equip.comp.DigitalUnitLte;
import hr.hashcode.cables.equip.comp.DigitalUnitStandard;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.EcPort;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.MiniRbs;
import hr.hashcode.cables.equip.comp.MiniRbsFactory;
import hr.hashcode.cables.equip.comp.Port;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnit;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.Rbs;
import hr.hashcode.cables.equip.comp.RbsFactory;
import hr.hashcode.cables.equip.comp.RiPort;
import hr.hashcode.cables.equip.comp.Site;
import hr.hashcode.cables.equip.comp.Subrack;
import hr.hashcode.cables.equip.comp.SystemContext;
import hr.hashcode.cables.equip.comp.Technology;
import hr.hashcode.cables.equip.rules.PlacementRules;
import hr.hashcode.cables.util.Util;

public final class DumpToSite {

	private static boolean debug = false;

	enum SubrackChoice {
		Upper(0), Lower(1);

		private final int s;

		SubrackChoice(int s) {
			this.s = s;
		}

		public int toSubrack() {
			return s;
		}

		public SubrackChoice other() {
			if (this == Upper)
				return Lower;
			else
				return Upper;
		}
	}

	private static final List<String> miniTypes = Collections.singletonList("RBS6601");
	static final String Rbs6601_1 = "1/BFL 901 009/1";
	static final String Rbs6601_2 = "1/BFL 901 009/4";

	public static class HwPlacement {
		public final Site site;
		public final List<String> warnings;

		HwPlacement(Site site, List<String> warnings) {
			this.warnings = warnings;
			this.site = site;
		}
	}

	public static HwPlacement hwPlacement(CabinetSystem system, RbsFactory macroFactory, MiniRbsFactory miniFactory,
	                                      PlacementRules placementRules) {
		Site site = new Site();
		SystemContext context = new SystemContext(system);
		List<String> warnings = new ArrayList<>();

		List<MacroRbs> rbses = new ArrayList<>();
		List<MiniRbs> minis = new ArrayList<>();
		for (Cabinet logicalCabinet : system.joint.cabinets) {
			Container container = logicalCabinet.container;
			if (debug) System.out.println("Product number for " + logicalCabinet.type + " is " + logicalCabinet.productNumber);

			boolean macro = !miniTypes.contains(logicalCabinet.type);
			if (macro) {

				// One logical cabinet = one real cabinet
				MacroRbs rbsCab;
				if (!logicalCabinet.type.equals("RBS6102")) {
					rbsCab = macroFactory.create(logicalCabinet.type);
					if (debug) System.out.println("Created " + logicalCabinet.name + " / " + logicalCabinet.type);
				} else {
					// Determine RBS6102 subtype
					final String subtype = decide6102subtype(system, logicalCabinet);
					rbsCab = macroFactory.create(logicalCabinet.type, subtype);
					if (debug) System.out.println("Created " + logicalCabinet.name + " / 6102 " + subtype);
				}
				List<String> localWarnings = fillMacro(rbsCab, container, placementRules, context);
				warnings.addAll(localWarnings);
				rbses.add(rbsCab);
			} else {
				// Consider splitting into multiple real cabinets. This happens, for example, when dumps indicate that DUW and R503 are
				// in the same cabinet, even though there is no place for both in single 6601
				int numDuws = container.duws.size();
				int numOther = container.r503s.size() + container.bbs.size() + container.duses.size();

				// Create new cabinets
				// Implicit assumption: all real cabinets in one logical cabinet have same subtype
				List<MiniRbs> localMinis = new ArrayList<>();
				int numberOfCabinets = numDuws + (int) Math.ceil(0.5 * numOther);
				for (int i = 0; i < numberOfCabinets; i++) {
					final String subtype = decide6601subtype(logicalCabinet, system);
					MiniRbs newRbs = miniFactory.create("RBS6601", subtype);
					localMinis.add(newRbs);
				}

				// Place HW by cabinets. First DUWs, then BBs, DUSes and R503s
				int cabinetIndex = 0;
				for (Extended<DigitalUnitWcdma> duw : container.duws) {
					MiniRbs myMini = localMinis.get(cabinetIndex);
					myMini.set(duw.item);
					cabinetIndex += 1;
				}
				List<Component> othersList = new ArrayList<>();
				container.bbs.forEach(x -> othersList.add(x.item));
				container.duses.forEach(x -> othersList.add(x.item));
				container.r503s.forEach(x -> othersList.add(x.item));
				for (int i = 0; i < numOther; i += 2) {
					MiniRbs myMini = localMinis.get(cabinetIndex);

					Component component = othersList.get(i);
					List<String> localWarnings1 = fillMini(myMini, component, "1");
					warnings.addAll(localWarnings1);

					if (i + 1 < othersList.size()) {
						Component otherComponent = othersList.get(i + 1);
						List<String> localWarnings2 = fillMini(myMini, otherComponent, "2");
						warnings.addAll(localWarnings2);
					}

					cabinetIndex += 1;
				}

				minis.addAll(localMinis);
			}
		}
		site.rbses.addAll(rbses);
		site.miniRbses.addAll(minis);

		// Move HW that is outside of the cabinet
		site.antennas.addAll(system.joint.antennas);
		system.joint.container.remotes.forEach(r -> site.remotes.add(r.item));
		for (Cabinet cabinet : system.joint.cabinets) {
			cabinet.container.remotes.forEach(r -> site.remotes.add(r.item));
		}

		transferCables(system, site);

		// Fill auxiliary mapping structures
		for (Entry<DigitalUnit, Set<Band>> entry : DumpUtil.digitalToBands(system).entrySet())
			site.duToBands.put(entry.getKey(), entry.getValue());
		for (Entry<R503, Set<Band>> entry : DumpUtil.r503ToBands(system).entrySet())
			site.r503ToBands.put(entry.getKey(), entry.getValue());
		for (Entry<Band, Set<RadioUnitBase>> entry : DumpUtil.radiosByBand(system).entrySet())
			site.radios.put(entry.getKey(), entry.getValue());

		return new HwPlacement(site, warnings);
	}

	private static String decide6102subtype(CabinetSystem system, Cabinet logicalCabinet) {
		final String subtype;
		String prodNum = logicalCabinet.productNumber;
		Pattern pattern = Pattern.compile(".*R(\\d).");
		if (prodNum != null && pattern.matcher(prodNum).matches()) { // See Ericsson presentation, slide 41
			char match = pattern.matcher(prodNum).group(1).charAt(0);
			if (match >= '5') {
				subtype = "2";
			} else {
				subtype = "1";
			}
		} else {
			subtype = DumpUtil.ecPortsContain(system, "IN") ? "1" : "2";
		}
		return subtype;
	}

	private static String decide6601subtype(Cabinet logicalCabinet, CabinetSystem system) {
		String subtype = null;
		String prodNum = logicalCabinet.productNumber;
		if (prodNum != null && (prodNum.equals(Rbs6601_1) || prodNum.equals(Rbs6601_2))) {
			// try through RBS product number first
			if (prodNum.equals(Rbs6601_1)) {
				subtype = "1";
			} else {
				subtype = "2";
			}
		} else {
			// try through SUP product number second
			for (ExtendedEcOther ecOther : logicalCabinet.container.ecOthers) {
				if (ecOther.name.contains("SUP") && ecOther.productNumber != null) {
					String supProdNum = ecOther.productNumber;
					switch (supProdNum) {
						case Rbs6601_1:
							subtype = "1";
							break;
						case Rbs6601_2:
							subtype = "2";
							break;
						default:
							// Nothing to assign, subtype remains null
							break;
					}
				}
			}

			if (subtype == null)
				subtype = DumpUtil.ecPortsContain(system, "A") ? "1" : "2";
		}
		return subtype;
	}

	// Assumes (and does not check) that system and site represent the same thing
	private static void transferCables(CabinetSystem system, Site site) {
		site.riConnectors.addAll(system.joint.riCables);
//		site.rfConnectors.addAll(system.joint.rfCables);
		site.idlConnectors.addAll(system.joint.idlCables);

		List<Extended> allHw = DumpUtil.getAllHardware(system);
		IdentityHashMap<Port, Component> portToUnit = DumpUtil.calculatePortToUnit(system, allHw);
		IdentityHashMap<Component, Rbs> componentToRbs = DumpUtil.componentToRbs(site);

		for (Connector<EcPort> cable : system.joint.ecCables) {
			Component first = portToUnit.get(cable.first());
			Component second = portToUnit.get(cable.second());

			if (first == null && second == null) {
				continue; // check this in more detail
			}

			Component comp = first != null ? first : second;
			Rbs rbs = componentToRbs.get(comp);
			if (rbs == null) continue; // This is here temporarily

			EcPort ecPort = getRbsEcPortByName(rbs, second.name());
			if (ecPort != null) {
				EcPort originEc = first != null ? cable.first() : cable.second();
				Connector<EcPort> newCable = new Connector<>(originEc, ecPort);
				site.ecConnectors.add(newCable);
			}
		}
	}

	private static EcPort getRbsEcPortByName(Rbs rbs, String portName) {
		if (rbs instanceof MacroRbs) {
			MacroRbs macroRbs = (MacroRbs) rbs;
			return macroRbs.shu().ecPort(portName);
		} else if (rbs instanceof MiniRbs) {
			MiniRbs miniRbs = (MiniRbs) rbs;
			return miniRbs.ecPorts().port(portName);
		} else {
			throw new IllegalArgumentException(rbs.toString());
		}
	}

	private static List<String> fillMini(MiniRbs rbs, Component component, String slot) {
		List<String> warnings = new ArrayList<>();
		if (component.getClass() == DigitalUnitStandard.class) {
			DigitalUnitStandard dus = (DigitalUnitStandard) component;
			rbs.set(slot, dus);
		} else if (component.getClass() == DigitalUnitLte.class) {
			DigitalUnitLte dul = (DigitalUnitLte) component;
			rbs.set(slot, dul);
		} else if (component.getClass() == DigitalUnitBaseband.class) {
			DigitalUnitBaseband bb = (DigitalUnitBaseband) component;
			rbs.set(slot, bb);
		} else if (component.getClass() == R503.class) {
			R503 r = (R503) component;
			rbs.set(slot, r);
		} else {
			throw new IllegalArgumentException(component.toString());
		}
		return warnings;
	}

	private static List<String> fillMacro(MacroRbs rbsCab, Container container, PlacementRules placementRules, SystemContext context) {
		List<String> warnings = new ArrayList<>();

		// Place radios and DUWS
		Map<Band, Set<InCabinetExtendedRadio>> bandToRadio = DumpNormalize.bandsToRadios(container.radios);
		if (bandToRadio.keySet().size() == 0) {
			// No radios inside cabinet, only RRU units potentially connected to DUWs
			switch (container.duws.size()) {
				case 0:
					// Nothing to place
					break;
				case 1: {
					// Place the only duw in upper subrack
					DigitalUnitWcdma firstDuw = container.duws.get(0).item;
					Subrack subrack = rbsCab.subracks().get(SubrackChoice.Upper.toSubrack());
					subrack.setDigital(firstDuw);
					break;
				}
				case 2: {
					DigitalUnitWcdma firstDuw = container.duws.get(0).item;
					DigitalUnitWcdma secondDuw = container.duws.get(1).item;

					// sort digital units by bands
					IdentityHashMap<DigitalUnit, Set<Band>> toBands = context.digitalToBands();
					Set<Band> firstBands = toBands.get(firstDuw);
					Set<Band> secondBands = toBands.get(secondDuw);

					Subrack upper = rbsCab.subracks().get(SubrackChoice.Upper.toSubrack());
					Subrack lower = rbsCab.subracks().get(SubrackChoice.Lower.toSubrack());

					for (Band band : Band.values()) {
						// This is OK because bands must be disjoint sets
						if (firstBands.contains(band)) {
							upper.setDigital(firstDuw);
							lower.setDigital(secondDuw);
							break;
						} else if (secondBands.contains(band)) {
							upper.setDigital(secondDuw);
							lower.setDigital(firstDuw);
							break;
						}
					}
					break;
				}
			}
		} else if (bandToRadio.keySet().size() == 1) {
			List<InCabinetExtendedRadio> radios = new ArrayList<>();
			bandToRadio.values().forEach(radios::addAll);
			DumpUtil.require(radios.size() <= 6, radios);

			// Decide where to put radio group based on RbsSubrack
			SubrackChoice subrackChoice = decideOnSubrack(radios);
			Subrack subrack = rbsCab.subracks().get(subrackChoice.toSubrack());

			// How many duws are there?
			DigitalUnitWcdma duw = (container.duws.size() >= 1 ? container.duws.get(0).item : null);
			DigitalUnitWcdma remainder = (container.duws.size() >= 2 ? container.duws.get(1).item : null);

			// Swap if remainder DUW is actually connected to RUs
			if (remainder != null && context.areConnected(remainder, Util.map(radios, x -> x.item))) {
				DigitalUnitWcdma temp = remainder;
				remainder = duw;
				duw = temp;
			}

			placeRadiosAndDuw(duw, radios, subrack);

			// In case there is other DUW (perhaps connected to outside radio units)
			if (remainder != null) {
				Subrack otherSubrack = rbsCab.subracks().get(subrackChoice.other().toSubrack());
				otherSubrack.setDigital(remainder);
			}
		} else if (bandToRadio.keySet().size() == 2) {

			// When there are two radio groups, select first and decide where to place it based on 'decideOnSubrack'.
			// Place the other where the first isn't

			Iterator<Entry<Band, Set<InCabinetExtendedRadio>>> iterator = bandToRadio.entrySet().iterator();
			Entry<Band, Set<InCabinetExtendedRadio>> first = iterator.next();
			Entry<Band, Set<InCabinetExtendedRadio>> second = iterator.next();

			// Order first and second by band priorities
			if (first.getKey().compareTo(second.getKey()) > 0) {
				Entry<Band, Set<InCabinetExtendedRadio>> temp = first;
				first = second;
				second = temp;
			}

			// Main placement logic goes here:
			// 0) Take radios by band priority. There are two radio groups in this branch
			// 1) For first group, decide based on location (this depends on dump type: either subrack, slot or radio name)
			// 2) Figure out how many DUWs there are, and which one matches first group
			// 3) Place first radio group up or down, corresponding DUW (if exists)
			// 4) Place second radio group where the first isn't, with corresponding DUW (if exists)

			List<RadioUnit> firstRadiosRaw = Util.map(first.getValue(), x -> x.item);
			List<RadioUnit> secondRadiosRaw = Util.map(second.getValue(), x -> x.item);
			List<InCabinetExtendedRadio> radios = new ArrayList<>(first.getValue());
			DumpUtil.require(radios.size() <= 6, radios);

			// Decide where to put radio group based on RbsSubrack
			SubrackChoice firstRadiosPos = decideOnSubrack(radios);

			// Decide where to place DUWs
			DigitalUnitWcdma duwA = null;
			DigitalUnitWcdma duwB = null;
			if (container.duws.size() >= 1) duwA = container.duws.get(0).item;
			if (container.duws.size() >= 2) duwB = container.duws.get(1).item;
			TwoDuws twoDuws = decideOnDuw(duwA, duwB, firstRadiosPos, firstRadiosRaw, secondRadiosRaw, context);

			// Place first
			Subrack subrack = rbsCab.subracks().get(firstRadiosPos.toSubrack());
			placeRadiosAndDuw(twoDuws.getDuw(firstRadiosPos), radios, subrack);

			// Place second
			List<InCabinetExtendedRadio> secondRadios = new ArrayList<>(second.getValue());
			DumpUtil.require(secondRadios.size() <= 6, secondRadios);

			SubrackChoice secondRadiosPos = firstRadiosPos.other();
			Subrack secondSubrack = rbsCab.subracks().get(secondRadiosPos.toSubrack());

			placeRadiosAndDuw(twoDuws.getDuw(secondRadiosPos), secondRadios, secondSubrack);
		} else {
			throw new IllegalArgumentException(bandToRadio.toString());
		}

		switch (container.bbs.size()) {
			case 0:
				// nothing to place
				break;
			case 1:
				DigitalUnitBaseband bb = container.bbs.get(0).item;
				placementRules.placeBb(bb, rbsCab);
				break;
			default:
				throw new IllegalArgumentException(container.duses.toString());
		}

		// Place LTE DUSES
		switch (container.duses.size()) {
			case 0:
				// nothing to place
				break;
			case 1:
				DigitalUnitStandard dus = container.duses.get(0).item;
				placementRules.placeDus(dus, rbsCab);
				break;
			default:
				throw new IllegalArgumentException(container.duses.toString());
		}

		switch (container.r503s.size()) {
			case 0:
				// nothing to place
				break;
			case 1:
				Extended<R503> toPlace = container.r503s.get(0);
				placementRules.placeR503(toPlace.item, rbsCab);
				break;
			case 2:
				Extended<R503> toPlace1 = container.r503s.get(0);
				placementRules.placeR503(toPlace1.item, rbsCab);

				Extended<R503> toPlace2 = container.r503s.get(1);
				placementRules.placeR503(toPlace2.item, rbsCab);
				break;
			default:
				throw new IllegalArgumentException(container.r503s.toString());
		}
		return warnings;
	}

	private static void placeRadiosAndDuw(DigitalUnitWcdma duw, List<InCabinetExtendedRadio> radios, Subrack subrack) {

		// Do not count on input radios being ordered in any way - sort the radios as they should be in the subrack
		radios.sort(DumpUtil.RadioInGroupComparator);

		int multi = 1; // if this is 2, place to every other location
		int offset = 0; // if this is 1, start from second location

		boolean isWcdma = radios.stream().anyMatch(x -> x.wcdma() != null);
		boolean isLte = radios.stream().anyMatch(x -> x.lte() != null);
		boolean allSameTech = isWcdma ^ isLte;
		if (radios.size() <= 3 && allSameTech) {
			Technology tech = isLte ? Technology.LTE : Technology.WCDMA;

			multi = 2;
			offset = (tech == Technology.WCDMA) ? 0 : 1;
		}

		// Place radios
		for (int i = 0; i < radios.size(); i++) {
			int pos = multi * i + offset;

			// This should be OK since only RadioUnit should be inside cabinet
			RadioUnit radio = radios.get(i).item;
			subrack.setRadio(pos, radio);
		}

		// Place Duw next to it
		if (duw != null)
			subrack.setDigital(duw);
	}

	// L locations examples: RbsSubrack:1,RbsSlot=1, RbsSubrack:2,RbsSlot=1
	// W locations examples: RbsSubrack:1,RbsSlot=3, RbsSubrack:1,RbsSlot=5
	//
	// Empirically, LTE slots will be sequential and split between two subracks. Our best guess is that RbsSubrack=1 is upper, =2 is lower.
	// WCDMA slots all come in same subrack, with primary band in slots 3, 7, 11 and secondary band in slots 5, 9 and 13.
	// Bonus for WCDMA: it is even possible that there are two DUWs, not connected, each placing its radios in 3, 7 and 11, in same cabinet.
	// Only if 2 radio groups are on same DUW or 2 interconnected DUWs can we expect (3,7,11) and (5,9,13).
	//
	// Note that G2 will not have locations set and will be placed to 1 by default
	private static SubrackChoice decideOnSubrack(Collection<InCabinetExtendedRadio> radios) {
		for (InCabinetExtendedRadio radio : radios) {

			if (radio.lte() != null) {
				if (radio.lte().dumpType == DumpType.ERBS) {
					CabinetLocation location = radio.lte().location.get(); // OK to get if dump is ERBS
					if (location.subrack.equals("RbsSubrack:2")) return SubrackChoice.Lower;
				}
				if (radio.lte().dumpType == DumpType.MSRBS) {
					Pattern pattern = Pattern.compile(".*-2-[0-9]");
					if (pattern.matcher(radio.lte().radioName).matches()) {
						return SubrackChoice.Lower;
					}
				}
			}
			if (radio.wcdma() != null) {
				CabinetLocation location = radio.wcdma().location.get(); // OK to get if dump is WCDMA
				Pattern pattern = Pattern.compile("RbsSlot=(5|9|13)");
				if (pattern.matcher(location.subrack).matches()) {
					return SubrackChoice.Lower;
				}
			}
		}
		return SubrackChoice.Upper;
	}

	// This function matches given DUWs with given radios.
	// Example: if DUW A connects to first radio group, it is matched to its position (upper or lower) in return
	private static TwoDuws decideOnDuw(DigitalUnitWcdma duwA, DigitalUnitWcdma duwB, SubrackChoice firstSubrackPos,
	                                   List<RadioUnit> firstRadiosRaw,
	                                   List<RadioUnit> secondRadiosRaw,
	                                   SystemContext context) {
		if (duwA == null && duwB == null) {
			return TwoDuws.empty();
		}

		DumpUtil.require(duwA != null, duwB);

		// 'toFirst' should be read as: DUW A placed to first radio, DUW B to placed to second radio
		TwoDuws toFirst = TwoDuws.duwOnPosition(duwA, firstSubrackPos, duwB);
		TwoDuws toSecond = TwoDuws.duwOnPosition(duwA, firstSubrackPos.other(), duwB);

		RiPort portDuwA = duwA.riPort("A");
		RiPort portDuwB = duwB != null ? duwB.riPort("A") : null;

		// Each DUW, if present, can be connected to radio set in three different ways: through priority-connection (from port A),
		// through secondary-connection (from port D) or not at all (e.g. DUW is connected to remote units).
		if (context.areConnected(portDuwA, firstRadiosRaw)) {
			return toFirst;
		} else if (context.areConnected(portDuwA, secondRadiosRaw)) {
			return toSecond;
		} else if (context.areConnected(portDuwB, firstRadiosRaw)) {
			return toSecond;
		} else if (context.areConnected(portDuwB, secondRadiosRaw)) {
			return toFirst;
		} else // If we got this far, no DUW is connected through priority ports
			if (context.areConnected(duwA, firstRadiosRaw)) {
				return toFirst;
			} else if (context.areConnected(duwA, secondRadiosRaw)) {
				return toSecond;
			} else if (context.areConnected(duwB, firstRadiosRaw)) {
				return toSecond;
			} else if (context.areConnected(duwB, secondRadiosRaw)) {
				return toFirst;
			} else {
				return toFirst; // Both DUWs have only remotes
			}
	}

	private static class TwoDuws {
		DigitalUnitWcdma upper;
		DigitalUnitWcdma lower;

		private TwoDuws(DigitalUnitWcdma upper, DigitalUnitWcdma lower) {
			this.upper = upper;
			this.lower = lower;
		}

		DigitalUnitWcdma getDuw(SubrackChoice pos) {
			if (pos == SubrackChoice.Upper)
				return upper;
			else
				return lower;
		}

		static TwoDuws empty() {
			return new TwoDuws(null, null);
		}

		static TwoDuws duwOnPosition(DigitalUnitWcdma one, SubrackChoice onePos, DigitalUnitWcdma two) {
			if (onePos == SubrackChoice.Upper)
				return new TwoDuws(one, two);
			else
				return new TwoDuws(two, one);
		}
	}
}
