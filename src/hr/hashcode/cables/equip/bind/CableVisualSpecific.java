package hr.hashcode.cables.equip.bind;

import static hr.hashcode.cables.equip.bind.CabinetSystemVisual.getPointDistance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;

public class CableVisualSpecific extends Shape {
	@Override
	public com.sun.javafx.geom.Shape impl_configShape() {
		return null;
	}
	/*public static int LINE_OFFSET = 10;
	public static int HARDWARE_OFFSET = 8;
	public static int DIRECT_OFFSET = 10;
	public static Color DEFAULT_COLOR = Color.BLACK;
	
	static List<Node> collisionNodes = new ArrayList<>();
	static Map<UnitVisualController.Type, UnitVisualController.Type> directConnection = new HashMap<>();
	static Map<UnitVisualController.Type, Direction> hardwareTypeToStartDirection = new HashMap<>();

	static {
		directConnection.put(UnitVisualController.Type.RADIO_UNIT, UnitVisualController.Type.RADIO_UNIT);
		directConnection.put(UnitVisualController.Type.RADIO_UNIT_EXTERNAL, UnitVisualController.Type.RADIO_UNIT_EXTERNAL);
		directConnection.put(UnitVisualController.Type.RADIO_UNIT_SIMPLE, UnitVisualController.Type.RADIO_UNIT_SIMPLE);

		hardwareTypeToStartDirection.put(UnitVisualController.Type.RADIO_UNIT, Direction.UP);
		hardwareTypeToStartDirection.put(UnitVisualController.Type.RADIO_UNIT_SIMPLE, Direction.UP);
		hardwareTypeToStartDirection.put(UnitVisualController.Type.RADIO_UNIT_EXTERNAL, Direction.UP);
	}

	Line startLine;
	double endX;
	double endY;
	List<Line> polyLine = new ArrayList<>();
	Node scene;
	Color color;

	PortController port1;
	PortController port2;

	public enum Direction {
		LEFT,
		RIGHT,
		UP,
		DOWN,
		STAY
	}

	@Override
	public com.sun.javafx.geom.Shape impl_configShape() {
		return null;
	}

	public CableVisualSpecific(Node scene, PortController port1, PortController port2) {
		this(scene, port1, port2, DEFAULT_COLOR);
	}

	public CableVisualSpecific(Node scene, PortController port1, PortController port2, Color color) {
		this.scene = scene;
		this.color = color;
		this.port1 = port1;
		this.port2 = port2;
		this.polyLine = new ArrayList<>();
	}

	public static void setCollisionNodes(List<Node> collisionNodes) {
		CableVisualSpecific.collisionNodes = collisionNodes;
	}

	public void setColor(Color color) {
		polyLine.forEach(l -> l.setStroke(color));
	}

	private Bounds bounds(Node n) {
		return scene.sceneToLocal(n.localToScene(n.getBoundsInLocal()));
	}

	private boolean nodeHorizontal(Node node) {
		Bounds b = bounds(node);
		return b.getMaxX() - b.getMinX() > b.getMaxY() - b.getMinY();
	}

	public void connect() {
		Bounds b1 = bounds(port1.getView());
		double startX = x(b1);
		double startY = y(b1);

		Bounds b2 = bounds(port2.getView());
		endX = x(b2);
		endY = y(b2);

		if(isDirectConnection()) {
			startLine = getLine(startX, startY, endX, endY);
		} else {
			// try to connect direct

			if(hardwareTypeToStartDirection.containsKey(port1.getUnit().getType())) {
				//Line line1 = getLineInTheDirection(hardwareTypeToStartDirection.get((port1.getUnit().getType()), startX, startY));
			}
			if(nodeHorizontal(port1.getUnit().getView())) {
				// first line is horizontal
				appendLine(startX, startY, startX, endY);
				appendLine(startX, endY, endX, endY);
			} else {
				// first line is vertical;
				appendLine(startX, startY, endX, startY);
				appendLine(endX, startY, endX, endY);
			}
			//startLine = getStartLine();
			//connectStartLineToEnd();
		}
		if(startLine != null)
		polyLine.add(0, startLine);
	}


	public void registerConnection(Group g, BooleanProperty switach) {
		for (Line l : polyLine) {
			l.strokeWidthProperty().bind(Bindings.when(switach).then(2).otherwise(0));
		}
		g.getChildren().addAll(polyLine);
	}


	private boolean isDirectConnection() {
		return directConnection.containsKey(port1.getUnit().getType()) &&
					   directConnection.get(port1.getUnit().getType()) == port2.getUnit().getType() ||
					   directConnection.containsKey(port2.getUnit().getType()) &&
							   directConnection.get(port2.getUnit().getType()) == port1.getUnit().getType();
	}


	private double x(Bounds bounds) {
		return (bounds.getMaxX() + bounds.getMinX()) / 2;
	}

	private double y(Bounds bounds) {
		return (bounds.getMaxY() + bounds.getMinY()) / 2;
	}

	public void addLineToEnd(double x, double y) {
		appendLine(x, y, endX, endY);
	}

	public void appendLine(double startX, double startY, double endX, double endY) {
		if(startX == endX && startY == endY) {
			return;
		}
		if (polyLine.isEmpty()) {
			polyLine.add(getLine(startX, startY, endX, endY));
		} else {
			Line last = polyLine.get(polyLine.size() - 1);
			if (last.getEndX() == startX && last.getEndY() == startY) {
				Line newLine = getLine(startX, startY, endX, endY);
				if(getDirection(last) == getDirection(newLine)) {
					last.setEndX(newLine.getEndX());
					last.setEndY(newLine.getEndY());
				} else if(!oppositeDirections(getDirection(last), getDirection(newLine))) {
					polyLine.add(newLine);
				}
			}
		}
	}

	public Line getLine(double startX, double startY, double endX, double endY) {
		Line line = new Line();
		line.setStartX(startX);
		line.setStartY(startY);
		line.setEndX(endX);
		line.setEndY(endY);
		line.setStroke(color);
		return line;
	}

	private Line getStartLine() {
		Bounds b = scene.sceneToLocal(port1.getUnit().getView().localToScene(port1.getUnit().getView().getBoundsInLocal()));
		double startX = x(b);
		double startY = y(b);
		if(hardwareTypeToStartDirection.containsKey(port1.getUnit().getType())) {
			Direction direction = hardwareTypeToStartDirection.get(port1.getUnit().getType());
			return getLineInTheDirection(direction, startX, startY, getDistanceToHardware(direction, startX, startY, port1.getUnit()));
		}
		double dUp = getPointDistance(startX, startY, startX, b.getMinY());
		double dDown = getPointDistance(startX, startY, startX, b.getMaxY());
		double dLeft = getPointDistance(startX, startY, b.getMinX(), startY);
		double dRight = getPointDistance(startX, startY, b.getMaxX(), startY);
		if (Math.max(dUp, dDown) < Math.max(dLeft, dRight)) {
			//go vertical to the direction of endpoint
			double endToY = startY - endY < 0 ? startY + dDown + HARDWARE_OFFSET : startY - dUp - HARDWARE_OFFSET;
			return getLine(startX, startY, startX, endToY);
		} else {
			//go horizontal to the direction of endpoint
			double endToX = startX - endX < 0 ? startX + dRight + HARDWARE_OFFSET : startX - dLeft - HARDWARE_OFFSET ;
			return getLine(startX, startY, endToX, startY);
		}
	}

	private void connectStartLineToEnd() {
		Direction prevDirection = getDirection(startLine);

		double currentX = startLine.getEndX();
		double currentY = startLine.getEndY();

		Node currentHardware = null;

		while (!finished(currentX, currentY)) {
			boolean moved = false;
			// get horizontal direction
			Direction horizontal = getMustGoDirectionX(currentX);
			if (horizontal != Direction.STAY && !oppositeDirections(horizontal, prevDirection)) {
				// check if can go to direction and go
				currentHardware = geCollisionNode(currentX, currentY);
				if (currentHardware == null) {
					double length = getNextLineLength(horizontal, currentX, currentY);
					if (length > 0) {
						double endX = horizontal == Direction.RIGHT ? currentX + length : currentX - length;
						appendLine(currentX, currentY, endX, currentY);
						currentX = endX;
						prevDirection = getPreviousDirection();
						moved = true;
					}
				} else if (currentHardware == port2.getUnit().getView()) {
					connectToEnd(horizontal, prevDirection, currentX, currentY);
					break;
				}
			}
			Direction vertical = getMustGoDirectionY(currentY);
			if (vertical != Direction.STAY && !oppositeDirections(vertical, prevDirection)) {
				// check if can go to direction and go
				currentHardware = geCollisionNode(currentX, currentY);
				if (currentHardware == null) {
					double length = getNextLineLength(vertical, currentX, currentY);
					if (length > 0) {
						double endY = vertical == Direction.DOWN ? currentY + length : currentY - length;
						appendLine(currentX, currentY, currentX, endY);
						currentY = endY;
						prevDirection = getPreviousDirection();
						moved = true;
					}
				} else if (currentHardware == port2.getUnit().getView()) {
					connectToEnd(vertical, prevDirection, currentX, currentY);
					break;
				}
			}

			if (!moved && currentHardware != null) {
				// go around hardware
				if (addLinesAroundHardwareOrConnect(prevDirection, horizontal, currentX, currentY, scene, currentHardware) ||
							addLinesAroundHardwareOrConnect(prevDirection, vertical, currentX, currentY, scene, currentHardware)) {
					prevDirection = getPreviousDirection();
					currentX = getCurrentX();
					currentY = getCurrentY();
					moved = true;
				}
			}

			if (!moved) {
				// connect to the end
				if(!connectToEnd(horizontal, prevDirection, currentX, currentY) && !connectToEnd(vertical, prevDirection, currentX, currentY)) {
					addLineToEnd(currentX, currentY);
				}
				break;
			}
		}
	}

	private Line getLineInTheDirection(Direction direction, double startX, double startY, double length) {
		switch (direction) {

			case LEFT:
				return getLine(startX, startY, startX - length, startY);
			case RIGHT:
				return getLine(startX, startY, startX + length, startY);
			case UP:
				return getLine(startX, startY, startX, startY - length);
			case DOWN:
				return getLine(startX, startY, startX, startY + length);
			default:
				return getLine(startX, startY, startX, startY);
		}
	}

	private double getDistanceToHardware(Direction direction, double startX, double startY, UnitVisualController hw) {
		Bounds bounds = bounds(hw.getView());
		switch (direction) {

			case LEFT:
				return startX >= bounds.getMinX() ? startX - bounds.getMinX() : Double.MAX_VALUE;
			case RIGHT:
				return startX <= bounds.getMaxX() ? bounds.getMaxX() - startX : Double.MAX_VALUE;
			case UP:
				return startY >= bounds.getMinY() ? startY - bounds.getMinY() : Double.MAX_VALUE;
			case DOWN:
				return startY <= bounds.getMaxY() ? bounds.getMaxY() - startY : Double.MAX_VALUE;
			default:
				return 0;
		}
	}

	// TODO
	private boolean addLinesAroundHardwareOrConnect(Direction prevDirection, Direction mustGoDirection, double x, double y, Node scene,
														   Node hw) {
		double connectionLenght = (x == endX || y == endY) && !polyLine.isEmpty() ? getPointDistance(x, y, endX, endY)  : Double.MAX_VALUE;
		Bounds b = scene.sceneToLocal(hw.localToScene(hw.getBoundsInLocal()));
		double dUp = getPointDistance(x, y, x, b.getMinY());
		double dDown = getPointDistance(x, y, x, b.getMaxY());
		double dLeft = getPointDistance(x, y, b.getMinX(), y);
		double dRight = getPointDistance(x, y, b.getMaxX(), y);
		if (mustGoDirection == Direction.DOWN && prevDirection == Direction.DOWN && dDown < connectionLenght) {
			double newX = dLeft < dRight ? x - dLeft : x + dRight;
			appendLine(x, y, newX, y);
			appendLine(newX, y, newX, y + dDown + HARDWARE_OFFSET);
		} else if (mustGoDirection == Direction.UP && prevDirection == Direction.UP && dUp < connectionLenght) {
			double newX = dLeft < dRight ? x - dLeft : x + dRight;
			appendLine(x, y, newX, y);
			appendLine(newX, y, newX, y - dUp - HARDWARE_OFFSET);
		} else if (mustGoDirection == Direction.RIGHT && prevDirection == Direction.RIGHT && dRight < connectionLenght) {
			double newY = dUp < dDown ? y - dUp : y + dDown;
			appendLine(x, y, x, newY);
			appendLine(x, newY, x + dRight + HARDWARE_OFFSET, newY);
		} else if (mustGoDirection == Direction.LEFT && prevDirection == Direction.LEFT && dLeft < connectionLenght) {
			double newY = dUp < dDown ? y - dUp : y + dDown;
			appendLine(x, y, x, newY);
			appendLine(x, newY, x - dLeft - HARDWARE_OFFSET, newY);
		} else {
			if(polyLine.isEmpty()) {
				if(mustGoDirection == Direction.DOWN && prevDirection != Direction.UP) {
					appendLine(x, y, x, y + dDown);
				} else if(mustGoDirection == Direction.UP && prevDirection != Direction.DOWN) {
					appendLine(x, y, x, y - dUp);
				} else if(mustGoDirection == Direction.LEFT && prevDirection != Direction.RIGHT) {
					appendLine(x, y, x - dLeft, y);
				} else if(mustGoDirection == Direction.RIGHT && prevDirection != Direction.LEFT) {
					appendLine(x, y, x + dRight, y);
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		return true;
	}

	public boolean connectToEnd(Direction mustGoDirection, Direction previousDirection, double x, double y) {
		if(oppositeDirections(mustGoDirection, previousDirection)) {
			return false;
		}
		if (mustGoDirection == Direction.DOWN || mustGoDirection == Direction.RIGHT) {
			if (previousDirection != Direction.UP) {
				appendLine(x, y, x, endY);
				addLineToEnd(x, endY);
			} else {
				appendLine(x, y, endX, y);
				addLineToEnd(endX, y);
			}
		} else {
			if (previousDirection != Direction.DOWN) {
				appendLine(x, y, x, endY);
				addLineToEnd(x, endY);
			} else {
				appendLine(x, y, endX, y);
				addLineToEnd(endX, y);
			}
		}
		return true;
	}

	private Node geCollisionNode(double x, double y) {
		for (Node node : collisionNodes) {
			if (scene.sceneToLocal(node.localToScene(node.getBoundsInLocal())).contains(x, y)) {
				return node;
			}
		}
		return null;
	}

	private Direction getMustGoDirectionX(double x) {
		if (x == endX) {
			return Direction.STAY;
		} else if (x < endX) {
			return Direction.RIGHT;
		} else {
			return Direction.LEFT;
		}
	}

	private Direction getMustGoDirectionY(double y) {
		if (y == endY) {
			return Direction.STAY;
		} else if (y < endY) {
			return Direction.DOWN;
		} else {
			return Direction.UP;
		}
	}

	private boolean directionHorizontal(Direction direction) {
		return direction == Direction.LEFT || direction == Direction.RIGHT;
	}

	private boolean directionVertical(Direction direction) {
		return direction == Direction.UP || direction == Direction.DOWN;
	}

	private double getDistanceToClosestCollisionNode(Direction direction, double x, double y) {
		double distance = Double.MAX_VALUE;
		for (Node node : collisionNodes) {
			Bounds bounds = scene.sceneToLocal(node.localToScene(node.getBoundsInLocal()));
			if (directionHorizontal(direction)) {
				if (direction == Direction.RIGHT && bounds.getMinX() > x && bounds.getMaxY() > y && bounds.getMinY() < y) {
					distance = Math.min(distance, bounds.getMinX() - x);
				} else if (direction == Direction.LEFT && bounds.getMaxX() < x && bounds.getMaxY() > y && bounds.getMinY() < y) {
					distance = Math.min(distance, x - bounds.getMaxX());
				}
			} else if (directionVertical(direction)) {
				if (direction == Direction.DOWN && bounds.getMinY() > y && bounds.getMaxX() > x && bounds.getMinX() < x) {
					distance = Math.min(distance, bounds.getMinY() - y);
				} else if (direction == Direction.UP && bounds.getMaxY() < y && bounds.getMaxX() > x && bounds.getMinX() < x) {
					distance = Math.min(distance, y - bounds.getMaxY());
				}
			}
		}
		return distance;
	}

	private double getNextLineLength(Direction direction, double x, double y) {
		double lenght = 0;
		if (directionHorizontal(direction)) {
			lenght = Math.min(getDistanceToClosestCollisionNode(direction, x, y), Math.abs(x - endX));
		} else if (directionVertical(direction)) {
			lenght = Math.min(getDistanceToClosestCollisionNode(direction, x, y), Math.abs(y - endY));
		}
		return lenght;
	}

	private boolean oppositeDirections(Direction d1, Direction d2) {
		return (d1 == Direction.LEFT && d2 == Direction.RIGHT) || (d2 == Direction.LEFT && d1 == Direction.RIGHT) ||
					   (d1 == Direction.DOWN && d2 == Direction.UP) || (d1 == Direction.UP && d2 == Direction.DOWN);
	}

	private Direction getDirection(Line line) {
		if (line.getStartX() == line.getEndX()) {
			return line.getStartY() - line.getEndY() < 0 ? Direction.DOWN : Direction.UP;
		} else if (line.getStartY() == line.getEndY()) {
			return line.getStartX() - line.getEndX() < 0 ? Direction.RIGHT : Direction.LEFT;
		}
		return Direction.STAY;
	}

	private Direction getPreviousDirection() {
		if (polyLine.isEmpty()) {
			return Direction.STAY;
		}
		return getDirection(polyLine.get(polyLine.size() - 1));
	}

	private double getCurrentX() {
		if (polyLine.isEmpty()) {
			return startLine.getEndX();
		}
		return polyLine.get(polyLine.size() - 1).getEndX();
	}

	private double getCurrentY() {
		if (polyLine.isEmpty()) {
			return startLine.getEndY();
		}
		return polyLine.get(polyLine.size() - 1).getEndY();
	}

	private boolean finished(double x, double y) {
		return x == endX && y == endY;
	}

	public boolean lineHorizontal(Line line) {
		return line.getStartY() == line.getEndY();
	}

	public boolean lineVertical(Line line) {
		return line.getStartX() == line.getEndX();
	}

	public boolean linesOverlapHorizontal(Line line1, Line line2) {
		return lineHorizontal(line1) && lineHorizontal(line2) && Math.abs(line1.getEndY() - line2.getEndY()) < LINE_OFFSET &&
					   !(Math.max(line1.getStartX(), line1.getEndX()) <= Math.min(line2.getStartX(), line2.getEndX()) ||
								 Math.max(line2.getStartX(), line2.getEndX()) <= Math.min(line1.getStartX(), line1.getEndX()));
	}

	public boolean linesOverlapVertical(Line line1, Line line2) {
		return lineVertical(line1) && lineVertical(line2) && Math.abs(line1.getEndX() - line2.getEndX()) < LINE_OFFSET &&
					   !(Math.max(line1.getStartY(), line1.getEndY()) <= Math.min(line2.getStartY(), line2.getEndY()) ||
								 Math.max(line2.getStartY(), line2.getEndY()) <= Math.min(line1.getStartY(), line1.getEndY()));
	}

	private void adjustFirstLine(double value, BiConsumer<Line, Double> setStart) {
		Line line = polyLine.get(0);
		double startX = line.getStartX();
		double startY = line.getStartY();
		setStart.accept(line, value);
		Line newLine = getLine(startX, startY, line.getStartX(), line.getStartY());
		polyLine.add(0, newLine);
		startLine = newLine;
	}

	private void adjustLine(int index, double value, BiConsumer<Line, Double> setEnd) {
		if(index < polyLine.size()) {
			Line line = polyLine.get(index);
			double endX = line.getEndX();
			double endY = line.getEndY();
			setEnd.accept(line, value);
			Line newLine = getLine(line.getEndX(), line.getEndY(), endX, endY);
			polyLine.add(index + 1, newLine);
		}
	}

	private void adjustLines(int index, double value, Function<Line, Boolean> lineDirection, BiConsumer<Line, Double> setStart,
									BiConsumer<Line, Double> setEnd) {
		int i = index;
		Line line = polyLine.get(i);
		while (i > 0 && lineDirection.apply(line)) {
			setStart.accept(line, value);
			if(i < polyLine.size() - 1) {
				setEnd.accept(line, value);
			} else {
				adjustLine(polyLine.size() - 1, value, setEnd);
			}
			--i;
			line = polyLine.get(i);
		}
		setEnd.accept(line, value);
		if(i == 0) {
			adjustFirstLine(value, setStart);
		}
		i = index + 1;
		if(i < polyLine.size()) {
			line = polyLine.get(i);
			while (i < polyLine.size() - 1 && lineDirection.apply(line)) {
				setStart.accept(line, value);
				setEnd.accept(line, value);
				++i;
				line = polyLine.get(i);
			}
			setStart.accept(line, value);
			if(i == polyLine.size() - 1) {
				adjustLine(i, value, setEnd);
			}
		}
	}

	public boolean lineStraight(Line line) {
		return line.getStartX() == line.getEndX() || line.getStartY() == line.getEndY();
	}

	private void adjustLinesHorizontal(int index, double value) {
		adjustLines(index, value, line -> lineHorizontal(line),  (line, v) -> line.setStartY(v), (line, v) -> line.setEndY(v));
	}

	private void adjustLinesVertical(int index, double value) {
		adjustLines(index, value, line -> lineVertical(line), (line, v) -> line.setStartX(v), (line, v) -> line.setEndX(v));
	}

	public void adjustByLine(Line line) {
		for (int i = 0; i < polyLine.size(); i++) {
			Line l = polyLine.get(i);
			if (linesOverlapHorizontal(line, l)) {
				adjustLinesHorizontal(i, l.getStartY() + LINE_OFFSET);
				break;
			} else if (linesOverlapVertical(line, l)) {
				adjustLinesVertical(i, l.getStartX() + LINE_OFFSET);
				break;
			}
		}
	}

	public void adjust(List<CableVisualSpecific> connections) {
		for (CableVisualSpecific c : connections) {
			adjust(c);
		}
	}

	public void adjust(CableVisualSpecific connection) {
		for (Line line : connection.polyLine) {
			adjustByLine(line);
		}
	}*/

}