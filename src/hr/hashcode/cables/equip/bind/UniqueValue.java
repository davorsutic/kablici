package hr.hashcode.cables.equip.bind;

import java.util.Objects;

public class UniqueValue<T> {
	private T value;
	private boolean set;

	void register(T value) {
		if (set && !Objects.equals(this.value, value))
			throw new IllegalArgumentException(this.value + "," + value);
		this.value = value;
		set = true;
	}

	T value() {
		return value;
	}

}
