package hr.hashcode.cables.equip.bind;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BindTrace {

	private final List<String> radioSerials;
	private final List<String> externalFdns;

	BindTrace(List<String> radioSerials, List<String> externalFdns) {
		List<String> serials = new ArrayList<>();

		for (String serial : radioSerials)
			if (serial != null && serial.length() > 3)
				serials.add(serial);

		this.radioSerials = Collections.unmodifiableList(serials);

		List<String> fdns = new ArrayList<>();
		for (String fdn : externalFdns) {
			if (fdn == null || fdn.isEmpty() || fdn.contains("NOT") && fdn.contains("AVAILABLE"))
				continue;

			int index = fdn.indexOf("ManagedElement=");
			if (index == 0)
				continue;
			else if (index > 0)
				fdn = fdn.substring(0, index - 1);
			fdns.add(fdn);
		}
		this.externalFdns = Collections.unmodifiableList(fdns);

	}

	public List<String> radioSerials() {
		return radioSerials;
	}

	public List<String> externalFdns() {
		return externalFdns;
	}

}
