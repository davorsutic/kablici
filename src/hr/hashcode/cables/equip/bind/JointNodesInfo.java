package hr.hashcode.cables.equip.bind;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import hr.hashcode.cables.equip.bind.SingleNodeInfo.CabinetLocation;
import hr.hashcode.cables.equip.comp.Antenna;
import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.DigitalUnitBaseband;
import hr.hashcode.cables.equip.comp.DigitalUnitStandard;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.EcPort;
import hr.hashcode.cables.equip.comp.IdlPort;
import hr.hashcode.cables.equip.comp.PortSet;
import hr.hashcode.cables.equip.comp.PortSet.SinglePortSet;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnit;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.RemoteRadioUnit;
import hr.hashcode.cables.equip.comp.RfPort;
import hr.hashcode.cables.equip.comp.RiPort;

public class JointNodesInfo {

	public enum DumpType {
		RBS, ERBS, MSRBS
	}

	static class Location {
		final String subrack;
		final String slot;

		Location(String subrack, String slot) {
			this.subrack = subrack;
			this.slot = slot;
		}
	}

	public double longitude;
	public double latitude;

	public String name;

	public Container container = new Container();
	public List<Cabinet> cabinets = new ArrayList<>();
	public List<ExtendedHub> hubs = new ArrayList<>();
	public List<Antenna> antennas = new ArrayList<>();

	public List<Connector<RiPort>> riCables = new ArrayList<>();
	public List<Connector<EcPort>> ecCables = new ArrayList<>();
	public List<Connector<IdlPort>> idlCables = new ArrayList<>();
	public List<Connector<RfPort>> rfCables = new ArrayList<>();

	public static class Cabinet {
		final public String name;
		final public String type;
		final public String productNumber;

		public Container container = new Container();

		public Cabinet(String name, String type, String productNumber) {
			this.name = Objects.requireNonNull(name);
			this.type = Objects.requireNonNull(type);
			this.productNumber = productNumber;
		}
	}

	public static class Extended<T extends Component> {
		public T item;
	}

	public static class ExtendedDigital<T extends DigitalUnit> extends Extended<T> {
		Location location;
	}

	public static class ExtendedHub implements Component {

		public final String name;
		public final PortSet<EcPort> ports = PortSet.ecFactory.ports("UN", "CLU",
				"Ain", "A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8",
				"Bin", "B0", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8",
				"A", "B", "C", "D", "E", "X", "Y", "SAU");

		public ExtendedHub(String name) {
			this.name = name;
		}

		public EcPort undefined() {
			return ports.port("UN");
		}

		@Override
		public String name() {
			return name;
		}
	}

	public abstract static class RadioData {
		final String radioName;
		final String normalizedCell;
		final List<String> cells;
		final DumpType dumpType;

		RadioData(String radioName, String normalizedCell, List<String> cells, DumpType dumpType) {
			this.radioName = Objects.requireNonNull(radioName);
			this.normalizedCell = Objects.requireNonNull(normalizedCell);
			this.cells = new ArrayList<>(cells);
			this.dumpType = Objects.requireNonNull(dumpType);
		}
	}

	static class InCabinetRadioData extends RadioData {
		final Optional<CabinetLocation> location;

		InCabinetRadioData(Optional<CabinetLocation> location, String radioName, List<String> cells, DumpType dumpType) {
			super(radioName, cells.get(0), cells, dumpType);
			this.location = location;
		}
	}

	static class RemoteRadioData extends RadioData {

		RemoteRadioData(String radioName, List<String> cells, DumpType dumpType) {
			super(radioName, cells.get(0), cells, dumpType);
		}
	}

	public static abstract class ExtendedRadio<T extends RadioUnitBase> extends Extended<T> {
		final Band band;

		ExtendedRadio(Band band) {
			this.band = Objects.requireNonNull(band);
		}

		abstract RadioData lte();

		abstract RadioData wcdma();
	}

	public static class InCabinetExtendedRadio extends ExtendedRadio<RadioUnit> {

		private InCabinetRadioData lte;
		private InCabinetRadioData wcdma;

		public InCabinetExtendedRadio(Band band, List<InCabinetRadioData> radioData) {
			super(band);
			InCabinetRadioData first = radioData.get(0);
			if (first.dumpType == DumpType.RBS)
				wcdma = first;
			else
				lte = first;

			if (radioData.size() == 2) {
				InCabinetRadioData second = radioData.get(1);
				if (second.dumpType == DumpType.RBS)
					wcdma = second;
				else
					lte = second;
			}
		}

		@Override
		InCabinetRadioData lte() {
			return this.lte;
		}

		@Override
		InCabinetRadioData wcdma() {
			return wcdma;
		}
	}

	public static class RemoteExtendedRadio extends ExtendedRadio<RemoteRadioUnit> {

		private RemoteRadioData lte;
		private RemoteRadioData wcdma;

		public RemoteExtendedRadio(Band band, List<RemoteRadioData> radioData) {
			super(band);
			RemoteRadioData first = radioData.get(0);
			if (first.dumpType == DumpType.RBS)
				wcdma = first;
			else
				lte = first;

			if (radioData.size() == 2) {
				RemoteRadioData second = radioData.get(1);
				if (second.dumpType == DumpType.RBS)
					wcdma = second;
				else
					lte = second;
			}
		}

		@Override
		RemoteRadioData lte() {
			return lte;
		}

		@Override
		RemoteRadioData wcdma() {
			return wcdma;
		}
	}

	public static class ExtendedEcOther implements Component {
		public final String name;
		public final String productNumber;
		public final SinglePortSet<EcPort> ecPort = PortSet.ecFactory.port("EC");

		ExtendedEcOther(String name, String productNumber) {
			this.name = name;
			this.productNumber = productNumber;
		}

		@Override
		public String name() {
			return name;
		}
	}

	public static class Container {
		public List<Extended<DigitalUnitWcdma>> duws = new ArrayList<>();
		public List<Extended<DigitalUnitStandard>> duses = new ArrayList<>();
		public List<Extended<DigitalUnitBaseband>> bbs = new ArrayList<>();
		public List<InCabinetExtendedRadio> radios = new ArrayList<>();
		public List<RemoteExtendedRadio> remotes = new ArrayList<>();
		public List<Extended<R503>> r503s = new ArrayList<>();
		public List<ExtendedEcOther> ecOthers = new ArrayList<>();
	}

}
