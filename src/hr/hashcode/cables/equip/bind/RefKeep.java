package hr.hashcode.cables.equip.bind;

import java.util.IdentityHashMap;

import hr.hashcode.cables.equip.bind.Extractor.Corset;

class RefKeep<K extends Corset, V> {

	private final IdentityHashMap<K, V> map = new IdentityHashMap<>();

	void register(K key, V value) {
		map.put(key, value);
	}

	boolean isPresent(K key) {
		return map.containsKey(key);
	}

	V get(K key) {
		if (key == null)
			return null;
		V value = map.get(key);
		if (value == null)
			throw new IllegalArgumentException(String.valueOf(key)); // TODO put some other exception
		return value;
	}
}
