package hr.hashcode.cables.equip.bind;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.bind.DumpToSite.HwPlacement;
import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.MiniRbsFactory;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.RbsFactory;
import hr.hashcode.cables.equip.comp.Site;
import hr.hashcode.cables.equip.rules.PlacementRules;
import hr.hashcode.cables.fetch.moshell.ParseMoshellDump;
import hr.hashcode.cables.gui.FileFormats;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.Serialize;
import hr.hashcode.cables.util.Util;

public class MainSutic {

	private static NodeState read(File file) {
		byte[] bytes;
		try {
			bytes = Util.drain(FileFormats.tryGzip(file));
		} catch (IOException e) {
			return null;
		}
		NodeState state = null;
		try {
			try (ObjectInputStream objInput = new ObjectInputStream(new ByteArrayInputStream(bytes))) {
				state = (NodeState) objInput.readObject();
			} catch (IOException | ClassNotFoundException e) {
				// do nothing
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		try {
			if (state == null)
				state = ParseMoshellDump.read(bytes);
		} catch (Throwable t) {
			t.printStackTrace();
		}

		try {
			if (state == null)
				state = Serialize.fromBytes(bytes);
		} catch (Throwable t) {
			t.printStackTrace();
		}

		return state;
	}

	public static Site toSite(CabinetSystem system) {

		final InputStream rbsesStream = RbsFactory.class.getResourceAsStream("rbses.xml");
		final InputStream miniRbsesStream = MiniRbsFactory.class.getResourceAsStream("rbses.xml");
		final InputStream placementRulesStream = PlacementRules.class.getResourceAsStream("placement_rules.xml");

		RbsFactory macroFactory = RbsFactory.of(rbsesStream);
		MiniRbsFactory miniFactory = MiniRbsFactory.of(miniRbsesStream);
		PlacementRules placementRules = PlacementRules.of(placementRulesStream);

		DumpNormalize.normalize(system);
		HwPlacement placement = DumpToSite.hwPlacement(system, macroFactory, miniFactory, placementRules);
		return placement.site;
	}

	public static void main(String[] args) {
		String dumpLocation = "data/all_good/";
		String dumpSuffix = ".gz";

		final InputStream rbsesStream = RbsFactory.class.getResourceAsStream("rbses.xml");
		final InputStream miniRbsesStream = MiniRbsFactory.class.getResourceAsStream("rbses.xml");
		final InputStream placementRulesStream = PlacementRules.class.getResourceAsStream("placement_rules.xml");

		RbsFactory macroFactory = RbsFactory.of(rbsesStream);
		MiniRbsFactory miniFactory = MiniRbsFactory.of(miniRbsesStream);
		PlacementRules placementRules = PlacementRules.of(placementRulesStream);

		List<NodeState> nodeStates = new ArrayList<>();
		for (File file : Objects.requireNonNull(new File(dumpLocation).listFiles((dir, name) -> name.endsWith(dumpSuffix)))) {
			NodeState state = read(file);
			nodeStates.add(state);
		}

		List<CabinetSystem> systems = Bind.systems(nodeStates);

		long start = System.nanoTime();
		for (int i = 0; i < systems.size(); i++) {
			CabinetSystem system = systems.get(i);
			System.out.println("\n");
			System.out.println(" **** SYSTEM" + "\t" + i + "\t" + system.infos.stream().map(x -> x.name).collect(Collectors.joining(",")));

			try {
				DumpNormalize.normalize(system);
			} catch (Exception iae) {
				iae.printStackTrace();
				System.out.println("BAD THINGS: " + i + system.infos.stream().map(x -> x.name).collect(Collectors.joining(",")));
				continue;
			}
			HwPlacement placement = DumpToSite.hwPlacement(system, macroFactory, miniFactory, placementRules);

			Site site = placement.site;
			try {
				DumpSiteChecker.check(system, site);
			} catch (Exception iae) {
				iae.printStackTrace();
				System.out.println("BAD CHECK: " + i);
				continue;
			}

			// print band to radio
			for (Entry<Band, Set<RadioUnitBase>> entry : site.radios.entrySet()) {
				System.out.println(entry.getKey() + " -> " + entry.getValue());
			}
			System.out.println();
			for (Entry<DigitalUnit, Set<Band>> entry : site.duToBands.entrySet()) {
				System.out.println(entry.getKey() + " -> " + entry.getValue());
			}
			System.out.println();
			for (Entry<R503, Set<Band>> entry : site.r503ToBands.entrySet()) {
				System.out.println(entry.getKey() + " -> " + entry.getValue());
			}
		}
		long end = System.nanoTime();
		System.out.println("Done in " + (end - start) / 1e9);

	}

}
