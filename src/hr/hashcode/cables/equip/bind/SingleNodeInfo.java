package hr.hashcode.cables.equip.bind;

import java.lang.reflect.Field;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.bind.JointNodesInfo.DumpType;
import hr.hashcode.cables.equip.comp.BandTech;
import hr.hashcode.cables.ran.Version;

public class SingleNodeInfo {

	public Version.Type type;
	public String name;
	public RawManagedElement node;
	public RawEquipmentSupportFunction eqSupport;
	public List<RawCabinet> cabinets = new ArrayList<>();
	public List<RawDuw> duws = new ArrayList<>();
	public List<RawDus> duses = new ArrayList<>();
	public List<RawSubrack> subracks = new ArrayList<>();
	public List<RawRbsSubrack> rbsSubracks = new ArrayList<>();
	public List<RawExternalNode> extNodes = new ArrayList<>();
	public List<RawPiuLink> piuLinks = new ArrayList<>();
	public List<RawRuw> radios = new ArrayList<>();
	public List<RawRemoteRuw> remotes = new ArrayList<>();
	public List<RawSector> sectors = new ArrayList<>();
	public List<RawSectorAntenna> antennas = new ArrayList<>();
	public List<RawDigitalCable> digitalCables = new ArrayList<>();
	public List<RawAntFeederCable> antennaCables = new ArrayList<>();
	public List<RawHwUnit> hwUnits = new ArrayList<>();
	public List<RawIub> iubs = new ArrayList<>();
	public List<RawRbsSynchronization> syncs = new ArrayList<>();
	public List<RawEcBus> ecBuses = new ArrayList<>();
	public List<RawRus> ruses = new ArrayList<>();
	public List<RawRemoteRus> remoteRuses = new ArrayList<>();
	public List<RawBb> bbs = new ArrayList<>();
	public List<RawR503> r503s = new ArrayList<>();
	public List<RawHw> hws = new ArrayList<>();

	public List<RawAntenna> rawAntennas = new ArrayList<>();

	public List<RawEUtranCell> cells = new ArrayList<>();
	public List<RawSectorCarrier> secCarriers = new ArrayList<>();
	public List<RawSectorEquipmentFunction> sefs = new ArrayList<>();

	public List<List<? extends Fields>> lists() {
		return Arrays.asList(Arrays.asList(node, eqSupport), cabinets, ecBuses, iubs, cells, secCarriers, syncs, duws,
				duses, bbs, r503s, hwUnits, hws, subracks, rbsSubracks, extNodes, piuLinks, radios, remotes, ruses,
				remoteRuses, sectors, sefs, antennas, antennaCables, digitalCables);
	}

	public static class RawManagedElement extends Fields {
		public String fdn;
		public String productName;
		public String logicalName;
		public String productNumber;
		public String site;
	}

	public static class RawEquipmentSupportFunction extends Fields {
		public final Boolean supportSystemControl;

		public RawEquipmentSupportFunction(Boolean supportSystemControl) {
			this.supportSystemControl = supportSystemControl;
		}

	}

	public static class ProductInfo extends Fields {

		public final String productName;
		public final String productNumber;

		public ProductInfo(String productName, String productNumber) {
			this.productName = productName;
			this.productNumber = productNumber;
		}
	}

	public static class SerialProductInfo extends ProductInfo {

		public final String serial;

		public SerialProductInfo(String productName, String productNumber, String serial) {
			super(productName, productNumber);
			this.serial = serial;
		}
	}

	public static class RawCabinet extends Fields {
		public final String identifier;
		public final String sharedId;
		public final SerialProductInfo info;

		public RawCabinet(String identifier, String sharedId, SerialProductInfo info) {
			this.identifier = identifier;
			this.sharedId = sharedId;
			this.info = info;
		}

		@Override
		public String toString() {
			return sharedId;
		}
	}

	public static class EcBusConn extends Fields {
		public final RawEcBus ecBus;
		public final String ecHubPosition;

		public EcBusConn(RawEcBus ecBus, String ecHubPosition) {
			this.ecBus = ecBus;
			this.ecHubPosition = ecHubPosition;
		}

		@Override
		public String toString() {
			return ecBus + ":" + ecHubPosition;
		}

	}

	public static class RawUnit extends Fields {
		public SerialProductInfo productInfo;
		public EcBusConn ecConn;
		public RawCabinet positionRef;
	}

	private static class RawDigitalUnit extends RawUnit implements RiUnit {
	}

	private static class RawDigitalUnitG1 extends RawDigitalUnit {
		public CabinetLocation location;
		public RawPiu piu;

		@Override
		public String toString() {
			return String.valueOf(location);
		}
	}

	private static class RawDigitalUnitG2 extends RawDigitalUnit {
	}

	public static class RawDuw extends RawDigitalUnitG1 {
	}

	public static class RawDus extends RawDigitalUnitG1 {
	}

	public static class RawBb extends RawDigitalUnitG2 {
	}

	public static class RawR503 extends RawUnit implements RiUnit {
	}

	public static class RawPiu extends Fields {

		public String productName;
		public String productNumber;
		public Integer boardWidth;
		public String role;
	}

	public static class RawSubrack extends Fields {
		public String cabinetPosition;
		public Integer subrackNumber;
		public String subrackPosition;
		public RawSubrackProdType subrackProdType;
		public String transmisionType;
	}

	public static class RawSubrackProdType extends Fields {
		public String productNumber;
		public String productName;
		public String productInfo;
		public RawBoardPositions boardPositions;
	}

	public static class RawBoardPositions extends Fields {
		public Integer cmxbPositionA;
		public Integer cmxbPositionB;
		public Integer scuPositionA;
		public Integer scuPositionB;
		public Integer tuPositionA;
		public Integer tuPositionB;
	}

	public static class RawRadio extends Fields implements RiUnit {
		public String name;
		public RawPiu piu;
		public Integer position;
		public RawCoordinates positionCoordinates;
		public SerialProductInfo productInfo;
		public RawCabinet positionRef;
		public Set<BandTech> bands = new LinkedHashSet<>();
		public Set<String> cells = new TreeSet<>();
		public CabinetLocation location;
		public DumpType dumpType;
	}

	public static class RawRuw extends RawRadio {
		public String auType;
		public String plugInUnit;
		public Integer slotPosition;

		public String uniqueHwId;

	}

	public static class RawRemoteRuw extends RawRuw {
	}

	public static class RawRemoteRus extends RawRadio {
	}

	public static class RawRus extends RawRadio {
		public String positionInformation;
	}

	public static class RawCoordinates extends Fields {
		public final Double longitude;
		public final Double latitude;

		public RawCoordinates(Double longitude, Double latitude) {
			this.longitude = longitude;
			this.latitude = latitude;
		}
	}

	public static class RawExternalNode extends Fields {
		public String fdn;
		public Boolean informationOnly;
		public String logicalName;
		public String radioAccessTechnology;
		public String supportSystemControl;
		public EcBusConn ecBusConn;
		public String equipmentSupportFunctionRef; // only L
	}

	public static class RawPiuLink extends Fields {
		public final RawDuw primary;
		public final RawDuw secondary;

		public RawPiuLink(RawDuw primary, RawDuw secondary) {
			this.primary = primary;
			this.secondary = secondary;
		}
	}

	public interface RiUnit {
	}

	public static class RawSector extends Fields {
		public Integer band;
		public String beamDirection;
		public RawCoordinates coordinates;
		public Boolean mixedModeRadio;
		public Integer dlBandwidth;
		public List<String> sectorAntennas;
	}

	public static class RawSectorAntenna extends Fields {
		public Integer antennaType;
		public String beamDirection;
		public String positionRef;
		public List<RawAntennaBranch> branches;
	}

	public static class RawAntenna extends Fields {
		String name;
		Set<String> branches = new TreeSet<>();

		public String branch(String original) {
			char result = 'A';
			for (String name : branches) {
				if (name.equals(original))
					return String.valueOf(result);
				result++;
			}
			throw new IllegalArgumentException(original);
		}
	}

	public static class RawAntennaBranch extends Fields {
		String branchName;
		Integer fqBandHighEdge;
		Integer fqBandLowEdge;
	}

	public static class RawAntFeederCable extends Fields {
		public RawRadio radio;
		public String radioPort;
		public RawAntenna antenna;
		public String antennaPort;
	}

	public static class RawDigitalCable extends Fields {
		public final RiUnit unit1;
		public final String port1;
		public final RiUnit unit2;
		public final String port2;

		public RawDigitalCable(RiUnit unit1, String port1, RiUnit unit2, String port2) {
			this.unit1 = unit1;
			this.port1 = port1;
			this.unit2 = unit2;
			this.port2 = port2;
		}
	}

	public static class RawRbsSubrack extends Fields {
		public Integer noOfSlots;
		public String subrackPosition;
	}

	public static class RawHwUnit extends RawUnit {
		public RawPiu piu;
		public Integer position;
	}

	public static class RawIub extends Fields {
		public Integer rbsId;
		public String userLabel;
	}

	public static class RawRbsSynchronization extends Fields {
		public String masterTu;
		public RawDuw piu1;
		public RawDuw piu2;
		public String tim1;
		public String tim2;
	}

	public static class RawEcBus extends Fields {
		public String name;
		public String connectionType;
		public String ecBusConnectorRef;

		public String equipmentSupportFunctionRef; // L only

		@Override
		public String toString() {
			return name;
		}
	}

	public static class RawEUtranCell extends Fields {
		RawCoordinates coordinates;
		RawDus hostingDigitalUnit;
		public Integer earfcndl;
		public Integer freqBand;
		public List<String> sectorCarrier;
	}

	public static class RawHw extends RawUnit {
	}

	public static class RawSectorCarrier extends Fields {
		public String sectorFunctionRef;
	}

	public static class RawSectorEquipmentFunction extends Fields {
		public List<String> rfBranchesRef;
	}

	public static class Fields {

		public String report() {
			StringBuilder builder = new StringBuilder();
			String name = getClass().getSimpleName();
			String raw = "raw";
			if (name.toLowerCase().startsWith(raw.toLowerCase()))
				name = name.substring(raw.length());
			builder.append(name).append(":");
			Deque<Class<?>> classes = new ArrayDeque<>();
			for (Class<?> temp = getClass(); temp != Fields.class; temp = temp.getSuperclass())
				classes.addFirst(temp);

			for (Class<?> clazz : classes)
				for (Field field : clazz.getDeclaredFields()) {
					Object value;
					try {
						value = field.get(this);
					} catch (IllegalArgumentException | IllegalAccessException e) {
						value = "KABLICI_ERROR";
					}
					builder.append("\n").append(field.getName()).append("=").append(tab(value));
				}
			return builder.toString();
		}

		private static final Pattern patt = Pattern.compile("^", Pattern.MULTILINE);

		private static String tab(Object value) {
			if (value == null)
				return null;
			return Arrays.stream(patt.split(value.toString())).map(x -> "\t" + x).collect(Collectors.joining())
					.trim();
		}

		@Override
		public String toString() {
			return report();
		}
	}

	public static abstract class CabinetLocation implements Comparable<CabinetLocation> {
		public final String subrack;
		public final String slot;

		CabinetLocation(String subrack, String slot) {
			this.subrack = Objects.requireNonNull(subrack);
			this.slot = Objects.requireNonNull(slot);
		}

		@Override
		public int compareTo(CabinetLocation o) {
			int subrack = this.subrack.compareTo(o.subrack);
			if (subrack != 0) {
				return subrack;
			} else {
				return slot.compareTo(slot);
			}
		}
	}

	public static class RbsSubrackLocation extends CabinetLocation {
		public RbsSubrackLocation(String subrack, String slot) {
			super(subrack, slot);
		}

		@Override
		public String toString() {
			return "RbsSubrack:" + subrack + ",RbsSlot=" + slot;
		}

	}

	public static class SubrackLocation extends CabinetLocation {

		public SubrackLocation(String subrack, String slot) {
			super(subrack, slot);
		}

		@Override
		public String toString() {
			return "Subrack:" + subrack + ",Slot=" + slot;
		}
	}

}
