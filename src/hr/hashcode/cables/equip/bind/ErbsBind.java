package hr.hashcode.cables.equip.bind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.bind.Extractor.Corset;
import hr.hashcode.cables.equip.bind.Extractor.CorsetStruct;
import hr.hashcode.cables.equip.bind.JointNodesInfo.DumpType;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.EcBusConn;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawAntFeederCable;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawAntenna;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawBoardPositions;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawCabinet;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawCoordinates;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawDigitalCable;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawDus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawEUtranCell;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawEcBus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawEquipmentSupportFunction;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawExternalNode;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawManagedElement;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawPiu;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawR503;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRadio;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRbsSubrack;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRemoteRus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawSectorCarrier;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawSectorEquipmentFunction;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawSubrack;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawSubrackProdType;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RbsSubrackLocation;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RiUnit;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.SerialProductInfo;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.SubrackLocation;
import hr.hashcode.cables.equip.comp.BandTech;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.Version;

public class ErbsBind {

	private static SerialProductInfo productInfo(ProductData productData) {
		String name = productData.productName();
		String number = productData.productNumber();
		String serial = productData.serialNumber();
		return new SerialProductInfo(name, number, serial);
	}

	private final RefKeep<EcBus, RawEcBus> ecBuses = new RefKeep<>();
	private final RefKeep<Cabinet, RawCabinet> cabinets = new RefKeep<>();
	private final RefKeep<PlugInUnit, RawDus> duses = new RefKeep<>();
	private final RefKeep<Corset, RiUnit> riUnits = new RefKeep<>();

	private EcBusConn ecConn(Corset corset) {
		List<EcPort> ecPorts = corset.children(EcPort.class);
		if (ecPorts.size() == 0)
			return null;
		if (ecPorts.size() > 1)
			throw new IllegalArgumentException(corset.toString());
		EcPort ecPort = ecPorts.get(0);
		EcBus ecBus = ecPort.ecBusRef();
		return new EcBusConn(ecBuses.get(ecBus), ecPort.hubPosition());
	}

	private static RawPiu piuType(PiuType piuType) {
		RawPiu rawPiu = new RawPiu();
		rawPiu.productName = piuType.productData().productName();
		rawPiu.productNumber = piuType.productData().productNumber();
		rawPiu.role = String.valueOf(piuType.role());
		rawPiu.boardWidth = piuType.boardWidth();
		return rawPiu;
	}

	private RiUnit findRiParent(Corset corset) {
		for (Corset temp = corset; temp != null; temp = temp.parent())
			if (riUnits.isPresent(temp))
				return riUnits.get(temp);

		throw new IllegalArgumentException(corset.toString());
	}

	public SingleNodeInfo start(NodeState state) {

		Extractor extractor = new Extractor(state, ErbsBind.class);

		SingleNodeInfo info = new SingleNodeInfo();
		info.type = Version.Type.ERBS;

		for (EcBus bus : extractor.extract(EcBus.class)) {
			RawEcBus raw = new RawEcBus();
			info.ecBuses.add(raw);
			raw.name = bus.name();
			raw.connectionType = bus.connectionType();
			raw.ecBusConnectorRef = String.valueOf(bus.ecBusConnectorRef());
			ecBuses.register(bus, raw);
		}

		String managedElementProductName = null;
		for (ManagedElement me : extractor.extract(ManagedElement.class)) {
			info.node = new RawManagedElement();
			info.node.fdn = state.fdn;
			info.node.productName = me.productName();
			managedElementProductName = me.productName();
			info.node.productNumber = me.productNumber();
			info.node.site = me.site();
			info.node.logicalName = me.logicalName();
			info.name = me.userLabel();
		}

		for (EquipmentSupportFunction eq : extractor.extract(EquipmentSupportFunction.class)) {
			info.eqSupport = new RawEquipmentSupportFunction(eq.supportSystemControl());
		}

		List<Cabinet> myCabinets = extractor.extract(Cabinet.class);
		for (Cabinet cabinet : myCabinets) {
			String sharedId = cabinet.sharedCabinetIdentifier();

			ProductData data = cabinet.productData();
			String identifier = cabinet.cabinetIdentifier();

			String productName = BindUtil.productName(data.productName(), identifier, managedElementProductName, myCabinets.size());
			SerialProductInfo prodInfo = new SerialProductInfo(productName, data.productNumber(), data.serialNumber());

			RawCabinet raw = new RawCabinet(identifier, sharedId, prodInfo);
			info.cabinets.add(raw);
			this.cabinets.register(cabinet, raw);
		}

		for (ExternalNode ext : extractor.extract(ExternalNode.class)) {
			RawExternalNode raw = new RawExternalNode();
			raw.fdn = ext.fullDistinguishedName();
			raw.informationOnly = ext.informationOnly();
			raw.logicalName = ext.logicalName();
			raw.radioAccessTechnology = String.valueOf(ext.radioAccessTechnology());
			raw.supportSystemControl = String.valueOf(ext.supportSystemControl());
			info.extNodes.add(raw);
			raw.ecBusConn = ecConn(ext);
			raw.equipmentSupportFunctionRef = String.valueOf(ext.equipmentSupportFunctionRef());
		}

		for (PlugInUnit unit : extractor.extract(PlugInUnit.class)) {

			RawDus raw = new RawDus();
			raw.ecConn = ecConn(unit);
			raw.positionRef = this.cabinets.get(unit.positionRef());
			Slot slot = unit.parent();
			ProductData prodData = slot.productData();
			raw.location = new SubrackLocation(slot.parent().name(), slot.name());
			raw.productInfo = productInfo(prodData);
			raw.piu = piuType(unit.piuType());
			this.duses.register(unit, raw);
			this.riUnits.register(unit, raw);
			info.duses.add(raw);
		}

		for (Subrack subrack : extractor.extract(Subrack.class)) {
			RawSubrack raw = new RawSubrack();
			info.subracks.add(raw);
			raw.cabinetPosition = subrack.cabinetPosition();
			raw.subrackNumber = subrack.subrackNumber();
			raw.subrackPosition = subrack.subrackPosition();
			raw.subrackProdType = new RawSubrackProdType();
			AdminProductData prodData = subrack.subrackProdTypeRef().productData();
			raw.subrackProdType.productInfo = prodData.productInfo();
			raw.subrackProdType.productName = prodData.productName();
			raw.subrackProdType.productNumber = prodData.productNumber();
			raw.transmisionType = String.valueOf(subrack.subrackProdTypeRef().transmissionType());

			RawBoardPositions rawBoard = new RawBoardPositions();
			raw.subrackProdType.boardPositions = rawBoard;
			BoardTypePositions board = subrack.subrackProdTypeRef().boardPositions();
			rawBoard.cmxbPositionA = board.cmxbPositionA();
			rawBoard.cmxbPositionB = board.cmxbPositionB();
			rawBoard.scuPositionA = board.scuPositionA();
			rawBoard.scuPositionB = board.scuPositionB();
			rawBoard.tuPositionA = board.tuPositionA();
			rawBoard.tuPositionB = board.tuPositionB();
		}

		for (RbsSubrack subrack : extractor.extract(RbsSubrack.class)) {
			RawRbsSubrack raw = new RawRbsSubrack();
			info.rbsSubracks.add(raw);
			raw.noOfSlots = subrack.noOfSlots();
			raw.subrackPosition = subrack.subrackPosition();
		}

		Map<AuxPlugInUnit, RawRadio> radios = new IdentityHashMap<>();
		for (AuxPlugInUnit unit : extractor.extract(AuxPlugInUnit.class)) {
			SerialProductInfo prodInfo = productInfo(unit.productData());
			String prodName = prodInfo.productName;

			RiUnit riUnit;
			if (prodName.startsWith("RU") || unit.unitType().startsWith("RU")) {
				RawRus radio = new RawRus();
				radio.name = unit.name();
				riUnit = radio;
				radio.position = unit.position();
				radio.positionInformation = unit.positionInformation();
				radio.productInfo = prodInfo;
				radio.piu = piuType(unit.piuType());
				radio.dumpType = DumpType.ERBS;

				double longitude = 0.000001 * unit.positionCoordinates().longitude();
				double latitude = 0.000001 * unit.positionCoordinates().latitude();
				radio.positionCoordinates = new RawCoordinates(longitude, latitude);
				radio.positionRef = cabinets.get(unit.positionRef());

				Corset parent = unit.parent();
				if (parent instanceof RbsSlot) {
					RbsSlot rbsSlot = (RbsSlot) parent;
					radio.location = new RbsSubrackLocation(rbsSlot.parent().name(), rbsSlot.name());
				}
				info.ruses.add(radio);
				radios.put(unit, radio);
			} else if (prodName.startsWith("RRU") || prodName.startsWith("Radio") || prodName.startsWith("ARETU") || prodName.startsWith(
					"IRU") || unit.unitType().startsWith("IRU")) {
				RawRemoteRus radio = new RawRemoteRus();
				radio.name = unit.name();
				riUnit = radio;
				radio.position = unit.position();
				radio.productInfo = prodInfo;
				radio.piu = piuType(unit.piuType());
				radio.dumpType = DumpType.ERBS;

				double longitude = 0.000001 * unit.positionCoordinates().longitude();
				double latitude = 0.000001 * unit.positionCoordinates().latitude();
				radio.positionCoordinates = new RawCoordinates(longitude, latitude);
				radio.positionRef = cabinets.get(unit.positionRef());

				info.remoteRuses.add(radio);
				radios.put(unit, radio);
			} else if (prodName.startsWith("Baseband R503")) {
				RawR503 raw = new RawR503();
				riUnit = raw;
				raw.positionRef = cabinets.get(unit.positionRef());
				raw.productInfo = prodInfo;
				raw.ecConn = ecConn(unit);
				info.r503s.add(raw);
			} else if (prodName.startsWith("RD")) {
				// TODO this is RADIO DOT. fill this if needed
				continue;
			} else
				throw new IllegalArgumentException(prodName);
			riUnits.register(unit, riUnit);
		}

		for (AntennaUnitGroup group : extractor.extract(AntennaUnitGroup.class)) {
			RawAntenna antenna = new RawAntenna();
			info.rawAntennas.add(antenna);
			for (RfBranch branch : group.children(RfBranch.class)) {
				antenna.branches.add(branch.name());
				RawAntFeederCable cable = new RawAntFeederCable();
				cable.antenna = antenna;
				cable.antennaPort = branch.name();
				RfPort rfPort = branch.rfPortRef();
				AuxPlugInUnit unit = null;
				for (Corset temp = rfPort.parent(); temp != null; temp = temp.parent()) {
					if (temp instanceof AuxPlugInUnit) {
						unit = (AuxPlugInUnit) temp;
						break;
					}
				}
				cable.radio = radios.get(unit);
				cable.radioPort = rfPort.name();
				info.antennaCables.add(cable);
			}
		}

		for (EUtranCellFDD cell : extractor.extract(EUtranCellFDD.class)) {
			RawEUtranCell raw = new RawEUtranCell();
			raw.coordinates = new RawCoordinates(cell.longitude() / 1e6, cell.latitude() / 1e6);
			raw.earfcndl = cell.earfcndl();
			raw.freqBand = cell.freqBand();

			raw.sectorCarrier =
					Arrays.stream(cell.sectorCarrierRef()).map(String::valueOf).collect(Collectors.toList());
			raw.hostingDigitalUnit = duses.get(cell.hostingDigitalUnit());
			info.cells.add(raw);
			for (SectorCarrier sectorCarrier : cell.sectorCarrierRef()) {
				for (AntennaBranch branch : sectorCarrier.sectorFunctionRef().rfBranchRef()) {
					if (branch instanceof RfBranch) {
						RfBranch rfBranch = (RfBranch) branch;
						RfPort rfPort = rfBranch.rfPortRef();
						for (Corset temp = rfPort.parent(); temp != null; temp = temp.parent())
							if (temp instanceof AuxPlugInUnit) {
								RawRadio rawRadio = radios.get(temp);
								rawRadio.bands.add(BandTech.lte(cell.freqBand()));
								rawRadio.cells.add(cell.name());
								break;
							}
					} else if (branch instanceof MulticastAntennaBranch) {
						// TODO do something
					}
				}
			}
		}

		for (SectorCarrier carrier : extractor.extract(SectorCarrier.class)) {
			RawSectorCarrier raw = new RawSectorCarrier();

			info.secCarriers.add(raw);
			raw.sectorFunctionRef = String.valueOf(carrier.sectorFunctionRef());
		}

		for (SectorEquipmentFunction seq : extractor.extract(SectorEquipmentFunction.class)) {
			RawSectorEquipmentFunction raw = new RawSectorEquipmentFunction();
			info.sefs.add(raw);
			raw.rfBranchesRef = Arrays.stream(seq.rfBranchRef()).map(String::valueOf).collect(Collectors.toList());
		}

		for (RiLink riLink : extractor.extract(RiLink.class)) {
			RiPort port1 = riLink.riPortRef1();
			RiPort port2 = riLink.riPortRef2();

			RiUnit unit1 = findRiParent(port1);
			RiUnit unit2 = findRiParent(port2);

			RawDigitalCable raw = new RawDigitalCable(unit1, port1.name(), unit2, port2.name());

			info.digitalCables.add(raw);

		}

		return info;
	}

	public static BindTrace trace(NodeState state) {
		List<String> radioSerials = new ArrayList<>();
		List<String> externalFdns = new ArrayList<>();

		Extractor extractor = new Extractor(state, ErbsBind.class);

		for (ExternalNode ext : extractor.extract(ExternalNode.class)) {
			String fdn = ext.fullDistinguishedName();
			externalFdns.add(fdn);
		}

		for (AuxPlugInUnit unit : extractor.extract(AuxPlugInUnit.class)) {

			ProductData data = unit.productData();
			if (data != null) {
				String prodName = data.productName();

				if (prodName.startsWith("RU") || unit.unitType() != null && unit.unitType().startsWith("RU")
						|| prodName.startsWith("RRU") || prodName.startsWith("Radio") || prodName.startsWith("ARETU")) {
					String serial = data.serialNumber();
					radioSerials.add(serial);
				}
			}
		}

		return new BindTrace(radioSerials, externalFdns);
	}

	private interface EquipmentSupportFunction extends Corset {
		Boolean supportSystemControl();
	}

	private enum RadioAccessTechnology {
		NOT_AVAILABLE, GSM, WCDMA, LTE, MULTI_STANDARD, TRANSMISSION;
	}

	private interface RiLink extends Corset {
		RiPort riPortRef1();

		RiPort riPortRef2();
	}

	private interface ManagedElement extends Corset {
		String productName();

		String logicalName();

		String userLabel();

		String productNumber();

		String site();

	}

	private interface ExternalNode extends Corset {

		EquipmentSupportFunction equipmentSupportFunctionRef();

		String fullDistinguishedName();

		String logicalName();

		RadioAccessTechnology radioAccessTechnology();

		Boolean informationOnly();

		SupportSystemControl supportSystemControl();
	}

	private enum SupportSystemControl {
		FALSE, TRUE, NOT_AVAILABLE
	}

	private interface Cabinet extends Corset {
		String fullDistinguishedName();

		String sharedCabinetIdentifier();

		String cabinetIdentifier();

		ProductData productData();
	}

	private interface SectorCarrier extends Corset {
		SectorEquipmentFunction sectorFunctionRef();
	}

	private interface SectorEquipmentFunction extends Corset {
		AntennaBranch[] rfBranchRef();
	}

	private interface EUtranCellFDD extends Corset {

		Integer longitude();

		Integer latitude();

		Integer earfcndl();

		Integer freqBand();

		PlugInUnit hostingDigitalUnit();

		SectorCarrier[] sectorCarrierRef();
	}

	private interface RiPort extends Corset {

	}

	private interface Slot extends Corset {
		@Override
		Subrack parent();

		ProductData productData();

		SlotState slotState();
	}

	private enum SlotState {
		FREE, USED, COVERED_BY_PIU;
	}

	private interface Subrack extends Corset {

		String cabinetPosition();

		SubrackType subrackType();

		SubrackProdType subrackProdTypeRef();

		Integer subrackNumber();

		String subrackPosition();

	}

	private interface RbsSubrack extends Corset {

		Integer noOfSlots();

		String subrackPosition();
	}

	private interface RbsSlot extends Corset {
		Integer slotPosition();
	}

	private interface SubrackProdType extends Corset {

		AdminProductData productData();

		BoardTypePositions boardPositions();

		Integer defNumberOfSlots();

		TransmissionType transmissionType();
	}

	private enum TransmissionType {
		ATM_TRANSMISSION, ETHERNET10G, ETHERNET_CABLE, ETHERNET_1G_10G, ETHERNET_1G_10G_40G;
	}

	private interface BoardTypePositions extends CorsetStruct {
		Integer cmxbPositionA();

		Integer cmxbPositionB();

		Integer scuPositionA();

		Integer scuPositionB();

		Integer tuPositionA();

		Integer tuPositionB();
	}

	private enum SubrackType {
		HUB, DEVICE;
	}

	private interface EcBus extends Corset {
		String connectionType();

		Corset ecBusConnectorRef();

		EquipmentSupportFunction equipmentSupportFunctionRef();
	}

	private interface EcPort extends Corset {
		Integer cascadingOrder();

		EcBus ecBusRef();

		String hubPosition();

	}

	private interface AuxPlugInUnit extends Corset {

		ProductData productData();

		Integer position();

		String positionInformation();

		String unitType();

		Cabinet positionRef();

		PiuType piuType();

		PositionStruct positionCoordinates();
	}

	private interface PositionStruct extends CorsetStruct {
		Integer longitude();

		Integer latitude();

		Integer altitude();
	}

	private interface PlugInUnit extends Corset {

		@Override
		Slot parent();

		Cabinet positionRef();

		String unitType();

		PiuType piuType();

	}

	private enum PiuRole {
		MP, BP, OTHERS, CMXB, GP;
	}

	private interface PiuType extends Corset {
		AdminProductData productData();

		Integer boardWidth();

		PiuRole role();
	}

	private interface ProductData extends CorsetStruct {
		String productName();

		String productNumber();

		String serialNumber();
	}

	private interface AdminProductData extends CorsetStruct {
		String productName();

		String productNumber();

		String productInfo();
	}

	private interface RfPort extends Corset {

		String dlFrequencyRanges();
	}

	private interface AntennaBranch extends Corset {

	}

	private interface RfBranch extends AntennaBranch {
		RfPort rfPortRef();
	}

	private interface MulticastAntennaBranch extends AntennaBranch {

	}

	private interface AntennaUnitGroup extends Corset {

	}

}
