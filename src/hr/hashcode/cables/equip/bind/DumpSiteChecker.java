package hr.hashcode.cables.equip.bind;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import hr.hashcode.cables.equip.bind.JointNodesInfo.Cabinet;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Container;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Extended;
import hr.hashcode.cables.equip.bind.JointNodesInfo.RemoteExtendedRadio;
import hr.hashcode.cables.equip.comp.Component;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DigitalUnitBaseband;
import hr.hashcode.cables.equip.comp.DigitalUnitStandard;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.Port;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.Site;
import hr.hashcode.cables.equip.comp.SiteContext;

public class DumpSiteChecker {

	private static void requireEmpty(Collection<?> col) {
		if (!col.isEmpty())
			throw new IllegalArgumentException(col.toString());
	}

	private static <T> List<T> collect(CabinetSystem system, Function<Container, Collection<T>> extract) {
		List<T> result = new ArrayList<>();
		for (Cabinet cabinet : system.joint.cabinets) {
			Collection<T> ts = extract.apply(cabinet.container);
			result.addAll(ts);
		}
		return result;
	}

	private static void matchSize(Collection<?> first, Collection<?> second) {
		if (first.size() != second.size())
			throw new IllegalArgumentException(first.size() + "," + second.size());
	}

	public static void check(CabinetSystem system, Site site) {
		SiteContext context = SiteContext.from(site);

		// There is no unassigned HW
		requireEmpty(system.joint.container.duws);
		requireEmpty(system.joint.container.duses);
		requireEmpty(system.joint.container.bbs);
		requireEmpty(system.joint.container.r503s);
		requireEmpty(system.joint.container.radios);
//		requireEmpty(system.joint.container.remotes);

		// Check that hardware numbers match
		matchSize(collect(system, cab -> cab.duws), context.duws());
		matchSize(collect(system, cab -> cab.duses), context.duses());
		matchSize(collect(system, cab -> cab.bbs), context.bbs());
		matchSize(collect(system, cab -> cab.r503s), context.r503s());
		matchSize(collect(system, cab -> cab.radios), context.radiosFlat());

		// Remotes deserve special treatment
		List<RemoteExtendedRadio> remotes = collect(system, cab -> cab.remotes);
		remotes.addAll(system.joint.container.remotes);
		matchSize(remotes, context.remotes());

		// Check that cable numbers match
		matchSize(system.joint.riCables, site.riConnectors);
//		matchSize(system.joint.rfCables, site.rfConnectors);

		// Check that all DU appear in du --> band map
		for (Extended<DigitalUnitWcdma> duw : collect(system, cab -> cab.duws))
			if (!site.duToBands.containsKey(duw.item)) throw new IllegalArgumentException(duw.toString());
		for (Extended<DigitalUnitStandard> duses : collect(system, cab -> cab.duses))
			if (!site.duToBands.containsKey(duses.item)) throw new IllegalArgumentException(duses.toString());
		for (Extended<DigitalUnitBaseband> bbs : collect(system, cab -> cab.bbs))
			if (!site.duToBands.containsKey(bbs.item)) throw new IllegalArgumentException(bbs.toString());

		// Check that all r503s appear in r503 --> band map
		for (R503 r503 : context.r503s()) {
			if (!site.r503ToBands.containsKey(r503)) {
				throw new IllegalArgumentException(r503.toString());
			}
		}

		// Check that all radios appear in band --> radio map. If they don't check that they are RET devices (not connected through RI cbl)
		Set<RadioUnitBase> allRadios = new HashSet<>();
		collect(system, cab -> cab.radios).forEach(x -> allRadios.add(x.item));
		remotes.forEach(x -> allRadios.add(x.item));
		Set<RadioUnitBase> inMap = site.radios.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());
		allRadios.removeAll(inMap);

		DumpUtil.require(allRadios.isEmpty(), allRadios);

		// Check that for each cable, where is a unit
		List<Component> siteHw = new ArrayList<>();
		collect(system, cab -> cab.duws).forEach(x -> siteHw.add(x.item));
		collect(system, cab -> cab.duses).forEach(x -> siteHw.add(x.item));
		collect(system, cab -> cab.bbs).forEach(x -> siteHw.add(x.item));
		collect(system, cab -> cab.r503s).forEach(x -> siteHw.add(x.item));
		collect(system, cab -> cab.radios).forEach(x -> siteHw.add(x.item));
		remotes.forEach(x -> siteHw.add(x.item));

		Set<Port> portSet = Collections.newSetFromMap(new IdentityHashMap<>());
		for (Component component : siteHw)
			portSet.addAll(DumpUtil.ports(component));

		List<Connector<?>> cables = new ArrayList<>();
//		cables.addAll(system.joint.ecCables); --> not transferred
		cables.addAll(system.joint.idlCables);
		cables.addAll(system.joint.riCables);
//		cables.addAll(system.joint.rfCables); --> not transferred
		for (Connector<?> cable : cables) {
			if (!portSet.contains(cable.first()) || !portSet.contains(cable.second())) {
				throw new IllegalArgumentException(cable.first().name() + "," + cable.second().name());
			}
		}
	}
}
