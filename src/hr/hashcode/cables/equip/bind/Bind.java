package hr.hashcode.cables.equip.bind;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static hr.hashcode.cables.equip.comp.Technology.LTE;

import hr.hashcode.cables.equip.bind.JointNodesInfo.Cabinet;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Container;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Extended;
import hr.hashcode.cables.equip.bind.JointNodesInfo.ExtendedDigital;
import hr.hashcode.cables.equip.bind.JointNodesInfo.ExtendedEcOther;
import hr.hashcode.cables.equip.bind.JointNodesInfo.ExtendedHub;
import hr.hashcode.cables.equip.bind.JointNodesInfo.InCabinetExtendedRadio;
import hr.hashcode.cables.equip.bind.JointNodesInfo.InCabinetRadioData;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Location;
import hr.hashcode.cables.equip.bind.JointNodesInfo.RadioData;
import hr.hashcode.cables.equip.bind.JointNodesInfo.RemoteExtendedRadio;
import hr.hashcode.cables.equip.bind.JointNodesInfo.RemoteRadioData;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.CabinetLocation;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawAntFeederCable;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawAntenna;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawBb;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawCabinet;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawCoordinates;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawDigitalCable;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawDus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawDuw;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawEUtranCell;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawEcBus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawExternalNode;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawPiuLink;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawR503;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRadio;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRemoteRus;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRemoteRuw;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRuw;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawSector;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawUnit;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RiUnit;
import hr.hashcode.cables.equip.comp.Antenna;
import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DigitalUnitBaseband;
import hr.hashcode.cables.equip.comp.DigitalUnitStandard;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.EcPort;
import hr.hashcode.cables.equip.comp.IdlPort;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnit;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.RemoteRadioUnit;
import hr.hashcode.cables.equip.comp.RfPort;
import hr.hashcode.cables.equip.comp.RiPort;
import hr.hashcode.cables.equip.comp.RiPort.RiPortable;
import hr.hashcode.cables.ran.NodeInfo;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.Version;
import hr.hashcode.cables.util.Util;

public class Bind {

	private static SingleNodeInfo of(NodeState state) {
		NodeInfo info = NodeInfo.info(state);
		System.out.println(info.fdn);
		switch (info.type) {
			case RBS:
				return new RbsBind().start(state);
			case ERBS:
				return new ErbsBind().start(state);
			case MSRBS:
				return new MsrbsBind().start(state);
			default:
				throw new IllegalArgumentException(info.type.toString());
		}

	}

	public static List<CabinetSystem> systems(List<NodeState> states) {
		List<SingleNodeInfo> infos = Util.map(states, Bind::of);
		return fromInfos(infos);
	}

	private static List<CabinetSystem> fromInfos(List<SingleNodeInfo> infos) {

		Map<String, List<SingleNodeInfo>> map = infos.stream()
				.collect(Collectors.groupingBy(x -> x.node.fdn));

		Map<SingleNodeInfo, Set<SingleNodeInfo>> connected = new IdentityHashMap<>();
		Map<String, Set<SingleNodeInfo>> radios = new HashMap<>();

		for (SingleNodeInfo info : infos) {

			for (RawExternalNode node : info.extNodes) {
				String fdn = node.fdn;
				if (fdn == null || fdn.isEmpty() || "NOT AVAILABLE".equals(fdn))
					continue;

				int index = fdn.indexOf("ManagedElement=");
				if (index == 0)
					fdn = "";
				else if (index > 0)
					fdn = fdn.substring(0, index - 1);

				List<SingleNodeInfo> candidates = map.getOrDefault(fdn, Collections.emptyList());
				for (SingleNodeInfo cand : candidates) {
					connected.computeIfAbsent(cand, x -> Util.identitySet()).add(info);
					connected.computeIfAbsent(info, x -> Util.identitySet()).add(cand);
				}

			}
			List<RawRadio> radioUnits = new ArrayList<>();
			radioUnits.addAll(info.radios);
			radioUnits.addAll(info.ruses);
			radioUnits.addAll(info.remotes);
			radioUnits.addAll(info.remoteRuses);

			for (RawRadio radio : radioUnits) {
				String serial = radio.productInfo.serial;
				if (serial == null || serial.length() < 4)
					continue;
				radios.computeIfAbsent(serial, x -> Util.identitySet()).add(info);
			}
		}

		for (Set<SingleNodeInfo> set : radios.values()) {
			for (SingleNodeInfo first : set)
				for (SingleNodeInfo second : set) {
					if (first == second)
						continue;
					connected.computeIfAbsent(first, x -> Util.identitySet()).add(second);
					connected.computeIfAbsent(second, x -> Util.identitySet()).add(first);
				}
		}

		Set<SingleNodeInfo> visited = Util.identitySet();
		List<List<SingleNodeInfo>> groups = new ArrayList<>();

		for (SingleNodeInfo info : infos) {
			if (!visited.add(info))
				continue;
			List<SingleNodeInfo> group = new ArrayList<>();
			Deque<SingleNodeInfo> deque = new ArrayDeque<>();
			deque.add(info);
			while (!deque.isEmpty()) {
				SingleNodeInfo next = deque.removeFirst();
				group.add(next);
				for (SingleNodeInfo conn : connected.getOrDefault(next, Collections.emptySet()))
					if (visited.add(conn))
						deque.addLast(conn);
			}
			groups.add(group);
		}

		List<CabinetSystem> systems = new ArrayList<>();
		Map<RiUnit, RiPortable> riPortables = new IdentityHashMap<>();
		Map<RawRadio, RadioUnitBase> radioMap = new IdentityHashMap<>();
		for (List<SingleNodeInfo> group : groups) {
			CabinetSystem system = new CabinetSystem();
			systems.add(system);
			system.infos.addAll(group);

			// Map cabinets based on .sharedId
			Map<String, List<RawCabinet>> cabinets = new HashMap<>();
			Map<String, List<RawRadio>> rawRadios = new LinkedHashMap<>();
			for (SingleNodeInfo info : group) {
				for (RawCabinet cabinet : info.cabinets) {
					// If shared ID is missing, serial is taken in its place. If same cabinet exists without shared ID and with it,
					// it is possible that one gets serial, the other shared ID. They will later be merged when we normalize dumps
					String key = (cabinet.sharedId.isEmpty() ? cabinet.info.serial : cabinet.sharedId);
					cabinets.computeIfAbsent(key, x -> new ArrayList<>()).add(cabinet);
				}
				List<RawRadio> list = new ArrayList<>();
				list.addAll(info.radios);
				list.addAll(info.remoteRuses);
				list.addAll(info.remotes);
				list.addAll(info.ruses);
				for (RawRadio radio : list) {
					rawRadios.computeIfAbsent(radio.productInfo.serial, x -> new ArrayList<>()).add(radio);
				}
			}

			// Create and add cabinets
			JointNodesInfo joint = new JointNodesInfo();

			for (SingleNodeInfo info : group) {
				String fdn = info.node.fdn;
				int index = fdn.lastIndexOf('=');
				String name = fdn.substring(index + 1);

				if (info.type == Version.Type.ERBS || info.type == Version.Type.MSRBS)
					joint.name = name;
				else if (joint.name == null) // for RBS set name only if it has not been set by some other node
					joint.name = name;
			}

			Map<RawCabinet, Cabinet> rawToNew = new IdentityHashMap<>();
			Pattern pattern = Pattern.compile("(RBS|SUP)\\s*([0-9]{4})");
			for (Map.Entry<String, List<RawCabinet>> entry : cabinets.entrySet()) {
				String product = null;
				String productNumber = null;
				for (RawCabinet cabinet : entry.getValue()) {

					// Product name (e.g. RBS6601)
					Matcher matcher = pattern.matcher(cabinet.info.productName);
					if (matcher.find()) {
						String current = "RBS" + matcher.group(2);
						if (product != null && !product.equals(current))
							throw new IllegalArgumentException(product + "," + current);
						product = current;
					} else if (cabinet.info.productName.length() > "RBS".length())
						throw new IllegalArgumentException(cabinet.info.productName);

					// Product number (e.g. 1/BFL901009/1)
					String number = cabinet.info.productNumber;
					// 3 is a magic number, we need last 3 characters to conclude RBS subtype. See Ericsson presentation, slide 41
					if (number.length() >= 3) {
						if (productNumber != null && !productNumber.equals(number)) {
							throw new IllegalArgumentException(productNumber + "," + number);
						}
						productNumber = number;
					}
				}

				Cabinet cabinet = new Cabinet(entry.getKey(), product, productNumber);
				joint.cabinets.add(cabinet);
				for (RawCabinet raw : entry.getValue())
					rawToNew.put(raw, cabinet);
			}

			// Do something with radios
			int radioCounter = 1;
			for (Map.Entry<String, List<RawRadio>> entry : rawRadios.entrySet()) {

				List<List<RawRadio>> llist;
				String serial = entry.getKey();
				if (serial == null || serial.length() < 4)
					llist = entry.getValue().stream().map(Collections::singletonList).collect(Collectors.toList());
				else
					llist = Collections.singletonList(entry.getValue());

				for (List<RawRadio> list : llist) {
					UniqueValue<Boolean> remote = new UniqueValue<>();
					UniqueValue<String> type = new UniqueValue<>();
					UniqueValue<Band> band = new UniqueValue<>();

					// These 3 should all have same iteration order, we depend on this in dump to site conversion.
					// TODO make a better structure for this, not three separate collections
					List<RadioData> radioDatas = new ArrayList<>();

					int unitCount = 0;
					Cabinet cabinetDecision = null;
					for (RawRadio radio : list) {
						boolean aretu = (radio instanceof RawRuw) && ((RawRuw) radio).auType.equals("ARETU");
						boolean lteIru = (radio instanceof RawRemoteRus) && radio.name.contains("IRU");
						if (aretu || lteIru)
							continue; // RET units are not real radios, they don't have band/cells. IRU is not supported earlier

						unitCount += 1;

						if (radio instanceof RawRemoteRuw || radio instanceof RawRemoteRus)
							remote.register(true);
						else
							remote.register(false);

						if (radio.bands.size() != 1) {
							throw new IllegalArgumentException(radio.bands.toString()); // There can be only one band per radio
						}
						band.register(radio.bands.iterator().next().band());

						// If position refs do not agree (and this happened), take LTE one
						Cabinet cabinet = rawToNew.get(radio.positionRef);
						if (cabinet != null && (cabinetDecision == null || radio.bands.stream().anyMatch(tb -> tb.technology() == LTE)))
							cabinetDecision = cabinet;
						type.register(radio.productInfo.productName);

						ArrayList<String> sortedCells = new ArrayList<>(radio.cells);
						sortedCells.sort(String::compareTo);
						final RadioData radioData;
						if (remote.value()) {
							radioData = new RemoteRadioData(radio.name, sortedCells, radio.dumpType);
						} else {
							Optional<CabinetLocation> location = Optional.ofNullable(radio.location);
							radioData = new InCabinetRadioData(location, radio.name, sortedCells, radio.dumpType);
						}
						radioDatas.add(radioData);
					}
					if (unitCount == 0) continue; // All units were ARETU or IRU
					Container container = cabinetDecision == null ? joint.container : cabinetDecision.container;

					RiPortable riPortable;
					RadioUnitBase radioBase;
					String suffix = String.valueOf(radioCounter);
					if (remote.value()) {
						RemoteRadioUnit radio = new RemoteRadioUnit("Remote-" + suffix, type.value());
						radioBase = radio;
						riPortable = radio;
						RemoteExtendedRadio extended = new RemoteExtendedRadio(band.value(),
								Util.map(radioDatas, r -> (RemoteRadioData) r));
						extended.item = radio;
						container.remotes.add(extended);
					} else {
						RadioUnit radio = new RadioUnit("RU-" + suffix, type.value());
						riPortable = radio;
						radioBase = radio;
						InCabinetExtendedRadio extended = new InCabinetExtendedRadio(band.value(), Util.map(radioDatas, r ->
								(InCabinetRadioData) r));
						extended.item = radio;
						container.radios.add(extended);
					}
					radioCounter += 1;

					for (RawRadio radio : list) {
						radioMap.put(radio, radioBase);
						riPortables.put(radio, riPortable);
					}
				}
			}

			Map<RfPort, List<RawAntenna>> portToAntennas = new IdentityHashMap<>();
			Map<RawAntenna, List<RfPort>> antennasToPort = new IdentityHashMap<>();
			List<RawAntenna> all = new ArrayList<>();

			for (SingleNodeInfo info : group) {

				all.addAll(info.rawAntennas);

				for (RawAntFeederCable cable : info.antennaCables) {
					RfPort radioPort = radioMap.get(cable.radio).rfPort(cable.radioPort);
					RawAntenna antenna = cable.antenna;

					portToAntennas.computeIfAbsent(radioPort, x -> new ArrayList<>()).add(cable.antenna);
					antennasToPort.computeIfAbsent(antenna, x -> new ArrayList<>()).add(radioPort);
				}
			}

			Set<RawAntenna> visitedAntenna = Util.identitySet();
			Map<RawAntenna, Antenna> antennas = new IdentityHashMap<>();

			for (RawAntenna raw : all) {
				if (!visitedAntenna.add(raw))
					continue;
				Antenna antenna = new Antenna("Antenna-" + raw.name);
				joint.antennas.add(antenna);
				Deque<RawAntenna> queue = new ArrayDeque<>();
				queue.addLast(raw);
				while (!queue.isEmpty()) {
					RawAntenna next = queue.removeFirst();
					antennas.put(next, antenna);
					for (RfPort port : antennasToPort.getOrDefault(next, Collections.emptyList()))
						for (RawAntenna other : portToAntennas.getOrDefault(port, Collections.emptyList()))
							if (visitedAntenna.add(other))
								queue.addLast(other);
				}
			}

			int count = 0;
			double longitude = 0;
			double latitude = 0;

			for (SingleNodeInfo info : group) {

				Container singleCabinet = null;
				if (info.cabinets.size() == 1) {
					RawCabinet rawCabinet = info.cabinets.get(0);
					singleCabinet = rawToNew.get(rawCabinet).container;
				}

				List<RawCoordinates> coords = new ArrayList<>();
				for (RawSector sector : info.sectors)
					coords.add(sector.coordinates);
				for (RawEUtranCell cell : info.cells)
					coords.add(cell.coordinates);
				for (RawCoordinates coord : coords) {
					if (coord != null && coord.latitude != 0 && coord.longitude != 0) {
						count++;
						longitude += coord.longitude;
						latitude += coord.latitude;
					}
				}

				Map<RawEcBus, ExtendedHub> hubs = new IdentityHashMap<>();
				for (RawEcBus ecBus : info.ecBuses) {
					ExtendedHub hub = new ExtendedHub(ecBus.name);
					joint.hubs.add(hub);
					hubs.put(ecBus, hub);
				}

				Map<RawDuw, DigitalUnitWcdma> duws = new IdentityHashMap<>();
				for (RawDuw raw : info.duws) {
					String name = "DUW";
					String type = raw.productInfo.productName;
					Cabinet cabinet = rawToNew.get(raw.positionRef);
					Location location = new Location(raw.location.subrack, raw.location.slot);
					ExtendedDigital<DigitalUnitWcdma> duw = new ExtendedDigital<>();
					duw.item = new DigitalUnitWcdma(name, type);
					duw.location = location;

					Container container = cabinet == null
							? (singleCabinet == null ? joint.container : singleCabinet)
							: cabinet.container;

					container.duws.add(duw);
					riPortables.put(raw, duw.item);
					duws.put(raw, duw.item);
					if (raw.ecConn != null) {
						ExtendedHub hub = hubs.get(raw.ecConn.ecBus);
						String position = raw.ecConn.ecHubPosition;
						EcPort other;
						if (position == null || position.isEmpty())
							other = hub.undefined();
						else
							other = hub.ports.port(position);
						joint.ecCables.add(new Connector<>(duw.item.ecPort(), other));
					}
				}

				for (RawDus raw : info.duses) {
					String name = "DUS";
					String type = raw.productInfo.productName;
					Cabinet cabinet = rawToNew.get(raw.positionRef);
					Location location = new Location(raw.location.subrack, raw.location.slot);
					ExtendedDigital<DigitalUnitStandard> dus = new ExtendedDigital<>();
					dus.item = new DigitalUnitStandard(name, type);
					dus.location = location;

					Container container = cabinet == null
							? (singleCabinet == null ? joint.container : singleCabinet)
							: cabinet.container;

					container.duses.add(dus);
					riPortables.put(raw, dus.item);

					if (raw.ecConn != null) {
						ExtendedHub hub = hubs.get(raw.ecConn.ecBus);
						String position = raw.ecConn.ecHubPosition;
						EcPort other;
						if (position == null || position.isEmpty())
							other = hub.undefined();
						else
							other = hub.ports.port(position);

						joint.ecCables.add(new Connector<>(dus.item.ecPort(), other));
					}

				}

				for (RawBb raw : info.bbs) {
					String name = "BB";
					String type = raw.productInfo.productName;
					Extended<DigitalUnitBaseband> bb = new Extended<>();
					bb.item = new DigitalUnitBaseband(name, type);
					Cabinet cabinet = rawToNew.get(raw.positionRef);

					Container container = cabinet == null
							? (singleCabinet == null ? joint.container : singleCabinet)
							: cabinet.container;

					container.bbs.add(bb);
					riPortables.put(raw, bb.item);

					ExtendedHub hub = hubs.get(raw.ecConn.ecBus);
					String position = raw.ecConn.ecHubPosition;
					EcPort other;
					if (position == null || position.isEmpty())
						other = hub.undefined();
					else
						other = hub.ports.port(position);

					joint.ecCables.add(new Connector<>(bb.item.ecPort(), other));

				}

				for (RawR503 raw : info.r503s) {
					String name = "R503";

					Extended<R503> r503 = new Extended<>();
					r503.item = new R503(name);
					Cabinet cabinet = rawToNew.get(raw.positionRef);

					Container container = cabinet == null
							? (singleCabinet == null ? joint.container : singleCabinet)
							: cabinet.container;

					container.r503s.add(r503);
					riPortables.put(raw, r503.item);

					if (raw.ecConn != null) {
						ExtendedHub hub = hubs.get(raw.ecConn.ecBus);
						String position = raw.ecConn.ecHubPosition;
						EcPort other;
						if (position == null || position.isEmpty())
							other = hub.undefined();
						else
							other = hub.ports.port(position);

						joint.ecCables.add(new Connector<>(r503.item.ecPort(), other));
					}
				}

				List<RawUnit> otherUnits = new ArrayList<>();
				otherUnits.addAll(info.hwUnits);
				otherUnits.addAll(info.hws);

				for (RawUnit unit : otherUnits) {
					Cabinet cabinet = rawToNew.get(unit.positionRef);

					Container container = cabinet == null
							? (singleCabinet == null ? joint.container : singleCabinet)
							: cabinet.container;

					ExtendedEcOther ecOther =
							new ExtendedEcOther(unit.productInfo.productName, unit.productInfo.productNumber);
					container.ecOthers.add(ecOther);

					if (unit.ecConn != null) {
						ExtendedHub hub = hubs.get(unit.ecConn.ecBus);
						String position = unit.ecConn.ecHubPosition;
						EcPort other;
						if (position == null || position.isEmpty() || position.equals("NA"))
							other = hub.undefined();
						else
							other = hub.ports.port(position);

						joint.ecCables.add(new Connector<>(ecOther.ecPort.port(), other));
					}
				}

				for (RawDigitalCable cable : info.digitalCables) {
					RiPort first = riPortables.get(cable.unit1).riPort(cable.port1);
					RiPort second = riPortables.get(cable.unit2).riPort(cable.port2);
					Connector<RiPort> riCable = new Connector<>(first, second);
					joint.riCables.add(riCable);
				}

				Set<RfPort> rfPorts = Util.identitySet();
				for (RawAntFeederCable cable : info.antennaCables) {
					RfPort radioPort = radioMap.get(cable.radio).rfPort(cable.radioPort);
					if (!rfPorts.add(radioPort))
						continue;
					RfPort antennaPort =
							antennas.get(cable.antenna).rfPort(cable.antenna.branch(cable.antennaPort));
					joint.rfCables.add(new Connector<>(radioPort, antennaPort));
				}

				for (RawPiuLink link : info.piuLinks) {
					IdlPort idl1 = duws.get(link.primary).idlPort();
					IdlPort idl2 = duws.get(link.secondary).idlPort();
					joint.idlCables.add(new Connector<>(idl1, idl2));
				}
			}

			if (count != 0) {
				joint.longitude = longitude / count;
				joint.latitude = latitude / count;
			}

			system.joint = joint;
		}

		return systems;
	}

}
