package hr.hashcode.cables.equip.bind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import hr.hashcode.cables.equip.bind.JointNodesInfo.Cabinet;
import hr.hashcode.cables.equip.bind.JointNodesInfo.Extended;
import hr.hashcode.cables.equip.bind.JointNodesInfo.ExtendedEcOther;
import hr.hashcode.cables.equip.bind.JointNodesInfo.ExtendedHub;
import hr.hashcode.cables.equip.bind.JointNodesInfo.ExtendedRadio;
import hr.hashcode.cables.equip.bind.JointNodesInfo.InCabinetExtendedRadio;
import hr.hashcode.cables.equip.comp.Antenna;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.Port;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.RfPort;
import hr.hashcode.cables.equip.comp.RiPort;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;

public class CabinetSystemVisual extends HBox {

	private final Map<Port, Label> portNodes = new IdentityHashMap<>();

	private Label registerPort(Port port) {
		Label label = new Label(port.name());
		label.setPrefHeight(20);
		label.setPrefWidth(20);
		label.setAlignment(Pos.CENTER);
		setBorderStyle(label);
		portNodes.put(port, label);
		return label;
	}

	private void connect(List<? extends Connector<?>> list, Node ref, List<Shape> shapes, BooleanProperty show) {
		for (Connector<?> conn : list) {
			Shape shape = connect(ref, conn);
			shape.strokeWidthProperty().bind(Bindings.when(show).then(1).otherwise(0));
			shape.setStroke(Color.RED);
			shapes.add(shape);
		}
	}

	private Line connect(Node ref, Connector<?> connector) {
		Node node1 = portNodes.get(connector.first());
		Node node2 = portNodes.get(connector.second());
		if (node1 == null || node2 == null)
			throw new IllegalArgumentException(connector.first().name() + "," + connector.second().name());

		ObjectBinding<Bounds> bounds1 = bounds(node1, ref);
		ObjectBinding<Bounds> bounds2 = bounds(node2, ref);

		Line line = new Line();
		line.startXProperty().bind(x(bounds1));
		line.startYProperty().bind(y(bounds1));
		line.endXProperty().bind(x(bounds2));
		line.endYProperty().bind(y(bounds2));
		return line;
	}

	public static double getPointDistance(double x1, double y1, double x2, double y2) {
		return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
	}

	private static ObjectBinding<Bounds> bounds(Node node, Node ref) {
		ReadOnlyObjectProperty<Bounds> bounds = node.boundsInLocalProperty();
		return Bindings.createObjectBinding(() -> {
			return ref.sceneToLocal(node.localToScene(bounds.get()));
		}, bounds);
	}

	private static DoubleBinding x(ObservableValue<Bounds> bounds) {
		return Bindings.createDoubleBinding(() -> (bounds.getValue().getMaxX() + bounds.getValue().getMinX()) / 2,
				bounds);
	}

	private static DoubleBinding y(ObservableValue<Bounds> bounds) {
		return Bindings.createDoubleBinding(() -> (bounds.getValue().getMaxY() + bounds.getValue().getMinY()) / 2,
				bounds);
	}

	public static class Switches {
		public final BooleanProperty ec;
		public final BooleanProperty ri;
		public final BooleanProperty idl;
		public final BooleanProperty rf;

		private Switches(BooleanProperty ec, BooleanProperty ri, BooleanProperty idl, BooleanProperty rf) {
			this.ec = ec;
			this.ri = ri;
			this.idl = idl;
			this.rf = rf;
		}
	}

	CabinetSystemVisual(CabinetSystem system) {

		CheckBox ec = new CheckBox("EC cables");
		CheckBox ri = new CheckBox("RI cables");
		CheckBox idl = new CheckBox("IDL cables");
		CheckBox rf = new CheckBox("RF cables");

		VBox controls = new VBox(ec, ri, idl, rf);

		Switches switches = new Switches(ec.selectedProperty(), ri.selectedProperty(), idl.selectedProperty(),
				rf.selectedProperty());

		DoubleProperty zoom = new SimpleDoubleProperty(1);
		Group group = new Group();

		group.scaleXProperty().bind(zoom);
		group.scaleYProperty().bind(zoom);
		group.scaleZProperty().bind(zoom);

		Button zoomIn = new Button("Zoom in");
		zoomIn.setOnAction(e -> zoom.set(zoom.get() + 0.1));
		Button zoomOut = new Button("Zoom out");
		zoomOut.setOnAction(e -> zoom.set(zoom.get() - 0.1));

		HBox buttons = new HBox(zoomIn, zoomOut);
		VBox left = new VBox(controls, buttons);

		getChildren().add(left);
		getChildren().add(new ScrollPane(new Group(group)));

		VBox total = new VBox();

		HBox antennas = new HBox();

		for (Antenna antenna : system.joint.antennas)
			antennas.getChildren().add(new AntennaVisual(antenna));

		HBox hubs = new HBox();
		for (ExtendedHub hub : system.joint.hubs)
			hubs.getChildren().add(new HubVisual(hub));
		HBox digitals = new HBox();
		HBox outRadios = new HBox();
		HBox others = new HBox();

		appendDigital(digitals, system.joint.container.duws);
		appendDigital(digitals, system.joint.container.duses);
		appendDigital(digitals, system.joint.container.bbs);
		appendR503(digitals, system.joint.container.r503s);
		appendRadio(outRadios, system.joint.container.radios);
		appendRadio(outRadios, system.joint.container.remotes);
		appendOthers(others, system.joint.container.ecOthers);

		total.getChildren().addAll(antennas, hubs, others, digitals, outRadios);

		HBox cabs = new HBox();
		total.getChildren().add(cabs);
		for (Cabinet cabinet : system.joint.cabinets) {
			VBox cabBox = new VBox();
			cabs.getChildren().add(cabBox);
			Label label = new Label(cabinet.type + "\n" + cabinet.name);
			setBorderStyle(label);
			cabBox.getChildren().add(label);

			HBox inRadios = new HBox();
			HBox inOthers = new HBox();
			appendDigital(cabBox, cabinet.container.duws);
			appendDigital(cabBox, cabinet.container.duses);
			appendDigital(cabBox, cabinet.container.bbs);
			appendR503(cabBox, cabinet.container.r503s);
			appendRadio(inRadios, cabinet.container.radios);
			appendOthers(inOthers, cabinet.container.ecOthers);
			cabBox.getChildren().addAll(inOthers, inRadios);
			setBorderStyle(cabBox);

		}

		List<Shape> lines = new ArrayList<>();
		connect(system.joint.riCables, total, lines, switches.ri);
		connect(system.joint.ecCables, total, lines, switches.ec);
		connect(system.joint.idlCables, total, lines, switches.idl);
		connect(system.joint.rfCables, total, lines, switches.rf);

		group.getChildren().add(total);
		group.getChildren().addAll(lines);

	}

	private void appendOthers(Pane region, List<ExtendedEcOther> units) {
		for (ExtendedEcOther ext : units)
			region.getChildren().add(new EcOtherVisual(ext));
	}

	private void appendDigital(Pane region, List<? extends Extended<? extends DigitalUnit>> units) {
		for (Extended<? extends DigitalUnit> ext : units)
			region.getChildren().add(new DigitalUnitVisual(ext));
	}

	private void appendRadio(Pane region, List<? extends ExtendedRadio<?>> units) {
		for (ExtendedRadio<?> ext : units)
			region.getChildren().add(new RadioUnitVisual(ext));
	}

	private void appendR503(Pane region, List<? extends Extended<? extends R503>> units) {
		for (Extended<? extends R503> ext : units)
			region.getChildren().add(new R503Visual(ext));
	}

	static void setBorderStyle(Region region) {
		region.setStyle("-fx-border-color: blue;" +
				"-fx-border-width: 1;");
	}

	private class DigitalUnitVisual extends HBox {
		private DigitalUnitVisual(Extended<? extends DigitalUnit> extDu) {
			setPadding(new Insets(10, 10, 10, 10));

			DigitalUnit du = extDu.item;

			HBox riPorts = new HBox();
			riPorts.setSpacing(10);
			riPorts.setPadding(new Insets(3, 3, 3, 3));
			for (RiPort riPort : du.riPorts().ports()) {
				Label port = registerPort(riPort);
				riPorts.getChildren().add(port);
			}

			Node ecPort = registerPort(du.ecPorts().port());
			HBox ports = new HBox(riPorts, ecPort);
			ports.setSpacing(20);
			ports.setPadding(new Insets(3, 3, 3, 3));

			if (du instanceof DigitalUnitWcdma) {
				DigitalUnitWcdma duw = (DigitalUnitWcdma) du;
				Node idlPort = registerPort(duw.idlPorts().port());
				ports.getChildren().add(idlPort);
			}

			setSpacing(30);
			getChildren().addAll(new Label(du.name() + "\n" + du.type()), ports);
			setBorderStyle(this);
		}
	}

	private class RadioUnitVisual extends VBox {

		public RadioUnitVisual(ExtendedRadio<?> ext) {
			RadioUnitBase radio = ext.item;
			setPadding(new Insets(10, 10, 10, 10));

			String Enter = "\n";
			String band = ext.band + " " + (ext.lte() != null ? "L" : "") + (ext.wcdma() != null ? "W" : "") + Enter;

			List<String> cells = new ArrayList<>();
			cells.addAll(ext.lte() != null ? ext.lte().cells : Collections.emptyList());
			cells.addAll(ext.wcdma() != null ? ext.wcdma().cells : Collections.emptyList());

			List<String> locations = new ArrayList<>();
			if (ext instanceof InCabinetExtendedRadio) {
				InCabinetExtendedRadio inCabinet = (InCabinetExtendedRadio) ext;
				if (inCabinet.lte() != null && inCabinet.lte().location.isPresent())
					locations.add(inCabinet.lte().location.get().toString());
				if (inCabinet.wcdma() != null && inCabinet.wcdma().location.isPresent())
					locations.add(inCabinet.wcdma().location.get().toString());
			}

			Label additional = new Label(band + Enter + cells + Enter + locations);
			Label label = new Label(radio.name() + "\n" + radio.type());
			HBox labels = new HBox(label, additional);
			labels.setSpacing(10);
			labels.setRotate(-90);

			VBox ports = new VBox();
			ports.setPadding(new Insets(3, 3, 3, 3));

			for (RfPort port : radio.rfPorts().ports()) {
				Label portLabel = registerPort(port);
				portLabel.setPrefWidth(50);
				ports.getChildren().add(portLabel);
			}
			for (RiPort port : radio.riPorts().ports()) {
				Label portV = registerPort(port);
				portV.setPrefWidth(50);
				ports.getChildren().add(portV);
			}

			setSpacing(50);
			getChildren().addAll(new Group(labels), ports);

			setBorderStyle(this);
		}

	}

	private class R503Visual extends HBox {

		R503Visual(Extended<? extends R503> ext) {
			setPadding(new Insets(10, 10, 10, 10));
			R503 r503 = ext.item;
			HBox riPorts = new HBox();
			riPorts.setSpacing(10);
			riPorts.setPadding(new Insets(3, 3, 3, 3));

			riPorts.getChildren().add(riGroup(r503, 16, 4));
			riPorts.getChildren().add(riGroup(r503, 12, 4));
			riPorts.getChildren().add(riGroup(r503, 7, 4));
			riPorts.getChildren().add(riGroup(r503, 3, 3));

			getChildren().addAll(new Label("R503"), riPorts, registerPort(r503.ecPort()));
			setSpacing(30);
			setBorderStyle(this);
		}

		private HBox riGroup(R503 r503, int start, int count) {
			HBox box = new HBox();
			box.setSpacing(5);
			box.setPadding(new Insets(3, 3, 3, 3));
			for (int i = start; i > start - count; i--) {
				Label label = registerPort(r503.riPorts().port(String.valueOf(i)));
				box.getChildren().add(label);
			}
			return box;
		}

	}

	private class HubVisual extends VBox {

		HubVisual(ExtendedHub hub) {
			setPadding(new Insets(10, 10, 10, 10));
			getChildren().add(new Label(hub.name));
			for (String prefix : Arrays.asList("A", "B")) {
				HBox row = new HBox();
				row.setPadding(new Insets(3, 3, 3, 3));
				row.getChildren().add(registerPort(hub.ports.port(prefix + "in")));
				for (int i = 0; i <= 8; i++)
					row.getChildren().add(registerPort(hub.ports.port(prefix + i)));
				getChildren().add(row);
			}
			getChildren().add(registerPort(hub.ports.port("CLU")));
			HBox row = new HBox();
			row.setPadding(new Insets(3, 3, 3, 3));
			getChildren().add(row);
			for (String name : Arrays.asList("A", "B", "C", "D", "E", "X", "Y", "SAU")) {
				row.getChildren().add(registerPort(hub.ports.port(name)));
			}
			getChildren().add(registerPort(hub.undefined()));
			setBorderStyle(this);
		}
	}

	private class EcOtherVisual extends VBox {
		EcOtherVisual(ExtendedEcOther other) {
			setPadding(new Insets(10, 10, 10, 10));
			getChildren().add(new Label(other.name));
			getChildren().add(registerPort(other.ecPort.port()));
			setBorderStyle(this);
		}
	}

	private class AntennaVisual extends VBox {
		AntennaVisual(Antenna antenna) {
			setPadding(new Insets(10, 10, 10, 10));
			getChildren().add(new Label("Ant"));
			HBox ports = new HBox();
			ports.setPadding(new Insets(3, 3, 3, 3));
			getChildren().add(ports);
			ports.getChildren().add(registerPort(antenna.rfPort("A")));
			ports.getChildren().add(registerPort(antenna.rfPort("B")));
			setBorderStyle(this);
		}
	}

}
