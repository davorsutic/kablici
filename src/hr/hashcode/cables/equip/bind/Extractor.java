package hr.hashcode.cables.equip.bind;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import hr.hashcode.cables.ran.Meta.MoEnum;
import hr.hashcode.cables.ran.NodeState;
import hr.hashcode.cables.ran.NodeState.ManagedArray;
import hr.hashcode.cables.ran.NodeState.ManagedAttribute;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.ran.NodeState.ManagedStruct;
import hr.hashcode.cables.util.Util;

class Extractor {

	static interface Corset {

		NodeState state();

		ManagedObject under();

		Corset parent();

		String name();

		<T extends Corset> List<T> children(Class<T> clazz);

	}

	static interface CorsetStruct {
		ManagedStruct under();
	}

	private final NodeState state;
	private final Map<ManagedObject, Corset> map = new IdentityHashMap<>();
	private final Map<String, Class<? extends Corset>> classes;

	Extractor(NodeState state, Class<?> enclosing) {
		this.state = state;

		Map<String, Class<? extends Corset>> classes = new HashMap<>();
		for (Class<?> temp = enclosing; temp != null; temp = temp.getSuperclass())
			for (Class<?> clazz : temp.getDeclaredClasses())
				if (Corset.class.isAssignableFrom(clazz))
					classes.put(clazz.getSimpleName(), clazz.asSubclass(Corset.class));

		this.classes = classes;
	}

	<T extends Corset> List<T> extract(Class<T> clazz) {
		String className = clazz.getSimpleName();
		if (classes.getOrDefault(className, Corset.class) != clazz)
			throw new IllegalArgumentException(clazz.toString());
		List<ManagedObject> instances = Util.filter(state.objects(), x -> x.moClass.name.equals(className));
		List<T> result = new ArrayList<>();

		for (ManagedObject instance : instances) {
			Corset funObject = wrap(instance);
			result.add(clazz.cast(funObject));
		}
		return result;
	}

	private <T extends Corset> List<T> children(ManagedObject object, Class<T> clazz) {
		List<T> list = new ArrayList<>();
		for (ManagedObject obj : state.objects())
			if (obj.parent == object && obj.moClass.name.equals(clazz.getSimpleName()))
				list.add(clazz.cast(wrap(obj)));
		return list;
	}
	private Corset wrap(ManagedObject object) {
		if (object == null)
			return null;
		Corset instance = map.get(object);
		if (instance != null)
			return instance;

		Corset parent = wrap(object.parent);

		InvocationHandler handler = (proxy, method, args) -> {
			String name = method.getName();
			if ("parent".equals(name))
				return parent;
			else if ("name".equals(name))
				return object.name;
			else if ("under".equals(name))
				return object;
			else if ("state".equals(name))
				return state;
			else if ("children".equals(name)) {
				Class<? extends Corset> clazz = ((Class<?>) args[0]).asSubclass(Corset.class);
				return children(object, clazz);
			} else if ("toString".equals(name))
				return object.toString();
			else if ("hashCode".equals(name))
				return object.hashCode();
			else if ("equals".equals(name)) {
				Object arg = args[0];
				if (arg instanceof Corset) {
					Corset cast = (Corset) arg;
					return object == cast.under();
				} else
					return false;
			} else {
				for (ManagedAttribute attr : object.attrs())
					if (attr.attribute.name.equals(name)) {
						Class<?> retType = method.getReturnType();
						return value(retType, attr.value);
					}
				System.err.println(object + ":" + name);
				return null;
			}
		};

		Class<? extends Corset> clazz = classes.getOrDefault(object.moClass.name, Corset.class);

		Class<?>[] ifaces = {clazz};
		Object proxy = Proxy.newProxyInstance(Bind.class.getClassLoader(), ifaces, handler);
		Corset result = clazz.cast(proxy);
		map.put(object, result);
		return result;
	}

	private Object value(Class<?> retType, Object value) {
		if (value == null)
			return null;
		if (Corset.class.isAssignableFrom(retType)) {
			if (value instanceof ManagedObject)
				return wrap((ManagedObject) value);
			else
				throw new IllegalArgumentException();
		} else if (CorsetStruct.class.isAssignableFrom(retType)) {
			if (value instanceof ManagedStruct)
				return wrap((ManagedStruct) value, retType.asSubclass(CorsetStruct.class));
			else
				throw new IllegalArgumentException();
		} else if (retType.isEnum()) {
			if (value instanceof MoEnum.Element) {
				MoEnum.Element element = (MoEnum.Element) value;

				@SuppressWarnings("unchecked")
				Class<? extends Enum<?>> cast = (Class<? extends Enum<?>>) retType.asSubclass(Enum.class);

				return member(cast, element.name);
			} else
				throw new IllegalArgumentException();
		} else if (retType.isArray()) {
			if (value instanceof ManagedArray) {
				ManagedArray manArray = (ManagedArray) value;
				Class<?> compType = retType.getComponentType();
				int length = manArray.length();
				Object[] array = (Object[]) Array.newInstance(compType, length);
				for (int i = 0; i < length; i++)
					array[i] = value(compType, manArray.value(i));
				return array;
			} else
				throw new IllegalArgumentException();
		} else
			return value;
	}

	private <V extends Enum<V>> Object member(Class<? extends Enum<?>> clazz, String name) {
		@SuppressWarnings("unchecked")
		Class<V> cast = (Class<V>) clazz;
		return Enum.valueOf(cast, name);
	}

	private <T extends CorsetStruct> T wrap(ManagedStruct struct, Class<T> clazz) {
		InvocationHandler handler = (proxy, method, args) -> {
			String name = method.getName();
			if ("under".equals(name))
				return struct;
			else if ("toString".equals(name))
				return struct.toString();
			else if ("hashCode".equals(name))
				return struct.hashCode();
			else if ("equals".equals(name)) {
				Object arg = args[0];
				if (arg instanceof CorsetStruct) {
					CorsetStruct cast = (CorsetStruct) arg;
					return struct == cast.under();
				} else
					return false;
			} else {
				for (ManagedAttribute attr : struct.attrs())
					if (attr.attribute.name.equals(name)) {
						Class<?> retType = method.getReturnType();
						return value(retType, attr.value);
					}
				return null;
			}
		};
		Class<?>[] ifaces = {clazz};
		Object proxy = Proxy.newProxyInstance(Bind.class.getClassLoader(), ifaces, handler);
		return clazz.cast(proxy);
	}

}
