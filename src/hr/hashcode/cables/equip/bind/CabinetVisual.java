package hr.hashcode.cables.equip.bind;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import hr.hashcode.cables.equip.bind.SingleNodeInfo.Fields;
import hr.hashcode.cables.equip.bind.SingleNodeInfo.RawRuw;
import hr.hashcode.cables.equip.comp.Site;
import hr.hashcode.cables.visual.site.SiteVisual;
import javafx.beans.property.BooleanProperty;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Region;

public class CabinetVisual {

	private Label box(Fields fields) {
		Label label = new Label(fields.report());
		setBorderStyle(label);
		return label;
	}

	public static class Switches {
		public final BooleanProperty ec;
		public final BooleanProperty ri;
		public final BooleanProperty idl;
		public final BooleanProperty rf;

		private Switches(BooleanProperty ec, BooleanProperty ri, BooleanProperty idl, BooleanProperty rf) {
			this.ec = ec;
			this.ri = ri;
			this.idl = idl;
			this.rf = rf;
		}
	}

	public final Map<CabinetSystem, List<Tab>> singleInfoTabs;
	public final Map<CabinetSystem, Tab> jointTabs;
	public final Map<CabinetSystem, Tab> siteTabs;

	public CabinetVisual(List<Site> sites, List<CabinetSystem> systems) {

		TabPane total = new TabPane();
		total.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		this.singleInfoTabs = new IdentityHashMap<>();
		this.jointTabs = new IdentityHashMap<>();
		this.siteTabs = new IdentityHashMap<>();

		int count = 0;
		for (CabinetSystem system : systems) {
			List<Tab> tabs = new ArrayList<>();
			singleInfoTabs.put(system, tabs);
			for (SingleNodeInfo info : system.infos) {

				FlowPane pane = new FlowPane();

				pane.setPrefHeight(800);
				pane.setPrefWidth(1500);

				Collections.sort(info.radios, Comparator.comparing((RawRuw x) -> x.auType));

				for (List<? extends Fields> list : info.lists())
					for (Fields raw : list)
						if (raw != null)
							pane.getChildren().addAll(box(raw));

				String fdn = info.node.fdn;
				String name = fdn.substring(fdn.lastIndexOf('=') + 1);

				Tab tab = new Tab(name, new ScrollPane(pane));
				tabs.add(tab);
			}

			Tab joint = new Tab("Joint", new CabinetSystemVisual(system));
			jointTabs.put(system, joint);

			Site site = sites.get(count);
			count++;
			Tab siteTab = new Tab("Site", new SiteVisual(site));
			siteTabs.put(system, siteTab);

		}

	}

	private static void setBorderStyle(Region... regions) {
		for (Region region : regions) {
			region.setStyle("-fx-border-color: blue;" +
					"-fx-border-width: 1;");
		}
	}

}
