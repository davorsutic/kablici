package hr.hashcode.cables.equip.musenki;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.util.Util;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.stage.Stage;

public class Musenki {

	private static class Hashed {

		private final Object[] objects;

		Hashed(Object... objects) {
			this.objects = objects.clone();
		}

		@Override
		public int hashCode() {
			return Arrays.hashCode(objects);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof Hashed))
				return false;
			Hashed other = (Hashed) obj;
			return Arrays.equals(objects, other.objects);
		}

		@Override
		public String toString() {
			return Arrays.toString(objects);
		}

	}

	static class Configuration extends Hashed {
		final Band band;
		final String system;
		final String cabinetType;
		final String cabinetConf;
		final String wcdmaBbu;
		final String lteBbu;
		final String r503;
		final String rru;
		final String sector;

		private Configuration(Band band, String system, String cabinetType, String cabinetConf, String wcdmaBbu,
				String lteBbu, String r503, String rru, String sector) {
			super(band, system, cabinetType, cabinetConf, wcdmaBbu, lteBbu, r503, rru);
			this.band = band;
			this.system = system;
			this.cabinetType = cabinetType;
			this.cabinetConf = cabinetConf;
			this.wcdmaBbu = wcdmaBbu;
			this.lteBbu = lteBbu;
			this.r503 = r503;
			this.rru = rru;
			this.sector = sector;
		}
	}

	static class Joint extends Hashed {
		final int line;
		final String configuration;
		final List<Band> wcdmaPrimary;
		final Band ltePrimary;
		final List<Configuration> confs;

		private final Map<Band, Configuration> confMap;

		private Joint(int line, String configuration, List<Band> wcdmaPrimary, Band ltePrimary,
				List<Configuration> confs) {
			super(configuration, ltePrimary, wcdmaPrimary, confs);
			this.line = line;
			this.configuration = configuration;
			this.wcdmaPrimary = wcdmaPrimary;
			this.ltePrimary = ltePrimary;
			this.confs = confs;
			this.confMap = Util.toMap(confs, x -> x.band);
		}

		Configuration get(Band band) {
			return confMap.get(band);
		}

		@Override
		public String toString() {
			return line + ":" + configuration + "," + ltePrimary + "," + wcdmaPrimary + "\t" + confs;
		}
	}

	private static String filter(String string) {
		if (string == null)
			return null;
		String trim = string.trim();
		if (trim.isEmpty() || "-".equals(string))
			return null;
		return trim;
	}

	private static Configuration conf(String[] line, int start, Band band) {
		String system = filter(line[start]);
		String cabinetType = filter(line[start + 1]);
		String cabinetConf = filter(line[start + 2]);
		String wcdmaBbu = filter(line[start + 3]);
		String lteBbu = filter(line[start + 4]);
		String r503 = filter(line[start + 5]);
		String rru = filter(line[start + 6]);
		String sector = filter(line[start + 7]);

		if (system == null)
			return null;

		return new Configuration(band, system, cabinetType, cabinetConf, wcdmaBbu, lteBbu, r503, rru, sector);
	}

	private static List<Band> bands(String string) {
		if (filter(string) == null)
			return Collections.emptyList();
		List<Band> list = new ArrayList<>();
		String[] elems = string.split("\\W");
		for (String elem : elems)
			list.add(band(elem));
		return list;
	}

	private static Band band(String string) {
		if (filter(string) == null)
			return null;
		return Band.valueOf(string);
	}

	private static class BandList implements Comparable<BandList> {
		private final List<Band> bands;

		BandList(List<Band> bands) {
			this.bands = bands;
		}

		@Override
		public int compareTo(BandList o) {
			int i;
			for (i = 0; i < bands.size() && i < o.bands.size(); i++) {
				int c = bands.get(i).compareTo(o.bands.get(i));
				if (c != 0)
					return c;
			}

			if (i > bands.size())
				return 1;
			if (i > o.bands.size())
				return -1;

			return 0;
		}

		@Override
		public int hashCode() {
			return bands.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof BandList))
				return false;
			BandList other = (BandList) obj;
			return bands.equals(other.bands);
		}

		@Override
		public String toString() {
			return bands.toString();
		}

	}

	public static class VisualMusenki extends Application {

		private static List<Merge> merges;

		@Override
		public void start(Stage stage) {

			TreeView<Object> tree = new TreeView<>();

			TreeItem<Object> root = new TreeItem<>();
			tree.setRoot(root);
			tree.setShowRoot(false);

			Map<String, List<Merge>> map =
					merges.stream().collect(Collectors.groupingBy(x -> x.conf));
			map = new TreeMap<>(map);

			for (Map.Entry<String, List<Merge>> entry : map.entrySet()) {
				TreeItem<Object> node = new TreeItem<>(entry.getKey());
				root.getChildren().add(node);

				Map<BandList, List<Merge>> byBands =
						entry.getValue().stream().collect(Collectors.groupingBy(x -> new BandList(x.bands)));
				byBands = new TreeMap<>(byBands);

				for (Map.Entry<BandList, List<Merge>> entry2 : byBands.entrySet()) {
					TreeItem<Object> node2 = new TreeItem<>(entry2.getKey());
					node.getChildren().add(node2);

					Map<String, List<Merge>> byBbu =
							entry2.getValue().stream().collect(Collectors.groupingBy(x -> x.lteBbu));
					byBbu = new TreeMap<>(byBbu);

					for (Map.Entry<String, List<Merge>> entry3 : byBbu.entrySet()) {
						TreeItem<Object> node3 = new TreeItem<>(entry3.getKey());
						node3.setExpanded(true);
						node2.getChildren().add(node3);

						for (Merge merge : entry3.getValue()) {
							TreeItem<Object> node4 = new TreeItem<>(merge);
							node3.getChildren().add(node4);
						}
					}

				}
			}

			Scene scene = new Scene(tree);
			stage.setScene(scene);

			stage.show();
		}

		private static void go(List<Merge> merges) {
			VisualMusenki.merges = merges;
			launch();
		}

	}

	private static final boolean visual = true;

	public static void main(String[] args) {

		List<Merge> merges = merges();

		if (visual)
			VisualMusenki.go(merges);

	}

	public static List<Merge> merges() {

		List<String[]> lines = lines();

		List<Joint> joints = new ArrayList<>();

		for (int i = 2; i < lines.size(); i++) {
			String[] line = lines.get(i);
			Configuration imt = conf(line, 4, Band.B1);
			Configuration pb = conf(line, 12, Band.B8);
			Configuration icg = conf(line, 20, Band.B11);
			Configuration dcs = conf(line, 28, Band.B3);
			List<Configuration> confs = new ArrayList<>(Arrays.asList(imt, pb, icg, dcs));

			String configuration = filter(line[1]);
			List<Band> wcdmaPrimary = bands(line[2]);
			Band ltePrimary = band(line[3]);

			while (confs.remove(null));
			if (confs.size() > 0) {
				Joint joint = new Joint(i + 1, configuration, wcdmaPrimary, ltePrimary, confs);
				joints.add(joint);
			} else
				System.err.println("Skipping line: " + (i + 1));
		}

		summary(joints);

		System.out.println(joints.size());

		String shared = "共有";
		final String hybrid = "Hybrid";
		final String dbsn = "DBSN";

		List<Merge> merges = new ArrayList<>();
		outer: for (Joint joint : joints) {
			Band ltePrimary = joint.ltePrimary;
			Configuration prim = joint.get(ltePrimary);

			List<Band> bands = bands(prim.cabinetConf);
			if (bands.get(0) != ltePrimary) {
				System.err.println("Error 0: " + joint);
				continue outer;
			}

			Map<Band, BandConf> confMap = new EnumMap<>(Band.class);

			for (Configuration configuration : joint.confs) {
				if (configuration != prim) {
					if (!configuration.cabinetConf.replaceAll("\\s+", "").equals(ltePrimary + shared)) {
						System.err.println("Error 2: " + joint);
						continue outer;
					}
					if (!configuration.cabinetConf.equals(configuration.lteBbu)) {
						System.err.println("Error 3: " + joint);
						continue outer;
					}
				}

				Wcdma wcdma;
				String wcdmaBbu = configuration.wcdmaBbu;
				if (wcdmaBbu == null)
					wcdma = null;
				else if (wcdmaBbu.startsWith(ltePrimary.toString()) && wcdmaBbu.endsWith(shared))
					wcdma = new SharedWcdma();
				else
					wcdma = new ProperWcdma(wcdmaBbu);
				boolean systemW = configuration.system.contains("W");
				if ((wcdma != null) != systemW)
					System.err.println(
							"Warning: line " + joint.line + ", " + configuration.band + ", " + configuration.system);

				String radio = configuration.rru;
				boolean r503;
				switch (configuration.r503) {
					case "Yes":
						r503 = true;
						break;
					case "No":
						r503 = false;
						break;
					default: {
						System.err.println("Error 6: " + joint.line);
						continue outer;
					}
				}

				Band band = configuration.band;

				BandConf bandConf;
				switch (configuration.cabinetType) {
					case hybrid:
					case dbsn:
						if (r503) {
							System.err.println("Error 7: " + joint.line);
							continue outer;
						}

						if (wcdma != null && !(wcdma instanceof SharedWcdma)) {
							System.err.println("Error 5: " + joint.line);
							continue outer;
						}
						SharedWcdma sharedWcdma = (SharedWcdma) wcdma;
						if (configuration.cabinetType.equals(hybrid))
							bandConf = new Hybrid(band, sharedWcdma, radio);
						else
							bandConf = new Dbsn(band, sharedWcdma, radio);
						break;
					default:
						bandConf = new Classic(band, configuration.cabinetType, radio, r503, wcdma);
				}
				confMap.put(band, bandConf);
			}

			List<BandConf> confs = new ArrayList<>();
			for (Band band : bands) {
				BandConf conf = confMap.remove(band);
				if (conf == null) {
					System.err.println("Error 7: " + joint.line);
					continue outer;
				}
				confs.add(conf);
			}

			List<Special> special = new ArrayList<>();
			for (BandConf rest : confMap.values()) {
				if (!(rest instanceof Special)) {
					System.err.println("Error 8: " + joint.line);
					continue;
				}
				special.add((Special) rest);
			}

			Merge merge =
					new Merge(joint.line, joint.configuration, bands, ltePrimary, prim.lteBbu, confs, special, joint);
			merges.add(merge);

		}

		for (Merge merge : merges) {
			boolean isHybrid;
			if (!isRemote(merge.confs.get(0))) {
				List<BandConf> confss = new ArrayList<>();
				confss.addAll(merge.confs);
				confss.addAll(merge.special);
				if (confss.size() > 1) {
					isHybrid = true;
					for (BandConf conf : confss) {
						if (conf.band == merge.primaryBand)
							continue;
						if (!isRemote(conf))
							isHybrid = false;

					}
				} else
					isHybrid = false;
			} else
				isHybrid = false;
			if (isHybrid != merge.conf.startsWith(hybrid))
				System.out.println(merge.joint);
		}

		System.out.println();

		for (Merge merge : merges) {
			List<BandConf> confss = new ArrayList<>();
			confss.addAll(merge.confs);
			confss.addAll(merge.special);
			List<Band> wPrim = merge.joint.wcdmaPrimary;
			if (merge.conf.contains("MCC")) {
				for (BandConf conf : merge.confs) {
					if (conf.wcdma instanceof ProperWcdma) {
						if (!wPrim.contains(conf.band))
							System.out.println(merge.joint);
					}
				}
			} else if (wPrim.size() > 1)
				System.out.println(merge.joint);
		}

		return merges;

	}

	private static boolean isRemote(BandConf bandConf) {
		switch (bandConf.radio) {
			case "Radio2217":
			case "Radio2203":
			case "RRUS11":
			case "RRUS12":
				return true;
			case "RUS80":
				return false;
			default:
				throw new IllegalArgumentException();
		}
	}

	static class Merge extends Hashed {

		final int line;
		final String conf;
		final List<Band> bands;
		final Band primaryBand;
		final String lteBbu;

		final List<BandConf> confs;
		final List<Special> special;
		final Joint joint;

		private Merge(int line, String conf, List<Band> bands, Band primaryBand, String lteBbu,
				List<BandConf> confs, List<Special> special, Joint joint) {
			super(conf, new LinkedHashSet<>(bands), primaryBand, confs);
			this.joint = joint;
			this.line = line;
			this.conf = conf;
			this.bands = bands;
			this.primaryBand = primaryBand;
			this.lteBbu = lteBbu;
			this.special = special;
			this.confs = confs;
		};

		BandConf get(Band band) {
			for (BandConf conf : confs) {
				if (conf.band == band)
					return conf;
			}
			for (Special conf : special)
				if (conf.band == band)
					return conf;
			return null;
		}

		@Override
		public String toString() {
			String wcdma = joint.wcdmaPrimary.isEmpty() ? "" : joint.wcdmaPrimary + ", ";
			return line + ":" + wcdma + confs + (special.isEmpty() ? "" : "\t" + special);
		}

	}

	abstract static class BandConf {
		final Band band;
		final String radio;
		final Wcdma wcdma;
		final boolean r503;

		BandConf(Band band, String radio, Wcdma wcdma, boolean r503) {
			this.band = band;
			this.radio = radio;
			this.wcdma = wcdma;
			this.r503 = r503;
		}

		@Override
		public String toString() {
			return band.toString();
		}
	}

	static class Classic extends BandConf {

		final String rbs;

		private Classic(Band band, String rbs, String radio, boolean r503, Wcdma wcdma) {
			super(band, radio, wcdma, r503);
			this.rbs = rbs;
		}

		@Override
		public String toString() {
			return super.band + ": " + rbs + "," + super.radio + (super.r503 ? ",R503" : "")
					+ (super.wcdma == null ? "" : "," + super.wcdma);
		}

	}

	abstract static class Special extends BandConf {
		final String type;
		private final SharedWcdma wcdma;

		Special(Band band, String type, SharedWcdma wcdma, String radio) {
			super(band, radio, wcdma, false);
			this.type = type;
			this.wcdma = wcdma;
		}

		@Override
		public String toString() {
			return super.band + ": " + type + "," + super.radio + (wcdma == null ? "" : "," + wcdma);
		}
	}

	public abstract static class Wcdma {}

	public static class SharedWcdma extends Wcdma {

		@Override
		public String toString() {
			return "W";
		}

	}

	public static class ProperWcdma extends Wcdma {
		private final String bbu;

		private ProperWcdma(String bbu) {
			this.bbu = bbu;
		}

		@Override
		public String toString() {
			return bbu;
		}
	}

	private static class Hybrid extends Special {
		Hybrid(Band band, SharedWcdma wcdma, String radio) {
			super(band, "Hybrid", wcdma, radio);
		}
	}

	private static class Dbsn extends Special {

		Dbsn(Band band, SharedWcdma wcdma, String radio) {
			super(band, "DBSN", wcdma, radio);
		}
	}

	private static <T> void summarize(String description, List<Joint> joints, Function<Configuration, T> function) {
		System.out.println(description);
		Map<Band, Set<T>> map = new EnumMap<>(Band.class);

		for (Joint joint : joints) {
			for (Configuration c : joint.confs) {
				T value = function.apply(c);
				if (value != null)
					map.computeIfAbsent(c.band, x -> new TreeSet<>()).add(value);
			}
		}

		map.forEach((x, y) -> System.out.println("\t" + x + ": " + y));
		System.out.println();
	}

	private static void summary(List<Joint> joints) {
		summarize("System", joints, x -> x.system);
		summarize("CabConf", joints, x -> x.cabinetConf);
		summarize("CabType", joints, x -> x.cabinetType);
		summarize("DUS", joints, x -> x.lteBbu);
		summarize("DUW", joints, x -> x.wcdmaBbu);
		summarize("Radio", joints, x -> x.rru);
	}

	private static final Path excel = Paths.get("data", "Worksheet in Hash_Config tool development_fixed.xlsx");
	private static final Path binary = Paths.get("data", "musenki.dat");

	@SuppressWarnings("unchecked")
	static List<String[]> lines() {
		try (ObjectInputStream input = new ObjectInputStream(Files.newInputStream(binary))) {
			return (List<String[]>) input.readObject();
		} catch (ClassNotFoundException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unused")
	private static void ser() {
		List<String[]> lines = new ArrayList<>();
		Workbook workbook;
		try {
			workbook = WorkbookFactory.create(excel.toFile());
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			throw new RuntimeException(e);
		}
		Sheet sheet = workbook.getSheetAt(0);
		for (Row row : sheet) {
			String[] line = new String[row.getLastCellNum()];
			for (Cell cell : row) {
				CellType type = cell.getCellTypeEnum();
				String value;
				if (type == CellType.STRING)
					value = cell.getStringCellValue();
				else if (type == CellType.BLANK)
					value = null;
				else if (type == CellType.NUMERIC)
					value = String.valueOf(cell.getNumericCellValue());
				else
					throw new IllegalArgumentException();
				line[cell.getColumnIndex()] = value;
			}
			lines.add(line);
			System.out.println(line.length);
		}

		try (ObjectOutputStream output = new ObjectOutputStream(Files.newOutputStream(binary))) {
			output.writeObject(lines);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
