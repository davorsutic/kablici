package hr.hashcode.cables.equip.musenki;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import hr.hashcode.cables.equip.build.Builder;
import hr.hashcode.cables.equip.build.Radio;
import hr.hashcode.cables.equip.build.SystemDesc;
import hr.hashcode.cables.equip.build.SystemDesc.MacroCabinetDesc;
import hr.hashcode.cables.equip.build.SystemDesc.MiniCabinetDesc;
import hr.hashcode.cables.equip.build.SystemDesc.RadioDesc;
import hr.hashcode.cables.equip.build.SystemDesc.SectorDesc;
import hr.hashcode.cables.equip.build.SystemDesc.Wcdma;
import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.equip.comp.MiniRbsFactory;
import hr.hashcode.cables.equip.comp.RbsFactory;
import hr.hashcode.cables.equip.comp.Site;
import hr.hashcode.cables.equip.comp.Site.UnitQuantity;
import hr.hashcode.cables.equip.musenki.Musenki.BandConf;
import hr.hashcode.cables.equip.musenki.Musenki.Classic;
import hr.hashcode.cables.equip.musenki.Musenki.Configuration;
import hr.hashcode.cables.equip.musenki.Musenki.Merge;
import hr.hashcode.cables.equip.musenki.Musenki.ProperWcdma;
import hr.hashcode.cables.equip.musenki.Musenki.SharedWcdma;
import hr.hashcode.cables.equip.musenki.Musenki.Special;
import hr.hashcode.cables.equip.rules.DcRules;
import hr.hashcode.cables.equip.rules.EcRules;
import hr.hashcode.cables.equip.rules.IdlRules;
import hr.hashcode.cables.equip.rules.OtherRules;
import hr.hashcode.cables.equip.rules.PlacementRules;
import hr.hashcode.cables.equip.rules.RfRules;
import hr.hashcode.cables.equip.rules.RiRules;
import hr.hashcode.cables.gui.GuiUtil;
import hr.hashcode.cables.util.Util;
import hr.hashcode.cables.visual.conn.MainController;
import hr.hashcode.cables.visual.huge.SiteView;
import hr.hashcode.cables.visual.site.SiteVisual;
import javafx.beans.Observable;
import javafx.beans.binding.Binding;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableBooleanValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Separator;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;

public class ConfChoice extends VBox {

	private static final boolean additionalFeatures = Boolean.parseBoolean(System.getProperty("additionalFeatures"));
	private static final boolean extra = Boolean.parseBoolean(System.getProperty("extra"));

	private final FilteredList<Merge> filtered;

	private final TableView<Merge> mergeView;
	private final TableView<Merge> excelView;
	private final List<BandChoice> bandChoices;
	private final LooseChoice<Band> primaryBand;
	private final LooseChoice<String> digitalUnit;

	private final EcRules ecRules = EcRules.of(EcRules.class.getResourceAsStream("ec_rules.xml"));
	private final DcRules dcRules = DcRules.of(DcRules.class.getResourceAsStream("dc_rules.xml"));
	private final RfRules rfRules = RfRules.of(RfRules.class.getResourceAsStream("rf_rules.xml"));
	private final RiRules riRules = RiRules.of(RiRules.class.getResourceAsStream("ri_rules.xml"));
	private final IdlRules idlRules = IdlRules.of(IdlRules.class.getResourceAsStream("idl_rules.xml"));
	private final PlacementRules placementRules =
			PlacementRules.of(PlacementRules.class.getResourceAsStream("placement_rules.xml"));
	private final OtherRules otherRules = OtherRules.of(OtherRules.class.getResourceAsStream("other_rules.xml"));
	private final RbsFactory rbsFactory = RbsFactory.of(RbsFactory.class.getResourceAsStream("rbses.xml"));
	private final MiniRbsFactory miniRbsFactory =
			MiniRbsFactory.of(MiniRbsFactory.class.getResourceAsStream("rbses.xml"));

	public ConfChoice(List<Merge> merges) {
		HBox choicesBox = new HBox();
		this.bandChoices = new ArrayList<>();
		for (Band band : Band.values()) {
			BandChoice choice = new BandChoice(band);
			bandChoices.add(choice);
			choicesBox.getChildren().addAll(choice, new Separator(Orientation.VERTICAL));

		}
		choicesBox.setSpacing(10);
		choicesBox.setAlignment(Pos.CENTER);

		this.primaryBand = new LooseChoice<>("Primary LTE band", Band.values());

		this.digitalUnit = new LooseChoice<>("Digital unit", "DUS41", "BB5216");

		HBox commonOption = new HBox(primaryBand, digitalUnit);
		commonOption.setSpacing(50);
		commonOption.setAlignment(Pos.CENTER);

		VBox settingBox = new VBox(choicesBox, commonOption);
		settingBox.setSpacing(20);
		settingBox.setAlignment(Pos.CENTER);

		this.filtered = new FilteredList<>(FXCollections.observableArrayList(merges));

		this.mergeView = new TableView<>(filtered);
		this.excelView = new TableView<>(filtered);

		excelView.setSelectionModel(mergeView.getSelectionModel());

		VBox.setVgrow(mergeView, Priority.ALWAYS);

		column(mergeView, "Line", x -> x.line);
		column(mergeView, "Configuration", x -> x.conf);
		column(mergeView, "LTE DU", x -> x.lteBbu);
		column(mergeView, "Bands", x -> x.bands);
		column(mergeView, "WCDMA Bands", x -> x.joint.wcdmaPrimary);

		for (BandChoice bandChoice : bandChoices) {
			Band band = bandChoice.band;
			TableColumn<Merge, ?> upper = new TableColumn<>(band.name());
			column(upper, "R503", x -> conf(x, band, y -> y.r503));
			column(upper, "Radio", x -> conf(x, band, y -> y.radio));
			column(upper, "Wcdma", x -> conf(x, band, y -> y.wcdma));
			column(upper, "Cabinet", x -> conf(x, band, y -> {
				if (y instanceof Classic)
					return ((Classic) y).rbs;
				else if (y instanceof Special)
					return ((Special) y).type;
				else
					return "UNKOWN";
			}));
			mergeView.getColumns().add(upper);
		}

		column(excelView, "Line", x -> x.line);
		column(excelView, "Configu", x -> x.joint.configuration);
		column(excelView, "W_Prim",
				x -> x.joint.wcdmaPrimary.stream().map(String::valueOf).collect(Collectors.joining("&")));
		column(excelView, "L_Prim", x -> x.joint.ltePrimary);

		for (Band band : Band.values()) {
			TableColumn<Merge, ?> column = new TableColumn<>(band.name());
			excelView.getColumns().add(column);

			column(column, "System", x -> confExcel(x, band, y -> y.system));
			column(column, "Cabinet", x -> confExcel(x, band, y -> y.cabinetType));
			column(column, "Cabinet", x -> confExcel(x, band, y -> y.cabinetConf));
			column(column, "BBU(W)", x -> confExcel(x, band, y -> y.wcdmaBbu));
			column(column, "BBU(L)", x -> confExcel(x, band, y -> y.lteBbu));
			column(column, "R503", x -> confExcel(x, band, y -> y.r503));
			column(column, "RRU", x -> confExcel(x, band, y -> y.rru));
			column(column, "セクタ", x -> confExcel(x, band, y -> y.sector));
		}

		Label count = new Label();
		count.textProperty().bind(Bindings.createStringBinding(
				() -> "Filtered configurations: " + filtered.size() + "/" + merges.size(),
				filtered.predicateProperty()));

		HBox viewBox = new HBox();
		HBox.setHgrow(excelView, Priority.ALWAYS);
		VBox.setVgrow(excelView, Priority.ALWAYS);
		HBox.setHgrow(mergeView, Priority.ALWAYS);
		VBox.setVgrow(mergeView, Priority.ALWAYS);
		HBox.setHgrow(viewBox, Priority.ALWAYS);
		VBox.setVgrow(viewBox, Priority.ALWAYS);

		TableChoice processed = new TableChoice("Processed", mergeView);
		TableChoice original = new TableChoice("Original", excelView);

		ChoiceBox<TableChoice> chooseView =
				new ChoiceBox<>(FXCollections.observableArrayList(processed, original));
		chooseView.valueProperty().addListener((v, o, n) -> viewBox.getChildren().setAll(n.table));
		chooseView.setValue(processed);

		HBox chooseBox = new HBox(new Label("Table type:"), chooseView);
		chooseBox.setSpacing(10);
		chooseBox.setAlignment(Pos.CENTER_LEFT);

		Button drawCombinations = new Button("_Draw");
		Button drawAll = new Button("Draw _All");

		mergeView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		BooleanBinding noneSelected = mergeView.getSelectionModel().selectedItemProperty().isNull();
		ReadOnlyIntegerProperty listSizeProperty = new SimpleListProperty<>(filtered).sizeProperty();
		BooleanBinding differentThanOne = listSizeProperty.isNotEqualTo(1);

		drawCombinations.disableProperty().bind(noneSelected.and(differentThanOne));
		drawAll.disableProperty().bind(listSizeProperty.isEqualTo(0));

		Runnable drawIt = () -> {
			ObservableList<Merge> items = mergeView.getSelectionModel().getSelectedItems();
			List<Merge> target;
			if (items.isEmpty())
				target = Collections.singletonList(mergeView.getItems().get(0));
			else
				target = new ArrayList<>(items);
			draw(target);
		};

		drawCombinations.setOnAction(e -> drawIt.run());

		drawAll.setOnAction(e -> draw(new ArrayList<>(mergeView.getItems())));

		viewBox.setOnKeyReleased(e -> {
			if (e.getCode() == KeyCode.ENTER)
				drawIt.run();
		});

		for (TableView<Merge> table : Arrays.asList(mergeView, excelView)) {
			table.setRowFactory(x -> {
				TableRow<Merge> row = new TableRow<>();
				row.setOnMouseClicked(e -> {
					if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2)
						drawIt.run();
				});
				return row;
			});
		}

		HBox countBox = new HBox(count);
		VBox infoBox = new VBox(chooseBox, countBox);
		infoBox.setSpacing(10);
		infoBox.setAlignment(Pos.CENTER_LEFT);
		HBox drawBox = new HBox(drawCombinations, drawAll);
		drawBox.setSpacing(10);
		drawBox.setAlignment(Pos.CENTER);
		HBox.setHgrow(drawBox, Priority.ALWAYS);
		HBox actionBox = new HBox(infoBox, drawBox);

		HBox buffer = new HBox();
		VBox upper = new VBox(settingBox, actionBox);
		upper.setSpacing(50);
		HBox upperLayer = new HBox(upper, buffer);
		getChildren().addAll(upperLayer, viewBox);

		List<Observable> observables = new ArrayList<>();

		for (BandChoice choice : bandChoices)
			observables.addAll(choice.observables());
		observables.addAll(primaryBand.observables());
		observables.addAll(digitalUnit.observables());

		Observable[] array = Util.toArray(observables, Observable.class);

		Binding<Predicate<Merge>> predicateBind = Bindings.createObjectBinding(() -> this::fits, array);
		this.filtered.predicateProperty().bind(predicateBind);

		setSpacing(20);
		setPadding(new Insets(20));
	}

	private static class TableChoice {
		private final String name;
		private final TableView<Merge> table;

		TableChoice(String name, TableView<Merge> table) {
			this.name = name;
			this.table = table;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	private static <T> T confExcel(Merge merge, Band band, Function<Configuration, T> function) {
		Configuration conf = merge.joint.get(band);
		if (conf == null)
			return null;
		return function.apply(conf);
	}
	private static <T> T conf(Merge merge, Band band, Function<BandConf, T> function) {
		BandConf conf = merge.get(band);
		if (conf == null)
			return null;
		return function.apply(conf);
	}

	private static <T> TableColumn<Merge, T> column(TableView<Merge> table, String name, Function<Merge, T> extract) {
		return column(table.getColumns(), name, extract, String::valueOf);
	}

	private static <T> TableColumn<Merge, T> column(TableColumn<Merge, ?> column, String name,
			Function<Merge, T> extract) {
		return column(column.getColumns(), name, extract, String::valueOf);
	}

	private static <T> TableColumn<Merge, T> column(List<TableColumn<Merge, ?>> columns, String name,
			Function<Merge, T> extract,
			Function<T, String> display) {

		TableColumn<Merge, T> column = new TableColumn<>(name);

		column.setCellValueFactory(x -> new SimpleObjectProperty<>(extract.apply(x.getValue())));
		column.setCellFactory(x -> new TableCell<Merge, T>() {
			@Override
			protected void updateItem(T item, boolean empty) {
				if (item == null || empty)
					setText(null);
				else
					setText(display.apply(item));
			}
		});
		columns.add(column);
		return column;
	}

	private boolean fits(Merge merge) {
		for (BandChoice choice : bandChoices) {
			BandConf conf = merge.get(choice.band);

			boolean chosen = !choice.sectors.isEmpty().getValue();
			boolean supported = conf != null;
			if (chosen != supported)
				return false;

			if (!primaryBand.fits(merge.primaryBand))
				return false;

			if (!digitalUnit.fits(merge.lteBbu))
				return false;

			if (chosen) {
				boolean wcdmaSupported = conf.wcdma != null;
				if (!choice.wcdma.fits(wcdmaSupported))
					return false;

				String rbs;
				if (conf instanceof Classic) {
					rbs = ((Classic) conf).rbs;
				} else
					rbs = "Shared";

				if (!choice.rbs.fits(rbs))
					return false;

				if (!choice.r503.fits(conf.r503))
					return false;

				if (!choice.radio.fits(conf.radio))
					return false;
			}
		}
		return true;
	}

	private static abstract class Loose<T> extends HBox {

		private final Label label;

		Loose(String name) {
			this.label = new Label(name + ":");
			getChildren().add(label);
			setAlignment(Pos.CENTER);
			setSpacing(10);
		}

		abstract boolean fits(T value);

		abstract List<Observable> observables();

		abstract ObservableValue<Boolean> isEmpty();

		abstract Region contents();

		Label label() {
			return label;
		}

	}

	private static class LooseCheck extends Loose<Boolean> {

		private final CheckBox choice;
		private final ObservableBooleanValue transparent;

		LooseCheck(String name, ObservableBooleanValue transparent) {
			super(name);
			this.transparent = transparent;
			this.choice = new CheckBox();
			choice.setAllowIndeterminate(true);
			choice.setIndeterminate(true);

			getChildren().add(choice);
		}

		@Override
		boolean fits(Boolean value) {
			if (transparent.get() || choice.isIndeterminate())
				return true;
			else
				return value == choice.isSelected();
		}

		@Override
		List<Observable> observables() {
			return Arrays.asList(choice.indeterminateProperty(), choice.selectedProperty());
		}

		@Override
		ObservableValue<Boolean> isEmpty() {
			return choice.indeterminateProperty();
		}

		@Override
		Region contents() {
			return choice;
		}

	}

	private static class LooseChoice<T> extends Loose<T> {

		private final ChoiceBox<T> choice;
		private final ObservableBooleanValue transparent;

		@SafeVarargs
		LooseChoice(String name, T... values) {
			this(name, new SimpleBooleanProperty(false), values);
		}

		@SafeVarargs
		LooseChoice(String name, ObservableBooleanValue transparent, T... values) {
			super(name);
			this.transparent = transparent;
			Set<T> set = new LinkedHashSet<>();
			set.add(null);
			set.addAll(Arrays.asList(values));
			this.choice = new ChoiceBox<>(FXCollections.observableArrayList(set));
			getChildren().add(choice);
		}

		@Override
		boolean fits(T value) {
			if (transparent.get())
				return true;
			T selected = choice.getValue();
			return selected == null || selected.equals(value);
		}

		@Override
		List<Observable> observables() {
			return Arrays.asList(choice.valueProperty());
		}

		@Override
		ObservableValue<Boolean> isEmpty() {
			return choice.valueProperty().isNull();
		}

		@Override
		public String toString() {
			return String.valueOf(choice.getValue());
		}

		@Override
		Region contents() {
			return choice;
		}

	}

	private static class BandChoice extends VBox {

		private static final int maxSectors = 3;

		private final Band band;
		private final LooseChoice<Integer> sectors;
		private final LooseChoice<String> rbs;
		private final LooseCheck wcdma;
		private final LooseCheck r503;
		private final LooseChoice<String> radio;

		BandChoice(Band band) {
			this.band = band;
			Text bandMark = new Text(band.name());
			bandMark.setFont(Font.font(16));
			HBox titleBox = new HBox(bandMark);
			titleBox.setAlignment(Pos.CENTER);

			Integer[] array = IntStream.rangeClosed(1, maxSectors)
					.mapToObj(Integer::valueOf)
					.toArray(Integer[]::new);

			this.sectors = new LooseChoice<>("Sectors", array);

			BooleanBinding transparent = sectors.choice.valueProperty().isNull();

			this.rbs = new LooseChoice<>("RBS", transparent, "RBS6201", "RBS6202", "RBS6102", "RBS6131", "RBS6601",
					"Shared");
			this.wcdma = new LooseCheck("WCDMA", transparent);
			this.r503 = new LooseCheck("R503", transparent);
			r503.disableProperty().bind(rbs.isEmpty());
			this.radio = new LooseChoice<>("Radio", transparent, "RUS80", "RRUS11", "RRUS12", "Radio2203", "Radio2217");

			GridPane pane = new GridPane();
			pane.setPadding(new Insets(0, 0, 0, 20));
			int count = 0;
			for (Loose<?> box : Arrays.asList(rbs, radio, wcdma, r503)) {
				pane.add(box.label(), 0, count);
				pane.add(box.contents(), 1, count);
				count++;
			}

			for (int i = 0; i < 2; i++) {
				ColumnConstraints constraints = new ColumnConstraints();
				constraints.setHalignment(i == 0 ? HPos.RIGHT : HPos.LEFT);
				pane.getColumnConstraints().add(constraints);
			}

			pane.disableProperty().bind(sectors.isEmpty());
			pane.setVgap(20);
			pane.setHgap(10);
			setSpacing(30);
			getChildren().addAll(titleBox, sectors, pane);
		}

		List<Observable> observables() {
			List<Observable> observables = new ArrayList<>();
			for (Loose<?> loose : Arrays.asList(sectors, rbs, wcdma, r503, radio))
				observables.addAll(loose.observables());
			return observables;
		}

	}
	private void draw(List<Merge> merges) {
		if (merges.size() == 1) {
			Merge merge = merges.get(0);
			Site site = site(merge);

			Stage stage = new Stage();
			Window window = GuiUtil.window(this);
			GuiUtil.acceptIcons(stage, window);
			GuiUtil.acceptLocation(stage, window);

			TabPane tabs = new TabPane();
			tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

			SiteView siteView = new SiteView(site);
			Node hugeTabContents;
			if (extra) {
				ObservableList<UnitQuantity> quantList = FXCollections.observableArrayList(site.inventory.quantities());
				TableView<UnitQuantity> quantities = new TableView<>(quantList);
				TableColumn<UnitQuantity, String> name = new TableColumn<>("Name");
				name.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().name()));
				TableColumn<UnitQuantity, String> type = new TableColumn<>("Type");
				type.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().type()));
				TableColumn<UnitQuantity, Integer> count = new TableColumn<>("Count");
				count.setCellValueFactory(x -> new SimpleObjectProperty<>(x.getValue().count()));
				quantities.getColumns().add(name);
				quantities.getColumns().add(type);
				quantities.getColumns().add(count);
				quantities.getSortOrder().add(name);
				HBox.setHgrow(siteView, Priority.ALWAYS);
				SplitPane split = new SplitPane(quantities, siteView);
				split.setDividerPositions(0.1);
				hugeTabContents = split;
			} else
				hugeTabContents = siteView;

			Tab hugeTab = new Tab("Overview", hugeTabContents);
			Tab connTab = new Tab("Connection flow", new MainController(site).getView());
			tabs.getTabs().addAll(hugeTab, connTab);

			if (additionalFeatures) {
				SiteVisual siteVisual = new SiteVisual(site);
				Tab siteTab = new Tab("Site", siteVisual);
				tabs.getTabs().addAll(siteTab);
			}

			Scene scene = new Scene(tabs);
			stage.setScene(scene);
			stage.setTitle("Configuration no. " + merge.line);
			stage.setMaximized(true);
			stage.show();
		} else {
			Map<Merge, Node> overviews = new IdentityHashMap<>();
			Map<Merge, Node> connections = new IdentityHashMap<>();

			ListView<Merge> listView = new ListView<>(FXCollections.observableArrayList(merges));
			listView.setCellFactory(x -> new ListCell<Merge>() {
				@Override
				protected void updateItem(Merge item, boolean empty) {
					super.updateItem(item, empty);
					if (empty || item == null)
						setText(null);
					else {
						StringBuilder builder = new StringBuilder();
						builder.append("Line " + item.line + ": " + item.lteBbu + " ");
						List<String> bandInfos = new ArrayList<>();
						for (BandConf conf : item.confs) {
							String bandInfo = conf.band + ":" + (conf.wcdma != null ? "L+W" : "L") + ", " + conf.radio;
							String cabinet = item.joint.get(conf.band).cabinetType;
							if (cabinet.startsWith("RBS"))
								bandInfo += ", " + cabinet;
							bandInfos.add(bandInfo);
						}
						builder.append(bandInfos.stream().collect(Collectors.joining(",", "[", "]")));
						setText(builder.toString());
					}

				}
			});

			listView.getSelectionModel().select(0);
			TabPane tabs = new TabPane();
			tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
			Tab overviewTab = new Tab("Overview");
			Tab connectionTab = new Tab("Connection flow");
			tabs.getTabs().addAll(overviewTab, connectionTab);

			Binding<Node> overviewBinding = Bindings.createObjectBinding(() -> {
				Merge merge = listView.getSelectionModel().getSelectedItem();
				if (merge == null)
					return new Region();
				Node node = overviews.get(merge);
				if (node != null)
					return node;
				else {
					Site site = site(merge);
					System.out.println(site);
					SiteView siteView =
							new SiteView(site);
					node = siteView;
					overviews.put(merge, node);
				}
				return node;
			}, listView.getSelectionModel().selectedItemProperty());
			overviewTab.contentProperty().bind(overviewBinding);

			Binding<Node> connectionBinding = Bindings.createObjectBinding(() -> {
				Merge merge = listView.getSelectionModel().getSelectedItem();
				if (merge == null)
					return new Region();
				Node node = connections.get(merge);
				if (node != null)
					return node;
				else {
					Site site = site(merge);
					System.out.println(site);
					System.out.println();
					MainController controller = new MainController(site);
					node = controller.getView();
					connections.put(merge, node);
				}
				return node;
			}, listView.getSelectionModel().selectedItemProperty());
			connectionTab.contentProperty().bind(connectionBinding);

			SplitPane pane = new SplitPane(listView, tabs);
			pane.setDividerPositions(0.3);

			Stage stage = new Stage();
			Window window = GuiUtil.window(this);
			GuiUtil.acceptIcons(stage, window);
			GuiUtil.acceptLocation(stage, window);

			Scene scene = new Scene(pane);
			stage.setScene(scene);
			stage.setTitle("Multiple configurations");
			stage.setMaximized(true);
			stage.show();

		}
	}

	private Site site(Merge merge) {

		Band primary = merge.primaryBand;
		String lteBbu = merge.lteBbu;

		Map<Band, String> cabinetNames = new HashMap<>();
		Map<Band, SectorDesc> sectorDescs = new HashMap<>();
		Map<Band, List<Band>> fosterBands = new HashMap<>();

		for (Configuration conf : merge.joint.confs) {
			boolean sharesCabinet = conf.cabinetType.contains("共有") || conf.cabinetType.equals("DBSN")
					|| conf.cabinetType.equals("Hybrid");
			if (sharesCabinet) {
				int index = conf.cabinetType.indexOf("共有");
				Band base;
				if (index != -1)
					base = Band.valueOf(conf.cabinetType.substring(0, index));
				else
					base = primary;
				fosterBands.computeIfAbsent(base, x -> new ArrayList<>()).add(conf.band);
			} else {
				cabinetNames.put(conf.band, conf.cabinetType);
			}

			int sectors = -1;
			for (BandChoice choice : bandChoices) {
				if (choice.band == conf.band)
					sectors = choice.sectors.choice.getValue();
			}

			String radioType = conf.rru;
			boolean remote = radioType.startsWith("RRUS") || radioType.startsWith("Radio");
			Radio radio = new Radio(radioType, remote);
			RadioDesc radioDesc = new RadioDesc(sectors, radio);
			Wcdma wcdma;
			BandConf bandConf = merge.get(conf.band);
			Musenki.Wcdma w = bandConf.wcdma;
			SectorDesc sector;
			if (w instanceof ProperWcdma) {
				if (sharesCabinet)
					wcdma = Wcdma.SECONDARY;
				else
					wcdma = Wcdma.PRIMARY;
			} else if (w instanceof SharedWcdma)
				wcdma = Wcdma.PRIMARY;
			else
				wcdma = Wcdma.NONE;
			sector = new SectorDesc(conf.band, radioDesc, wcdma);
			sectorDescs.put(conf.band, sector);
		}

		List<MacroCabinetDesc> macros = new ArrayList<>();
		List<MiniCabinetDesc> minis = new ArrayList<>();

		for (Map.Entry<Band, String> entry : cabinetNames.entrySet()) {
			Band band = entry.getKey();
			String cabinetName = entry.getValue();

			List<SectorDesc> sectors = new ArrayList<>();
			SectorDesc baseSector = sectorDescs.get(band);
			sectors.add(baseSector);
			int duws = 0;
			if (baseSector.wcdma() == Wcdma.PRIMARY)
				duws = 1;
			else if (baseSector.wcdma() == Wcdma.SECONDARY)
				duws = 0;
			for (Band foster : fosterBands.getOrDefault(band, Collections.emptyList())) {
				SectorDesc sector = sectorDescs.get(foster);
				sectors.add(sector);
				if (sector.wcdma() == Wcdma.SECONDARY)
					duws = 2;
				else if (duws == 0 && sector.wcdma() == Wcdma.PRIMARY)
					duws = 1;
			}
			if (cabinetName.contains("6601")) {
				MiniCabinetDesc mini =
						new MiniCabinetDesc(sectors, "DUW 30", duws, miniRbsFactory.blueprint(cabinetName, "1"));
				minis.add(mini);
			} else {
				String subtype = cabinetName.contains("6102") ? "1" : null;
				MacroCabinetDesc macro =
						new MacroCabinetDesc(sectors, "DUW 30", duws, rbsFactory.blueprint(cabinetName, subtype));
				macros.add(macro);
			}
		}

		SystemDesc system = new SystemDesc(lteBbu, primary, macros, minis);

		Builder builder = new Builder(ecRules, dcRules, rfRules, riRules, idlRules, otherRules, placementRules);
		Site site = builder.build(system);

		return site;
	}

}
