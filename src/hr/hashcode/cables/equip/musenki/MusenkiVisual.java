package hr.hashcode.cables.equip.musenki;

import java.util.List;

import hr.hashcode.cables.equip.musenki.Musenki.Merge;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MusenkiVisual extends Application {

	private List<Merge> merges;

	@Override
	public void init() {
		this.merges = Musenki.merges();
	}

	@Override
	public void start(Stage stage) {
		ConfChoice confChoice = new ConfChoice(merges);

		Scene scene = new Scene(confChoice);
		stage.setScene(scene);
		stage.setMaximized(true);
		stage.show();
	}

	public static void main(String[] args) {
		launch();
	}

}
