package hr.hashcode.cables.equip.build;

public class Radio {
	private final String type;
	private final boolean remote;

	public Radio(String type, boolean remote) {
		this.type = type;
		this.remote = remote;
	}

	public String type() {
		return type;
	}

	public boolean remote() {
		return remote;
	}
}
