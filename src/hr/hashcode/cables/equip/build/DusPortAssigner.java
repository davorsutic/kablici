package hr.hashcode.cables.equip.build;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import hr.hashcode.cables.equip.comp.Band;

public class DusPortAssigner {

	private final Map<Band, String> byBand;
	private final Set<String> direct;

	public DusPortAssigner(Map<Band, String> map, Collection<String> direct) {
		this.byBand = new HashMap<>(map);
		this.direct = new LinkedHashSet<>(direct);
	}

	String band(Band band) {
		String port = byBand.get(band);
		direct.remove(port);
		return port;
	}

	String direct() {
		if (direct.isEmpty())
			return null;
		String result = direct.iterator().next();
		direct.remove(result);
		return result;
	}

}
