package hr.hashcode.cables.equip.build;

import java.util.List;

import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.MacroRbs.RbsBlueprint;
import hr.hashcode.cables.equip.comp.MiniRbs;
import hr.hashcode.cables.equip.comp.MiniRbs.Blueprint;

public class SystemDesc {

	public enum Wcdma {

		PRIMARY(true, 0), SECONDARY(true, 1), NONE(false, -1);

		final boolean exists;
		final int index;

		private Wcdma(boolean exists, int index) {
			this.exists = exists;
			this.index = index;
		}

	}

	private final String bbUnit;
	private final Band primary;
	private final List<MacroCabinetDesc> macroCabinets;
	private final List<MiniCabinetDesc> miniCabinets;

	public SystemDesc(String bbUnit, Band primary, List<MacroCabinetDesc> macroCabinets,
			List<MiniCabinetDesc> miniCabinets) {
		this.bbUnit = bbUnit;
		this.primary = primary;
		this.macroCabinets = macroCabinets;
		this.miniCabinets = miniCabinets;
	}

	public List<MacroCabinetDesc> macroCabinets() {
		return macroCabinets;
	}

	public List<MiniCabinetDesc> miniCabinets() {
		return miniCabinets;
	}

	public String bbUnit() {
		return bbUnit;
	}

	public Band primary() {
		return primary;
	}

	public static class RadioDesc {
		private final int sectors;
		private final Radio radio;

		public RadioDesc(int sectors, Radio radio) {
			this.sectors = sectors;
			this.radio = radio;
		}

		public int sectors() {
			return sectors;
		}

		public Radio radio() {
			return radio;
		}
	}

	public static class SectorDesc {
		private final Band band;
		private final RadioDesc radio;
		private final Wcdma wcdma;

		public SectorDesc(Band band, RadioDesc radio, Wcdma wcdma) {
			this.band = band;
			this.radio = radio;
			this.wcdma = wcdma;
		}

		public Band band() {
			return band;
		}

		public RadioDesc radio() {
			return radio;
		}

		public Wcdma wcdma() {
			return wcdma;
		}

	}

	public static class CabinetDesc {
		private final List<SectorDesc> sectors;
		private final String duwType;
		private final int duws;

		CabinetDesc(List<SectorDesc> sectors, String duwType, int duws) {
			this.sectors = sectors;
			this.duwType = duwType;
			this.duws = duws;
		}

		public List<SectorDesc> sectors() {
			return sectors;
		}

		public String duwType() {
			return duwType;
		}

		public int duws() {
			return duws;
		}

	}

	public static class MacroCabinetDesc extends CabinetDesc {
		private final MacroRbs.RbsBlueprint blueprint;

		public MacroCabinetDesc(List<SectorDesc> sectors, String duwType, int duws, RbsBlueprint blueprint) {
			super(sectors, duwType, duws);
			this.blueprint = blueprint;
		}

		public MacroRbs.RbsBlueprint blueprint() {
			return blueprint;
		}
	}

	public static class MiniCabinetDesc extends CabinetDesc {
		private final MiniRbs.Blueprint blueprint;

		public MiniCabinetDesc(List<SectorDesc> sectors, String duwType, int duws, Blueprint blueprint) {
			super(sectors, duwType, duws);
			this.blueprint = blueprint;
		}

		public MiniRbs.Blueprint blueprint() {
			return blueprint;
		}
	}

}
