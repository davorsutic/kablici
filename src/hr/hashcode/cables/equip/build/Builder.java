package hr.hashcode.cables.equip.build;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import hr.hashcode.cables.equip.build.SystemDesc.MacroCabinetDesc;
import hr.hashcode.cables.equip.build.SystemDesc.MiniCabinetDesc;
import hr.hashcode.cables.equip.build.SystemDesc.RadioDesc;
import hr.hashcode.cables.equip.build.SystemDesc.SectorDesc;
import hr.hashcode.cables.equip.build.SystemDesc.Wcdma;
import hr.hashcode.cables.equip.comp.Antenna;
import hr.hashcode.cables.equip.comp.Band;
import hr.hashcode.cables.equip.comp.Cable;
import hr.hashcode.cables.equip.comp.Connector;
import hr.hashcode.cables.equip.comp.DcPort;
import hr.hashcode.cables.equip.comp.DigitalUnit;
import hr.hashcode.cables.equip.comp.DigitalUnitBaseband;
import hr.hashcode.cables.equip.comp.DigitalUnitStandard;
import hr.hashcode.cables.equip.comp.DigitalUnitWcdma;
import hr.hashcode.cables.equip.comp.EcPort;
import hr.hashcode.cables.equip.comp.MacroRbs;
import hr.hashcode.cables.equip.comp.MiniRbs;
import hr.hashcode.cables.equip.comp.R503;
import hr.hashcode.cables.equip.comp.RadioUnit;
import hr.hashcode.cables.equip.comp.RadioUnitBase;
import hr.hashcode.cables.equip.comp.RemoteRadioUnit;
import hr.hashcode.cables.equip.comp.RfPort;
import hr.hashcode.cables.equip.comp.RiPort;
import hr.hashcode.cables.equip.comp.Site;
import hr.hashcode.cables.equip.rules.DcRules;
import hr.hashcode.cables.equip.rules.EcRules;
import hr.hashcode.cables.equip.rules.IdlRules;
import hr.hashcode.cables.equip.rules.OtherRules;
import hr.hashcode.cables.equip.rules.PlacementRules;
import hr.hashcode.cables.equip.rules.RfRules;
import hr.hashcode.cables.equip.rules.RiRules;
import hr.hashcode.cables.util.Util;

public class Builder {

	private static final String R503 = "R503";
	private static final String DIGITAL_UNIT = "Digital Unit";
	private static final String RI_CABLE = "RI cable";
	private static final String EC_CABLE = "EC cable";
	private static final String DC_CABLE = "DC cable";
	private static final String RF_CABLE = "RF cable";
	private static final String IDL_CABLE = "IDL cable";
	private static final String CROSS_CONNECT = "X-connect cable";

	private final R503PortAssigner.Strategy splitterStrategy;
	private final CascadeStrategy cascadeStrategy;
	private final RadioSlotAllocationStrategy radioSlotAllocationStrategy;

	private final EcRules ecRules;
	private final DcRules dcRules;
	private final RfRules rfRules;
	private final RiRules riRules;
	private final IdlRules idlRules;
	private final OtherRules otherRules;
	private final PlacementRules placementRules;

	public Builder(EcRules ecRules, DcRules dcRules, RfRules rfRules, RiRules riRules, IdlRules idlRules,
			OtherRules otherRules, PlacementRules placementRules) {
		this.ecRules = ecRules;
		this.dcRules = dcRules;
		this.rfRules = rfRules;
		this.riRules = riRules;
		this.idlRules = idlRules;
		this.otherRules = otherRules;
		this.placementRules = placementRules;
		this.splitterStrategy = R503PortAssigner.Strategy.SPLIT;
		this.cascadeStrategy = CascadeStrategy.CASCADE;
		this.radioSlotAllocationStrategy = RadioSlotAllocationStrategy.GREEDY;
	}

	public Site build(SystemDesc system) {

		Site site = new Site();

		boolean isBb = system.bbUnit().startsWith("BB");

		DigitalUnit du;
		DigitalUnitStandard dus;
		DigitalUnitBaseband bb;

		if (isBb) {
			bb = new DigitalUnitBaseband("BB", system.bbUnit());
			dus = null;
			du = bb;
		} else {
			dus = new DigitalUnitStandard("DUS", system.bbUnit());
			bb = null;
			du = dus;
		}

		site.inventory.add(DIGITAL_UNIT, du.type());
		boolean hasR503s;
		if (system.macroCabinets().size() + system.miniCabinets().size() > 1)
			hasR503s = true;
		else {
			List<SectorDesc> descs = new ArrayList<>();
			for (MacroCabinetDesc macro : system.macroCabinets())
				descs.addAll(macro.sectors());
			for (MiniCabinetDesc mini : system.miniCabinets())
				descs.addAll(mini.sectors());

			int total = descs.stream().mapToInt(x -> x.radio().sectors()).sum();

			hasR503s = total > du.riPorts().ports().size();
		}

		DusPortAssigner dusPortAssigner;
		if (hasR503s)
			dusPortAssigner = riRules.withR503();
		else
			dusPortAssigner = riRules.withoutR503();

		for (MacroCabinetDesc cabinet : system.macroCabinets()) {
			MacroRbs rbs;
			if (hasR503s)
				rbs = withR503(site, cabinet, du, dusPortAssigner);
			else
				rbs = onlyDus(site, cabinet, du, dusPortAssigner);

			if (Util.map(cabinet.sectors(), SectorDesc::band).contains(system.primary())) {
				if (dus != null)
					placementRules.placeDus(dus, rbs);
				else
					placementRules.placeBb(bb, rbs);
			}

			List<Cable<EcPort>> ecCables = ecRules.connect(rbs);
			ecCables.forEach(x -> site.ecConnectors.add(x.connector()));
			ecCables.forEach(x -> site.inventory.add(EC_CABLE, x.name()));

			List<Cable<DcPort>> dcCables = dcRules.connect(rbs);
			dcCables.forEach(x -> site.dcConnectors.add(x.connector()));
			dcCables.forEach(x -> site.inventory.add(DC_CABLE, x.name()));
		}

		for (MiniCabinetDesc cabinet : system.miniCabinets()) {
			List<MiniRbs> rbses;
			if (hasR503s)
				rbses = miniWithR503(site, cabinet, du, dusPortAssigner);
			else
				rbses = miniOnlyDus(site, cabinet, du, dusPortAssigner);

			if (Util.map(cabinet.sectors(), SectorDesc::band).contains(system.primary())) {
				if (dus != null)
					rbses.get(0).set(1, dus);
				else
					rbses.get(0).set(1, bb);
			}

			for (MiniRbs rbs : rbses) {
				List<Cable<EcPort>> ecCables = ecRules.connect(rbs);
				ecCables.forEach(x -> site.ecConnectors.add(x.connector()));
				ecCables.forEach(x -> site.inventory.add(EC_CABLE, x.name()));

				List<Cable<DcPort>> dcCables = dcRules.connect(rbs);
				dcCables.forEach(x -> site.dcConnectors.add(x.connector()));
				dcCables.forEach(x -> site.inventory.add(DC_CABLE, x.name()));
			}
		}
		return site;
	}

	private enum CascadeStrategy {
		CASCADE, NO_CASCADE;
	}

	private enum RadioSlotAllocationStrategy {
		GREEDY, STINGY;
	}

	private MacroRbs withR503(Site site, MacroCabinetDesc cabinet, DigitalUnit dus,
			DusPortAssigner dusPortAssigner) {
		R503 r503 = new R503();

		MacroRbs rbs = cabinet.blueprint().get();
		site.rbses.add(rbs);

		DigitalUnitWcdma[] duws = new DigitalUnitWcdma[cabinet.duws()];
		for (int i = 0; i < cabinet.duws(); i++) {
			duws[i] = new DigitalUnitWcdma("DUW" + (i + 1), cabinet.duwType());
			if (i == 0)
				placementRules.placePrimaryDuw(duws[i], rbs);
			else if (i == 1)
				placementRules.placeSecondaryDuw(duws[i], rbs);
			site.inventory.add(DIGITAL_UNIT, duws[i].type());
		}

		if (duws.length == 2) {
			site.idlConnectors.addAll(idlRules.connect(duws[0], duws[1]));
			site.riConnectors.addAll(riRules.interDuw(duws[0], duws[1]));
		}

		placementRules.placeR503(r503, rbs);
		site.inventory.add(R503, r503.name());

		int[] start = new int[cabinet.duws()];

		int radioIndex = 0;

		R503PortAssigner portAssigner = splitterStrategy.get();

		for (SectorDesc sectorDesc : cabinet.sectors()) {
			Band band = sectorDesc.band();
			RadioDesc radioDesc = sectorDesc.radio();
			Wcdma wcdma = sectorDesc.wcdma();

			String riDus = dusPortAssigner.band(band);
			int riR503 = portAssigner.nextInput();

			Connector<RiPort> dusToR503 = new Connector<>(dus.riPort(riDus), r503.riPort(String.valueOf(riR503)));
			site.riConnectors.add(dusToR503);

			site.r503ToBands.computeIfAbsent(r503, x -> new HashSet<>()).add(band);
			site.duToBands.computeIfAbsent(dus, x -> new HashSet<>()).add(band);
			if (wcdma.exists)
				site.duToBands.computeIfAbsent(duws[wcdma.index], x -> new HashSet<>()).add(band);

			for (int i = 0; i < sectorDesc.radio().sectors(); i++) {

				Antenna antenna = new Antenna(band + " Sec" + (i + 1));
				site.antennas.add(antenna);

				List<RadioUnitBase> createdRadios = new ArrayList<>();
				if (radioDesc.radio().remote()) {
					RemoteRadioUnit remoteUnit =
							new RemoteRadioUnit("Radio " + band + " Sec" + (i + 1), radioDesc.radio().type());
					createdRadios.add(remoteUnit);
					site.remotes.add(remoteUnit);

					List<Cable<RfPort>> rfCables = rfRules.noCascade(remoteUnit, antenna);
					rfCables.forEach(x -> site.rfConnectors.add(x.connector()));
					rfCables.forEach(x -> site.inventory.add(RF_CABLE, x.name()));

					if (wcdma.exists) {
						int index = wcdma.index;
						DigitalUnitWcdma duw = duws[wcdma.index];
						RiPort duwPort = duw.riPorts().ports().get(start[index]++);

						site.riConnectors.addAll(riRules.remote(remoteUnit, duwPort,
								r503.riPort(String.valueOf(portAssigner.nextOutput()))));
					} else {
						site.riConnectors.addAll(riRules.remote(remoteUnit, null,
								r503.riPort(String.valueOf(portAssigner.nextOutput()))));
					}

				} else {

					if (cascadeStrategy == CascadeStrategy.CASCADE) {

						final RadioUnit first;
						final RadioUnit second;

						if (wcdma.exists) {

							RadioUnit wcdmaRadio =
									new RadioUnit("Radio " + band + "W Sec" + (i + 1), radioDesc.radio().type());
							RadioUnit lteRadio =
									new RadioUnit("Radio " + band + "L Sec" + (i + 1), radioDesc.radio().type());

							int index = wcdma.index;
							DigitalUnitWcdma duw = duws[wcdma.index];
							RiPort duwPort = duw.riPorts().ports().get(start[index]++);

							site.riConnectors.addAll(riRules.cascadeMixed(wcdmaRadio, lteRadio, duwPort,
									r503.riPort(String.valueOf(portAssigner.nextOutput()))));

							first = wcdmaRadio;
							second = lteRadio;
						} else {
							RadioUnit lteRadio1 =
									new RadioUnit("Radio " + band + "L1 Sec" + (i + 1), radioDesc.radio().type());
							RadioUnit lteRadio2 =
									new RadioUnit("Radio " + band + "L2 Sec" + (i + 1), radioDesc.radio().type());

							site.riConnectors.addAll(riRules.cascadeLte(lteRadio1, lteRadio2,
									r503.riPort(String.valueOf(portAssigner.nextOutput()))));

							first = lteRadio1;
							second = lteRadio2;
						}

						List<Cable<RfPort>> rfCables = rfRules.cascade(first, second, antenna);
						rfCables.forEach(x -> site.rfConnectors.add(x.connector()));
						rfCables.forEach(x -> site.inventory.add(RF_CABLE, x.name()));

						createdRadios.add(first);
						createdRadios.add(second);

						// placement
						rbs.subracks().get(radioIndex / 6).setRadio(radioIndex % 6, first);
						radioIndex++;
						rbs.subracks().get(radioIndex / 6).setRadio(radioIndex % 6, second);
						radioIndex++;
					} else {
						RadioUnit radio = new RadioUnit("Radio " + band + " Sec" + (i + 1), radioDesc.radio().type());
						if (wcdma.exists) {

							int index = wcdma.index;
							DigitalUnitWcdma duw = duws[wcdma.index];
							RiPort duwPort = duw.riPorts().ports().get(start[index]++);

							site.riConnectors.addAll(riRules.noCascade(radio, duwPort,
									r503.riPort(String.valueOf(portAssigner.nextOutput()))));
						} else {
							site.riConnectors.addAll(riRules.noCascade(radio, null,
									r503.riPort(String.valueOf(portAssigner.nextOutput()))));
						}

						List<Cable<RfPort>> rfCables = rfRules.noCascade(radio, antenna);
						rfCables.forEach(x -> site.rfConnectors.add(x.connector()));
						rfCables.forEach(x -> site.inventory.add(RF_CABLE, x.name()));
						createdRadios.add(radio);

						rbs.subracks().get(radioIndex / 6).setRadio(radioIndex % 6, radio);
						radioIndex++;

					}
					site.radios.computeIfAbsent(band, x -> Util.identitySet()).addAll(createdRadios);
				}
			}
			portAssigner.endBand();
		}

		return rbs;
	}

	private MacroRbs onlyDus(Site site, MacroCabinetDesc cabinet, DigitalUnit dus,
			DusPortAssigner dusPortAssigner) {

		MacroRbs rbs = cabinet.blueprint().get();
		site.rbses.add(rbs);

		DigitalUnitWcdma[] duws = new DigitalUnitWcdma[cabinet.duws()];
		for (int i = 0; i < cabinet.duws(); i++) {
			duws[i] = new DigitalUnitWcdma("DUW" + (i + 1), cabinet.duwType());
			if (i == 0)
				placementRules.placePrimaryDuw(duws[i], rbs);
			else if (i == 1)
				placementRules.placeSecondaryDuw(duws[i], rbs);
			site.inventory.add(DIGITAL_UNIT, duws[i].type());
		}

		if (duws.length == 2) {
			site.idlConnectors.addAll(idlRules.connect(duws[0], duws[1]));
			site.riConnectors.addAll(riRules.interDuw(duws[0], duws[1]));
		}

		int[] start = new int[cabinet.duws()];

		int radioIndex = 0;

		for (SectorDesc sectorDesc : cabinet.sectors()) {
			Band band = sectorDesc.band();
			RadioDesc radioDesc = sectorDesc.radio();
			Wcdma wcdma = sectorDesc.wcdma();
			List<RadioUnitBase> createdRadios = new ArrayList<>();

			site.duToBands.computeIfAbsent(dus, x -> new HashSet<>()).add(band);
			if (wcdma.exists)
				site.duToBands.computeIfAbsent(duws[wcdma.index], x -> new HashSet<>()).add(band);

			for (int i = 0; i < sectorDesc.radio().sectors(); i++) {

				Antenna antenna = new Antenna(band + " Sec" + (i + 1));
				site.antennas.add(antenna);

				if (radioDesc.radio().remote()) {
					RemoteRadioUnit remoteUnit =
							new RemoteRadioUnit("Radio " + band + " Sec" + (i + 1), radioDesc.radio().type());
					createdRadios.add(remoteUnit);
					site.remotes.add(remoteUnit);

					List<Cable<RfPort>> rfCables = rfRules.noCascade(remoteUnit, antenna);
					rfCables.forEach(x -> site.rfConnectors.add(x.connector()));
					rfCables.forEach(x -> site.inventory.add(RF_CABLE, x.name()));

					if (wcdma.exists) {
						int index = wcdma.index;
						DigitalUnitWcdma duw = duws[wcdma.index];
						RiPort duwPort = duw.riPorts().ports().get(start[index]++);

						site.riConnectors.addAll(riRules.remote(remoteUnit, duwPort,
								dus.riPort(String.valueOf(dusPortAssigner.direct()))));

					} else {

						site.riConnectors.addAll(riRules.remote(remoteUnit, null,
								dus.riPort(String.valueOf(dusPortAssigner.direct()))));

					}

				} else {

					if (cascadeStrategy == CascadeStrategy.CASCADE) {

						final RadioUnit first;
						final RadioUnit second;

						if (wcdma.exists) {

							RadioUnit wcdmaRadio =
									new RadioUnit("Radio " + band + "W Sec" + (i + 1), radioDesc.radio().type());
							RadioUnit lteRadio =
									new RadioUnit("Radio " + band + "L Sec" + (i + 1), radioDesc.radio().type());

							int index = wcdma.index;
							DigitalUnitWcdma duw = duws[wcdma.index];
							RiPort duwPort = duw.riPorts().ports().get(start[index]++);

							site.riConnectors.addAll(riRules.cascadeMixed(wcdmaRadio, lteRadio, duwPort,
									dus.riPort(String.valueOf(dusPortAssigner.direct()))));

							first = wcdmaRadio;
							second = lteRadio;
						} else {
							RadioUnit lteRadio1 =
									new RadioUnit("Radio " + band + "L1 Sec" + (i + 1), radioDesc.radio().type());
							RadioUnit lteRadio2 =
									new RadioUnit("Radio " + band + "L2 Sec" + (i + 1), radioDesc.radio().type());

							site.riConnectors.addAll(riRules.cascadeLte(lteRadio1, lteRadio2,
									dus.riPort(String.valueOf(dusPortAssigner.direct()))));

							first = lteRadio1;
							second = lteRadio2;
						}
						site.inventory.add(RI_CABLE, otherRules.cascade());

						site.inventory.add(CROSS_CONNECT, otherRules.crossConnect());
						site.inventory.add(CROSS_CONNECT, otherRules.crossConnect());

						List<Cable<RfPort>> rfCables = rfRules.cascade(first, second, antenna);
						rfCables.forEach(x -> site.rfConnectors.add(x.connector()));
						rfCables.forEach(x -> site.inventory.add(RF_CABLE, x.name()));

						createdRadios.add(first);
						createdRadios.add(second);

						// placement
						rbs.subracks().get(radioIndex / 6).setRadio(radioIndex % 6, first);
						radioIndex++;
						rbs.subracks().get(radioIndex / 6).setRadio(radioIndex % 6, second);
						radioIndex++;
					} else {
						RadioUnit radio = new RadioUnit("Radio " + band + " Sec" + (i + 1), radioDesc.radio().type());

						if (wcdma.exists) {

							int index = wcdma.index;
							DigitalUnitWcdma duw = duws[wcdma.index];
							RiPort duwPort = duw.riPorts().ports().get(start[index]++);

							site.riConnectors.addAll(riRules.noCascade(radio, duwPort,
									dus.riPort(String.valueOf(dusPortAssigner.direct()))));
						} else {
							site.riConnectors.addAll(riRules.noCascade(radio, null,
									dus.riPort(String.valueOf(dusPortAssigner.direct()))));
						}

						List<Cable<RfPort>> rfCables = rfRules.noCascade(radio, antenna);
						rfCables.forEach(x -> site.rfConnectors.add(x.connector()));
						rfCables.forEach(x -> site.inventory.add(RF_CABLE, x.name()));

						createdRadios.add(radio);

						rbs.subracks().get(radioIndex / 6).setRadio(radioIndex % 6, radio);
						radioIndex++;

					}
					site.radios.computeIfAbsent(band, x -> Util.identitySet()).addAll(createdRadios);
				}
			}
		}

		return rbs;
	}

	private List<MiniRbs> miniWithR503(Site site, MiniCabinetDesc cabinet, DigitalUnit dus,
			DusPortAssigner dusPortAssigner) {
		R503 r503 = new R503();

		MiniRbs[] duwRbses = new MiniRbs[cabinet.duws()];
		DigitalUnitWcdma[] duws = new DigitalUnitWcdma[cabinet.duws()];
		for (int i = 0; i < cabinet.duws(); i++) {
			duws[i] = new DigitalUnitWcdma("DUW" + (i + 1), cabinet.duwType());
			duwRbses[i] = cabinet.blueprint().create();
			duwRbses[i].set(duws[i]);
		}
		site.miniRbses.addAll(Arrays.asList(duwRbses));

		if (duws.length == 2) {
			site.idlConnectors.addAll(idlRules.connect(duws[0], duws[1]));
			site.riConnectors.addAll(riRules.interDuw(duws[0], duws[1]));
		}

		MiniRbs rbs = cabinet.blueprint().create();
		rbs.set(0, r503);
		site.miniRbses.add(rbs);

		// placementRules.placeR503(r503, rbs);

		int[] start = new int[cabinet.duws()];

		R503PortAssigner portAssigner = splitterStrategy.get();

		for (SectorDesc sectorDesc : cabinet.sectors()) {
			Band band = sectorDesc.band();
			RadioDesc radioDesc = sectorDesc.radio();
			Wcdma wcdma = sectorDesc.wcdma();

			site.r503ToBands.computeIfAbsent(r503, x -> new HashSet<>()).add(band);
			site.duToBands.computeIfAbsent(dus, x -> new HashSet<>()).add(band);
			if (wcdma.exists)
				site.duToBands.computeIfAbsent(duws[wcdma.index], x -> new HashSet<>()).add(band);

			String riDus = dusPortAssigner.band(band);
			int riR503 = portAssigner.nextInput();

			Connector<RiPort> dusToR503 = new Connector<>(dus.riPort(riDus), r503.riPort(String.valueOf(riR503)));
			site.riConnectors.add(dusToR503);

			for (int i = 0; i < sectorDesc.radio().sectors(); i++) {

				Antenna antenna = new Antenna(band + " Sec" + (i + 1));
				site.antennas.add(antenna);

				RemoteRadioUnit remoteUnit =
						new RemoteRadioUnit("Radio " + band + " Sec" + (i + 1), radioDesc.radio().type());
				site.radios.computeIfAbsent(band, x -> Util.identitySet()).add(remoteUnit);
				site.remotes.add(remoteUnit);

				List<Cable<RfPort>> rfCables = rfRules.noCascade(remoteUnit, antenna);
				rfCables.forEach(x -> site.rfConnectors.add(x.connector()));
				rfCables.forEach(x -> site.inventory.add(RF_CABLE, x.name()));

				if (wcdma.exists) {
					int index = wcdma.index;
					DigitalUnitWcdma duw = duws[wcdma.index];
					RiPort duwPort = duw.riPorts().ports().get(start[index]++);

					site.riConnectors.addAll(riRules.remote(remoteUnit, duwPort,
							r503.riPort(String.valueOf(portAssigner.nextOutput()))));

				} else {

					site.riConnectors.addAll(riRules.remote(remoteUnit, null,
							r503.riPort(String.valueOf(portAssigner.nextOutput()))));

				}

			}
			portAssigner.endBand();
		}

		List<MiniRbs> result = new ArrayList<>();
		result.add(rbs);
		result.addAll(Arrays.asList(duwRbses));

		return result;
	}

	private List<MiniRbs> miniOnlyDus(Site site, MiniCabinetDesc cabinet, DigitalUnit dus,
			DusPortAssigner dusPortAssigner) {

		MiniRbs[] duwRbses = new MiniRbs[cabinet.duws()];
		DigitalUnitWcdma[] duws = new DigitalUnitWcdma[cabinet.duws()];
		for (int i = 0; i < cabinet.duws(); i++) {
			duws[i] = new DigitalUnitWcdma("DUW" + (i + 1), cabinet.duwType());
			duwRbses[i] = cabinet.blueprint().create();
			duwRbses[i].set(duws[i]);
		}

		if (duws.length == 2) {
			site.idlConnectors.addAll(idlRules.connect(duws[0], duws[1]));
			site.riConnectors.addAll(riRules.interDuw(duws[0], duws[1]));
		}

		MiniRbs rbs = cabinet.blueprint().create();

		// placementRules.placeR503(r503, rbs);

		int[] start = new int[cabinet.duws()];

		for (SectorDesc sectorDesc : cabinet.sectors()) {
			Band band = sectorDesc.band();
			RadioDesc radioDesc = sectorDesc.radio();
			Wcdma wcdma = sectorDesc.wcdma();

			site.duToBands.computeIfAbsent(dus, x -> new HashSet<>()).add(band);
			if (wcdma.exists)
				site.duToBands.computeIfAbsent(duws[wcdma.index], x -> new HashSet<>()).add(band);

			for (int i = 0; i < sectorDesc.radio().sectors(); i++) {

				Antenna antenna = new Antenna(band + " Sec" + (i + 1));
				site.antennas.add(antenna);

				RemoteRadioUnit remoteUnit =
						new RemoteRadioUnit("Radio " + band + " Sec" + (i + 1), radioDesc.radio().type());
				site.radios.computeIfAbsent(band, x -> Util.identitySet()).add(remoteUnit);
				site.remotes.add(remoteUnit);

				List<Cable<RfPort>> rfCables = rfRules.noCascade(remoteUnit, antenna);
				rfCables.forEach(x -> site.rfConnectors.add(x.connector()));
				rfCables.forEach(x -> site.inventory.add(RF_CABLE, x.name()));

				if (wcdma.exists) {
					int index = wcdma.index;
					DigitalUnitWcdma duw = duws[wcdma.index];
					RiPort duwPort = duw.riPorts().ports().get(start[index]++);

					site.riConnectors.addAll(riRules.remote(remoteUnit, duwPort,
							dus.riPort(String.valueOf(dusPortAssigner.direct()))));

				} else {

					site.riConnectors.addAll(riRules.remote(remoteUnit, null,
							dus.riPort(String.valueOf(dusPortAssigner.direct()))));

				}

			}
		}

		List<MiniRbs> result = new ArrayList<>();
		result.add(rbs);
		result.addAll(Arrays.asList(duwRbses));
		site.miniRbses.addAll(result);

		return result;
	}

}
