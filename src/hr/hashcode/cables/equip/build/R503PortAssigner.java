package hr.hashcode.cables.equip.build;

import java.util.function.Supplier;

abstract class R503PortAssigner {

	enum Strategy implements Supplier<R503PortAssigner> {
		SEQUENTIAL(SequentialR503PortAssignment::new),
		SPLIT(SplitR503PortAssignment::new);

		private final Supplier<R503PortAssigner> constructor;

		private Strategy(Supplier<R503PortAssigner> constructor) {
			this.constructor = constructor;
		}

		@Override
		public R503PortAssigner get() {
			return constructor.get();
		}
	}

	abstract int nextOutput();

	private int input = 1;
	int nextInput() {
		int value = input;
		if (value == 4)
			throw new IllegalStateException();
		input++;
		return value;
	}

	abstract void endBand();

	private static class SequentialR503PortAssignment extends R503PortAssigner {
		private int output = 16;

		@Override
		int nextOutput() {
			int value = output;
			if (value == 3)
				throw new IllegalStateException();
			output--;
			if (output == 8)
				output--;
			return value;
		}

		@Override
		void endBand() {}

	}

	private static class SplitR503PortAssignment extends R503PortAssigner {

		private static final int[] splitStart = {16, 12, 7};
		private int splitter;
		private int output;

		@Override
		int nextOutput() {
			if (splitter == 3 || output == 4)
				throw new IllegalStateException(splitter + "," + output);
			int value = splitStart[splitter] - output;
			output++;
			return value;
		}

		@Override
		void endBand() {
			if (splitter == 3)
				throw new IllegalStateException();
			splitter++;
			output = 0;
		}

	}

}
