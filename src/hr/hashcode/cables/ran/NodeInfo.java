package hr.hashcode.cables.ran;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import hr.hashcode.cables.ran.NodeState.ManagedAttribute;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.ran.NodeState.ManagedStruct;
import hr.hashcode.cables.ran.Version.Type;

public class NodeInfo {

	public final Type type;
	public final String fdn;
	public final String productName;
	public final String userLabel;
	public final String logicalName;
	public final String mimName;
	public final String mimVersion;
	public final String site;
	public final ZonedDateTime time;

	private NodeInfo(Type type, String fdn, String productName, String userLabel, String logicalName, String mimName,
			String mimVersion, String site, ZonedDateTime time) {
		this.type = type;
		this.fdn = fdn;
		this.productName = productName;
		this.userLabel = userLabel;
		this.logicalName = logicalName;
		this.mimName = mimName;
		this.mimVersion = mimVersion;
		this.site = site;
		this.time = time;
	}

	@Override
	public String toString() {
		return "NodeInfo [fdn=" + fdn + ", productName=" + productName + ", userLabel=" + userLabel + ", logicalName="
				+ logicalName + ", mimName=" + mimName + ", mimVersion=" + mimVersion + ", site=" + site + "]";
	}

	public static NodeInfo info(NodeState state) {

		ManagedObject managedElementData = first(state, "ManagedElementData");
		ManagedObject managedElement = first(state, "ManagedElement");

		if (managedElement == null)
			throw new IllegalArgumentException();

		List<ManagedAttribute> attrs = managedElement.attrs();
		String productName = value("productName", attrs);
		String userLabel = value("userLabel", attrs);
		String logicalName = value("logicalName", attrs);
		String mimName = value("mimInfo", "mimName", attrs);
		String mimVersion = value("mimInfo", "mimVersion", attrs);
		String site = value("site", attrs);
		Type type = state.meta().version().type();

		ZonedDateTime zoned;
		try {
			Long utcTime = value(managedElementData, "nodeUTCTime", Long.class);
			String localZone = value(managedElementData, "nodeLocalTimeZone");
			ZoneId zoneId = ZoneId.of(localZone, ZoneId.SHORT_IDS);
			Instant instant = Instant.ofEpochMilli(utcTime);
			zoned = ZonedDateTime.ofInstant(instant, zoneId);
		} catch (Exception e) {
			zoned = null;
		}

		return new NodeInfo(type, state.fdn, productName, userLabel, logicalName, mimName, mimVersion, site, zoned);
	}

	private static ManagedObject first(NodeState state, String className) {
		List<ManagedObject> list = instances(state, className);
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	private static List<ManagedObject> instances(NodeState state, String className) {
		List<ManagedObject> result = new ArrayList<>();
		for (ManagedObject object : state.objects()) {
			if (object.moClass.name.equals(className))
				result.add(object);
		}
		return result;
	}

	private static String value(ManagedObject object, String name) {
		if (object == null)
			return null;
		return value(name, object.attrs());
	}

	private static <T> T value(ManagedObject object, String name, Class<T> clazz) {
		return value(name, object.attrs(), clazz);
	}

	private static String value(String name, List<ManagedAttribute> attrs) {
		return value(name, attrs, String.class);
	}

	private static <T> T value(String name, List<ManagedAttribute> attrs, Class<T> clazz) {
		for (ManagedAttribute attr : attrs) {
			if (attr.attribute.name.equals(name)) {
				Object value = attr.value;
				if (clazz.isInstance(value))
					return clazz.cast(value);
			}
		}
		return null;
	}

	private static String value(String outer, String inner, List<ManagedAttribute> attrs) {
		for (ManagedAttribute attr : attrs) {
			if (attr.attribute.name.equals(outer)) {
				Object value = attr.value;
				if (value instanceof ManagedStruct) {
					return value(inner, ((ManagedStruct) value).attrs());
				}
			}
		}
		return null;
	}

}
