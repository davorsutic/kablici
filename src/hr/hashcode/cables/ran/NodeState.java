package hr.hashcode.cables.ran;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import hr.hashcode.cables.ran.Meta.MoAttr;
import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.Meta.MoEnum;
import hr.hashcode.cables.ran.Meta.MoStruct;
import hr.hashcode.cables.ran.Meta.MoType;
import hr.hashcode.cables.util.MoUtil;
import hr.hashcode.cables.util.Util;

public class NodeState implements Serializable {

	private static final long serialVersionUID = 1L;

	public static Builder newBuilder(Meta meta) {
		return new Builder(meta);
	}

	public static class Builder {

		private final Meta meta;
		private final Set<ManagedObject> set = Util.identitySet();
		private final Map<String, ManagedObject> byLdn = new HashMap<>();
		private final Map<Object, Object> cache = new HashMap<>();
		private ManagedObject root = null;

		private boolean finished;

		private void polish(Object[] array) {
			for (int i = 0; i < array.length; i++)
				array[i] = cache(array[i]);
		}

		private <T> T cache(T object) {
			Object retValue;
			if (object == null)
				retValue = null;
			else if (object instanceof ManagedObject)
				retValue = object;
			else if (object instanceof MoEnum.Element)
				retValue = object;
			else if (object instanceof FetchFailure)
				retValue = object;
			else {
				Object old = cache.get(object);
				if (old != null)
					retValue = old;
				else {
					if (object instanceof ManagedStruct) {
						ManagedStruct struct = (ManagedStruct) object;
						polish(struct.values);
						retValue = struct;
					} else if (object instanceof ManagedArray) {
						ManagedArray array = (ManagedArray) object;
						polish(array.values);
						retValue = array;
					} else if (object instanceof String || object instanceof Long || object instanceof Integer
							|| object instanceof Float || object instanceof Boolean)
						retValue = object;
					else if (object instanceof UnexpectedType)
						retValue = object;
					else
						throw new IllegalArgumentException(object.toString());
					cache.put(retValue, retValue);
				}
			}

			@SuppressWarnings("unchecked")
			T cast = (T) retValue;
			return cast;
		}

		private Builder(Meta meta) {
			this.meta = meta;
		}

		private void checkObject(ManagedObject object) {
			if (object != null && !set.contains(object))
				throw new IllegalArgumentException(object.toString());
		}

		public ManagedObject root() {
			return root;
		}

		public Meta meta() {
			return meta;
		}

		public ManagedObject newObject(String ldn) {
			ManagedObject created = byLdn.get(ldn);
			if (created != null)
				return created;
			int comma = ldn.lastIndexOf(',');
			int equals = ldn.lastIndexOf('=');
			if (equals == -1 || equals < comma)
				throw new IllegalArgumentException("Invalid LDN: " + ldn);
			ManagedObject parent;
			if (comma == -1)
				parent = null;
			else {
				String parentLdn = ldn.substring(0, comma);
				parent = byLdn.get(parentLdn);
				if (parent == null)
					parent = newObject(parentLdn);
			}

			String name = ldn.substring(equals + 1);
			String className = ldn.substring(comma + 1, equals);
			MoClass moClass = meta.byName(className);
			if (moClass == null) {
				if (parent != null)
					moClass = meta.byName(className, parent.moClass.mim.name);
				if (moClass == null)
					throw new IllegalArgumentException("Unsupported class: " + ldn);
			}
			return create(parent, moClass, name);
		}

		private ManagedObject create(ManagedObject parent, MoClass moClass, String name) {
			ManagedObject created = new ManagedObject(parent, moClass, cache(name));
			if (created.parent == null) {
				if (root != null)
					throw new IllegalArgumentException("Root already existing: " + root);
				else
					root = created;
			}
			String ldn = created.ldn();
			if (byLdn.containsKey(ldn))
				throw new IllegalArgumentException("Object already existing: " + ldn);
			byLdn.put(ldn, created);
			set.add(created);
			return created;
		}

		public ManagedObject newObject(ManagedObject parent, MoClass moClass, String name) {
			if (finished)
				throw new IllegalStateException();
			checkObject(parent);
			if (!meta.isMember(moClass))
				throw new IllegalArgumentException(moClass.toString());

			return create(parent, moClass, name);
		}

		public void set(ManagedObject object, MoAttr attribute, Object value) {
			if (finished)
				throw new IllegalStateException();
			if (!set.contains(object))
				throw new IllegalArgumentException(object.toString());
			if (!attribute.type.acceptable(value))
				throw new IllegalArgumentException(attribute + "," + value);
			object.values[attribute.index()] = cache(value);
		}

		public ManagedObject ref(String ldn) {
			return byLdn.get(ldn);
		}

		public NodeState build(String fdn) {
			return new NodeState(fdn, meta, set);
		}

	}

	public final String fdn;
	private final Meta meta;
	private final List<ManagedObject> objects;

	private NodeState(String fdn, Meta meta, Collection<ManagedObject> objects) {
		this.fdn = fdn;
		this.meta = meta;
		this.objects = Collections.unmodifiableList(MoUtil.ldnSort(objects));
	}

	public Meta meta() {
		return meta;
	}

	public List<ManagedObject> objects() {
		return objects;
	}

	public List<MoClass> classes() {
		return meta.classes();
	}

	public static class ManagedAttribute implements Serializable {

		private static final long serialVersionUID = 1L;

		public final MoAttr attribute;
		public final Object value;

		public ManagedAttribute(MoAttr attribute, Object value) {
			this.attribute = attribute;
			this.value = value;

			if (!attribute.type.acceptable(value))
				throw new IllegalArgumentException(value + "," + attribute);
		}

		@Override
		public String toString() {
			return attribute.name + "=" + value;
		}

	}

	public static class ManagedStruct implements Serializable {

		private static final long serialVersionUID = 1L;

		public final MoStruct moStruct;
		private final Object[] values;

		public ManagedStruct(MoStruct moStruct, List<ManagedAttribute> attrs) {
			this.moStruct = moStruct;
			this.values = new Object[moStruct.attrs.length];
			for (ManagedAttribute attr : attrs) {
				values[attr.attribute.index()] = attr.value;
			}
		}

		@Override
		public String toString() {
			return Arrays.stream(moStruct.attrs)
					.map(x -> x.name + "=" + values[x.index()])
					.collect(Collectors.joining(",", moStruct.name + "{", "}"));
		}

		public List<ManagedAttribute> attrs() {
			return Arrays.stream(moStruct.attrs)
					.map(x -> new ManagedAttribute(x, values[x.index()]))
					.collect(Collectors.toList());
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + moStruct.hashCode();
			result = prime * result + Arrays.hashCode(values);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof ManagedStruct))
				return false;
			ManagedStruct other = (ManagedStruct) obj;
			if (moStruct != other.moStruct)
				return false;
			return Arrays.equals(values, other.values);
		}

	}

	public static class ManagedArray implements Serializable {

		private static final long serialVersionUID = 1L;

		public final MoType elemType;
		private final Object[] values;

		public ManagedArray(MoType elemType, Object[] values) {
			this.values = values.clone();
			this.elemType = elemType;
			for (Object object : values)
				if (!elemType.acceptable(object))
					throw new IllegalArgumentException(elemType + "," + object + "," + Arrays.toString(values));
		}

		public int length() {
			return values.length;
		}

		public Object value(int index) {
			return values[index];
		}

		public Object[] values() {
			return values.clone();
		}

		@Override
		public String toString() {
			return elemType + Arrays.toString(values);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + elemType.hashCode();
			result = prime * result + Arrays.hashCode(values);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof ManagedArray))
				return false;
			ManagedArray other = (ManagedArray) obj;
			if (elemType != other.elemType)
				return false;
			return Arrays.equals(values, other.values);
		}
	}

	public static final FetchFailure FETCH_FAILURE = new FetchFailure();

	public static class FetchFailure implements Serializable {
		private static final long serialVersionUID = 1L;

		private FetchFailure() {}

		@Override
		public String toString() {
			return "FAIL";
		}

		protected Object readResolve() {
			return FETCH_FAILURE;
		}

	}

	public static class UnexpectedType implements Serializable {
		private static final long serialVersionUID = 1L;

		public final String actual;

		public UnexpectedType(String actual) {
			this.actual = actual;
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(actual);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof UnexpectedType))
				return false;
			UnexpectedType other = (UnexpectedType) obj;
			return Objects.equals(actual, other.actual);
		}

		@Override
		public String toString() {
			return "err:" + actual;
		}

	}

	public static class ManagedObject implements Serializable {

		private static final long serialVersionUID = 1L;

		public final ManagedObject parent;
		public final MoClass moClass;
		public final String name;
		private final Object[] values;

		private ManagedObject(ManagedObject parent, MoClass moClass, String name) {
			this.parent = parent;
			this.moClass = moClass;
			this.name = name;
			this.values = new Object[moClass.length()];
			if (moClass.key != null)
				this.values[moClass.key.index()] = name;
		}

		public List<ManagedAttribute> attrs() {
			return Arrays.stream(moClass.attrs())
					.map(x -> new ManagedAttribute(x, values[x.index()]))
					.collect(Collectors.toList());
		}

		public String ldn() {
			if (parent == null)
				return moClass + "=" + name;
			else
				return parent + "," + moClass + "=" + name;
		}

		@Override
		public String toString() {
			return ldn();
		}
	}

}
