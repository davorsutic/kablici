package hr.hashcode.cables.ran;

import java.io.Serializable;

public class Version implements Serializable {

	private static final long serialVersionUID = 1L;

	public static Version of(String mark) {
		for (Type type : Type.values())
			if (mark.startsWith(type.prefix()))
				return new Version(mark, type);
		throw new IllegalArgumentException(mark);
	}

	public enum Type {
		RBS("RBS_NODE_MODEL"),
		ERBS("ERBS_NODE_MODEL"),
		MSRBS("MSRBS_NODE_MODEL"),
		RNC("RNC_NODE_MODEL");

		private final String prefix;

		Type(String prefix) {
			this.prefix = prefix;
		}

		public String prefix() {
			return prefix;
		}

		public Version version(String suffix) {
			return new Version(prefix + "_" + suffix, this);
		}

	}

	private final String mark;
	private final Type type;

	private Version(String mark, Type type) {
		this.mark = mark;
		this.type = type;
	}

	public String mark() {
		return mark;
	}

	public Type type() {
		return type;
	}

	@Override
	public int hashCode() {
		return mark.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Version))
			return false;
		Version other = (Version) obj;
		return mark.equals(other.mark);
	}

	@Override
	public String toString() {
		return mark;
	}

}
