package hr.hashcode.cables.ran;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import hr.hashcode.cables.ran.NodeState.ManagedArray;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.ran.NodeState.ManagedStruct;
import hr.hashcode.cables.ran.NodeState.UnexpectedType;
import hr.hashcode.cables.util.MoUtil;
import hr.hashcode.cables.util.Util;

public class Meta implements Serializable {

	private static final long serialVersionUID = 1L;

	private final Version version;
	private final Map<String, Map<String, MoClass>> byNameAndMim;
	private final Map<String, MoClass> uniqueNames;
	private final List<MoClass> classes;
	private final List<Parent> parents;

	private final Set<MoClass> set;

	public Meta(Version version, Collection<MoClass> classes, List<Parent> parents) {
		this.version = version;
		this.classes = Collections.unmodifiableList(MoUtil.nameSort(classes));
		this.parents = new ArrayList<>(parents);
		this.set = Util.identitySet(this.classes);
		this.byNameAndMim = this.classes.stream()
				.collect(Collectors.groupingBy(x -> x.name, Collectors.toMap(x -> x.mim.name, x -> x)));
		this.uniqueNames = new HashMap<>();
		this.byNameAndMim.forEach((x, y) -> {
			if (y.size() == 1)
				uniqueNames.put(x, y.values().iterator().next());
		});
	}

	public Version version() {
		return version;
	}

	boolean isMember(MoClass moClass) { // TODO prebaciti ovo u NodeState
		return set.contains(moClass);
	}

	MoClass byName(String name) {
		return uniqueNames.get(name);
	}

	MoClass byName(String name, String mim) {
		return byNameAndMim.getOrDefault(name, Collections.emptyMap()).get(mim);
	}

	public List<MoClass> classes() {
		return classes;
	}

	public List<Parent> parents() {
		return new ArrayList<>(parents);
	}

	public interface MoType extends Serializable {
		Class<?> clazz();

		default boolean acceptable(Object value) {
			return value == null
					|| value == NodeState.FETCH_FAILURE
					|| value instanceof UnexpectedType
					|| clazz().isInstance(value);
		}
	};

	public enum SimpleType implements MoType {

		INTEGER(Integer.class, "int"),
		STRING(String.class, "string"),
		BOOLEAN(Boolean.class, "boolean"),
		FLOAT(Float.class, "float"),
		LONG(Long.class, "long"),
		REFERENCE(ManagedObject.class, "mo");

		private final Class<?> clazz;
		private final String name;

		private SimpleType(Class<?> clazz, String name) {
			this.clazz = clazz;
			this.name = name;
		}

		@Override
		public Class<?> clazz() {
			return clazz;
		}

		@Override
		public String toString() {
			return name;
		}

	}

	public static class MoArray implements MoType {
		private static final long serialVersionUID = 1L;

		public final MoType elemType;

		public MoArray(MoType elemType) {
			if (elemType instanceof MoArray)
				throw new IllegalArgumentException(elemType.toString());
			this.elemType = Objects.requireNonNull(elemType);
		}

		@Override
		public Class<ManagedArray> clazz() {
			return ManagedArray.class;
		}

		@Override
		public String toString() {
			return elemType + "[]";
		}
	}

	public static class MoAttr implements Serializable {
		private static final long serialVersionUID = 1L;

		public final String name;
		public final MoType type;
		public final String description;
		private int index = -1;

		public MoAttr(String name, MoType type, String description) {
			this.name = Objects.requireNonNull(name);
			this.type = Objects.requireNonNull(type);
			this.description = description;
		}

		int index() {
			return index;
		}

		@Override
		public String toString() {
			return name + "<" + type + ">";
		}

	}

	public static class MoStruct implements MoType {

		private static final long serialVersionUID = 1L;

		public final String name;
		public final MoAttr[] attrs;

		public MoStruct(String name, List<MoAttr> attrs) {
			this.name = Objects.requireNonNull(name);
			this.attrs = attrs(attrs);
		}

		public Class<ManagedStruct> clazz() {
			return ManagedStruct.class;
		}

		public String toString() {
			return name;
		}
	}

	public static class Mim implements Serializable {

		private static final long serialVersionUID = 1L;

		public final String name;
		public final String version;
		public final String release;
		public final String correction;

		public Mim(String name, String version, String release, String correction) {
			this.name = Objects.requireNonNull(name);
			this.version = version;
			this.release = release;
			this.correction = correction;
		}

		public Mim(String name) {
			this(name, null, null, null);
		}

		@Override
		public String toString() {
			return name;
		}

	}

	public static class MoClass implements Serializable {

		private static final long serialVersionUID = 1L;

		public final Mim mim;
		public final String name;
		private final MoAttr[] attrs;
		public final MoAttr key;
		public final String description;

		public MoClass(Mim mim, String name, List<MoAttr> attrs, MoAttr key, String description) {
			this.mim = mim;
			this.name = name;
			this.attrs = Meta.attrs(attrs);
			this.key = key;
			this.description = description;
		}

		public int length() {
			return attrs.length;
		}

		public String name() {
			return name;
		}

		public MoAttr[] attrs() {
			return attrs.clone();
		}

		public String toString() {
			return name;
		}

	}

	private static MoAttr[] attrs(List<MoAttr> attrs) {
		MoAttr[] array = attrs.stream().toArray(MoAttr[]::new);
		for (int i = 0; i < array.length; i++)
			array[i].index = i;
		return array;
	}

	public static class MoEnum implements MoType {

		private static final long serialVersionUID = 1L;

		public static class Element implements Serializable {

			private static final long serialVersionUID = 1L;

			public final String name;
			public final int index;
			public final String description;

			public Element(String name, int index, String description) {
				this.name = name;
				this.index = index;
				this.description = description;
			}

			public String toString() {
				return name + "(" + index + ")";
			}

		}

		public final String name;
		private final Element[] elements;
		private final Map<String, Element> byName;
		private final Map<Integer, Element> byIndex;

		public MoEnum(String name, List<Element> elements) {
			this.name = name;
			this.elements = Util.toArray(elements, Element.class);
			this.byIndex = mapAndProtect(elements, x -> x.index);
			this.byName = mapAndProtect(elements, x -> x.name);
		}

		public Element byName(String name) {
			return byName.get(name);
		}

		public Element byIndex(int index) {
			return byIndex.get(index);
		}

		public Element[] elements() {
			return elements.clone();
		}

		@Override
		public String toString() {
			return name;
		}

		@Override
		public Class<Element> clazz() {
			return Element.class;
		}

	}

	public static class Parent implements Serializable {

		private static final long serialVersionUID = 1L;

		public final MoClass parent;
		public final MoClass child;
		public final int lower;
		public final int upper;

		public Parent(MoClass parent, MoClass child, int lower, int upper) {
			this.parent = parent;
			this.child = child;
			this.lower = lower;
			this.upper = upper;
		}

		public String toString() {
			return parent + "," + child + "[" + lower + "-" + upper + "]";
		}
	}

	private static <K, V> Map<K, V> mapAndProtect(List<V> elements, Function<V, K> key) {
		Map<K, V> map = elements.stream().collect(Collectors.toMap(key, x -> x));
		return Collections.unmodifiableMap(map);
	}

}
