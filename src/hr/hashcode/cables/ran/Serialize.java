package hr.hashcode.cables.ran;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import hr.hashcode.cables.ran.Meta.Mim;
import hr.hashcode.cables.ran.Meta.MoArray;
import hr.hashcode.cables.ran.Meta.MoAttr;
import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.Meta.MoEnum;
import hr.hashcode.cables.ran.Meta.MoEnum.Element;
import hr.hashcode.cables.ran.Meta.MoStruct;
import hr.hashcode.cables.ran.Meta.MoType;
import hr.hashcode.cables.ran.Meta.Parent;
import hr.hashcode.cables.ran.Meta.SimpleType;
import hr.hashcode.cables.ran.NodeState.FetchFailure;
import hr.hashcode.cables.ran.NodeState.ManagedArray;
import hr.hashcode.cables.ran.NodeState.ManagedAttribute;
import hr.hashcode.cables.ran.NodeState.ManagedObject;
import hr.hashcode.cables.ran.NodeState.ManagedStruct;
import hr.hashcode.cables.ran.NodeState.UnexpectedType;

public class Serialize {

	private static final List<Handler<?>> handlers = handlers();

	private static List<Handler<?>> handlers() {
		List<Handler<?>> handlers = new ArrayList<>();
		handlers.add(new IntegerHandler());
		handlers.add(new StringHandler());
		handlers.add(new BooleanHandler());
		handlers.add(new FloatHandler());
		handlers.add(new LongHandler());
		handlers.add(new SimpleTypeHandler());
		handlers.add(new MoEnumElementHandler());
		handlers.add(new MoEnumHandler());
		handlers.add(new MoAttrHandler());
		handlers.add(new MoStructHandler());
		handlers.add(new MoClassHandler());
		handlers.add(new MimHandler());
		handlers.add(new MoArrayHandler());
		handlers.add(new FetchFailureHandler());
		handlers.add(new UnexpectedTypeHandler());
		handlers.add(new ManagedArrayHandler());
		handlers.add(new ManagedStructHandler());
		handlers.add(new ManagedObjectHandler());
		handlers.add(new NodeStateHandler());

		return handlers;
	}

	public static byte[] toBytes(NodeState state) {

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			serialize(state, output);
		} catch (IOException e) {
			throw new AssertionError();
		}
		return output.toByteArray();

	}

	public static NodeState fromBytes(byte[] array) {
		ByteArrayInputStream input = new ByteArrayInputStream(array);
		try {
			return deserialize(input);
		} catch (IOException e) {
			throw new AssertionError();
		}
	}

	public static NodeState deserialize(InputStream input) throws IOException {
		// TODO introduce exception for faulty data
		ReadContext context = new ReadContext(input);
		try {
			return context.read(NodeState.class);
		} catch (DeException e) {
			Throwable t = e.getCause();
			if (t == null)
				t = e;
			if (t instanceof IOException)
				throw (IOException) t;
			else
				throw new IOException(t);
		}
	}

	public static void serialize(NodeState state, OutputStream output) throws IOException {
		WriteContext context = new WriteContext(output);
		context.store(state, NodeState.class);
	}

	private static List<ManagedObject> sort(NodeState state) {
		List<ManagedObject> list = new ArrayList<>();
		Set<ManagedObject> set = Collections.newSetFromMap(new IdentityHashMap<>());
		for (ManagedObject object : state.objects()) {
			insert(object, list, set);
		}
		return list;
	}

	private static void insert(ManagedObject object, List<ManagedObject> list, Set<ManagedObject> set) {
		if (object == null || !set.add(object))
			return;
		insert(object.parent, list, set);
		list.add(object);
	}

	private static class ReadContext {
		private final Map<Class<?>, Handler<?>> handlers;
		private final Map<Class<?>, List<?>> indices;
		private final InputStream input;
		private NodeState.Builder builder;

		ReadContext(InputStream input) {
			this.input = input;
			this.handlers = new IdentityHashMap<>();
			this.indices = new IdentityHashMap<>();

			for (Handler<?> handler : Serialize.handlers) {
				Class<?> clazz = handler.clazz();
				this.handlers.put(clazz, handler);
				List<?> list = new ArrayList<>();
				list.add(null);
				this.indices.put(clazz, list);
			}
		}

		<T> List<T> list(Class<T> clazz) {
			@SuppressWarnings("unchecked")
			List<T> list = (List<T>) indices.get(clazz);
			if (list == null)
				throw new DeException(clazz);
			return list;
		}

		<T> Handler<T> handler(Class<T> clazz) {
			@SuppressWarnings("unchecked")
			Handler<T> handler = (Handler<T>) handlers.get(clazz);
			if (handler == null)
				throw new DeException(clazz);
			return handler;
		}

		<T> T reuse(Class<T> clazz) {
			int index = readInt();
			List<T> list = list(clazz);
			return list.get(index);
		}

		<T> T read(Class<T> clazz) {
			int index = readInt();
			List<T> list = list(clazz);
			if (index >= 0) {
				return list.get(index);
			} else {
				Handler<T> handler = handler(clazz);
				T value = handler.read(this);
				list.add(value);
				return value;
			}
		}

		<T> T[] readArray(Class<T> clazz) {
			if (clazz.isPrimitive())
				throw new DeException(clazz.toString());
			int len = readInt();
			@SuppressWarnings("unchecked")
			T[] array = (T[]) Array.newInstance(clazz, len);
			for (int i = 0; i < len; i++)
				array[i] = read(clazz);
			return array;
		}

		int readByte() {
			return Serialize.readByte(input);
		}

		int readInt() {
			return Serialize.readInt(input);
		}

		char readChar() {
			return Serialize.readChar(input);
		}

		long readLong() {
			return Serialize.readLong(input);
		}

		float readFloat() {
			return Serialize.readFloat(input);
		}

	}

	private static class WriteContext {

		private final Map<Class<?>, Handler<?>> handlers;
		private final Map<Class<?>, Map<?, Integer>> indices;
		private final OutputStream output;

		WriteContext(OutputStream output) {
			this.output = output;
			this.handlers = new IdentityHashMap<>();
			this.indices = new IdentityHashMap<>();

			for (Handler<?> handler : Serialize.handlers) {
				Class<?> clazz = handler.clazz();
				this.handlers.put(clazz, handler);
				Map<?, Integer> map = new IdentityHashMap<>();
				map.put(null, 0);
				this.indices.put(clazz, map);
			}
		}

		<T> Map<T, Integer> map(Class<T> clazz) {
			@SuppressWarnings("unchecked")
			Map<T, Integer> list = (Map<T, Integer>) indices.get(clazz);
			if (list == null)
				throw new DeException(clazz);
			return list;
		}

		<T> Handler<T> handler(Class<T> clazz) {
			@SuppressWarnings("unchecked")
			Handler<T> handler = (Handler<T>) handlers.get(clazz);
			if (handler == null)
				throw new DeException(clazz);
			return handler;
		}

		<T> void store(T object, Class<T> clazz) {
			Map<T, Integer> indices = map(clazz);
			Integer index = indices.get(object);
			if (index != null) {
				writeInt(index);
			} else {
				index = indices.size();
				indices.put(object, index);
				writeInt(-index);
				Handler<T> handler = handler(clazz);
				handler.write(this, object);
			}
		}

		<T> void storeArray(T[] object, Class<T> clazz) {
			int len = object.length;
			writeInt(len);
			for (T obj : object)
				store(obj, clazz);
		}

		<T> void reuse(T object, Class<T> clazz) {
			Map<T, Integer> indices = map(clazz);
			Integer index = indices.get(object);
			if (index != null) {
				writeInt(index);
			} else
				throw new DeException(object);
		}

		void writeByte(int value) {
			Serialize.writeByte(output, value);
		}

		void writeInt(int value) {
			Serialize.writeInt(output, value);
		}

		void writeChar(char value) {
			Serialize.writeChar(output, value);
		}

		void writeLong(long value) {
			Serialize.writeLong(output, value);
		}

		void writeFloat(float value) {
			Serialize.writeFloat(output, value);
		}

	}

	/************ FLOAT **************/
	private static void writeFloat(OutputStream output, float value) {
		writeInt(output, Float.floatToIntBits(value));
	}

	private static float readFloat(InputStream input) {
		return Float.intBitsToFloat(readInt(input));
	}

	/************ LONG **************/
	private static void writeLong(OutputStream output, long value) {
		writeInt(output, (int) (value >> 32));
		writeInt(output, (int) value);
	}

	private static long readLong(InputStream input) {
		return ((long) readInt(input) << 32) | readInt(input);
	}

	/************ CHAR **************/
	private static void writeChar(OutputStream output, char value) {
		writeByte(output, value >> 8);
		writeByte(output, value);
	}

	private static char readChar(InputStream input) {
		return (char) ((readByte(input) << 8) | readByte(input));
	}

	/*********** INT **************/
	private static void writeInt(OutputStream output, int value) {
		for (int i = 3; i >= 0; i--)
			writeByte(output, value >> 8 * i);
	}

	private static int readInt(InputStream input) {
		int temp = 0;
		for (int i = 0; i < 4; i++)
			temp = (temp << 8) | readByte(input);
		return temp;
	}
	/************ BYTE ************/

	private static void writeByte(OutputStream output, int value) {
		try {
			output.write(value);
		} catch (IOException e) {
			throw new DeException(e);
		}
	}

	private static int readByte(InputStream input) {
		try {
			int read = input.read();
			if (read == -1)
				throw new DeException();
			return read;
		} catch (IOException e) {
			throw new DeException(e);
		}
	}
	/*****************************/
	@SuppressWarnings("serial")
	private static class DeException extends RuntimeException {
		DeException() {}

		DeException(Throwable t) {
			super(t);
		}

		DeException(Object object) {
			super(String.valueOf(object));
		}

	}

	private abstract static class Handler<T> {
		abstract void write(WriteContext context, T value);

		abstract T read(ReadContext context);

		abstract Class<T> clazz();
	}

	private static class StringHandler extends Handler<String> {
		@Override
		void write(WriteContext context, String value) {
			int len = value.length();
			context.writeInt(len);

			boolean compressed = true;
			for (int i = 0; i < len; i++) {
				char ch = value.charAt(i);
				if ((ch >> 8) != 0) {
					compressed = false;
					break;
				}
			}

			if (compressed) {
				context.writeByte(1);
				for (int i = 0; i < len; i++)
					context.writeByte(value.charAt(i));
			} else {
				context.writeByte(0);
				for (int i = 0; i < len; i++)
					context.writeChar(value.charAt(i));
			}
		}

		@Override
		String read(ReadContext context) {
			int len = context.readInt();
			int compressed = context.readByte();
			char[] chars = new char[len];
			if (compressed == 1) {
				for (int i = 0; i < len; i++)
					chars[i] = (char) context.readByte();
			} else {
				for (int i = 0; i < len; i++)
					chars[i] = context.readChar();
			}
			return new String(chars);
		}

		@Override
		Class<String> clazz() {
			return String.class;
		}
	}

	@SuppressWarnings("unused")
	private static class NoCompressionStringHandler extends Handler<String> {
		@Override
		void write(WriteContext context, String value) {
			int len = value.length();
			context.writeInt(len);
			for (int i = 0; i < len; i++)
				context.writeChar(value.charAt(i));
		}

		@Override
		String read(ReadContext context) {
			int len = context.readInt();
			char[] chars = new char[len];
			for (int i = 0; i < len; i++)
				chars[i] = context.readChar();
			return new String(chars);
		}

		@Override
		Class<String> clazz() {
			return String.class;
		}
	}

	private static class IntegerHandler extends Handler<Integer> {
		@Override
		void write(WriteContext context, Integer value) {
			context.writeInt(value);
		}

		@Override
		Integer read(ReadContext context) {
			return context.readInt();
		}

		@Override
		Class<Integer> clazz() {
			return Integer.class;
		}
	}

	private static class FloatHandler extends Handler<Float> {
		@Override
		void write(WriteContext context, Float value) {
			context.writeFloat(value);
		}

		@Override
		Float read(ReadContext context) {
			return context.readFloat();
		}

		@Override
		Class<Float> clazz() {
			return Float.class;
		}
	}

	private static class BooleanHandler extends Handler<Boolean> {
		@Override
		void write(WriteContext context, Boolean value) {
			context.writeInt(value ? 1 : 0);
		}

		@Override
		Boolean read(ReadContext context) {
			int value = context.readInt();
			if (value == 0)
				return false;
			else if (value == 1)
				return true;
			else
				throw new DeException(value);
		}

		@Override
		Class<Boolean> clazz() {
			return Boolean.class;
		}
	}

	private static class LongHandler extends Handler<Long> {
		@Override
		void write(WriteContext context, Long value) {
			context.writeLong(value);
		}

		@Override
		Long read(ReadContext context) {
			return context.readLong();
		}

		@Override
		Class<Long> clazz() {
			return Long.class;
		}
	}

	private static class MimHandler extends Handler<Mim> {

		@Override
		void write(WriteContext context, Mim value) {
			context.store(value.name, String.class);
			context.store(value.release, String.class);
			context.store(value.version, String.class);
			context.store(value.correction, String.class);
		}

		@Override
		Mim read(ReadContext context) {
			String name = context.read(String.class);
			String release = context.read(String.class);
			String version = context.read(String.class);
			String revision = context.read(String.class);
			return new Mim(name, version, release, revision);
		}

		@Override
		Class<Mim> clazz() {
			return Mim.class;
		}

	}

	private static class MoClassHandler extends Handler<MoClass> {
		@Override
		void write(WriteContext context, MoClass value) {
			context.store(value.name, String.class);
			context.store(value.description, String.class);
			context.storeArray(value.attrs(), MoAttr.class);
			context.store(value.key, MoAttr.class);
			context.store(value.mim, Mim.class);
		}

		@Override
		MoClass read(ReadContext context) {
			String name = context.read(String.class);
			String description = context.read(String.class);
			MoAttr[] attrs = context.readArray(MoAttr.class);
			MoAttr key = context.read(MoAttr.class);
			Mim mim = context.read(Mim.class);
			return new MoClass(mim, name, Arrays.asList(attrs), key, description);
		}

		@Override
		Class<MoClass> clazz() {
			return MoClass.class;
		}
	}

	private static class MoAttrHandler extends Handler<MoAttr> {
		@Override
		void write(WriteContext context, MoAttr value) {
			context.store(value.name, String.class);
			context.store(value.description, String.class);
			writeType(context, value.type);
		}

		@Override
		MoAttr read(ReadContext context) {
			String name = context.read(String.class);
			String description = context.read(String.class);
			MoType type = readType(context);
			return new MoAttr(name, type, description);
		}

		@Override
		Class<MoAttr> clazz() {
			return MoAttr.class;
		}
	}

	static void writeType(WriteContext context, MoType type) {
		if (type instanceof SimpleType) {
			context.writeInt(0);
			context.store((SimpleType) type, SimpleType.class);
		} else if (type instanceof MoArray) {
			context.writeInt(1);
			context.store((MoArray) type, MoArray.class);
		} else if (type instanceof MoStruct) {
			context.writeInt(2);
			context.store((MoStruct) type, MoStruct.class);
		} else if (type instanceof MoEnum) {
			context.writeInt(3);
			context.store((MoEnum) type, MoEnum.class);
		} else
			throw new AssertionError(String.valueOf(type));
	}

	static MoType readType(ReadContext context) {
		int disc = context.readInt();
		Class<? extends MoType> clazz;
		if (disc == 0)
			clazz = SimpleType.class;
		else if (disc == 1)
			clazz = MoArray.class;
		else if (disc == 2)
			clazz = MoStruct.class;
		else if (disc == 3)
			clazz = MoEnum.class;
		else
			throw new DeException(disc);
		return context.read(clazz);
	}

	private static class SimpleTypeHandler extends Handler<SimpleType> {
		@Override
		void write(WriteContext context, SimpleType value) {
			context.store(value.name(), String.class);
		}

		@Override
		SimpleType read(ReadContext context) {
			String name = context.read(String.class);
			try {
				return SimpleType.valueOf(name);
			} catch (IllegalArgumentException | NullPointerException e) {
				throw new DeException(name);
			}
		}

		@Override
		Class<SimpleType> clazz() {
			return SimpleType.class;
		}
	}

	private static class MoEnumHandler extends Handler<MoEnum> {

		@Override
		void write(WriteContext context, MoEnum value) {
			context.store(value.name, String.class);
			context.storeArray(value.elements(), MoEnum.Element.class);
		}

		@Override
		MoEnum read(ReadContext context) {
			String name = context.read(String.class);
			Element[] elements = context.readArray(MoEnum.Element.class);
			return new MoEnum(name, Arrays.asList(elements));
		}

		@Override
		Class<MoEnum> clazz() {
			return MoEnum.class;
		}
	}

	private static class MoEnumElementHandler extends Handler<MoEnum.Element> {

		@Override
		void write(WriteContext context, Element value) {
			context.store(value.name, String.class);
			context.store(value.description, String.class);
			context.writeInt(value.index);
		}

		@Override
		Element read(ReadContext context) {
			String name = context.read(String.class);
			String description = context.read(String.class);
			int index = context.readInt();
			return new Element(name, index, description);
		}

		@Override
		Class<Element> clazz() {
			return MoEnum.Element.class;
		}

	}

	private static class MoArrayHandler extends Handler<MoArray> {

		@Override
		void write(WriteContext context, MoArray value) {
			writeType(context, value.elemType);
		}

		@Override
		MoArray read(ReadContext context) {
			MoType type = readType(context);
			return new MoArray(type);
		}

		@Override
		Class<MoArray> clazz() {
			return MoArray.class;
		}
	}

	private static class MoStructHandler extends Handler<MoStruct> {

		@Override
		void write(WriteContext context, MoStruct value) {
			context.store(value.name, String.class);
			context.storeArray(value.attrs, MoAttr.class);
		}

		@Override
		MoStruct read(ReadContext context) {
			String name = context.read(String.class);
			MoAttr[] attrs = context.readArray(MoAttr.class);
			return new MoStruct(name, Arrays.asList(attrs));
		}

		@Override
		Class<MoStruct> clazz() {
			return MoStruct.class;
		}
	}

	private static class ManagedObjectHandler extends Handler<ManagedObject> {
		@Override
		void write(WriteContext context, ManagedObject value) {
			context.reuse(value.parent, ManagedObject.class);
			context.reuse(value.moClass, MoClass.class);
			context.store(value.name, String.class);
		}

		@Override
		ManagedObject read(ReadContext context) {
			ManagedObject parent = context.reuse(ManagedObject.class);
			MoClass moClass = context.reuse(MoClass.class);
			String name = context.read(String.class);
			return context.builder.newObject(parent, moClass, name);
		}

		@Override
		Class<ManagedObject> clazz() {
			return ManagedObject.class;
		}
	}

	private static class ManagedStructHandler extends Handler<ManagedStruct> {

		@Override
		void write(WriteContext context, ManagedStruct value) {
			context.reuse(value.moStruct, MoStruct.class);
			for (ManagedAttribute attr : value.attrs())
				writeAttr(context, attr.attribute.type, attr.value);
		}

		@Override
		ManagedStruct read(ReadContext context) {
			MoStruct moStruct = context.reuse(MoStruct.class);
			List<ManagedAttribute> attributes = new ArrayList<>();
			for (MoAttr attr : moStruct.attrs) {
				Object value = readAttr(context, attr.type);
				attributes.add(new ManagedAttribute(attr, value));
			}
			return new ManagedStruct(moStruct, attributes);
		}

		@Override
		Class<ManagedStruct> clazz() {
			return ManagedStruct.class;
		}
	}
	private static class ManagedArrayHandler extends Handler<ManagedArray> {

		@Override
		void write(WriteContext context, ManagedArray value) {
			writeType(context, value.elemType); // TODO reuse
			int len = value.length();
			context.writeInt(len);
			for (int i = 0; i < len; i++)
				writeAttr(context, value.elemType, value.value(i));
		}

		@Override
		ManagedArray read(ReadContext context) {
			MoType type = readType(context); // TODO reuse
			int len = context.readInt();
			Object[] values = new Object[len];
			for (int i = 0; i < len; i++)
				values[i] = readAttr(context, type);
			return new ManagedArray(type, values);
		}

		@Override
		Class<ManagedArray> clazz() {
			return ManagedArray.class;
		}
	}

	private static class FetchFailureHandler extends Handler<FetchFailure> {

		@Override
		void write(WriteContext context, FetchFailure value) {}

		@Override
		FetchFailure read(ReadContext context) {
			return NodeState.FETCH_FAILURE;
		}

		@Override
		Class<FetchFailure> clazz() {
			return FetchFailure.class;
		}

	}
	private static class UnexpectedTypeHandler extends Handler<UnexpectedType> {

		@Override
		void write(WriteContext context, UnexpectedType value) {
			context.store(value.actual, String.class);
		}

		@Override
		UnexpectedType read(ReadContext context) {
			String value = context.read(String.class);
			return new UnexpectedType(value);
		}

		@Override
		Class<UnexpectedType> clazz() {
			return UnexpectedType.class;
		}
	}

	private static void writeAttr(WriteContext context, MoType type, Object value) {
		if (value instanceof FetchFailure) {
			context.writeInt(1);
			context.store((FetchFailure) value, FetchFailure.class);
		} else if (value instanceof UnexpectedType) {
			context.writeInt(2);
			context.store((UnexpectedType) value, UnexpectedType.class);
		} else {
			context.writeInt(0);
			writeAttr(context, type.clazz(), value);
		}
	}
	private static <T> void writeAttr(WriteContext context, Class<T> clazz, Object value) {
		context.store(clazz.cast(value), clazz);
	}

	private static Object readAttr(ReadContext context, MoType type) {
		int disc = context.readInt();
		Class<?> target;
		if (disc == 1) {
			target = FetchFailure.class;
		} else if (disc == 2) {
			target = UnexpectedType.class;
		} else if (disc == 0) {
			target = type.clazz();
		} else
			throw new DeException(disc);
		return context.read(target);
	}

	private static class NodeStateHandler extends Handler<NodeState> {

		@Override
		void write(WriteContext context, NodeState state) {
			context.store(state.fdn, String.class);

			Version version = state.meta().version();
			context.store(version.mark(), String.class);

			List<MoClass> classes = state.classes();
			context.writeInt(classes.size());
			for (MoClass moClass : classes)
				context.store(moClass, MoClass.class);

			List<Parent> parents = state.meta().parents();
			context.writeInt(parents.size());
			for (Parent parent : parents) {
				context.store(parent.parent, MoClass.class);
				context.store(parent.child, MoClass.class);
				context.writeInt(parent.lower);
				context.writeInt(parent.upper);
			}

			List<ManagedObject> objs = sort(state);

			context.writeInt(objs.size());
			for (ManagedObject object : objs)
				context.store(object, ManagedObject.class);

			for (ManagedObject object : objs) {
				for (ManagedAttribute attr : object.attrs())
					writeAttr(context, attr.attribute.type, attr.value);
			}

		}

		@Override
		NodeState read(ReadContext context) {
			String fdn = context.read(String.class);

			String versionMark = context.read(String.class);
			Version version = Version.of(versionMark);

			int classLen = context.readInt();
			List<MoClass> classes = new ArrayList<>(classLen);
			for (int i = 0; i < classLen; i++)
				classes.add(context.read(MoClass.class));

			int parentsLen = context.readInt();
			List<Parent> parents = new ArrayList<>(parentsLen);
			for (int i = 0; i < parentsLen; i++) {
				MoClass parent = context.read(MoClass.class);
				MoClass child = context.read(MoClass.class);
				int lower = context.readInt();
				int upper = context.readInt();
				parents.add(new Parent(parent, child, lower, upper));
			}

			Meta meta = new Meta(version, classes, parents);

			context.builder = NodeState.newBuilder(meta);

			int objLen = context.readInt();
			List<ManagedObject> objects = new ArrayList<>(objLen);
			for (int i = 0; i < objLen; i++)
				objects.add(context.read(ManagedObject.class));

			for (ManagedObject object : objects) {
				for (MoAttr attr : object.moClass.attrs()) {
					Object value = readAttr(context, attr.type);
					context.builder.set(object, attr, value);
				}
			}

			return context.builder.build(fdn);
		}

		@Override
		Class<NodeState> clazz() {
			return NodeState.class;
		}
	}
}
