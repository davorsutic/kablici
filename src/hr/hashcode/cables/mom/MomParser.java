package hr.hashcode.cables.mom;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import hr.hashcode.cables.ran.Meta;
import hr.hashcode.cables.ran.Meta.Mim;
import hr.hashcode.cables.ran.Meta.MoArray;
import hr.hashcode.cables.ran.Meta.MoAttr;
import hr.hashcode.cables.ran.Meta.MoClass;
import hr.hashcode.cables.ran.Meta.MoEnum;
import hr.hashcode.cables.ran.Meta.MoStruct;
import hr.hashcode.cables.ran.Meta.MoType;
import hr.hashcode.cables.ran.Meta.Parent;
import hr.hashcode.cables.ran.Meta.SimpleType;
import hr.hashcode.cables.ran.Version;
import hr.hashcode.cables.util.Xml;

class MomParser {

	MomParser() {}

	static Meta parse(Version ver, List<byte[]> files) { // TODO remove version as arg, calculate it instead
		try {
			long start = System.nanoTime();
			List<MimParser> parsers = new ArrayList<>();
			for (byte[] file : files) {
				Xml root = Xml.read(new ByteArrayInputStream(file));
				Xml models = root.child("models");
				Xml mimNode = models.child("mim");

				String mimName = mimNode.attribute("name");
				String version = mimNode.attribute("version");
				String release = mimNode.attribute("release");
				String correction = mimNode.attribute("correction");
				Mim mim = new Mim(mimName, version, release, correction);

				MimParser parser = new MimParser(models, mimNode, mim);
				parsers.add(parser);
			}

			IntermimParser intermim = new IntermimParser(parsers);
			intermim.start();
			// break;
			long end = System.nanoTime();
			System.out.println("total: " + (end - start) / 1_000_000);
			return intermim.meta(ver);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static class IntermimParser {

		private final Map<String, MimParser> parsers = new TreeMap<>();

		private final List<Parent> parents = new ArrayList<>();

		IntermimParser(List<MimParser> parsers) {
			for (MimParser parser : parsers)
				this.parsers.put(parser.mim.name, parser);
		}

		private static int count(Xml xml, int def) {
			if (xml == null)
				return def;
			return Integer.parseInt(xml.contents());
		}

		private MoClass moClass(Xml xml, MimParser current) throws MomException {
			Xml classNode = xml.child("hasClass");
			String className = classNode.attribute("name");
			Xml mimNode = classNode.child("mimName");
			MimParser parser;
			if (mimNode == null)
				parser = current;
			else {
				String mimName = mimNode.contents();
				parser = parsers.get(mimName);
				if (parser == null) {
					List<MoClass> candidates = new ArrayList<>();
					for (MimParser mimParser : this.parsers.values()) {
						MoClass cand = mimParser.classes.get(className);
						if (cand != null)
							candidates.add(cand);
					}
					if (candidates.size() == 1)
						return candidates.get(0);

					throw new MomException(mimName);
				}
			}
			return parser.classes.get(className);
		}

		void start() throws MomException {

			for (MimParser parser : parsers.values())
				parser.start();

			for (MimParser parser : parsers.values()) {
				for (Xml relationship : parser.relationships()) {
					Xml contNode = relationship.child("containment");
					if (contNode != null) {
						MoClass parentClass = moClass(contNode.child("parent"), parser);
						Xml childXml = contNode.child("child");
						MoClass childClass = moClass(childXml, parser);

						Xml card = childXml.child("cardinality");
						int min;
						int max;
						if (card == null) {
							min = -1;
							max = -1;
						} else {
							min = count(card.child("min"), 0);
							max = count(card.child("max"), Integer.MAX_VALUE);
						}
						Parent parent = new Parent(parentClass, childClass, min, max);
						parents.add(parent);

					}
				}
			}
		}

		Meta meta(Version version) {
			List<MoClass> classes = new ArrayList<>();
			for (MimParser parser : parsers.values())
				classes.addAll(parser.classes.values());
			return new Meta(version, classes, parents);
		}

	}

	private static class MomException extends Exception {
		private static final long serialVersionUID = 1L;

		private final String string;

		MomException(String value) {
			this.string = value;
		}

		public String toString() {
			return string;
		}

	}

	private static class MimParser {

		private final Xml modelsNode;
		private final Xml mimNode;
		private final Mim mim;
		private MimParser(Xml modelsNode, Xml mimNode, Mim mim) {
			this.modelsNode = modelsNode;
			this.mimNode = mimNode;
			this.mim = mim;
		}

		private final Map<String, MoEnum> enums = new TreeMap<>();
		private final Map<String, MoStruct> structs = new TreeMap<>();
		private final Map<String, MoClass> classes = new TreeMap<>();
		private final Map<MoType, MoArray> arrayTypes = new IdentityHashMap<>();
		private final Map<String, MoType> derivedTypes = new TreeMap<>();

		void start() throws MomException {
			enums();
			derivedTypes();
			structs();
			classes();
		}

		private void enums() throws MomException {
			for (Xml enumXml : mimNode.children("enum")) {
				String name = name(enumXml);
				List<MoEnum.Element> elements = new ArrayList<>();
				for (Xml memXml : enumXml.children("enumMember")) {
					String memName = name(memXml);
					int memIndex = Integer.parseInt(memXml.child("value").contents());
					String description = description(memXml);
					elements.add(new MoEnum.Element(memName, memIndex, description));
				}
				MoEnum moEnum = new MoEnum(name, elements);
				enums.put(name, moEnum);
			}
		}

		private void structs() throws MomException {
			for (Xml struct : mimNode.children("struct")) {
				List<MoAttr> attrs = new ArrayList<>();
				for (Xml attr : struct.children("structMember")) {
					MoType moType = type(attr);
					String name = name(attr);
					String description = description(attr);
					attrs.add(new MoAttr(name, moType, description));
				}
				sortAttributes(attrs);
				if (!attrs.isEmpty()) {
					String name = name(struct);
					MoStruct moStruct = new MoStruct(name, attrs);
					structs.put(name, moStruct);
				}
			}

		}

		private void derivedTypes() throws MomException {
			for (Xml derived : mimNode.children("derivedDataType")) {
				String name = name(derived);
				Xml baseType = derived.child("baseType");
				if (baseType == null)
					throw new MomException(name);
				MoType type = type(baseType);
				derivedTypes.put(name, type);
			}
		}

		private boolean hasChild(Xml xml, List<String> names) {
			for (Xml child : xml.children())
				if (names.contains(child.name()))
					return true;
			return false;
		}

		private boolean hasChild(Xml xml, String... names) {
			return hasChild(xml, Arrays.asList(names));
		}

		private void classes() throws MomException {
			for (Xml clazz : mimNode.children("class")) {
				List<MoAttr> attrs = new ArrayList<>();
				String className = name(clazz);

				if ("ManagedObject".equals(className))
					continue;

				MoAttr key = null;
				for (Xml attr : clazz.children("attribute")) {
					Xml dataType = attr.child("dataType");
					MoType moType = type(dataType);
					String name = name(attr);
					String description = description(attr);
					MoAttr moAttr = new MoAttr(name, moType, description);
					if (attr.child("key") != null) {
						if (key != null)
							throw new MomException(className + ":" + key + "," + moAttr);
						key = moAttr;
					}
					attrs.add(moAttr);
				}
				if (key == null) {
					String target = className + "Id";
					for (MoAttr attr : attrs) {
						if (attr.name.equalsIgnoreCase(target)) {
							if (key != null)
								throw new MomException(className + ":" + key + "," + attr);
							key = attr;
						}
					}
					if (key == null) {
						for (Xml child : clazz.children("attribute"))
							System.out.println(name(child));
						System.out.println(attrs);
						throw new MomException(className);
					}
				}
				sortAttributes(attrs);
				MoClass moClass = new MoClass(mim, className, attrs, key, description(clazz));
				classes.put(className, moClass);
			}
		}

		private static final List<String> longTypes = Arrays.asList("longlong", "uint64", "int64");
		private static final List<String> intTypes =
				Arrays.asList("long", "int8", "int16", "int32", "uint8", "uint16", "uint32");

		private MoType type(Xml xml) throws MomException {
			MoType moType;
			if (xml.child(moRef) != null)
				moType = SimpleType.REFERENCE;
			else if (xml.child(string) != null)
				moType = SimpleType.STRING;
			else if (hasChild(xml, intTypes))
				moType = SimpleType.INTEGER;
			else if (hasChild(xml, longTypes))
				moType = SimpleType.LONG;
			else if (xml.child(boolean_) != null)
				moType = SimpleType.BOOLEAN;
			else if (xml.child(double_) != null || xml.child(float_) != null)
				moType = SimpleType.FLOAT;
			else if (xml.child(enumRef) != null) {
				String enumName = name(xml.child(enumRef));
				moType = enums.get(enumName);
				if (moType == null)
					throw new MomException(enumName);
			} else if (xml.child(structRef) != null) {
				moType = structs.get(name(xml.child(structRef)));
				if (moType == null)
					throw new MomException(xml.children().toString());
			} else if (xml.child(sequence) != null) {
				Xml inner = xml.child(sequence);
				MoType elemType = type(inner);
				moType = arrayTypes.computeIfAbsent(elemType, MoArray::new);
			} else if (hasChild(xml, derivedDataTypeRef)) {
				String derivedType = name(xml.child(derivedDataTypeRef));
				moType = derivedTypes.get(derivedType);
				if (moType == null)
					throw new MomException(derivedType);
			} else
				throw new MomException(xml.children().toString());
			return moType;
		}

		private static final String derivedDataTypeRef = "derivedDataTypeRef";
		private static final String relationship = "relationship";
		private static final String interMim = "interMim";
		private static final String sequence = "sequence";
		private static final String structRef = "structRef";
		private static final String enumRef = "enumRef";
		private static final String moRef = "moRef";
		private static final String string = "string";
		private static final String boolean_ = "boolean";
		private static final String double_ = "double";
		private static final String float_ = "float";

		private List<Xml> relationships() {
			List<Xml> result = new ArrayList<>();
			result.addAll(mimNode.children(relationship));
			for (Xml interMim : modelsNode.children(interMim)) {
				result.addAll(interMim.children(relationship));
			}
			return result;
		}

		private static String name(Xml xml) throws MomException {
			String name = xml.attribute("name");
			if (name == null)
				throw new MomException(xml.name());
			return name;
		}

		private String description(Xml xml) {
			Xml descXml = xml.child("description");
			if (descXml == null)
				return null;
			return descXml.contents();
		}

		private static void sortAttributes(List<MoAttr> attributes) {
			Collections.sort(attributes, Comparator.comparing((MoAttr x) -> x.name));
		}

	}

}
