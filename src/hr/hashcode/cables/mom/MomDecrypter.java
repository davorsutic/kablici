package hr.hashcode.cables.mom;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.zip.ZipInputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import hr.hashcode.cables.util.Util;

class MomDecrypter {

	MomDecrypter() {}

	private static final String CODING_ALGHORITM = "PBEWithMD5AndDES";
	private static final String KEY_SPEC = "efMi/obrf4wtlog&D0nThaxX&mh=ERo1hjClwrSgngS0eN3{hg";
	private static final String TRANSFORMATION = "PBEWithMD5AndDES/CBC/PKCS5Padding";
	private static final byte[] SALT = {(byte) 0x3f, (byte) 0xb8, (byte) 0x7d,
			(byte) 0xc1, (byte) 0x47, (byte) 0xc8, (byte) 0x32, (byte) 0xa7};
	private static final int ITERATION_COUNT = 20;

	private static Cipher instance() {
		try {
			PBEParameterSpec ps = new PBEParameterSpec(SALT, ITERATION_COUNT);
			SecretKeyFactory kf = SecretKeyFactory.getInstance(CODING_ALGHORITM);
			SecretKey key = kf.generateSecret(new PBEKeySpec(KEY_SPEC.toCharArray()));

			Cipher decrypter = Cipher.getInstance(TRANSFORMATION);
			decrypter.init(Cipher.DECRYPT_MODE, key, ps);
			return decrypter;
		} catch (Exception e) {
			throw new AssertionError(e);
		}
	}

	static byte[] decrypt(byte[] data) {
		Cipher decrypter = instance();
		byte[] decrypted;
		try {
			decrypted = decrypter.doFinal(data);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			return null;
		}
		try (ZipInputStream zipStream = new ZipInputStream(new ByteArrayInputStream(decrypted))) {
			zipStream.getNextEntry();
			return Util.drain(zipStream);
		} catch (IOException e) {
			return null;
		}
	}

}
