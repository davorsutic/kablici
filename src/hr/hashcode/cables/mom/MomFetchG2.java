package hr.hashcode.cables.mom;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import hr.hashcode.cables.ran.Version;
import hr.hashcode.cables.util.Util;

public class MomFetchG2 {

	MomFetchG2() {}

	static {
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			TrustManager[] managers = {new TrustAll()};
			sc.init(null, managers, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier((string, ssls) -> true);
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			// logger.error("Trust no one!", e);
			// TODO do something
		}
	}

	private static class TrustAll implements X509TrustManager {

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return new X509Certificate[0];
		}

		@Override
		public void checkClientTrusted(X509Certificate[] certs, String authType) {}

		@Override
		public void checkServerTrusted(X509Certificate[] certs, String authType) {}
	}

	private static final String identifier = "identifier";
	private static final String version = "version";
	private static final String baseModelIdentifier = "baseModelIdentifier";
	private static final String baseModelVersion = "baseModelVersion";

	static List<byte[]> download(String address) {

		List<String> lines;
		try {
			lines = lines(address);
		} catch (IOException e) {
			return null;
		}

		List<String> mims = new ArrayList<>();

		for (String line : lines) {
			String mim = extract(line, identifier);
			if (mim == null)
				return null;

			mims.add(mim);
		}

		List<byte[]> result = new ArrayList<>();
		for (String mim : mims) {
			try {
				byte[] bytes = downloadFile(address, mim + ".xml");
				result.add(bytes);
			} catch (IOException e) {
				return null;
			}
		}
		return result;
	}

	private static String hex(byte b) {
		return Integer.toHexString(b & 0xFF);
	}

	private static String md5(String target) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			return "xxxx";
		}
		byte[] input = target.getBytes(StandardCharsets.UTF_8);
		byte[] digest = md.digest(input);
		return hex(digest[0]) + hex(digest[1]);
	}

	private static List<String> lines(String address) throws IOException {
		byte[] bytes = downloadFile(address, "index.html");
		String contents = new String(bytes);
		int start = contents.indexOf("<a href=\"");
		if (start == -1)
			return null;
		contents = contents.substring(start).trim();
		return Util.lines(contents);
	}

	public static Version version(String address) {
		List<String> lines;
		try {
			lines = lines(address);
		} catch (IOException e) {
			return null;
		}

		int[] total = new int[3];

		Set<String> mimVersions = new TreeSet<>();
		List<String> mims = new ArrayList<>();
		Set<String> baseModels = new TreeSet<>();

		for (String line : lines) {
			String mimId = extract(line, identifier);
			String mimVersion = extract(line, version);
			if (mimId == null || mimVersion == null)
				return null;

			mims.add(mimId);
			mimVersions.add(mimId + "_" + mimVersion);
			if (!components(mimVersion, total, 3))
				return null;

			String baseId = extract(line, baseModelIdentifier);
			String baseVersion = extract(line, baseModelVersion);

			if (baseId != null && baseVersion != null) {
				baseModels.add(baseId + "_" + baseVersion);
				if (!components(baseVersion, total, 2))
					return null;
			}
		}

		String sum = total[0] + "." + total[1] + "." + total[2];
		String target = Stream.concat(mimVersions.stream(), baseModels.stream())
				.collect(Collectors.joining(";", "", "\n"));
		String hash = md5(target);

		Version version = Version.Type.MSRBS.version(sum + "_" + hash);

		return version;
	}

	private static String extract(String line, String attribute) {
		Pattern pattern = Pattern.compile("(?<!\\w)" + Pattern.quote(attribute) + "=\"([^\"]*)\"");
		Matcher matcher = pattern.matcher(line);
		if (matcher.find())
			return matcher.group(1);
		return null;
	}

	private static boolean components(String string, int[] accumulator, int expected) {
		String[] elems = string.split("\\.");
		if (elems.length != expected)
			return false;
		try {
			for (int i = 0; i < elems.length; i++)
				accumulator[i] += Integer.parseInt(elems[i]);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	private static byte[] downloadFile(String ipAddress, String file) throws IOException {
		URL url = new URL("https://" + ipAddress + "/models/" + file);
		try (InputStream input = url.openStream()) {
			return Util.drain(input);
		}
	}
}
