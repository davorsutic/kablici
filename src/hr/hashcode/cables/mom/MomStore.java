package hr.hashcode.cables.mom;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.regex.Pattern;

import hr.hashcode.cables.ran.Meta;
import hr.hashcode.cables.ran.Version;
import hr.hashcode.cables.ran.Version.Type;
import hr.hashcode.cables.util.FileContents;

public final class MomStore {

	private static final String completeSuffix = "_COMPLETE";

	private static final int downloadThreads = 20;
	private static final int processingThreads = 10;

	private final List<Path> momPath;
	private final Map<Version, MomResult> results = new ConcurrentHashMap<>();
	private final Semaphore download = new Semaphore(downloadThreads, true);
	private final Semaphore process = new Semaphore(processingThreads, true);

	public MomStore(List<Path> momPath) {
		this.momPath = new ArrayList<>(momPath);
	}

	public Meta get(Version version, String ipAddress) {
		MomResult result = results.computeIfAbsent(version, MomResult::new);
		try {
			return result.get(ipAddress);
		} catch (InterruptedException e) {
			return null;
		}
	}

	private class MomResult {

		private final Version version;
		private final Object lock = new Object();

		private boolean finished;
		private Meta result;

		MomResult(Version version) {
			this.version = version;
		}

		Meta get(String ipAddress) throws InterruptedException {
			synchronized (lock) {
				if (finished) {
					System.out.println("Reusing MOM for " + version + "," + ipAddress);
					return result;
				}

				download.acquire();
				System.out.println("Fetching MOM for " + version);
				List<byte[]> files;
				try {
					if (version.type() == Type.MSRBS)
						files = g2Mom(version, ipAddress);
					else {
						files = Collections.singletonList(g1Mom(version, ipAddress));
					}
				} finally {
					download.release();
				}
				process.acquire();
				System.out.println("Processing MOM for " + version);
				try {
					this.result = MomParser.parse(version, files);
				} finally {
					process.release();
				}
				System.out.println("Finished MOM for " + version);
				this.finished = true;
				return result;
			}
		}
	}

	private byte[] g1Mom(Version version, String ipAddress) {
		byte[] contents = searchMomPath(version, completeSuffix);
		if (contents != null)
			return contents;
		contents = MomFetchG1.complete(version.type(), ipAddress);
		if (contents != null)
			return contents;
		contents = searchMomPath(version, "");
		if (contents != null)
			return contents;
		return MomFetchG1.regular(version.type(), ipAddress);
	}

	private List<byte[]> g2Mom(Version version, String ipAddress) {
		byte[] joint = searchMomPath(version, "");
		if (joint != null)
			return split(joint);
		System.out.println("Downloading MOM for " + version);
		List<byte[]> components = MomFetchG2.download(ipAddress);
		if (components != null)
			return components;
		return null;
	}

	private static final String xmlStart = "<?xml version=";
	private static final Pattern xmlStartPattern =
			Pattern.compile("(?=^" + Pattern.quote(xmlStart) + ")", Pattern.MULTILINE);

	private static List<byte[]> split(byte[] bytes) {
		String contents = new String(bytes, StandardCharsets.UTF_8);
		String[] elems = xmlStartPattern.split(contents);

		List<byte[]> components = new ArrayList<>();
		for (String elem : elems) {
			if (elem.startsWith(xmlStart))
				components.add(elem.getBytes(StandardCharsets.UTF_8));
		}
		return components;
	}

	private byte[] searchMomPath(Version version, String suffix) {
		for (Path path : momPath) {
			Path target = path.resolve(version.mark() + suffix + ".xml.gz");
			try {
				return FileContents.contents(target);
			} catch (IOException e) {
				continue;
			}
		}
		return null;
	}

}
