package hr.hashcode.cables.mom;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.EnumMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import hr.hashcode.cables.ran.Version;
import hr.hashcode.cables.ran.Version.Type;
import hr.hashcode.cables.util.Util;

class MomFetchG1 {

	MomFetchG1() {}

	private static final Map<Version.Type, String> regular = new EnumMap<>(Version.Type.class);
	private static final Map<Version.Type, String> complete = new EnumMap<>(Version.Type.class);

	private static void register(Map<Version.Type, String> map, Version.Type type, String address) {
		map.put(type, "/cello/oe/xml/" + address);
	}

	static {
		register(regular, Type.RNC, "rnc_node_mim.xml");
		register(regular, Type.RBS, "RbsNode.xml.jar");
		register(regular, Type.ERBS, "RbsNode.xml");
		register(complete, Type.RBS, "RbsNodeComplete.xml.jar");
		register(complete, Type.ERBS, "RbsNodeComplete.xml");
	}

	static byte[] regular(Version.Type type, String ipAddress) {
		return downloadG1(regular, type, ipAddress);
	}

	static byte[] complete(Version.Type type, String ipAddress) {
		return downloadG1(complete, type, ipAddress);
	}

	private static byte[] downloadG1(Map<Version.Type, String> map, Type type, String ipAddress) {
		if (type == Type.MSRBS)
			throw new IllegalArgumentException(type + "," + ipAddress);
		String path = map.get(type);
		if (path == null)
			return null;
		try (InputStream input = new URL("http://" + ipAddress + path).openStream()) {
			byte[] bytes = Util.drain(input);

			if (type == Type.RBS) {
				ZipInputStream zip = new ZipInputStream(new ByteArrayInputStream(bytes));
				while (true) {
					ZipEntry entry = zip.getNextEntry();
					if (entry == null)
						return null;
					if (entry.getName().equals("RbsNode.xml") || entry.getName().equals("RbsNodeComplete.xml")) {
						byte[] encrypted = Util.drain(zip);
						byte[] decrypted = MomDecrypter.decrypt(encrypted);
						if (decrypted != null)
							return decrypted;
						else
							return encrypted;
					}
				}
			} else
				return bytes;

		} catch (IOException e) {
			return null;
		}
	}

	public static void main(String[] args) {
		byte[] bytes = regular(Type.RBS, "localhost");
		System.out.println(new String(bytes));
	}

}
