package main;

import java.io.IOException;

import hr.hashcode.cables.fetch.Listener;

//this class is outside of hashcode package because it is executed on moshell server
// and we dont want anyone to see who is running it.
public class Main {

	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.println("Port required");
			return;
		}
		int port;
		try {
			port = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			System.err.println("Numeric argument required");
			return;
		}
		Listener listener = new Listener(port);

		try {
			listener.start();
			System.err.println("Server started");
		} catch (IOException e) {
			System.err.println("Problem with starting");
		}
	}

}
